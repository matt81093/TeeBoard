

For OBS users:

All widgets require the CLR Browser Source plugin.
https://obsproject.com/forum/resources/clr-browser-source-plugin.22/

Note: There is an issue where SWF's that are hosted locally won't show up 
with wmode=transparent unless you are in Single Process mode. 
You can find this setting in Settings -> Browser -> Runtime.

So make sure to set the SingleProcess option to True and restart OBS.

Once installed, add a CLR Browser plugin source and browse to the html file in the widget's folder.

All widgets are Flash based and for those to work propertly (transparent background) 
in the CLR Browser plugin the following is required:

 - In OBS, open the Settings window and select the "Browser" tab.
 - Select the "Runtime" tab
 - Look for the "SingleProcess" entry and set it to "True"
 - Click "Apply", click "OK" and restart OBS.

OBS users can use a special notification widget located in "notifications/obs-js". 
It is special in that it is HTML/Javascipt/CSS based. 
If you have the necessary HTML/Javascript skills you can modify the sample 
to your hearts content. 
For animations, the sample uses Greensock's Animation Platform (GAP), but you can 
use any Javascript animation engine. More info on GAP here: http://www.greensock.com/get-started-js/

=========================================================================================================

For XSplit users:

Add a "Media" source and browse to the .swf file in the widget's folder 
(e.g. C:\Users\[USERNAME]\Documents\TeeBoard\widgets\clock\teeboard-clock.swf).


=========================================================================================================

Dimensions (set these in the CLR Browser source in OBS):

 - Clock: 	400 x 100 pixels

 - Spectrum: 	512 x 256 pixels

 - Followers: 	width of the stream resolution x 40 pixels (e.g. 1280 x 40)

 - Subscribers: width of the stream resolution x 40 pixels (e.g. 1280 x 40)

 - Countdown: 	400 x 100 pixels