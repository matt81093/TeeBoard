package tv.twitch.api {
	
	/**
	 * Twitch API uri's.
	 * 
	 * @see <a href="https://github.com/justintv/Twitch-API">Twitch API</a>
	 */ 
	public class Kraken {
		
		/**
		 * Twitch api Root url. When called with token, returns basic user info and authentication status. 
		 * If authenticated, the response includes the status of the access-token and links to other related resources.
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/root.md">Twitch API</a>
		 */ 
		public static const ROOT_URL:String = "https://api.twitch.tv/kraken";
		
		/**
		 * Returns a <code>user</code> object. The returned result indicates if the authenticated user is partnered or not.
		 * Non partnered users can not run commercials.
		 * <pre>Authenticated, required scope: user_read</pre>
		 * 
		 * <p>
		 * Note:
		 * 	<li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/users.md#get-user">Twitch API</a>
		 */ 
		public static const USER_URL:String = "https://api.twitch.tv/kraken/user";
		
		/**
		 * Returns a channel object of an authenticated user. Channel object includes stream key.
		 * <br />
		 * <pre>Authenticated, required scope: user_read</pre>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/channels.md#get-channel">Twitch API</a>
		 */
		public static const USER_CHANNEL_URL:String = "https://api.twitch.tv/kraken/channel/";
		
		/**
		 * Returns a <code>channel</code> object for the specified channel name.
		 * <p>
		 * Note:
		 * 	<li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * 	<li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/channels.md#get-channelschannel">Twitch API</a>
		 */ 
		public static const CHANNELS_CHANNEL_URL:String = "https://api.twitch.tv/kraken/channels/:channel/";
		
		/**
		 * Returns the last 10 users that followed the given channel.
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/follows.md#get-channelschannelfollows">Twitch API</a>
		 */
		public static const CHANNEL_FOLLOWERS_URL:String = "https://api.twitch.tv/kraken/channels/:channel/follows?limit=10&offset=0&direction=desc";
		
		/**
		 * Returns the last 10 users that subscribed to the given channel.
		 * <pre>Authenticated, required scope: channel_subscriptions</pre>
		 * 
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/subscriptions.md#get-channelschannelsubscriptions">Twitch API</a>
		 */
		public static const CHANNEL_SUBSCRIBERS_URL:String = "https://api.twitch.tv/kraken/channels/:channel/subscriptions?limit=10&offset=0&direction=desc";
		
		/**
		 * Update channel's status or game.
		 * <pre>Authenticated, required scope: user_read</pre>
		 * 
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.PUT</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/channels.md#put-channelschannel">Twitch API</a>
		 */ 
		public static const CHANNEL_UPDATE_URL:String = "https://api.twitch.tv/kraken/channels/:channel";
		
		/**
		 * Start commercial on channel.
		 * <pre>Authenticated, required scope: user_read</pre>
		 * 
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to to be sent as <code>URLRequestMethod.POST</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/channels.md#post-channelschannelcommercial">Twitch API</a>
		 */ 
		public static const CHANNEL_COMMERCIAL_URL:String = "https://api.twitch.tv/kraken/channels/:channel/commercial";
		
		/**
		 *Returns a list of user objects who are editors of <code>:channel</code>.
		 * <pre>Authenticated, required scope: user_read</pre>
		 * 
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/channels.md#get-channelschanneleditors">Twitch API</a>
		 */
		public static const CHANNEL_EDITORS_URL:String = "https://api.twitch.tv/kraken/channels/:channel/editors";
		
		
		/**
		 * Returns an list of videos (channel broadcasts) ordered by time of creation, starting with the most recent from <code>:channel</code>.
		 * <p>
		 * URL query parameters:
		 * <li><code>limit</code>: optional - Maximum number of objects in array. Default is 10. Maximum is 100</li>
		 * <li><code>offset</code>: optional - Object offset for pagination. Default is 0.</li>
		 * <li><code>broadcasts</code>: optional - Returns only broadcasts when <code>true</code>. 
		 * Otherwise only highlights are returned. Default is <code>false</code>.</li>
		 * </p>
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/videos.md#get-channelschannelvideos">Twitch API</a>
		 */
		public static const CHANNEL_BROADCASTS_URL:String = "https://api.twitch.tv/kraken/channels/:channel/videos?limit=25&broadcasts=true";
		
		/**
		 * Returns an list of videos (channel highlights) ordered by time of creation, starting with the most recent from <code>:channel</code>.
		 * <p>
		 * URL query parameters:
		 * <li><code>limit</code>: optional - Maximum number of objects in array. Default is 10. Maximum is 100</li>
		 * <li><code>offset</code>: optional - Object offset for pagination. Default is 0.</li>
		 * <li><code>broadcasts</code>: optional - Returns only broadcasts when <code>true</code>. 
		 * Otherwise only highlights are returned. Default is <code>false</code>.</li>
		 * </p>
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/videos.md#get-channelschannelvideos">Twitch API</a>
		 */
		public static const CHANNEL_HIGHLIGHTS_URL:String = "https://api.twitch.tv/kraken/channels/:channel/videos?limit=25&broadcasts=false";
		
		/**
		 * Returns a stream object if live. 
		 * <p>
		 * When offline, the <code>stream</code> property of the returned result will be empty.
		 * When online, the returned result will include:
		 * 	<li>number of viewers</li>
		 * 	<li>broadcaster (obs/xsplit/etc)</li>
		 * 	<li>game</li>
		 * 	<li>status</li>
		 * </p>
		 * 
		 * <p>
		 * Note:
		 * <li>This constant contains <code>":channel"</code>, which needs to be replaced with the channel username.</li>
		 * <li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/streams.md#get-streamschannel">Twitch API</a>
		 */ 
		public static const STREAMS_CHANNEL_URL:String = "https://api.twitch.tv/kraken/streams/:channel";
		
		/**
		 * Returns a list of stream objects that the authenticated user is following.
		 * <pre>Authenticated, required scope: user_read</pre>
		 * 
		 * <p>
		 * Note:
		 * 	<li>Only returns followed streams that are currently live.</li>
		 * 	<li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/users.md#get-streamsfollowed">Twitch API</a>
		 */
		public static const STREAMS_FOLLOWED_URL:String = "https://api.twitch.tv/kraken/streams/followed";
		
		/**
		 * Returns a list of games objects sorted by number of current viewers on Twitch, most popular first.
		 * 
		 * <p>
		 * Note:
		 * 	<li>Data needs to be sent as <code>URLRequestMethod.GET</code>.</li>
		 * </p>
		 * 
		 * @see <a href="https://github.com/justintv/Twitch-API/blob/master/v3_resources/games.md#get-gamestop">Twitch API</a>
		 */ 
		public static const GAMES_TOP_URL:String = "https://api.twitch.tv/kraken/games/top";
		
	}
	
}
