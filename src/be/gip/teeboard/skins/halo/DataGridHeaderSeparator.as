package be.gip.teeboard.skins.halo {
	
	import flash.display.Graphics;
	import mx.skins.ProgrammaticSkin;
	
	/**
	 *  The skin for the separator between column headers in a DataGrid.
	 *  
	 *  @langversion 3.0
	 *  @playerversion Flash 9
	 *  @playerversion AIR 1.1
	 *  @productversion Flex 3
	 */
	public class DataGridHeaderSeparator extends ProgrammaticSkin {
		
		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  Constructor.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 9
		 *  @playerversion AIR 1.1
		 *  @productversion Flex 3
		 */
		public function DataGridHeaderSeparator() {
			super();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden properties
		//
		//--------------------------------------------------------------------------
		
		//----------------------------------
		//  measuredWidth
		//----------------------------------
		
		/**
		 *  @private
		 */
		override public function get measuredWidth():Number	{
			return 2;
		}
		
		//----------------------------------
		//  measuredHeight
		//----------------------------------
		
		/**
		 *  @private
		 */
		override public function get measuredHeight():Number {
			return 10;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		override protected function updateDisplayList(w:Number, h:Number):void {
			super.updateDisplayList(w, h);
			var g:Graphics = graphics;
			
			g.clear();
			
			// Highlight (left side)
			//g.lineStyle(1, 0x666666, 0.4);
			g.lineStyle(1, 0x4B4B4B, 0.4);
			g.moveTo(0, 0);
			g.lineTo(0, h);
			
			// Shadow (right side)
			g.lineStyle(1, 0x101010, 1); 
			g.moveTo(1, 0);
			g.lineTo(1, h);
		}
		
	}
	
}
