package be.gip.teeboard.skins {
	
	import flash.display.BitmapData;
	
	import mx.core.BitmapAsset;
	import mx.core.SpriteAsset;
	
	
	[Embed(source="/css/skins/ToolTip_background.png", scaleGridLeft="20", scaleGridRight="185", scaleGridTop="5", scaleGridBottom="39")]
	public class DonationToolTipBackground extends SpriteAsset {
		
		public function DonationToolTipBackground()	{
			super();
		}
	}
	
}