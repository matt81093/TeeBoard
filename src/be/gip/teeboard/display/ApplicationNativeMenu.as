package be.gip.teeboard.display {
	
	import be.gip.teeboard.events.ApplicationMenuEvent;
	import be.gip.teeboard.graphics.ApplicationIcon;
	import be.gip.teeboard.managers.NotificationsManager;
	
	import flash.desktop.DockIcon;
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemTrayIcon;
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.ScreenMouseEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.utils.Timer;
	
	[Event(name="about", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="donate", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="downloadDirectory", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="downloadManager", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="exit", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="help", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="patch", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="website", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="youtube", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="notificationChange", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="notificationOnTop", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	[Event(name="viewLog", type="be.gip.teeboard.events.ApplicationMenuEvent")]
	
	public class ApplicationNativeMenu extends EventDispatcher {
		
		private var nativeApplication:NativeApplication;
		
		private var trayMenu:NativeMenu;
		
		private var versionItem:NativeMenuItem;
		private var aboutItem:NativeMenuItem;
		private var donateItem:NativeMenuItem;
		private var downloadDirItem:NativeMenuItem;
		private var downloadManagerItem:NativeMenuItem;
		private var exitItem:NativeMenuItem;
		private var helpItem:NativeMenuItem;
		private var patchItem:NativeMenuItem;
		private var siteItem:NativeMenuItem;
		private var ytItem:NativeMenuItem;
		private var logItem:NativeMenuItem;
		private var minimizeItem:NativeMenuItem;
		
		private var notificationItem:NativeMenuItem;
		private var ontopItem:NativeMenuItem;
		
		private var trayTimer:Timer;
		private var firstClick:Boolean = false;
		private var _versionLabel:String;
		
		private var _notificationOnTop:Boolean = false;
		
		//------------------------------------
		// CONSTRUCTOR
		//------------------------------------
		public function ApplicationNativeMenu() {
			nativeApplication = NativeApplication.nativeApplication;
			init();
		}
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("ApplicationNativeMenu ::: init");
			trayTimer = new Timer(600, 1);
			trayTimer.addEventListener(TimerEvent.TIMER, tray_timerHandler);
			
			var descriptor:XML = nativeApplication.applicationDescriptor;
			var ns:Namespace = descriptor.namespaceDeclarations()[0];
			var vnumber:String = descriptor.ns::versionNumber;
			var vLabel:String = descriptor.ns::versionLabel;
			trace("    - versionLabel: ", vLabel);
			_versionLabel = vLabel;
			
			createMenu();
			setTrayTooltip();
		}
		
		//------------------------------------
		// createMenu()
		//------------------------------------
		private function createMenu():void {
			trace("ApplicationNativeMenu ::: createMenu");
			trace(NativeApplication.nativeApplication);
			trayMenu = new NativeMenu(); 
			
			// version info
			versionItem = trayMenu.addItem(new NativeMenuItem("TeeBoard: " + versionLabel));
			versionItem.enabled = false;
			
			// seperator
			trayMenu.addItem(new NativeMenuItem("", true));
			
			// Application help (youtube videos)
			helpItem = trayMenu.addItem(new NativeMenuItem("Help"));
			helpItem.keyEquivalent = "f1";
			helpItem.keyEquivalentModifiers = [];
			helpItem.addEventListener(Event.SELECT, helpItem_selectHandler);
			
			ytItem = trayMenu.addItem(new NativeMenuItem("Instructional Videos"));
			ytItem.keyEquivalent = "f2";
			ytItem.keyEquivalentModifiers = [];
			ytItem.addEventListener(Event.SELECT, ytItem_selectHandler);
			
			// Application patch notes
			patchItem = trayMenu.addItem(new NativeMenuItem("Patch Notes"));
			patchItem.keyEquivalent = "f3";
			patchItem.keyEquivalentModifiers = [];
			patchItem.addEventListener(Event.SELECT, patchItem_selectHandler);
			
			// Application about
			aboutItem = trayMenu.addItem(new NativeMenuItem("About TeeBoard"));
			aboutItem.keyEquivalent = "f4";
			aboutItem.keyEquivalentModifiers = [];
			aboutItem.addEventListener(Event.SELECT, aboutItem_selectHandler);
			
			// Application log file
			logItem = trayMenu.addItem(new NativeMenuItem("View Log"));
			logItem.keyEquivalent = "f6";
			logItem.keyEquivalentModifiers = [];
			logItem.addEventListener(Event.SELECT, logItem_selectHandler);
			
			// seperator
			trayMenu.addItem(new NativeMenuItem("", true));
			
			// Paypal Donate link
			donateItem = trayMenu.addItem(new NativeMenuItem("Donate"));
			donateItem.addEventListener(Event.SELECT, donateItem_selectHandler);
			
			// Application website
			siteItem = trayMenu.addItem(new NativeMenuItem("Website"));
			siteItem.addEventListener(Event.SELECT, siteItem_selectHandler);
			
			// seperator
			trayMenu.addItem(new NativeMenuItem("", true));
			
			// download items
			downloadManagerItem = trayMenu.addItem(new NativeMenuItem("Download Manager"));
			downloadManagerItem.addEventListener(Event.SELECT, downloadManagerItem_selectHandler);
			
			downloadDirItem = trayMenu.addItem(new NativeMenuItem("Open Download Folder"));
			downloadDirItem.addEventListener(Event.SELECT, downloadDirItem_selectHandler);
			
			// seperator
			trayMenu.addItem(new NativeMenuItem("", true));
			
			// notification submenu
			notificationItem = trayMenu.addItem(new NativeMenuItem("Notifcation Location"));
			
			var notificationSubMenu:NativeMenu = new NativeMenu();
			notificationSubMenu.addEventListener(Event.SELECT, notificationSubMenu_selectHandler);
			
			var custom:NativeMenuItem = notificationSubMenu.addItem(new NativeMenuItem("Custom"));
			custom.checked = false;
			custom.data = NotificationsManager.CUSTOM;
			
			var topleft:NativeMenuItem = notificationSubMenu.addItem(new NativeMenuItem("Top Left"));
			topleft.checked = false;
			topleft.data = NotificationsManager.TOP_LEFT
			
			var topright:NativeMenuItem = notificationSubMenu.addItem(new NativeMenuItem("Top Right"));
			topright.checked = false;
			topright.data = NotificationsManager.TOP_RIGHT;
			
			var bottomleft:NativeMenuItem = notificationSubMenu.addItem(new NativeMenuItem("Bottom Left"));
			bottomleft.checked = false;
			bottomleft.data = NotificationsManager.BOTTOM_LEFT;
			
			var bottomright:NativeMenuItem = notificationSubMenu.addItem(new NativeMenuItem("Bottom Right"));
			bottomright.checked = true;
			bottomright.data = NotificationsManager.BOTTOM_RIGHT;
			
			notificationItem.submenu = notificationSubMenu;
			
			ontopItem = trayMenu.addItem(new NativeMenuItem("Force Notification on top"));
			ontopItem.checked = false;
			ontopItem.addEventListener(Event.SELECT, ontop_selectHandler);
			
			// Show / hide application (minimize / activate)
			//showItem = trayMenu.addItem(new NativeMenuItem("Show / Hide"));
			//showItem.addEventListener(Event.SELECT, showItem_selectHandler);
			
			var tIcon:ApplicationIcon = new ApplicationIcon();
			
			if (NativeApplication.supportsSystemTrayIcon) {
				
				//minimizeItem = trayMenu.addItem(new NativeMenuItem("Minimize to system tray"));
				//minimizeItem.checked = _minimizeToTray;
				//minimizeItem.addEventListener(Event.SELECT, minimizeItem_selectHandler);
				
				// seperator
				trayMenu.addItem(new NativeMenuItem("", true));
				
				exitItem = trayMenu.addItem(new NativeMenuItem("Exit")); 
				exitItem.addEventListener(Event.SELECT, exitItem_selectHandler);
				
				var systray:SystemTrayIcon =  NativeApplication.nativeApplication.icon as SystemTrayIcon; 
				systray.bitmaps = [tIcon.bitmapData];
 				systray.menu = trayMenu;
				systray.addEventListener(ScreenMouseEvent.CLICK, systemtray_clickHandler);
				setTrayTooltip();
			} else if(NativeApplication.supportsDockIcon) {
				//128 icon
				var dock:DockIcon = NativeApplication.nativeApplication.icon as DockIcon;
				dock.bitmaps = [tIcon.bitmapData];
				dock.menu = trayMenu; 
			} 
		}
		
		//------------------------------------
		// setMinimizeToTray()
		//------------------------------------
		private function setMinimizeToTray():void {
			trace("ApplicationNativeMenu ::: setMinimizeToTray");
			if(minimizeItem) minimizeItem.checked = _minimizeToTray;
		}
		
		//------------------------------------
		// setNotificationLocation()
		//------------------------------------
		private function setNotificationLocation():void {
			trace("ApplicationNativeMenu ::: setNotificationLocation");
			var items:Array = notificationItem.submenu.items;
			for(var i:uint=0; i<items.length; i++) {
				if(items[i].data == _notificationLocation) {
					items[i].checked = true;
				}else {
					items[i].checked = false;
				}
			}
		}
		
		//------------------------------------
		// setNotificationOnTop()
		//------------------------------------
		private function setNotificationOnTop():void {
			trace("ApplicationNativeMenu ::: setNotificationOnTop");
			ontopItem.checked = _notificationOnTop;
		}
		
		//------------------------------------
		// notificationSubMenu_selectHandler()
		//------------------------------------
		private function notificationSubMenu_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: notificationSubMenu_selectHandler");
			var t:NativeMenu = event.currentTarget as NativeMenu;
			var item:NativeMenuItem = event.target as NativeMenuItem;
			// set notification location to selected item data
			_notificationLocation = item.data.toString();
			
			// set the selected item as selected
			setNotificationLocation();
			// dispatch event
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.NOTIFICATION_CHANGE));
		}
		
		//------------------------------------
		// ontop_selectHandler()
		//------------------------------------
		private function ontop_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: ontop_selectHandler");
			_notificationOnTop = !ontopItem.checked;
			ontopItem.checked = _notificationOnTop;
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.NOTIFICATION_ON_TOP));
		}
		
		//------------------------------------
		// tray_timerHandler()
		//------------------------------------
		private function tray_timerHandler(event:TimerEvent):void {
			trace("ApplicationNativeMenu ::: tray_timerHandler");
			firstClick = false;
		}
		
		//------------------------------------
		// setTrayTooltip()
		//------------------------------------
		private function setTrayTooltip():void {
			trace("ApplicationNativeMenu ::: setTrayTooltip");
			if (NativeApplication.supportsSystemTrayIcon) {
				var systray:SystemTrayIcon =  NativeApplication.nativeApplication.icon as SystemTrayIcon; 
				systray.tooltip = "TeeBoard: " + versionLabel;
			}
		}
		
		//------------------------------------
		// systemtray_clickHandler()
		//------------------------------------
		private function systemtray_clickHandler(event:ScreenMouseEvent):void {
			trace("ApplicationNativeMenu ::: systemtray_clickHandler");
			if(!firstClick) {
				firstClick = true;
				// start timer to reset firstClick
				trayTimer.start();
			}else {
				trayTimer.reset();
				firstClick = false;
				//dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.SHOW));
			}
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.SHOW));
		}
		
		//------------------------------------
		// donateItem_selectHandler()
		//------------------------------------
		private function donateItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: donateItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.DONATE));
		}
		
		//------------------------------------
		// siteItem_selectHandler()
		//------------------------------------
		private function siteItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: siteItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.WEBSITE));
		}
		
		//------------------------------------
		// aboutItem_selectHandler()
		//------------------------------------
		private function aboutItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: aboutItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.ABOUT));
		}
		
		//------------------------------------
		// patchItem_selectHandler()
		//------------------------------------
		private function patchItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: patchItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.PATCH_NOTES));
		}
		
		//------------------------------------
		// helpItem_selectHandler()
		//------------------------------------
		private function helpItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: helpItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.HELP));
		}
		
		//------------------------------------
		// logItem_selectHandler()
		//------------------------------------
		private function logItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: logItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.VIEW_LOG));
		}
		
		//------------------------------------
		// ytItem_selectHandler()
		//------------------------------------
		private function ytItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: ytItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.YOUTUBE));
		}
		
		//------------------------------------
		// downloadManagerItem_selectHandler()
		//------------------------------------
		private function downloadManagerItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: downloadManagerItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.DOWNLOAD_MANAGER));
		}
		
		//------------------------------------
		// downloadDirItem_selectHandler()
		//------------------------------------
		private function downloadDirItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: downloadDirItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.DOWNLOAD_DIRECTORY));
		}
		
		//------------------------------------
		// showItem_selectHandler()
		//------------------------------------
		private function showItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: showItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.SHOW));
		}
		
		//------------------------------------
		// minimizeItem_selectHandler()
		//------------------------------------
		private function minimizeItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: minimizeItem_selectHandler");
			var checked:Boolean = minimizeItem.checked;
			minimizeItem.checked = !checked;
			_minimizeToTray = !checked;
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.MINIMIZE_TO_TRAY));
		}
		
		//------------------------------------
		// exitItem_selectHandler()
		//------------------------------------
		private function exitItem_selectHandler(event:Event):void {
			trace("ApplicationNativeMenu ::: exitItem_selectHandler");
			dispatchEvent(new ApplicationMenuEvent(ApplicationMenuEvent.EXIT));
		}
		
		//=====================================
		//
		// GETTER / SETTER
		//
		//=====================================
		
		private var _notificationLocation:String;
		/**
		 * 
		 */ 
		public function get notificationLocation():String {
			return _notificationLocation;
		}
		public function set notificationLocation(value:String):void {
			if(value == _notificationLocation) return;
			_notificationLocation = value;
			setNotificationLocation();
		}
		
		public function get notificationOnTop():Boolean {
			return _notificationOnTop;
		}
		public function set notificationOnTop(value:Boolean):void {
			if(value == _notificationOnTop) return;
			_notificationOnTop = value;
			setNotificationOnTop();
		}
		
		public function get versionLabel():String {
			return _versionLabel;
		}
		
		private var _minimizeToTray:Boolean;
		/**
		 * 
		 */ 
		public function get minimizeToTray():Boolean {
			return _minimizeToTray;
		}
		public function set minimizeToTray(value:Boolean):void {
			_minimizeToTray = value;
			setMinimizeToTray();
		}

		
	}
	
}
