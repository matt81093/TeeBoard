package be.gip.teeboard.display {
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	
	import mx.core.UIComponent;
	
	public class ImageTile extends UIComponent {
		
		// private properties
		private var tileSprite:Sprite;
		private var sourceChanged:Boolean = false;
		private var tileBData:BitmapData;
		
		// internal getter setters
		private var _source:Class;
		
		//==================================================
		//
		// CONSTRUCTOR
		//
		//==================================================
		
		public function ImageTile() {
			super();
		}
		
		//==================================================
		//
		// OVERRIDE PROTECTED METHODS
		//
		//==================================================
		
		//------------------------------------------------------------------
		// createChildren()
		//------------------------------------------------------------------
		override protected function createChildren():void {
			// trace("ImageTile ::: createChildren");
			super.createChildren();
			tileSprite = new Sprite();
			//
			addChild(tileSprite as DisplayObject);
		}
		
		//------------------------------------------------------------------
		// measure()
		//------------------------------------------------------------------
		override protected function measure():void {
			// trace("ImageTile ::: measure");
			super.measure();
			measuredWidth = measuredMinWidth = 100;
            measuredHeight = measuredMinHeight = 100;
		}
		
		//------------------------------------------------------------------
		// updateDisplayList()
		//------------------------------------------------------------------
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			//trace("ImageTile ::: updateDisplayList");
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			//trace("    - unscaledWidth: "+unscaledWidth);
			//trace("    - unscaledHeight: "+unscaledHeight);
			// trace("    - source: "+__source);
			//trace("    - parent: ", this.parent);
			//trace("    - parent width: ", this.parent.width);
			//trace("    - parent height: ", this.parent.height);
			//trace("    - app measuredWidth: ", this.parentApplication.measuredWidth);
			//trace("    - app measuredHeight: ", this.parentApplication.measuredHeight);
			if(sourceChanged) {
				// trace("    - sourceChanged");
				sourceChanged = false;
				createImageTile();
			}
			drawImageTile();
		}
		
		//==================================================
		//
		// PRIVATE METHODS
		//
		//==================================================
		
		//------------------------------------------------------------------
		// createImageTile()
		//------------------------------------------------------------------
		private function createImageTile():void {
			//trace("ImageTile ::: createImageTile");
			//trace("    _source: ", _source);
			var bm:Bitmap = new _source();
			//trace("    - bitmap width: "+bm.width);
			//trace("    - bm height: "+bm.height);
			if(tileBData) {
				tileBData.dispose();
			}
			tileBData = bm.bitmapData;
		}
		
		//------------------------------------------------------------------
		// drawImageTile()
		//------------------------------------------------------------------
		private function drawImageTile():void {
			//trace("ImageTile ::: drawImageTile");
			var w:Number = unscaledWidth;
			var h:Number = unscaledHeight;
			//if(this.parentApplication == this.parent) {
				//w = Math.max(this.parentApplication.measuredWidth, w);
				//h = Math.max(this.parentApplication.measuredHeight, h);
			//}
			//trace("    - width: "+this.width);
			//trace("    - height: "+this.height);
			//trace("    - unscaledWidth: "+this.unscaledWidth);
			//trace("    - unscaledHeight: "+this.unscaledHeight);
			//trace("    - tileBData width: "+tileBData.width);
			//trace("    - tileBData height: "+tileBData.height);
			
			tileSprite.graphics.clear();
			tileSprite.graphics.beginBitmapFill(tileBData, new Matrix(), true, false);
			tileSprite.graphics.drawRect(0, 0, w, h);
		}
		
		//==================================================
		//
		// IMPLICIT GETTER SETTER METHODS
		//
		//==================================================
		
		//------------------------------------------------------------------
		// source
		//------------------------------------------------------------------
		public function set source(value:Class):void {
			// trace("ImageTile ::: set source");
			// trace("    - source: ", value);
			_source = value;
			sourceChanged = true;
			invalidateDisplayList();
		}
		public function get source():Class {
			// trace("ImageTile ::: get source");
			return _source;
		}
		
	}
	
}
