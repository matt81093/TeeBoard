package be.gip.teeboard.views {
	
	public interface IView {
		
		/**
		 * Refreshes the view, like hitting <code>F5</code> in the browser.
		 * 
		 */ 
		function refresh():void;
		
	}
	
}
