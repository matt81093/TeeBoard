package be.gip.teeboard.control {
	
	import be.gip.mvc.business.DataStoreLocator;
	import be.gip.mvc.control.FrontController;
	import be.gip.teeboard.commands.datastore.*;
	import be.gip.teeboard.events.DataStoreEvent;
	
	import flash.filesystem.File;
	
	public class DataStoreController extends FrontController {
		
		public static const APP_DATASTORE:File = File.applicationDirectory.resolvePath("db/teeboard.db");
		public static const STORAGE_DATASTORE:File = File.applicationStorageDirectory.resolvePath("db/teeboard.db");
		
		public function DataStoreController() {
			trace("DataStoreController ::: CONSTRUCTOR");
			initDataStore();
			initCommands();
		}
		
		private function initDataStore():void {
			trace("DataStoreController ::: initDataStore");
			trace("    - ESTABLISHING DATASTORE CONNECTION");
			if(!STORAGE_DATASTORE.exists) {
				APP_DATASTORE.copyTo(STORAGE_DATASTORE);
			}
			trace("    - datastore: ", DataStoreLocator.getInstance().getDataStore("teeboardSQLite"));
			DataStoreLocator.getInstance().getDataStore("teeboardSQLite").dataSource = STORAGE_DATASTORE;
		}
		
		private function initCommands():void {
			trace("DataStoreController ::: initCommands");
			addCommand(DataStoreEvent.GET_CHANNEL, GetChannelCommand);
			addCommand(DataStoreEvent.GET_CHANNELS, GetChannelsCommand);
			addCommand(DataStoreEvent.GET_CHANNEL_TITLES, GetChannelTitlesCommand);
		}
		
	}
	
}