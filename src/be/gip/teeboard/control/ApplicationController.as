package be.gip.teeboard.control {
	
	import be.gip.mvc.control.FrontController;
	import be.gip.teeboard.commands.*;
	import be.gip.teeboard.commands.backendless.GetAuthorCommand;
	import be.gip.teeboard.commands.backendless.GetPanelsCommand;
	import be.gip.teeboard.commands.notifications.*;
	import be.gip.teeboard.commands.tools.*;
	import be.gip.teeboard.commands.twobbler.*;
	import be.gip.teeboard.commands.widgets.*;
	import be.gip.teeboard.events.*;
	
	public class ApplicationController extends FrontController {
		
		public function ApplicationController()	{
			trace("ApplicationController ::: CONSTRUCTOR");
			initCommands();
		}
		
		protected function initCommands():void {
			
			this.addCommand(TwitchEvent.GET_ROOT, GetRootCommand);
			//
			this.addCommand(TwitchEvent.GET_USER, GetUserCommand);
			//
			this.addCommand(TwitchEvent.GET_CHANNEL, GetChannelCommand);
			this.addCommand(TwitchEvent.GET_EDITOR_CHANNEL, GetEditorChannelCommand);
			this.addCommand(TwitchEvent.GET_CHANNEL_FOLLOWERS, GetChannelFollowersCommand);
			this.addCommand(TwitchEvent.GET_CHANNEL_SUBSCRIBERS, GetChannelSubscribersCommand);
			this.addCommand(TwitchEvent.GET_CHANNEL_EDITORS, GetChannelEditorsCommand);
			this.addCommand(TwitchEvent.UPDATE_CHANNEL_STATUS, UpdateChannelStatusCommand);
			
			this.addCommand(TwitchEvent.GET_CHANNEL_DASHBOARD, GetDashboardCommand);
			//
			this.addCommand(TwitchEvent.RUN_COMMERCIAL, RunCommercialCommand);
			//
			this.addCommand(TwitchEvent.GET_STREAM, GetChannelStreamCommand);
			this.addCommand(TwitchEvent.GET_FOLLOWED_STREAMS, GetFollowedStreamsCommand);
			//
			this.addCommand(TwitchEvent.GET_CHANNEL_BROADCASTS, GetChannelBroadcastsCommand);
			this.addCommand(TwitchEvent.GET_CHANNEL_HIGHLIGHTS, GetChannelHighlightsCommand);
			
			// WIDGET EVENTS
			
			this.addCommand(WidgetEvent.WRITE_FP_CONFIG, WriteTrustedConfigCommand);
			
			this.addCommand(WidgetEvent.FIX, FixWidgetsCommand);
			
			// chat
			this.addCommand(WidgetEvent.GET_CHAT_CONFIG, GetChatConfigCommand);
			this.addCommand(WidgetEvent.UPDATE_CHAT_CONFIG, UpdateChatConfigCommand);
			// clock
			this.addCommand(WidgetEvent.GET_CLOCK_CONFIG, GetClockConfigCommand);
			this.addCommand(WidgetEvent.UPDATE_CLOCK_CONFIG, UpdateClockConfigCommand);
			this.addCommand(WidgetEvent.WRITE_CLOCK_DATE, WriteClockDateCommand);
			// countdown
			this.addCommand(WidgetEvent.GET_COUNTDOWN_CONFIG, GetCountdownConfigCommand);
			this.addCommand(WidgetEvent.UPDATE_COUNTDOWN_CONFIG, UpdateCountdownConfigCommand);
			// media
			this.addCommand(WidgetEvent.GET_MEDIA_CONFIG, GetMediaConfigCommand);
			this.addCommand(WidgetEvent.UPDATE_MEDIA_CONFIG, UpdateMediaConfigCommand);
			this.addCommand(WidgetEvent.GET_MEDIA_LISTING, GetMediaListingCommand);
			this.addCommand(WidgetEvent.CREATE_MEDIA_WIDGET, CreateMediaWidgetCommand);
			this.addCommand(WidgetEvent.REMOVE_MEDIA_WIDGET, RemoveMediaWidgetCommand);
			// poll
			this.addCommand(WidgetEvent.GET_POLL_CONFIG, GetPollConfigCommand);
			this.addCommand(WidgetEvent.UPDATE_POLL_CONFIG, UpdatePollConfigCommand);
			this.addCommand(WidgetEvent.WRITE_POLL, WritePollCommand);
			// spectrum
			this.addCommand(WidgetEvent.GET_SPECTRUM_CONFIG, GetSpectrumConfigCommand);
			this.addCommand(WidgetEvent.UPDATE_SPECTRUM_CONFIG, UpdateSpectrumConfigCommand);
			// tickertape
			this.addCommand(WidgetEvent.WRITE_TICKERTAPE_TEXT, WriteTickerTextCommand);
			
			// NOTIFICATION EVENTS
			
			// followers
			this.addCommand(NotificationEvent.WRITE_FOLLOWERS, WriteFollowersCommand);
			// subscribers
			this.addCommand(NotificationEvent.WRITE_SUBSCRIBERS, WriteSubscribersCommand);
			// donations
			this.addCommand(NotificationEvent.WRITE_DONATIONS, WriteDonationsCommand);
			this.addCommand(DonationsEvent.GET_DONATIONS, GetDonationsCommand);
			this.addCommand(DonationsEvent.VERIFY_SERVICE, VerifyServiceCommand);
			this.addCommand(DonationsEvent.GET_TWITCHPLUS_TOKEN, GetTwitchPlusTokenCommand);
			// new notification
			this.addCommand(NotificationEvent.NEW_NOTIFICATIONS, NotificationsCommand);
			
			// TWOBBER EVENTS
			this.addCommand(TwobblerEvent.GET_RECENT_TRACKS, GetRecentTracksCommand);
			
			// TOOLS EVENTS
			this.addCommand(OAuthTokenEvent.VALIDATE_TOKEN, ValidateTokenCommand);
			this.addCommand(GiantBombEvent.UPDATE_PLATFORM, UpdatePlatformCommand);
			
			// CHANNEL PANELS IMGUR EVENTS
			this.addCommand(ImgurEvent.GET_IMGUR_ALBUM, GetImgurAlbumCommand);
			
			// BACKENDLESS EVENTS
			//this.addCommand(BackendlessEvent.GET_PANELS, GetPanelsCommand);
			//this.addCommand(BackendlessEvent.GET_AUTHOR, GetAuthorCommand);
			
		}
		
	}
	
}
