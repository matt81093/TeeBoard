package be.gip.teeboard.managers {
	
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.dto.chat.MessageDTO;
	import be.gip.teeboard.events.IRCChatEvent;
	import be.gip.teeboard.events.TeeBoardLogEvent;
	import be.gip.teeboard.log.TeeLogger;
	
	import com.adobe.utils.StringUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.OutputProgressEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.Socket;
	
	import mx.events.PropertyChangeEvent;
	
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Event(name="chatMessage", type="be.gip.teeboard.events.IRCChatEvent")]
	
	[Event(name="pollMessage", type="be.gip.teeboard.events.IRCChatEvent")]
	
	[Bindable]
	
	/**
	 * Manages the connection to the Twitch IRC server.
	 */ 
	public class IRCManager extends EventDispatcher {
		
		private static var INSTANCE:IRCManager;
		
		/**
		 * 
		 */ 
		public static const SERVER_CONNECT:String = "serverConnect";
		
		/**
		 * 
		 */ 
		public static const SERVER_CLOSE:String = "serverClose";
		
		/**
		 * 
		 */ 
		public static const CHANNEL_JOIN:String = "channelJoin";
		
		private var _channelName:String;
		//public var channelName:String = "deezjavu";
		
		// 199.9.253.210
		// 199.9.253.199
		// 199.9.250.229
		// 199.9.250.230 (irc.twitch.tv)
		// irc.twitch.tv
		
		private var logger:TeeLogger = TeeLogger.getInstance();
		
		private var ircSocket:Socket;
		private var ircUrl:String = "irc.twitch.tv";
		private var ircPort:int = 80;
		//private var ircPort:int = 6667;
		
		private var jfan:String;
		
		private var namesReceived:Boolean = false;
		
		private var rawNames:String;
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function IRCManager() {
			trace("IRCManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one IRCManager instance allowed. " +
					"To access the Singleton instance, IRCManager.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():IRCManager {
			if(!INSTANCE) INSTANCE = new IRCManager();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("IRCManager ::: init");
			
			jfan = "justinfan" + new Date().time;
			trace("    - justinfan: ", jfan);
			
			ircSocket = new Socket();
			ircSocket.addEventListener(Event.CLOSE, socket_closeHandler);
			ircSocket.addEventListener(Event.CONNECT, socket_connectHandler);
			ircSocket.addEventListener(IOErrorEvent.IO_ERROR, socket_ioErrorHandler);
			ircSocket.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, socket_progressHandler);
			ircSocket.addEventListener(ProgressEvent.SOCKET_DATA, socket_dataHandler);
			ircSocket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, socket_securityErrorHandler);
			
		}
		
		//------------------------------------
		// socket_connectHandler()
		//------------------------------------
		private function socket_connectHandler(event:Event):void {
			trace("IRCManager ::: socket_connectHandler");
			// authenticate using justinfan when connected
			trace("    - sending NICK: ", jfan);
			trace("    - justinfan: ", jfan);
			
			namesReceived = false;
			rawNames = "";
			
			ircSocket.writeUTFBytes("NICK " + jfan + "\n");
			ircSocket.flush();
			
			//dispatchChangeEvent("connected");
			dispatchChangeEvent(SERVER_CONNECT);
			
			var log:LogMessage = new LogMessage();
			log.message = "IRCManager :: socket connected - logging in: " + jfan;
			logger.log(log);
		}
		
		//------------------------------------
		// socket_progressHandler()
		//------------------------------------
		private function socket_progressHandler(event:OutputProgressEvent):void {
			trace("IRCManager ::: socket_progressHandler");
			trace("    - BYTES PENDING: ", event.bytesPending);
			trace("    - BYTES TOTAL", event.bytesTotal);
			trace("------------------------------------------");
		}
		
		//------------------------------------
		// socket_dataHandler()
		//------------------------------------
		private function socket_dataHandler(event:ProgressEvent):void {
			trace("IRCManager ::: socket_dataHandler");
			
			trace("    - bytes available: ", ircSocket.bytesAvailable);
			trace("    - bytes pending: ", ircSocket.bytesPending);
			
			//var buffer:ByteArray = new ByteArray();
			//ircSocket.readBytes(buffer, 0, ircSocket.bytesAvailable);
			//trace("    - buffer length: ", buffer.length);
			//trace("    - buffer bytes: ", buffer.bytesAvailable);
			//var str:String = buffer.toString();
			
			var str:String = ircSocket.readUTFBytes(ircSocket.bytesAvailable);
			trace("    - message: ", str);
			
			var lines:Array = str.split("\r\n");
			var numLines:uint = lines.length;
			
			trace("    - message lines: ", numLines);
			
			// if message begins with :tmi.twitch.tv and is a NOTICE login may have failed 
			// there may be other NOTICE type messages to look into
			if(str.indexOf(":tmi.twitch.tv") == 0 && str.indexOf("NOTICE") != -1) {
				trace("    - failed to login");
				dispatchLogEvent("IRCManager :: failed to login");
				// :tmi.twitch.tv NOTICE * :Login unsuccessful
				// login unsuccessful
			}
			
			// :tmi.twitch.tv 001 = welcome messge
			// :tmi.twitch.tv 002 = host messge
			// :tmi.twitch.tv 003 = server is new message
			// :tmi.twitch.tv 004 = :- message
			// :tmi.twitch.tv 375 = :- message
			// :tmi.twitch.tv 372 = maze of twisty passages message
			// :tmi.twitch.tv 376 is end of (twitch) connection messages
			if(str.indexOf(":tmi.twitch.tv 376") != -1) {
				trace("    - joining chat room: ", channelName);
				// try joining room
				ircSocket.writeUTFBytes("JOIN #" + channelName + "\n");
				ircSocket.flush();
				dispatchLogEvent("IRCManager :: joining chat room: " + channelName);
			}
			
			// user joined channel
			// :justinfan1400028528297!justinfan1400028528297@justinfan1400028528297.tmi.twitch.tv JOIN #deezjavu
			if(str.indexOf(":" + jfan) == 0 && str.indexOf("JOIN #" + channelName) != -1) {
				trace("    - joined chat room: ", channelName);
				dispatchChangeEvent(CHANNEL_JOIN);
				dispatchLogEvent("IRCManager :: joined chat room: " + channelName);
			}
			
			// 353 = list of users
			
			// try and get user list
			// :justinfan1399563260032.tmi.twitch.tv 353 justinfan1399563260032 = #smitegame :justinfan1399563260032
			trace("    - jfan index: ", str.indexOf(":" + jfan));
			trace("    - jfan last index: ", str.lastIndexOf(":"+jfan));
			// this is not correct as the user list may be broken up in 2 separate messages
			// where the 2nd message begins with a user's name instead of ":justinfan123456"
			if(str.indexOf(":" + jfan) == 0 && namesReceived == false) {
				rawNames += str;
			}
			
			if(str.indexOf(":End of /NAMES list") != -1) {
				rawNames += str;
				namesReceived = true;
				trace("RAW NAMES LIST");
				trace(rawNames);
			}
			
			// if message begins with PING answer with PONG
			if(str.indexOf("PING") == 0) {
				trace("    - playing ping pong");
				ircSocket.writeUTFBytes("PONG" + "\n");
				ircSocket.flush();
			}
			
			// if message contains "PRIVMSG #channelName", it's a user chat message
			if(numLines == 2 && str.indexOf("PRIVMSG #" + channelName) != -1) {
				trace("    - parsing user chat message");
				var parts:Array = str.split("!");
				var from:String = parts.shift().toString().substring(1);
				var msg:String = StringUtil.trim(str.split("PRIVMSG #" + channelName + " :").pop());
				trace("    - from: ", from);
				trace("    - msg: ", msg);
				trace("    - first char: ", msg.charCodeAt(0));
				trace("    - last char: ", msg.charCodeAt(msg.length-1));
				var dto:MessageDTO = new MessageDTO();
				dto.to = channelName;
				//dto.time = new Date();
				
				var isPollVote:Boolean = false;
				
				if(msg.indexOf("ACTION ") == 0) {
					// this is a /me message
					dto.from = "* " + from;
					dto.text = msg.replace("ACTION ", "").split("<").join("&lt;").split(">").join("&gt;");
				}else {
					// normal user message
					dto.from = from;
					dto.text = msg.split("<").join("&lt;").split(">").join("&gt;");
					// poll votes begin with "#"
					if(msg.indexOf("#") == 0) {
						isPollVote = true;
					}
				}
				
				if(isPollVote) {
					dispatchPollEvent(dto);
				}
				
				dispatchChatEvent(dto);
			}
			
			trace("------------------------------------------");
		}
		
		//------------------------------------
		// socket_ioErrorHandler()
		//------------------------------------
		private function socket_ioErrorHandler(event:IOErrorEvent):void {
			trace("IRCManager ::: socket_ioErrorHandler");
			dispatchLogEvent("IRCManager :: ioError: " + event.errorID + " - " + event.text);
		}
		
		//------------------------------------
		// socket_securityErrorHandler()
		//------------------------------------
		private function socket_securityErrorHandler(event:SecurityErrorEvent):void {
			trace("IRCManager ::: socket_securityErrorHandler");
			dispatchLogEvent("IRCManager :: security error: " + event.errorID + " - " + event.text);
		}
		
		//------------------------------------
		// socket_closeHandler()
		//------------------------------------
		private function socket_closeHandler(event:Event):void {
			trace("IRCManager ::: socket_closeHandler");
			dispatchChangeEvent("closed");
			dispatchLogEvent("IRCManager :: irc connection closed");
		}
		
		//------------------------------------
		// dispatchChangeEvent()
		//------------------------------------
		private function dispatchChangeEvent(prop:String):void {
			trace("IRCManager ::: dispatchChangeEvent");
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = prop;
			dispatchEvent(propEvent);
		}
		
		//------------------------------------
		// dispatchChatEvent()
		//------------------------------------
		private function dispatchChatEvent(dto:MessageDTO):void {
			trace("IRCManager ::: dispatchChatEvent");
			var chatEvent:IRCChatEvent = new IRCChatEvent(IRCChatEvent.CHAT_MESSAGE);
			chatEvent.data = dto;
			dispatchEvent(chatEvent);
		}
		
		//------------------------------------
		// dispatchPollEvent()
		//------------------------------------
		private function dispatchPollEvent(dto:MessageDTO):void {
			trace("IRCManager ::: dispatchPollEvent");
			var pollEvent:IRCChatEvent = new IRCChatEvent(IRCChatEvent.POLL_MESSAGE);
			pollEvent.data = dto;
			dispatchEvent(pollEvent);
		}
		
		//------------------------------------
		// dispatchLogEvent()
		//------------------------------------
		private function dispatchLogEvent(message:String):void {
			trace("IRCManager ::: dispatchLogEvent");
			var log:LogMessage = new LogMessage();
			log.message = message;
			logger.log(log);
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 * 
		 */ 
		public function connect():void {
			trace("IRCManager ::: connect");
			trace("    - connected: ", ircSocket.connected);
			dispatchLogEvent("IRCManager :: connect - url: " + ircUrl + " - port: " + ircPort);
			
			if(ircSocket.connected) ircSocket.close();
			try { 
				trace("    - connecting to: ", ircUrl, ":", ircPort); 
				ircSocket.connect(ircUrl, ircPort); 
				dispatchLogEvent("IRCManager :: connecting to IRC");
			} catch (error:Error) { 
				trace(error.message); 
				dispatchLogEvent("IRCManager :: error connecting: " + error.message);
				ircSocket.close(); 
			}
		}
		
		/**
		 * Closes the current socket connection.
		 */ 
		public function close():void {
			if(ircSocket.connected) {
				ircSocket.close();
				dispatchChangeEvent(SERVER_CLOSE);
			}
		}
		
		//=====================================
		//
		// IMPLICIT GETTER/SETTERS
		//
		//=====================================
		
		public function get connected():Boolean {
			return ircSocket.connected;
		}
		
		/**
		 * The chat room to join.
		 */
		public function get channelName():String {
			return _channelName;
		}
		public function set channelName(value:String):void {
			if(value == _channelName) return;
			if(ircSocket.connected) {
				// leave current room and join new room
				ircSocket.writeUTFBytes("PART #" + _channelName + "\n");
				ircSocket.writeUTFBytes("JOIN #" + value + "\n");
				ircSocket.flush();
			}
			_channelName = value;
		}
		
	}
	
}
