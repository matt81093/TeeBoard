package be.gip.teeboard.managers {
	import be.gip.teeboard.core.WidgetWindow;
	import be.gip.teeboard.model.NotificationsModel;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.SecurityErrorEvent;
	import flash.events.StatusEvent;
	import flash.filesystem.File;
	import flash.net.LocalConnection;
	
	import mx.events.CloseEvent;
	
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	/**
	 * Manages the widget preview windows, so that only 1 preview per widget is open. 
	 */ 
	public class WidgetsPreviewManager extends EventDispatcher {
		
		private static var INSTANCE:WidgetsPreviewManager;
		
		private var chatWin:WidgetWindow;
		private var clockWin:WidgetWindow;
		private var countdownWin:WidgetWindow;
		private var giveawaysWin:WidgetWindow;
		private var mediaWin:WidgetWindow;
		private var notificationWin:WidgetWindow;
		private var pollWin:WidgetWindow;
		private var spectrumWin:WidgetWindow;
		private var tickertapeWin:WidgetWindow;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function WidgetsPreviewManager() {
			trace("WidgetsPreviewManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one WidgetsPreviewManager instance allowed. " +
					"To access the Singleton instance, WidgetsPreviewManager.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():WidgetsPreviewManager {
			if(!INSTANCE) INSTANCE = new WidgetsPreviewManager();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("WidgetsPreviewManager ::: init");
			
		}
		
		//------------------------------------
		// chat_closeHandler()
		//------------------------------------
		private function chat_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: chat_closeHandler");
			chatWin.removeEventListener(CloseEvent.CLOSE, chat_closeHandler);
			chatWin = null;
		}
		
		//------------------------------------
		// clock_closeHandler()
		//------------------------------------
		private function clock_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: clock_closeHandler");
			clockWin.removeEventListener(CloseEvent.CLOSE, clock_closeHandler);
			clockWin = null;
		}
		
		//------------------------------------
		// countdown_closeHandler()
		//------------------------------------
		private function countdown_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: countdown_closeHandler");
			countdownWin.removeEventListener(CloseEvent.CLOSE, countdown_closeHandler);
			countdownWin = null;
		}
		
		//------------------------------------
		// giveaways_closeHandler()
		//------------------------------------
		private function giveaways_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: giveaways_closeHandler");
			giveawaysWin.removeEventListener(CloseEvent.CLOSE, giveaways_closeHandler);
			giveawaysWin = null;
		}
		
		//------------------------------------
		// media_closeHandler()
		//------------------------------------
		private function media_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: media_closeHandler");
			mediaWin.removeEventListener(CloseEvent.CLOSE, media_closeHandler);
			mediaWin = null;
		}
		
		//------------------------------------
		// notification_closeHandler()
		//------------------------------------
		private function notification_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: notification_closeHandler");
			notificationWin.removeEventListener(CloseEvent.CLOSE, notification_closeHandler);
			notificationWin = null;
		}
		
		//------------------------------------
		// poll_closeHandler()
		//------------------------------------
		private function poll_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: poll_closeHandler");
			pollWin.removeEventListener(CloseEvent.CLOSE, poll_closeHandler);
			pollWin = null;
		}
		
		//------------------------------------
		// spectrum_closeHandler()
		//------------------------------------
		private function spectrum_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: spectrum_closeHandler");
			spectrumWin.removeEventListener(CloseEvent.CLOSE, spectrum_closeHandler);
			spectrumWin = null;
		}
		
		//------------------------------------
		// tickertape_closeHandler()
		//------------------------------------
		private function tickertape_closeHandler(event:Event):void {
			trace("WidgetsPreviewManager ::: tickertape_closeHandler");
			tickertapeWin.removeEventListener(CloseEvent.CLOSE, tickertape_closeHandler);
			tickertapeWin = null;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function showChat():void {
			trace("WidgetPreviewManager ::: showChat");
			if(chatWin != null) {
				chatWin.activate();
				return;
			}
			chatWin = new WidgetWindow();
			chatWin.open(true);
			chatWin.width = 400;
			chatWin.height = 500;
			chatWin.title = "Chat Widget";
			chatWin.widgetUrl = WidgetsModel.WIDGET_CHAT_DIR.resolvePath("teeboard-chat.html").url;
			chatWin.addEventListener(CloseEvent.CLOSE, chat_closeHandler);
		}
		
		/**
		 * 
		 */ 
		public function showClock():void {
			trace("WidgetPreviewManager ::: showClock");
			if(clockWin != null) {
				clockWin.activate();
				return;
			}
			clockWin = new WidgetWindow();
			clockWin.open(true);
			clockWin.width = 400;
			clockWin.height = 100;
			clockWin.title = "Clock Widget";
			clockWin.widgetUrl = WidgetsModel.WIDGET_CLOCK_DIR.resolvePath("teeboard-clock.html").url;
			clockWin.addEventListener(CloseEvent.CLOSE, clock_closeHandler);
		}
		
		/**
		 * 
		 * 
		 */		
		public function showCountdown():void {
			trace("WidgetPreviewManager ::: showCountdown");
			if(countdownWin != null) {
				countdownWin.activate();
				return;
			}
			countdownWin = new WidgetWindow();
			countdownWin.open(true);
			countdownWin.width = 400;
			countdownWin.height = 100;
			countdownWin.title = "Countdown Widget";
			countdownWin.widgetUrl = WidgetsModel.WIDGET_COUNTDOWN_DIR.resolvePath("teeboard-countdown.html").url;
			countdownWin.addEventListener(CloseEvent.CLOSE, countdown_closeHandler);
		}
		
		/**
		 * 
		 * 
		 */		
		public function showGiveaways():void {
			trace("WidgetPreviewManager ::: showGiveaways");
			if(giveawaysWin != null) {
				giveawaysWin.activate();
				return;
			}
			giveawaysWin = new WidgetWindow();
			giveawaysWin.open(true);
			giveawaysWin.width = 640;
			giveawaysWin.height = 160;
			giveawaysWin.title = "GiveAways Widget";
			giveawaysWin.widgetUrl = WidgetsModel.WIDGET_GIVEAWAY_DIR.resolvePath("teeboard-giveaways.html").url;
			giveawaysWin.addEventListener(CloseEvent.CLOSE, giveaways_closeHandler);
		}
		
		/**
		 * 
		 * 
		 */		
		public function showMedia():void {
			trace("WidgetPreviewManager ::: showMedia");
			var f:File = FileManager.getInstance().currentMediaFolder;
			var html:File = f.resolvePath("teeboard-media.html");
			var url:String = html.url;
			
			if(mediaWin != null) {
				mediaWin.activate();
				return;
			}
			mediaWin = new WidgetWindow();
			mediaWin.open(true);
			mediaWin.width = 640;
			mediaWin.height = 360;
			mediaWin.title = "Media Widget";
			mediaWin.widgetUrl = url;
			mediaWin.addEventListener(CloseEvent.CLOSE, media_closeHandler);
		}
		
		/**
		 * 
		 */
		public function updateMediaPreview():void {
			var f:File = FileManager.getInstance().currentMediaFolder;
			var html:File = f.resolvePath("teeboard-media.html");
			var url:String = html.url;
			if(mediaWin != null) {
				mediaWin.widgetUrl = url;
			}
		}
		
		/**
		 * 
		 * 
		 */		
		public function showNotification():void {
			trace("WidgetPreviewManager ::: showNotification");
			if(notificationWin != null) {
				notificationWin.activate();
				return;
			}
			notificationWin = new WidgetWindow();
			notificationWin.title = "Notifications Widget";
			notificationWin.open(true);
			notificationWin.width = 1080;
			notificationWin.height = 40;
			notificationWin.title = "Notifications Widget";
			notificationWin.widgetUrl = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("obs/teeboard-notifications.html").url;
			notificationWin.addEventListener(CloseEvent.CLOSE, notification_closeHandler);
		}
		
		/**
		 * 
		 * 
		 */		
		public function showPoll():void {
			trace("WidgetPreviewManager ::: showNotification");
			if(pollWin != null) {
				pollWin.activate();
				return;
			}
			pollWin = new WidgetWindow();
			pollWin.open(true);
			pollWin.width = 640;
			pollWin.height = 160;
			pollWin.title = "Poll Widget";
			pollWin.widgetUrl = WidgetsModel.WIDGET_POLL_OBS_DIR.resolvePath("teeboard-poll.html").url;
			pollWin.addEventListener(CloseEvent.CLOSE, poll_closeHandler);
		}
		
		/**
		 * Sets the height of the poll preview window based on the number of poll options.
		 */ 
		public function setPollSize(value:Number):void {
			trace("WidgetsPreviewManager ::: setPollSize");
			if(!pollWin){
				return;
			}
			// 10px border + 50px question + (num options * 50px)
			var h:Number = 10 + 50 + (value*50);
			trace("    - height: ", h);
			pollWin.height = h;
			pollWin.activate();
		}
		
		/**
		 * 
		 * 
		 */		
		public function showSpectrum():void {
			trace("WidgetPreviewManager ::: showSpectrum");
			if(spectrumWin) {
				spectrumWin.activate();
				return;
			}
			spectrumWin = new WidgetWindow();
			spectrumWin.open(true);
			spectrumWin.width = 512;
			spectrumWin.height = 256;
			spectrumWin.title = "Spectrum Widget";
			spectrumWin.widgetUrl = WidgetsModel.WIDGET_SPECTRUM_DIR.resolvePath("teeboard-spectrum.html").url;
			spectrumWin.addEventListener(CloseEvent.CLOSE, spectrum_closeHandler);
		}
		
		/**
		 * Not implemented / do not use. 
		 */
		public function showTickerTape():void {
			trace("WidgetPreviewManager ::: showTickerTape");
			if(tickertapeWin) {
				tickertapeWin.activate();
				return;
			}
			tickertapeWin = new WidgetWindow();
			tickertapeWin.open(true);
			tickertapeWin.width = 640;
			tickertapeWin.height = 100;
			tickertapeWin.title = "TickerTape Widget";
			tickertapeWin.widgetUrl = WidgetsModel.WIDGET_TICKERTAPE_DIR.resolvePath("teeboard-tickertape.html").url;
			tickertapeWin.addEventListener(CloseEvent.CLOSE, tickertape_closeHandler);
		}
		
	}
	
}