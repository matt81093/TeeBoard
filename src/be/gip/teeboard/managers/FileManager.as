package be.gip.teeboard.managers {
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import mx.utils.ObjectUtil;
	
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class FileManager extends EventDispatcher {
		
		public static const APP_DATASTORE:File = File.applicationDirectory.resolvePath("db/teeboard.db");
		public static const STORAGE_DATASTORE:File = File.applicationStorageDirectory.resolvePath("db/teeboard.db");
		
		public static const APP_WIDGETS_DIR:File = File.applicationDirectory.resolvePath("widgets");
		
		public static const WIDGET_MEDIA_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/media/");
		
		public static const WIDGET_TICKERTAPE_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/tickertape/");
		
		public static const CHAT_CSS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/chat/");
		
		private static var INSTANCE:FileManager;
		
		/**
		 * A list of <code>File</code> objects (Media widget directories).
		 */ 
		public var mediaFolders:Array;
		
		public var currentMediaFolder:File;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function FileManager() {
			trace("FileManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one ApplicationFileManager instance allowed. " +
					"To access the Singleton instance, use ApplicationFileManager.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():FileManager {
			if(!INSTANCE) INSTANCE = new FileManager();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("FileManager ::: init");
			// create chat css folder
			if(!CHAT_CSS_DIR.exists) {
				var f:File = File.applicationDirectory.resolvePath("chat");
				trace("    - app chat dir: ", f);
			}
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function getSelectedMediaIndex():int {
			trace("FileManager ::: getSelectedMediaIndex");
			var idx:int = -1;
			if(currentMediaFolder != null) {
				idx = getMediaIndex(currentMediaFolder);
			}
			return idx;
		}
		
		/**
		 * 
		 */ 
		public function getMediaIndex(value:File):int {
			var idx:int = -1;
			var len:uint = mediaFolders.length;
			for(var i:uint=0; i<len; i++) {
				if(mediaFolders[i].name == value.name) {
					idx = i;
					break;
				}
			}
			return idx;
		}
	}
	
}