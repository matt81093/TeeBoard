package be.gip.teeboard.managers {
	
	import be.gip.teeboard.dto.SocketDTO;
	import be.gip.teeboard.dto.chat.IChatDTO;
	import be.gip.teeboard.socket.data.SocketData;
	import be.gip.teeboard.utils.WidgetType;
	
	import com.adobe.serialization.json.JSONDecoder;
	import com.adobe.serialization.json.JSONEncoder;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.OutputProgressEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.ServerSocketConnectEvent;
	import flash.events.StatusEvent;
	import flash.net.LocalConnection;
	import flash.net.ServerSocket;
	import flash.net.Socket;
	
	import mx.events.PropertyChangeEvent;
	import mx.utils.ObjectUtil;
	
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	public class WidgetsConnectionManager extends EventDispatcher {
		
		private static var INSTANCE:WidgetsConnectionManager;
		
		private static const SOCKET_ADDRESS:String = "127.0.0.1";
		private static const SOCKET_PORT:int = 9200;
		
		private var server:ServerSocket;
		private var widgetSocket:Socket;
		
		private var widgetSockets:Array;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function WidgetsConnectionManager() {
			trace("WidgetsConnectionManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one WidgetsConnectionManager instance allowed. " +
					"To access the Singleton instance, WidgetsConnectionManager.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():WidgetsConnectionManager {
			if(!INSTANCE) INSTANCE = new WidgetsConnectionManager();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function init():void {
			trace("WidgetsConnectionManager ::: init");
			// create socket and listen for incoming connections
			server = new ServerSocket();
			server.addEventListener(ServerSocketConnectEvent.CONNECT, server_connectHandler);
			server.bind(SOCKET_PORT, SOCKET_ADDRESS);
			server.listen();
			
			widgetSockets = new Array();
			
		}
		
		//------------------------------------
		// setSocketType()
		//------------------------------------
		private function setSocketType(socket:Socket, type:String):void {
			trace("WidgetsConnectionManager ::: setSocketType");
			trace("    - widget socket type: ", type);
			var dto:SocketDTO;
			var i:uint = 0;
			var len:uint = widgetSockets.length;
			for(i=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.socket == socket) {
					trace("    - found socket: ", "index = ",  i);
					// set socket/widget type
					dto.type = type;
					break;
				}
			}
		}
		
		//------------------------------------
		// conn_statusHandler()
		//------------------------------------
		private function conn_statusHandler(event:StatusEvent):void {
			trace("WidgetsConnectionManager ::: conn_statusHandler");
			trace(event.toString());
			trace("    - code: ", event.code);
			trace("    - level: ", event.level);
		}
		
		//------------------------------------
		// conn_securityErrorHandler()
		//------------------------------------
		private function conn_securityErrorHandler(event:SecurityErrorEvent):void {
			trace("WidgetsConnectionManager ::: conn_securityErrorHandler");
			trace("    - text: ", event.text);
		}
		
		//------------------------------------
		// server_connectHandler()
		//------------------------------------
		private function server_connectHandler(event:ServerSocketConnectEvent):void {
			trace("WidgetsConnectionManager ::: server_connectHandler");
			// store connected socket reference
			var dto:SocketDTO = new SocketDTO();
			dto.socket = event.socket;
			var ws:Socket = event.socket;
			// add listeners to connected socket
			ws.addEventListener(ProgressEvent.SOCKET_DATA, widgetSocket_dataHandler);
			ws.addEventListener(Event.CONNECT, widgetSocket_connectHandler);
			ws.addEventListener(Event.CLOSE, widgetSocket_closeHandler);
			ws.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, widgetSocket_progressHandler);
			ws.addEventListener(IOErrorEvent.IO_ERROR, widgetSocket_ioErrorHandler);
			ws.addEventListener(SecurityErrorEvent.SECURITY_ERROR, widgetSocket_securityErrorHandler);
			// store reference to socket
			widgetSockets.push(dto);
		}
		
		//------------------------------------
		// widgetSocket_connectHandler()
		//------------------------------------
		private function widgetSocket_connectHandler(event:Event):void {
			trace("WidgetsConnectionManager ::: widgetSocket_connectHandler");
			
		}
		
		//------------------------------------
		// widgetSocket_dataHandler()
		//------------------------------------
		private function widgetSocket_dataHandler(event:ProgressEvent):void {
			trace("WidgetsConnectionManager ::: widgetSocket_dataHandler");
			// var buffer:ByteArray = new ByteArray();
			// widgetSocket.readBytes(buffer, 0, widgetSocket.bytesAvailable);
			//trace("    - received: " + buffer.toString());
			var s:Socket = event.currentTarget as Socket;
			var p:String;
			
			var msg:String = s.readUTFBytes(s.bytesAvailable);
			trace("    - msg: ", msg);
			if(msg.toString().indexOf("policy-file-request") != -1) {
				trace("    - sending policy file");
				p = getPolicy();
				trace(p);
				s.writeUTFBytes(p);
				s.writeByte(0);
				s.flush();
			}
			
			// each widget sends its "type" as soon as it connects.
			// We need to find the connected socket and assign the correct type, 
			// so we can then send messages to specific widget type sockets only.
			
			if(msg.indexOf(WidgetType.ALL) == 0) {
				// 3RD PARTY WIDGET
				setSocketType(s, WidgetType.ALL);
			}else if(msg.indexOf(WidgetType.CHAT) == 0) {
				// CHAT WIDGET
				setSocketType(s, WidgetType.CHAT);
				// dispatch event that a new chat widget has connected which needs chat settings
				dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, null, "chatSockets"));
			}else if(msg.indexOf(WidgetType.CLOCK) == 0) {
				// CLOCK WIDGET
				setSocketType(s, WidgetType.CLOCK);
			}else if(msg.indexOf(WidgetType.COUNTDOWN) == 0) {
				// COUNTDOWN WIDGET
				setSocketType(s, WidgetType.COUNTDOWN);
			}else if(msg.indexOf(WidgetType.MEDIA) == 0) {
				// MEDIA WIDGET
				setSocketType(s, WidgetType.MEDIA);
			}else if(msg.indexOf(WidgetType.MESSAGE) == 0) {
				// MESSAGE WIDGET
				setSocketType(s, WidgetType.MESSAGE);
			}else if(msg.indexOf(WidgetType.NOTIFICATION) == 0) {
				// NOTIFICATION WIDGET
				setSocketType(s, WidgetType.NOTIFICATION);
			}else if(msg.indexOf(WidgetType.POLL) == 0) {
				// POLL WIDGET
				setSocketType(s, WidgetType.POLL);
			}else if(msg.indexOf(WidgetType.SPECTRUM) == 0) {
				// SPECTRUM WIDGET
				setSocketType(s, WidgetType.SPECTRUM);
			}else if(msg.indexOf(WidgetType.TICKERTAPE) == 0) {
				// TICKERTAPE WIDGET
				setSocketType(s, WidgetType.TICKERTAPE);
			}else if (msg.indexOf(WidgetType.TWOBBLER) == 0) {
				// TWOBBLER WIDGET
				setSocketType(s, WidgetType.TWOBBLER);
			}
			
		}
		
		//------------------------------------
		// widgetSocket_progressHandler()
		//------------------------------------
		private function widgetSocket_progressHandler(event:OutputProgressEvent):void {
			trace("WidgetsConnectionManager ::: widgetSocket_progressHandler");
			
		}
		
		//------------------------------------
		// widgetSocket_ioErrorHandler()
		//------------------------------------
		private function widgetSocket_ioErrorHandler(event:IOErrorEvent):void {
			trace("WidgetsConnectionManager ::: widgetSocket_ioErrorHandler");
			
		}
		
		//------------------------------------
		// widgetSocket_securityErrorHandler()
		//------------------------------------
		private function widgetSocket_securityErrorHandler(event:SecurityErrorEvent):void {
			trace("WidgetsConnectionManager ::: widgetSocket_securityErrorHandler");
			
		}
		
		//------------------------------------
		// widgetSocket_closeHandler()
		//------------------------------------
		private function widgetSocket_closeHandler(event:Event):void {
			trace("WidgetsConnectionManager ::: widgetSocket_closeHandler");
			trace("    - currentTarget: ", event.currentTarget);
			// find the now closed socket and remove it
			var s:Socket = event.currentTarget as Socket;
			var len:uint = widgetSockets.length;
			var i:uint;
			var ws:Socket;
			var item:SocketDTO;
			// chat sockets
			for(i=0; i<len; i++) {
				item = widgetSockets[i] as SocketDTO;
				ws = item.socket;
				if(ws == s) {
					trace("    - removing widget socket from list: ", i);
					ws.removeEventListener(Event.CLOSE, widgetSocket_closeHandler);
					ws.removeEventListener(Event.CONNECT, widgetSocket_connectHandler);
					ws.removeEventListener(IOErrorEvent.IO_ERROR, widgetSocket_ioErrorHandler);
					ws.removeEventListener(OutputProgressEvent.OUTPUT_PROGRESS, widgetSocket_progressHandler);
					ws.removeEventListener(ProgressEvent.SOCKET_DATA, widgetSocket_dataHandler);
					ws.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, widgetSocket_securityErrorHandler);
					widgetSockets.splice(i, 1);
					break;
				}
			}
			trace("    - sockets left: ", widgetSockets.length);
		}
		
		//------------------------------------
		// getPolicy()
		//------------------------------------
		private function getPolicy():String {
			trace("WidgetsConnectionManager ::: getPolicy");
			var xml:String = '<?xml version="1.0" encoding="UTF-8"?>\n';
			xml += '<!DOCTYPE cross-domain-policy SYSTEM "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n';
			xml += '<cross-domain-policy>\n';
			xml += '    <allow-access-from domain="*" to-ports="*"/>\n';
			xml += '</cross-domain-policy>\n';
			return xml;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Sends data via a <code>Socket</code> to the connected chat widgets.
		 */ 
		public function sendChatMessage(value:IChatDTO):void {
			trace("WidgetsConnectionManager ::: sendChatMessage");
			// send message to connected sockets
			trace(ObjectUtil.toString(value));
			var json:String = JSON.stringify({type:WidgetType.CHAT, data:value});
			var len:uint = widgetSockets.length;
			var dto:SocketDTO;
			var s:Socket;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				s = dto.socket;
				if((s != null && s.connected) && dto.type == WidgetType.CHAT) {
					trace("    - found chat socket: ", i);
					s.writeUTFBytes(json);
					s.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the chat widget.
		 * <p>
		 * This will inform the chat widget to reload its config file, 
		 * which in turn will update the widget with the latest settings.
		 * </p>
		 */ 
		public function sendChatUpdate():void {
			trace("WidgetsConnectionManager ::: sendChatUpdate");
			var json:String = JSON.stringify({type:WidgetType.CHAT, data:{type:"updateConfig"}});
			trace("    - json: ", json);
			
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				//if(dto.type == WidgetType.CHAT || dto.type == WidgetType.ALL) {
				if(dto.type == WidgetType.CHAT) {
					trace("    - found chat socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the clock widget.
		 * <p>
		 * This will inform the clock widget to reload its config file, 
		 * which in turn will update the clock with the latest settings.
		 * </p>
		 */ 
		public function sendClockUpdate():void {
			trace("WidgetsConnectionManager ::: sendClockUpdate");
			var json:String = JSON.stringify({type:WidgetType.CLOCK, data:{type:"updateConfig"}});
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.CLOCK) {
					trace("    - found clock socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the countdown widget.
		 * <p>
		 * This will inform the countdown widget to reload its config file, 
		 * which in turn will update the countdown with the latest settings.
		 * </p>
		 */ 
		public function sendCountdownUpdate():void {
			trace("WidgetsConnectionManager ::: sendCountdownUpdate");
			var json:String = JSON.stringify({type:WidgetType.COUNTDOWN, data:{type:"updateConfig"}});
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.COUNTDOWN) {
					trace("    - found countdown socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the media widget(s).
		 * <p>
		 * This will inform the media widget to reload its config file, 
		 * which in turn will update the media with the latest settings.
		 * </p>
		 */ 
		public function sendMediaUpdate(value:String):void {
			trace("WidgetsConnectionManager ::: sendMediaUpdate");
			var json:String = JSON.stringify({type:WidgetType.MEDIA, data:{type:"updateConfig", mediaName:value}});
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.MEDIA) {
					trace("    - found media socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * 
		 */ 
		public function sendMediaControl(mediaName:String, action:String):void {
			trace("WidgetsConnectionManager ::: sendMediaControl");
			var json:String = JSON.stringify({type:WidgetType.MEDIA, data:{type:"mediaControl", mediaName:mediaName, action:action}});
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.MEDIA) {
					trace("    - found media socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the notification widget(s).
		 * 
		 */ 
		public function sendNotification(value:Object):void {
			trace("WidgetsConnectionManager ::: sendNotification");
			//var sd:Object = {type:"NOTIFICATION", data:value};
			var sd:SocketData = new SocketData();
			sd.type = WidgetType.NOTIFICATION;
			sd.data = value;
			
			var json:String = JSON.stringify(sd);
			trace("    - json: ", json);
			
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i];
				if(dto.type == WidgetType.NOTIFICATION || dto.type == WidgetType.ALL) {
					trace("    - found notification socket: ", i);
					dto.socket.writeUTFBytes(json);
					//dto.socket.writeObject({type:type, message:text, color:color, direction:direction});
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the message widget(s).
		 */ 
		public function sendNotificationMessage(value:Object):void {
			trace("WidgetsConnectionManager ::: sendNotificationMessage");
			var sd:SocketData = new SocketData();
			sd.type = WidgetType.MESSAGE;
			sd.data = value;
			
			var json:String = JSON.stringify(sd);
			trace("    - json: ", json);
			
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i];
				if(dto.type == WidgetType.MESSAGE || dto.type == WidgetType.ALL) {
					trace("    - found message socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the poll widget.
		 * <p>
		 * This will inform the poll widget to reload its config file, 
		 * which in turn will update the poll with the latest settings.
		 * </p>
		 */ 
		public function sendPollUpdate():void {
			trace("WidgetsConnectionManager ::: sendPollUpdate");
			var json:String = JSON.stringify({type:WidgetType.POLL, data:{type:"updateConfig"}});
			trace("    - json: ", json);
			
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.POLL || dto.type == WidgetType.ALL) {
					trace("    - found poll socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the poll widget.
		 */ 
		public function sendPollVotes(value:Object):void {
			trace("WidgetsConnectionManager ::: sendPollVotes");
			var data:Object = {type:WidgetType.POLL, data:value};
			var json:String = JSON.stringify(data);
			trace("    - json: ", json);
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.POLL || dto.type == WidgetType.ALL) {
					trace("    - found poll socket: ", i);
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the countdown widget.
		 * <p>
		 * This will inform the spectrum widget to reload its config file, 
		 * which in turn will update the spectrum with the latest settings.
		 * </p>
		 */ 
		public function sendSpectrumUpdate():void {
			trace("WidgetsConnectionManager ::: sendSpectrumUpdate");
			var json:String = JSON.stringify({type:WidgetType.SPECTRUM, data:{type:"updateConfig"}});
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.SPECTRUM) {
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via <code>Socket</code> to the tickertape widget.
		 * <p>
		 * This will inform the tickertape widget to reload its config file, 
		 * which in turn will update the tickertape with the latest settings.
		 * </p>
		 */ 
		public function sendTickertapeUpdate():void {
			trace("WidgetsConnectionManager ::: sendTickertapeUpdate");
			var json:String = JSON.stringify({type:WidgetType.TICKERTAPE, data:{type:"updateConfig"}});
			var dto:SocketDTO;
			var len:uint = widgetSockets.length;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				if(dto.type == WidgetType.TICKERTAPE) {
					trace("    - sending data to tickertape socket");
					dto.socket.writeUTFBytes(json);
					dto.socket.flush();
				}
			}
		}
		
		/**
		 * Sends data via a <code>Socket</code> to the connected Twobbler (Now Playing) widgets.
		 */ 
		public function sendTwobblerUpdate(value:String):void {
			trace("WidgetsConnectionManager ::: sendTwobblerUpdate");
			// send message to connected sockets
			trace("    - track: ", value);
			var json:String = JSON.stringify({type:WidgetType.TWOBBLER, data:{type:"updateTrack", track:value}});
			trace("    - json: ", json);
			var len:uint = widgetSockets.length;
			var dto:SocketDTO;
			var s:Socket;
			for(var i:uint=0; i<len; i++) {
				dto = widgetSockets[i] as SocketDTO;
				s = dto.socket;
				if((s != null && s.connected) && (dto.type == WidgetType.TWOBBLER || dto.type == WidgetType.ALL)) {
					trace("    - sending data to twobbler socket");
					s.writeUTFBytes(json);
					s.flush();
				}
			}
		}
		
	}
	
}