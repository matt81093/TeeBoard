package be.gip.teeboard.managers {
	
	import be.gip.teeboard.core.VideoDownloadWindow;
	import be.gip.teeboard.events.DownloadEvent;
	import be.gip.teeboard.utils.VideoDownloadStatus;
	import be.gip.teeboard.vo.IVideoVO;
	import be.gip.teeboard.vo.VideoDownloadVO;
	import be.gip.teeboard.vo.vod.ChunkVO;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	/**
	 * Singleton.
	 */ 
	public class VideoDownloadManager extends EventDispatcher {
		
		/**
		 * Twitch API url that will return additional info about a given vod (broadcast or highlight).
		 * The returned data contains info (url, duration) about the vod's flv chunks.
		 */ 
		public static const VIDEO_INFO_URL:String = "https://api.twitch.tv/api/videos/:videoId?as3=t";
		
		/**
		 * TMSViewer download location. VOD's are stored in this directory.
		 * <p>
		 * The default location is "TeeBoard/vods" within the user's documents directory.
		 * </p>
		 */ 
		public static const TEEBOARD_VODS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/vods");
		
		/**
		 * List of vods to download.
		 * Includes both vods that have finished downloading and vods in queue.
		 */ 
		public var downloads:ArrayCollection;
		
		/**
		 * 
		 */ 
		private static var INSTANCE:VideoDownloadManager;
		
		/**
		 * The amount of data to download before writing it to disk.
		 * <p>
		 * Because Flash / Air chokes when downloading large files 
		 * and even runs out of memory when 1Gb is reached, 
		 * data is written to disk when this threshold is reached.
		 * </p>
		 */ 
		private static var THRESHOLD:uint = 50*1024*1024;
		
		private var tmpFile:File;
		private var vodStream:FileStream;
		
		private var downloadWin:VideoDownloadWindow;
		
		//=====================================
		//
		// CONSTRUCTOR 
		//
		//=====================================
		
		public function VideoDownloadManager() {
			trace("VideoDownloadManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one VideoDownloadManager instance allowed. " +
					"To access the Singleton instance, call VideoDownloadManager.getInstance().");
			}
			init();
		}
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("VideoDownloadManager ::: init");
			downloads = new ArrayCollection();
			if(!TEEBOARD_VODS_DIR.exists) {
				TEEBOARD_VODS_DIR.createDirectory();
			}
		}
		
		//------------------------------------
		// writeVodInfo()
		//------------------------------------
		private function writeVodInfo(vod:VideoDownloadVO):void {
			trace("VideoDownloadManager ::: writeVodInfo");
			var dir:File = TEEBOARD_VODS_DIR;
			var path:String = vod.relativePath + "/info.txt";
			var txt:File = dir.resolvePath(path);
			var text:String = vod.video.title + File.lineEnding;
			var len:uint = vod.numChunks;
			var chunk:ChunkVO;
			var idx:uint;
			text += "======================================" + File.lineEnding + File.lineEnding;
			text += vod.video.url + File.lineEnding;
			text += "playing: " + vod.video.game + File.lineEnding;
			text += "recorded on: " + vod.video.recordedAt.toString() + File.lineEnding;
			text += "======================================" + File.lineEnding + File.lineEnding;
			for(var i:uint=0; i<len; i++) {
				chunk = vod.info.chunks[i];
				idx = i+1;
				text += "part" + (idx<10 ? "0"+idx : idx.toString()) + " - duration: " + chunk.duration + File.lineEnding;
				text += "    " + chunk.url + File.lineEnding;
			}
			var fs:FileStream = new FileStream();
			fs.open(txt, FileMode.WRITE);
			fs.writeUTFBytes(text);
			fs.close();
		}
		
		//------------------------------------
		// writeVodBytes()
		//------------------------------------
		private function writeVodBytes(vod:VideoDownloadVO):void {
			//trace("VideoDownloadManager ::: writeVodBytes");
			//trace("    - WRITING BYTES TO TEMP FILE");
			var vo:VideoDownloadVO = vod;
			var s:URLStream = vo.stream;
			//trace("    - bytesAvailable: ", s.bytesAvailable);
			var ba:ByteArray = new ByteArray();
			s.readBytes(ba, 0, s.bytesAvailable);
			var fs:FileStream = vodStream;
			//fs.open(tmpFile, FileMode.APPEND);
			fs.writeBytes(ba);
			//fs.close();
			//trace("    - END WRITING BYTES TO TEMP FILE");
		}
		
		//------------------------------------
		// getCurrentActiveIndex()
		//------------------------------------
		private function getCurrentActiveIndex():int {
			trace("VideoDownloadManager ::: getCurrentActiveIndex");
			var idx:int = -1;
			var len:int = downloads.length;
			var vo:VideoDownloadVO;
			for(var i:int=0; i<len; i++) {
				vo = downloads.getItemAt(i) as VideoDownloadVO;
				if(vo.isDownloading) { 
					idx = i; 
					break;
				}
			}
			return idx;
		}
		
		//------------------------------------
		// startDownload()
		//------------------------------------
		private function startDownload():void {
			trace("VideoDownloadManager ::: startDownload");
			var len:int = downloads.length;
			var vo:VideoDownloadVO;
			var idx:int = getCurrentActiveIndex();
			trace("    - active download index: ", idx);
			// if a download is in progress, do nothing
			if(idx != -1) return;
			// find first queued item and start downloading
			for(var i:int=0; i<len; i++) {
				vo = downloads.getItemAt(i) as VideoDownloadVO;
				trace("    - idx: ", i);
				trace("    - vo status: ", vo.status);
				// start the first vod which status is not completed and/or in queue
				if(vo.status == VideoDownloadStatus.QUEUED) {
					vo.loadChunks();
					break;
				}
			}
		}
		
		//------------------------------------
		// info_completeHandler()
		//------------------------------------
		private function vod_infoCompleteHandler(event:Event):void {
			trace("VideoDownloadManager ::: info_completeHandler");
			var vo:VideoDownloadVO = event.currentTarget as VideoDownloadVO;
			trace("    - numChunks: ", vo.numChunks);
			var idx:int = getCurrentActiveIndex();
			// if there's no active vod, 
			// start downloading the current one
			if(idx == -1) {
				vo.loadChunks();
			}
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "downloads";
			dispatchEvent(propEvent);
		}
		
		//------------------------------------
		// vod_chunkStartHandler()
		//------------------------------------
		private function vod_chunkStartHandler(event:DownloadEvent):void {
			trace("VideoDownloadManager ::: vod_chunkStartHandler");
			// when a new chunk starts downloading
			// create a new temp file to write to
			tmpFile = File.createTempFile();
			trace("    - tmp file: ", tmpFile.nativePath);
			// open the file for writing
			vodStream = new FileStream();
			vodStream.open(tmpFile, FileMode.APPEND);
		}
		
		//------------------------------------
		// vod_chunkProgressHandler()
		//------------------------------------
		private function vod_chunkProgressHandler(event:DownloadEvent):void {
			//trace("VideoDownloadManager ::: vod_chunkProgressHandler");
			var vo:VideoDownloadVO = event.currentTarget as VideoDownloadVO;
			var s:URLStream = vo.stream;
			var bAvailable:uint = s.bytesAvailable;
			if(bAvailable > THRESHOLD) {
				trace("    - megabytes: ", (bAvailable / 1024 / 1024));
			}
			writeVodBytes(vo);
		}
		
		//------------------------------------
		// vod_chunkCompleteHandler()
		//------------------------------------
		private function vod_chunkCompleteHandler(event:Event):void {
			trace("VideoDownloadManager ::: vod_chunkCompleteHandler");
			var vo:VideoDownloadVO = event.currentTarget as VideoDownloadVO;
			var s:URLStream = vo.stream;
			var idx:int = vo.currentChunkIndex + 1;
			// try mp4
			var path:String = vo.relativePath + "/part" + (idx < 10 ? "0"+idx : idx.toString()) + ".flv";
			//var path:String = vo.relativePath + "/part" + (idx < 10 ? "0"+idx : idx.toString()) + ".mp4";
			var flv:File = TEEBOARD_VODS_DIR.resolvePath(path);
			trace("    - flv: ", flv.nativePath);
			// if there's still bytes to be written to file, do so
			if(s.bytesAvailable != 0) {
				writeVodBytes(vo);
			}
			// close the filestream
			vodStream.close();
			// now copy temp file to final location
			trace("    - MOVING FILE TO FINAL LOCATION");
			tmpFile.moveTo(flv, true);
			//trace("    - flv size: ", flv.size);
			//trace("    - mb size: ", (flv.size / 1024) / 1024);
		}
		
		//------------------------------------
		// vod_downloadCompleteHandler()
		//------------------------------------
		private function vod_downloadEndHandler(event:Event):void {
			trace("VideoDownloadManager ::: vod_downloadCompleteHandler");
			// save vod info to text file
			var vod:VideoDownloadVO = event.currentTarget as VideoDownloadVO;
			writeVodInfo(vod);
			// when vod has finished downloading, move to the next
			startDownload();
		}
		
		//------------------------------------
		// vod_downloadCanceledHandler()
		//------------------------------------
		private function vod_downloadCanceledHandler(event:Event):void {
			trace("VideoDownloadManager ::: vod_downloadCanceledHandler");
		}
		
		//------------------------------------
		// vodExists()
		//------------------------------------
		private function vodExists(vod:IVideoVO):Boolean {
			trace("VideoDownloadManager ::: vodExists");
			var exists:Boolean = false;
			var len:int = downloads.length;
			var vo:VideoDownloadVO;
			for(var i:int=0; i<len; i++) {
				vo = downloads.getItemAt(i) as VideoDownloadVO;
				if(vo.video.videoId == vod.videoId) {
					exists = true;
					break;
				}
			}
			return exists;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Singleton access point.
		 */ 
		public static function getInstance():VideoDownloadManager {
			if(INSTANCE == null) INSTANCE = new VideoDownloadManager();
			return INSTANCE;
		}
		
		/**
		 * 
		 */ 
		public function addItem(vod:IVideoVO):void {
			trace("VideoDownloadManager ::: addItem");
			if(vodExists(vod)) return;
			var vo:VideoDownloadVO = new VideoDownloadVO(vod);
			var videoId:String = vod.videoId;
			var apiUrl:String = VIDEO_INFO_URL.replace(":videoId", videoId);
			vo.addEventListener(DownloadEvent.INFO_COMPLETE, vod_infoCompleteHandler);
			vo.addEventListener(DownloadEvent.CHUNK_START, vod_chunkStartHandler);
			vo.addEventListener(DownloadEvent.CHUNK_PROGRESS, vod_chunkProgressHandler);
			vo.addEventListener(DownloadEvent.CHUNK_COMPLETE, vod_chunkCompleteHandler);
			vo.addEventListener(DownloadEvent.DOWNLOAD_END, vod_downloadEndHandler);
			vo.addEventListener(DownloadEvent.DOWNLOAD_CANCELED, vod_downloadCanceledHandler);
			vo.loadInfo(apiUrl);
			downloads.addItem(vo);
			showDownloadWindow();
		}
		
		/**
		 * 
		 */ 
		public function removeItem(vod:VideoDownloadVO):void {
			trace("VideoDownloadManager ::: removeItem");
			var len:int = downloads.length;
			var vo:VideoDownloadVO;
			var item:VideoDownloadVO;
			var dir:File;
			for(var i:int=0; i<len; i++) {
				vo = downloads.getItemAt(i) as VideoDownloadVO;
				if(vo.video.videoId == vod.video.videoId) {
					item = downloads.removeItemAt(i) as VideoDownloadVO;
					break;
				}
			}
			if(item) {
				// remove all listeners
				item.removeEventListener(DownloadEvent.INFO_COMPLETE, vod_infoCompleteHandler);
				item.removeEventListener(DownloadEvent.CHUNK_START, vod_chunkStartHandler);
				item.removeEventListener(DownloadEvent.CHUNK_COMPLETE, vod_chunkCompleteHandler);
				item.removeEventListener(DownloadEvent.DOWNLOAD_END, vod_downloadEndHandler);
				item.removeEventListener(DownloadEvent.DOWNLOAD_CANCELED, vod_downloadCanceledHandler);
				// check if the removed item is downloading
				trace("    - is downloading: ", item.isDownloading);
				if(item.isDownloading) {
					vodStream.close();
					tmpFile.deleteFile();
					// stop downloading
					item.stopChunks();
				}
				// remove chunks from disk - only if not all chunks have already downloaded
				if(item.status != VideoDownloadStatus.COMPLETED && item.relativePath != "") {
					dir = TEEBOARD_VODS_DIR.resolvePath(item.relativePath);
					trace("    - removing dir: ", dir.nativePath);
					if(dir.isDirectory && dir.exists) {
						try {
							dir.deleteDirectory(true);
						}catch(e:Error) {
							trace("    - unable to remove dir.. dir content might be in use");
						}
					}
				}
				// if nothing is currently downloading, start one
				startDownload();
			}
			
		}
		
		/**
		 * 
		 */ 
		public function openVodDirectory(vo:VideoDownloadVO):void {
			trace("VideoDownloadManager ::: openVodDirectory");
			if(vo.status != VideoDownloadStatus.COMPLETED) return;
			var f:File = TEEBOARD_VODS_DIR.resolvePath(vo.relativePath);
			trace("    - path: ", f.nativePath);
			if(f.exists) f.openWithDefaultApplication();
		}
		
		/**
		 * Opens the download manager window if not already open.
		 * 
		 */ 
		public function showDownloadWindow():void {
			trace("VideoDownloadManager ::: showDownloadWindow");
			if(downloadWin == null) {
				trace("    - creating VideoDownloadWindow");
				downloadWin = new VideoDownloadWindow();
				downloadWin.title = "TeeBoard Download Manager";
				downloadWin.open();
			}
			downloadWin.restore();
			downloadWin.activate();
		}
		
		/**
		 * Opens the vod download folder.
		 */ 
		public function openDownloadDirectory():void {
			trace("VideoDownloadManager ::: openDownloadDirectory");
			TEEBOARD_VODS_DIR.openWithDefaultApplication();
		}
		
	}
	
}
