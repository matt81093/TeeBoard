package be.gip.teeboard.managers {
	
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.core.NotificationWindow;
	import be.gip.teeboard.dto.widgets.NotificationDTO;
	import be.gip.teeboard.events.NotificationEvent;
	import be.gip.teeboard.vo.notifications.AbstractNotification;
	import be.gip.teeboard.vo.notifications.FollowerVO;
	
	import flash.display.Screen;
	import flash.events.EventDispatcher;
	import flash.events.NativeWindowBoundsEvent;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.collections.IList;
	import mx.events.FlexEvent;
	import mx.events.FlexNativeWindowBoundsEvent;
	import mx.utils.ObjectUtil;
	
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	public class NotificationsManager extends EventDispatcher {
		
		/**
		 * Notification window location type: custom
		 */ 
		public static const CUSTOM:String = "custom";
		
		/**
		 * Notification window location type: topLeft
		 */ 
		public static const TOP_LEFT:String = "topLeft";
		
		/**
		 * Notification window location type: topRight
		 */ 
		public static const TOP_RIGHT:String = "topRight";
		
		/**
		 * Notification window location type: botomLeft
		 */ 
		public static const BOTTOM_LEFT:String = "bottomLeft";
		
		/**
		 * Notification window location type: bottomRight
		 * <p>This is the default location</p>
		 */ 
		public static const BOTTOM_RIGHT:String = "bottomRight";
		
		/**
		* A list of all new notifications (followers, subs, donations), 
		* which acts as a queue for them to be displayed.
		*/ 
		public var notificationQueue:ArrayCollection;
		
		/**
		 * 
		 */ 
		public var isQRunning:Boolean = false;
		
		private static var INSTANCE:NotificationsManager;
		
		//-----------------------------------------------
		//
		// PRIVATE PROPERTIES
		//
		//-----------------------------------------------
		
		/**
		 * This shared object is the same as the ApplicationModel shared object, 
		 * so be careful when defining properties as the may be duplicates.
		 */ 
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/settings", "/");
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		/**
		 * The stored notification window bounds (size / position)
		 */ 
		private var _notificationBounds:Rectangle;
		
		/**
		 * 
		 */ 
		private var _notificationLocation:String = BOTTOM_RIGHT;
		
		/**
		 * 
		 */ 
		private var _notificationOnTop:Boolean = false;
		
		/**
		 * 
		 */ 
		private var _notificationWin:NotificationWindow;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		
		public function NotificationsManager() {
			trace("NotificationManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one NotificationManager instance allowed. " +
					"To access the Singleton instance, NotificationManager.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():NotificationsManager {
			if(!INSTANCE) INSTANCE = new NotificationsManager();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function init():void {
			trace("NotificationManager ::: init");
			var soBounds:Object;
			var winBounds:Rectangle;
			if(so.data.hasOwnProperty("notificationBounds") && so.data.notificationBounds != null) {
				soBounds = so.data.notificationBounds;
				//trace("    - stored notification bounds: ", soBounds);
				//trace("=============================================");
				winBounds = new Rectangle(soBounds.x, soBounds.y, soBounds.width, soBounds.height);
				// check if bounds are valid
				var screens:Array = Screen.getScreensForRectangle(winBounds);
				//trace(ObjectUtil.toString(screens));
				if(screens.length != 0) {
					//bounds are valid, use them
					_notificationBounds = winBounds;
				}
			}
			
			// get the stored notification location if exists
			// if none exists, a default is already set
			if(so.data.hasOwnProperty("notificationLocation")) {
				_notificationLocation = so.data.notificationLocation;
			}
			
			if(so.data.hasOwnProperty("notificationOnTop")) {
				_notificationOnTop = so.data.notificationOnTop;
			}
			
			//_donations = new ArrayCollection();
			
			// storage for notifications (queue  up all notification types);
			notificationQueue = new ArrayCollection();
			
		}
		
		//------------------------------------
		// window_nativeMoveHandler()
		//------------------------------------
		private function window_nativeMoveHandler(event:NativeWindowBoundsEvent):void {
			//trace("NotificationManager ::: window_nativeMoveHandler");
			//trace("    - before bounds: ", event.beforeBounds);
			//trace("    - after bounds: ", event.afterBounds);
			_notificationBounds = event.afterBounds;
			so.data.notificationBounds = event.afterBounds;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */
		//------------------------------------
		// initialize()
		//------------------------------------
		public function initialize():void {
			trace("NotificationManager ::: initialize");
			_notificationWin = new NotificationWindow();
			_notificationWin.addEventListener(NativeWindowBoundsEvent.MOVING, window_nativeMoveHandler);
			_notificationWin.open();
			_notificationWin.windowLocation = _notificationLocation;
			_notificationWin.onTop = _notificationOnTop;
			_notificationWin.windowBounds = _notificationBounds;
		}
		
		/**
		 * 
		 */ 
		public function addNotification(value:NotificationDTO):void {
			trace("NotificationManager ::: addNotification");
			//_donations.addItem(value);
			_notificationWin.addNotification(value);
		}
		
		/**
		 * 
		 */ 
		public function testNotifications():void {
			trace("NotificationManager ::: testNotifications");
			//_notificationWin.testNotifications();
		}
		
		/**
		 * Adds a notification (<code>AbstractNotification</code>) object to the notification queue.
		 * 
		 */ 
		public function addItemToQueue(value:AbstractNotification):void {
			trace("NotificationManager ::: addToQueue");
			notificationQueue.addItem(value);
			trace(ObjectUtil.toString(notificationQueue));
			
			if(!isQRunning) {
				dispatcher.dispatchEvent(new NotificationEvent(NotificationEvent.NEW_NOTIFICATIONS));
			}
		}
		
		/**
		 * Adds a list of notification (<code>AbstractNotification</code>) objects to the notification queue.
		 * 
		 */ 
		public function addListToQueue(list:IList):void {
			trace("NotificationManager ::: addToQueue");
			notificationQueue.addAll(list);
			trace(ObjectUtil.toString(notificationQueue));
			
			if(!isQRunning) {
				dispatcher.dispatchEvent(new NotificationEvent(NotificationEvent.NEW_NOTIFICATIONS));
			}
		}
		
		//=====================================
		//
		// IMPLICIT GETTER /SETTERS
		//
		//=====================================
		
		/**
		 * The location of the notification window on the user's screen.
		 * <p>The default value is "<code>bottomRight</code>", which is near the system tray.</p> 
		 * <p>Valid values are:
		 * <ul>
		 * 	<li>manual - <code>ApplicationNativeMenu.MANUAL</code></li>
		 * 	<li>topLeft - <code>ApplicationNativeMenu.TOP_LEFT</code></li>
		 * 	<li>topRight - <code>ApplicationNativeMenu.TOP_RIGHT</code></li>
		 * 	<li>bottomLeft - <code>ApplicationNativeMenu.BOTTOM_LEFT</code></li>
		 * 	<li>bottomRight - <code>ApplicationNativeMenu.BOTTOM_RIGHT</code></li>
		 * </ul>
		 * </p>
		 * 
		 */
		public function get notificationLocation():String {
			return _notificationLocation;
		}
		public function set notificationLocation(value:String):void {
			if(value == _notificationLocation) return;
			_notificationLocation = value;
			_notificationWin.windowLocation = value;
			so.data.notificationLocation = value;
			so.flush();
		}
		
		public function get notificationOnTop():Boolean {
			return _notificationOnTop;
		}
		public function set notificationOnTop(value:Boolean):void {
			if(value == _notificationOnTop) return;
			_notificationOnTop = value;
			_notificationWin.onTop = value;
			so.data.notificationOnTop = value;
			so.flush();
		}
		
	}
	
}