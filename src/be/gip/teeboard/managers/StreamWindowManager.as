package be.gip.teeboard.managers {
	
	import be.gip.teeboard.core.StreamWindow;
	import be.gip.teeboard.vo.ChannelVO;
	import be.gip.teeboard.vo.IVideoVO;
	import be.gip.teeboard.vo.StreamVO;
	
	import flash.desktop.NativeApplication;
	import flash.display.NativeWindow;
	import flash.events.EventDispatcher;
	
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class StreamWindowManager extends EventDispatcher {
		
		private static const STREAM_POPOUT_URL:String = "http://www.twitch.tv/:channelname/popout";
		
		private static var INSTANCE:StreamWindowManager;
		
		private var streamWindow:StreamWindow;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function StreamWindowManager() {
			trace("StreamWindowManager ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one StreamWindowManager instance allowed. " +
					"To access the Singleton instance, StreamWindowManager.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():StreamWindowManager {
			if(!INSTANCE) INSTANCE = new StreamWindowManager();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function init():void {
			trace("StreamWindowManager ::: init");
			
		}
		
		//------------------------------------
		// playVideo()
		//------------------------------------
		private function playVideo(value:IVideoVO):void {
			trace("StreamWindowManager ::: playVideo");
			if(!streamWindow || streamWindow.closed) {
				streamWindow = new StreamWindow();
				streamWindow.open();
			}
			trace(ObjectUtil.toString(value));
			streamWindow.title = value.channel.displayName + " - " + value.title;
			streamWindow.location = value.popoutUrl;
			NativeApplication.nativeApplication.activate();
		}
		
		//------------------------------------
		// playStream()
		//------------------------------------
		private function playStream(value:StreamVO):void {
			trace("StreamWindowManager ::: playStream");
			if(!streamWindow || streamWindow.closed) {
				streamWindow = new StreamWindow();
				streamWindow.open();
			}
			trace(ObjectUtil.toString(value));
			var channel:ChannelVO = value.channel;
			streamWindow.title = channel.displayName + " - " + channel.status;
			streamWindow.location = channel.popoutUrl;
			NativeApplication.nativeApplication.activate();
		}
		
		//------------------------------------
		// playChannel()
		//------------------------------------
		private function playChannel(value:ChannelVO):void {
			trace("StreamWindowManager ::: playChannel");
			if(!streamWindow || streamWindow.closed) {
				streamWindow = new StreamWindow();
				streamWindow.open();
			}
			trace(ObjectUtil.toString(value));
			streamWindow.title = value.displayName + " - " + value.status;
			streamWindow.location = value.popoutUrl;
			NativeApplication.nativeApplication.activate();
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		public function play(value:Object):void {
			trace("StreamWindowManager ::: play");
			if(value is ChannelVO) playChannel(value as ChannelVO);
			if(value is IVideoVO) playVideo(value as IVideoVO);
			if(value is StreamVO) playStream(value as StreamVO);
		}
	}
	
}
