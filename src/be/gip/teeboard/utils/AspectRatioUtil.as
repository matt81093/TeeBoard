package be.gip.teeboard.utils {
	
	/**
	 * Static utility class.
	 */ 
	public class AspectRatioUtil {
		
		public static const AR_UNKNOWN:String = "unknown";
		public static const AR_16x9:String = "16:9";
		public static const AR_16x10:String = "16:10";
		public static const AR_5x4:String = "5:4";
		public static const AR_4x3:String = "4:3";
		
		private static const ar16x9:Number = 16/9;
		private static const ar16x10:Number = 16/10;
		private static const ar5x4:Number = 5/4;
		private static const ar4x3:Number = 4/3;
		
		public static function getAspectRatio(width:Number, height:Number):String {
			var wh:Number = width/height;
			var ar:String = AR_UNKNOWN;
			switch(wh) {
				case ar16x9: {
					ar = AR_16x9;
					break;
				}
				case ar16x10: {
					ar = AR_16x10;
					break;
				}
				case ar5x4: {
					ar = AR_5x4;
					break;
				}
				case ar4x3: {
					ar = AR_4x3;
					break;
				}
			}
			return ar;
		}
		
	}
	
}
