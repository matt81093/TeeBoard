package be.gip.teeboard.utils {
	
	public class ViewType {
		
		public static const CLOCK:String = "Clock";
		
		public static const COUNT_DOWN:String = "Count Down";
		
		public static const MEDIA:String = "Media";
		
		public static const SPECTRUM:String = "Spectrum";
		
		public static const TICKER_TAPE:String = "Ticker Tape";
		
		public static const FOLLOWERS:String = "Followers";
		
		public static const SUBSCRIBERS:String = "Subscribers";
		
		public static const DONATIONS:String = "Donations";
		
		public static const MESSAGES:String = "Messages";
		
		public static const CHAT:String = "IRC Chat";
		
		public static const GIVEAWAYS:String = "Giveaways";
		
		public static const POLL:String = "Poll";
		
		public static const NOW_PLAYING:String = "Now Playing";
		
		public static const IRC_CHAT:String = "IRC Chat";
		
	}
	
}
