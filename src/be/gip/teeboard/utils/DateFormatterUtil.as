package be.gip.teeboard.utils {
	
	import mx.collections.ArrayCollection;
	import mx.formatters.DateFormatter;
	
	
	public class DateFormatterUtil {
		
		{
			DATE_FORMATS = new ArrayCollection();
			DATE_FORMATS.addItem({label:"Full", format:FULL});
			DATE_FORMATS.addItem({label:"Long", format:LONG});
			DATE_FORMATS.addItem({label:"Medium", format:MEDIUM});
			DATE_FORMATS.addItem({label:"Short", format:SHORT});
		}
		
		/**
		 * Tue June 30 2009
		 */ 
		public static var FULL:String = "EEEE MMMM D YYYY";
		
		/**
		 * June 30 2009
		 */ 
		public static var LONG:String = "MMMM DD YYYY";
		
		/**
		 * Jun 30 2009
		 */ 
		public static var MEDIUM:String = "MMM DD YYYY";
		
		/**
		 * 06/30/09
		 */ 
		public static var SHORT:String = "MM/DD/YY";
		
		private static var fmt:DateFormatter = new DateFormatter();
		
		private static var DATE_FORMATS:ArrayCollection;
		/**
		 * 
		 */ 
		public static function getDateFormats():ArrayCollection {
			return DATE_FORMATS;
		}
		
		/**
		 * Get the index of the specified format from the list of date formats.
		 * 
		 * @param format One of the static values from the <code>DateFormatterUtil</code> class.
		 * 
		 * @return If the format is found, returns the index of the format in the list of date formats, -1 if not found.
		 */ 
		public static function getDateFormatIndex(format:String):int {
			var len:int = DATE_FORMATS.length;
			var idx:int = -1;
			var f:Object;
			for(var i:int=0; i<len; i++) {
				f = DATE_FORMATS.getItemAt(i);
				if(f.format == format) {
					idx = i; 
					break;
				}
			}
			return idx;
		}
		
		/**
		 * Returns the formatted date. The default format value is <code>DateFormatterUtil.MEDIUM</code>.
		 */ 
		public static function formatDate(date:Date, format:String="MMM DD YYYY"):String {
			fmt.formatString = format;
			return fmt.format(date);
		}
		
	}
	
}