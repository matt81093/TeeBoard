package be.gip.teeboard.utils {
	
	public class WidgetType {
		
		public static const ALL:String = "teeboard-all";
		
		public static const CAM:String = "teeboard-cam";
		
		public static const CHAT:String = "teeboard-chat";
		
		public static const CLOCK:String = "teeboard-clock";
		
		public static const COUNTDOWN:String = "teeboard-countdown";
		
		public static const GIVEAWAY:String = "teeboard-giveaway";
		
		public static const MEDIA:String = "teeboard-media";
		
		public static const MESSAGE:String = "teeboard-message";
		
		public static const NOTIFICATION:String = "teeboard-notification";
		
		public static const POLL:String = "teeboard-poll";
		
		public static const SPECTRUM:String = "teeboard-spectrum";
		
		public static const TWOBBLER:String = "teeboard-twobbler";
		
		public static const TICKERTAPE:String = "teeboard-tickertape";
		
	}
	
}
