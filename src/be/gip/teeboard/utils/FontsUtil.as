package be.gip.teeboard.utils {
	
	import flash.text.Font;
	
	
	public class FontsUtil {
		
		/* static code block */
		{
			DEVICE_FONTS = Font.enumerateFonts(true);
			DEVICE_FONTS.sortOn("fontName", Array.CASEINSENSITIVE);
		}
		
		/**
		 * System fonts sorted by name using <code>Array.CASEINSENSITIVE</code>.
		 */ 
		public static var DEVICE_FONTS:Array;
		
		/**
		 * Returns system fonts sorted by name using <code>Array.CASEINSENSITIVE</code>.
		 */ 
		public static function getDeviceFonts():Array {
			trace("FontsUtil :: getDeviceFonts");
			return DEVICE_FONTS;
		}
		
		/**
		 * Returns the index of the font in the ordered device fonts list.
		 * 
		 * If the font is not found, returns -1.
		 */  
		public static function getFontIndex(fontName:String):int {
			trace("FontsUtil :: getFontIndex");
			var fnts:Array = DEVICE_FONTS;
			var i:uint;
			var idx:int = -1;
			var len:uint = fnts.length;
			var f:Font;
			for(i=0; i<len; i++) {
				f = fnts[i] as Font;
				if(f.fontName.toLowerCase() == fontName.toLowerCase()) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
	}
	
}