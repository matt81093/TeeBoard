package be.gip.teeboard.utils {
	
	/**
	 * All static class containing constants for video download status.
	 */ 
	public class VideoDownloadStatus {
		
		/**
		 * Indicates the video is in queue and hasn't started downloading yet.
		 * This is the default status for a <code>VideoDownloadVO</code>.
		 */ 
		public static const QUEUED:String = "statusQueued";
		
		/**
		 * Indicates the video is currently downloading.
		 */ 
		public static const DOWNLOADING:String = "statusDownloading";
		
		/**
		 * Indicates the video has finished downloading all chunks.
		 */ 
		public static const COMPLETED:String = "statusComplete";
		
	}
	
}
