package be.gip.teeboard.utils {
	
	public class NotificationsType {
		
		/**
		 * Notification type: donation
		 */ 
		public static const DONATION:String = "donation";
		
		/**
		 * Notification type: subscriber
		 */ 
		public static const SUBSCRIBER:String = "subscriber";
		
		/**
		 * Notification type: follower
		 */ 
		public static const FOLLOWER:String = "follower";
		
		/**
		 * Notification type: message
		 */ 
		public static const MESSAGE:String = "message";
		
		/**
		 * Notification type: test
		 */ 
		public static const TEST:String = "test";
		
	}
	
}
