package be.gip.teeboard.utils {
	
	import flash.utils.Dictionary;
	
	/**
	 * Static class.
	 */ 
	public class CurrencyUtil {
		
		{
			CURRENCY_CODES = new Dictionary();
			CURRENCY_CODES["ALL"] = "Lek";
			CURRENCY_CODES["AFN"] = "؋";
			CURRENCY_CODES["ARS"] = "$";
			CURRENCY_CODES["AWG"] = "ƒ";
			CURRENCY_CODES["AUD"] = "$";
			CURRENCY_CODES["AZN"] = "ман";
			CURRENCY_CODES["BSD"] = "$";
			CURRENCY_CODES["BBD"] = "$";
			CURRENCY_CODES["BYR"] = "p.";
			CURRENCY_CODES["BZD"] = "BZ$";
			CURRENCY_CODES["BMD"] = "$";
			CURRENCY_CODES["BOB"] = "$b";
			CURRENCY_CODES["BAM"] = "KM";
			CURRENCY_CODES["BWP"] = "P";
			CURRENCY_CODES["BGN"] = "лв";
			CURRENCY_CODES["BRL"] = "R$";
			CURRENCY_CODES["BND"] = "$";
			CURRENCY_CODES["KHR"] = "៛";
			CURRENCY_CODES["CAD"] = "$";
			CURRENCY_CODES["KYD"] = "$";
			CURRENCY_CODES["CLP"] = "$";
			CURRENCY_CODES["CNY"] = "¥";
			CURRENCY_CODES["COP"] = "$";
			CURRENCY_CODES["CRC"] = "₡";
			CURRENCY_CODES["HRK"] = "kn";
			CURRENCY_CODES["CUP"] = "₱";
			CURRENCY_CODES["CZK"] = "Kč";
			CURRENCY_CODES["DKK"] = "kr";
			CURRENCY_CODES["DOP"] = "RD$";
			CURRENCY_CODES["XCD"] = "$";
			CURRENCY_CODES["EGP"] = "£";
			CURRENCY_CODES["SVC"] = "$";
			CURRENCY_CODES["EEK"] = "kr";
			CURRENCY_CODES["EUR"] = "€";
			CURRENCY_CODES["FKP"] = "£";
			CURRENCY_CODES["FJD"] = "$";
			CURRENCY_CODES["GHC"] = "¢";
			CURRENCY_CODES["GIP"] = "£";
			CURRENCY_CODES["GTQ"] = "Q";
			CURRENCY_CODES["GGP"] = "£";
			CURRENCY_CODES["GYD"] = "$";
			CURRENCY_CODES["HNL"] = "L";
			CURRENCY_CODES["HKD"] = "$";
			CURRENCY_CODES["HUF"] = "Ft";
			CURRENCY_CODES["ISK"] = "kr";
			CURRENCY_CODES["INR"] = "Rp";
			CURRENCY_CODES["IDR"] = "Rp";
			CURRENCY_CODES["IRR"] = "﷼";
			CURRENCY_CODES["IMP"] = "£";
			CURRENCY_CODES["ILS"] = "₪";
			CURRENCY_CODES["JMD"] = "J$";
			CURRENCY_CODES["JPY"] = "¥";
			CURRENCY_CODES["JEP"] = "£";
			CURRENCY_CODES["KZT"] = "лв";
			CURRENCY_CODES["KPW"] = "₩";
			CURRENCY_CODES["KRW"] = "₩";
			CURRENCY_CODES["KGS"] = "лв";
			CURRENCY_CODES["LAK"] = "₭";
			CURRENCY_CODES["LVL"] = "Ls";
			CURRENCY_CODES["LBP"] = "£";
			CURRENCY_CODES["LRD"] = "$";
			CURRENCY_CODES["LTL"] = "Lt";
			CURRENCY_CODES["MKD"] = "ден";
			CURRENCY_CODES["MYR"] = "RM";
			CURRENCY_CODES["MUR"] = "₨";
			CURRENCY_CODES["MXN"] = "$";
			CURRENCY_CODES["MNT"] = "₮";
			CURRENCY_CODES["MZN"] = "MT";
			CURRENCY_CODES["NAD"] = "$";
			CURRENCY_CODES["NPR"] = "₨";
			CURRENCY_CODES["ANG"] = "ƒ";
			CURRENCY_CODES["NZD"] = "$";
			CURRENCY_CODES["NIO"] = "C$";
			CURRENCY_CODES["NGN"] = "₦";
			CURRENCY_CODES["KPW"] = "₩";
			CURRENCY_CODES["NOK"] = "kr";
			CURRENCY_CODES["OMR"] = "﷼";
			CURRENCY_CODES["PKR"] = "₨";
			CURRENCY_CODES["PAB"] = "B/.";
			CURRENCY_CODES["PYG"] = "Gs";
			CURRENCY_CODES["PEN"] = "S/.";
			CURRENCY_CODES["PHP"] = "₱";
			CURRENCY_CODES["PLN"] = "zł";
			CURRENCY_CODES["QAR"] = "﷼";
			CURRENCY_CODES["RON"] = "lei";
			CURRENCY_CODES["RUB"] = "руб";
			CURRENCY_CODES["SHP"] = "£";
			CURRENCY_CODES["SAR"] = "﷼";
			CURRENCY_CODES["RSD"] = "Дин.";
			CURRENCY_CODES["SCR"] = "₨";
			CURRENCY_CODES["SGD"] = "$";
			CURRENCY_CODES["SBD"] = "$";
			CURRENCY_CODES["SOS"] = "S";
			CURRENCY_CODES["ZAR"] = "R";
			CURRENCY_CODES["KRW"] = "₩";
			CURRENCY_CODES["LKR"] = "₨";
			CURRENCY_CODES["SEK"] = "kr";
			CURRENCY_CODES["CHF"] = "CHF";
			CURRENCY_CODES["SRD"] = "$";
			CURRENCY_CODES["SYP"] = "£";
			CURRENCY_CODES["TWD"] = "NT$";
			CURRENCY_CODES["THB"] = "฿";
			CURRENCY_CODES["TTD"] = "TT$";
			CURRENCY_CODES["TRY"] = "₤";
			CURRENCY_CODES["TRL"] = "₤";
			CURRENCY_CODES["TVD"] = "₴";
			CURRENCY_CODES["GBP"] = "£";
			CURRENCY_CODES["USD"] = "$";
			CURRENCY_CODES["UYU"] = "$U";
			CURRENCY_CODES["UZS"] = "лв";
			CURRENCY_CODES["VEF"] = "Bs";
			CURRENCY_CODES["VND"] = "₫";
			CURRENCY_CODES["YER"] = "﷼";
			CURRENCY_CODES["ZWD"] = "Z$";
		}
		
		public static var CURRENCY_CODES:Dictionary;
		
		/**
		 * returns the currency symbol for an ISO-4217 currency code.
		 * <p>
		 * <li><code>EUR</code> returns <code>€</code></li>
		 * <li><code>USD</code> retuns <code>$</code></li>
		 * <p>
		 */ 
		public static function getCurrencySymbol(isoCode:String):String {
			trace("CurrencyUtil ::: getCurrencySymbol");
			var s:String = "";
			//trace("    - has code: ", CURRENCY_CODES.hasOwnProperty(isoCode));
			if(CURRENCY_CODES.hasOwnProperty(isoCode)) s = CURRENCY_CODES[isoCode]; 
			return s;
		}
		
	}
	
}