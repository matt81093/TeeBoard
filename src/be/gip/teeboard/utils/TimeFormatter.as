package be.gip.teeboard.utils {
	
	import mx.formatters.DateFormatter;
	
	public class TimeFormatter {
		
		private static var fmt:DateFormatter = new DateFormatter();
		
		/**
		 * Formats a Date into JJ:NN:SS format and returns it.
		 */ 
		public static function formatDate(value:Date):String {
			fmt.formatString = "JJ:NN:SS";
			return fmt.format(value);
		}
		
		/**
		 * Converts a number (seconds) into a formatted string.
		 * The format is: HH:NN:SS
		 */ 
		public static function formatSeconds(value:Number):String {
			var d:Date = new Date(null, null, null, 0, 0, value);
			fmt.formatString = "JJ:NN:SS";
			return fmt.format(d);
		}
		
		/**
		 * Converts a number (milliseconds) into a formatted string.
		 * The format is: HH:NN:SS
		 */ 
		public static function formatMilliseconds(value:Number):String {
			var d:Date = new Date(null, null, null, 0, 0, 0, value);
			fmt.formatString = "JJ:NN:SS";
			return fmt.format(d);
		}
		
	}
	
}
