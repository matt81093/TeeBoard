package be.gip.teeboard.graphics {
	
	import flash.display.BitmapData;
	
	import mx.core.BitmapAsset;
	
	[Embed(source="/css/skins/copper-bg.png")]
	public class TileBackground extends BitmapAsset {
		
		public function TileBackground(bitmapData:BitmapData=null, pixelSnapping:String="auto", smoothing:Boolean=true) {
			super(bitmapData, pixelSnapping, smoothing);
		}
		
	}
	
}
