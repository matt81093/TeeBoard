package be.gip.teeboard.graphics {
	
	import flash.display.BitmapData;
	
	import mx.core.BitmapAsset;
	
	[Embed(source="/icons/appIcon_128.png")]
	public class ApplicationIcon extends BitmapAsset {
		
		public function ApplicationIcon(bitmapData:BitmapData=null, pixelSnapping:String="auto", smoothing:Boolean=true) {
			super(bitmapData, pixelSnapping, smoothing);
		}
		
	}
	
}
