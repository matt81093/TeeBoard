package be.gip.teeboard.net.api {
	
	
	public class Imgur {
		
		/**
		 * Url for retrieving an Imgur Album.
		 * 
		 * Replace <code>:albumId</code> with the imgur album id.
		 * 
		 * <code>https://api.imgur.com/3/album/:albumId</code>
		 * 
		 * <p>albumId: 2mQPO</p>
		 */ 
		public static const IMGUR_ALBUM_URL:String = "https://api.imgur.com/3/album/:albumId";
		
	}
	
}