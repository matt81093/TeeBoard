package be.gip.teeboard.net.api {
	
	
	public class Donations {
		
		/**
		 * Deprecated
		 */ 
		public static const STREAM_DONATIONS:String = "streamdonations";
		
		/**
		 * Deprecated
		 */ 
		public static const IM_RAISING:String = "imraising";
		
		/**
		 * Indicates donation tracking for donation-tracker.com
		 */ 
		public static const DONATION_TRACKER:String = "donation-tracker.com";
		
		/**
		 * Indicates donation tracking for donation-tracker.com
		 */ 
		public static const IM_RAISING2:String = "imraising.tv";
		
		/**
		 * Indicates donation tracking for streamtip.com
		 */ 
		public static const STREAM_TIP:String = "streamtip.com";
		
		/**
		 * Indicates donation tracking for twitchplus.com
		 */ 
		public static const TWITCH_PLUS:String = "twitchplus.com";
		
		/**
		 * Indicates donation tracking for twitchalerts.com
		 */ 
		public static const TWITCH_ALERTS:String = "twitchalerts.com";
		
		/**
		 * DonationTracker API url.
		 * 
		 * <li>Replace <code>:channel</code> with proper channel from the <code>DonationsModel</code>'s <code>selectedAccount</code>.</li>
		 * <li>Replace <code>:token</code> with proper token from the <code>DonationsModel</code>'s <code>selectedAccount</code>.</li>
		 * 
		 * @see http://www.donation-tracker.com/
		 * 
		 */ 
		public static const DONATION_TRACKER_URL:String = "https://www.donation-tracker.com/api/?channel=:channel&api_key=:key";
		
		/**
		 * ImRaising API v2 url.
		 * <p>URL requires token sent via header.</p>
		 * 
		 * <pre>https://imraising.tv/api/v1/donations?limit=10&sort=time</pre>
		 * 
		 * @see https://imraising.tv/main/api/
		 * 
		 */ 
		public static const IM_RAISING_URL2:String = "https://imraising.tv/api/v1/donations?limit=10&sort=-time";
		
		/**
		 * streamtip API url.
		 * <p>url requires a token and client id.</p>
		 * 
		 * <li>Replace <code>:channel</code> with proper channel from the <code>DonationsModel</code>'s <code>selectedAccount</code>.</li>
		 * <li>Replace <code>:token</code> with proper token from the <code>DonationsModel</code>'s <code>selectedAccount</code>.</li>
		 * 
		 * <pre>https://streamtip.com/api/tips?client_id=client_id&access_token=access_token</pre>
		 * 
		 * @see https://streamtip.com/api
		 * 
		 */ 
		public static const STREAM_TIP_URL:String = "https://streamtip.com/api/tips?client_id=:clientID&access_token=:key";
		
		/**
		 * 
		 */ 
		public static var TWITCH_ALERTS_URL:String = "https://www.twitchalerts.com/api/donations?access_token=:key";
		
		/**
		 * 
		 */ 
		public static var TWITCH_PLUS_URL:String = "https://www.twitchplus.com/twitchplus/api/v1/tip?access_token=:token";
		
		/**
		 * 
		 */ 
		public static var TWITCH_PLUS_TOP_URL:String = "https://www.twitchplus.com/twitchplus/api/v1/tip/topmonth?access_token=:token";
		
		/**
		 * Requires Client ID and API key via HTTP headers. Returns an access token when successful.
		 */ 
		//public static var TWITCH_PLUS_OAUTH_URL:String = "http://sandbox.twitchplus.com/twitchplus/api/v1/auth";
		public static var TWITCH_PLUS_OAUTH_URL:String = "https://www.twitchplus.com/twitchplus/api/v1/auth";
		
	}
	
}