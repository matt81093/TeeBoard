package be.gip.teeboard.net.api {
	
	public class Backendless {
		
		/**
		 * URL to access the Panels database table.
		 */ 
		public static const DATA_PANELS_URL:String = "https://api.backendless.com/v1/data/Panels?pageSize=100";
		
		/**
		 * URL to retrieve the Author of a Panel.
		 * 
		 * Replace <code>:authorId</code> with the panel's authorId.
		 * 
		 * <pre>https://api.backendless.com/v1/data/Authors/:authorId</pre>
		 * 
		 * @see http://backendless.com/documentation/data/rest/documentationdata_basic_search.htm Backendless Documentation
		 */ 
		public static const DATA_AUTHOR_URL:String = "https://api.backendless.com/v1/data/Authors/:authorId?pageSize=1";
		
	}
	
}