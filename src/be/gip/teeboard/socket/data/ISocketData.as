package be.gip.teeboard.socket.data {
	
	
	public interface ISocketData {
		
		function get type():String;
		function set type(value:String):void;
		
		function get data():Object;
		function set data(value:Object):void;
		
	}
	
}
