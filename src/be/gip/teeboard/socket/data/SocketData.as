package be.gip.teeboard.socket.data {
	
	public class SocketData implements ISocketData {
		
		private var _type:String;
		
		private var _data:Object;
		
		/**
		 * 
		 * @return 
		 * 
		 */		
		public function get type():String {
			return _type;
		}
		public function set type(value:String):void	{
			_type = value;
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */		
		public function get data():Object {
			return _data;
		}
		public function set data(value:Object):void	{
			_data = value;
		}
		
	}
	
}
