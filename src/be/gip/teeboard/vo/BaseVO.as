package be.gip.teeboard.vo {
	
	import flash.events.EventDispatcher;
	
	/**
	 * Abstract class, do not instantiate.
	 * <p>
	 * When extending this class, the <code>object</code> passed to the constructor 
	 * is stored in the <code>data</code> property.
	 * Use the <code>getProperty</code> method to access the <code>data</code> property values.
	 * </p>
	 */ 
	public class BaseVO extends EventDispatcher {
		
		protected var data:Object;
		
		public function BaseVO(value:Object) {
			data = value;
		}
		
		protected function getProperty(value:String):String {
			if(data && data.hasOwnProperty(value)) {
				return data[value]
			}
			return "";
		}
		
		/**
		 * Compares the object to the object passed as argument.
		 * Override this method to finetune the equality.
		 */ 
		public function equals(value:Object):Boolean {
			return this === value;
		}
		
	}
	
}
