package be.gip.teeboard.vo {
	
	[Bindable]
	/**
	 * Stream preview screenshots.
	 */
	public class ImagesVO {
		
		private var data:Object;
		
		public function ImagesVO(value:Object)	{
			data = value;
		}
		
		private function getProperty(value:String):String {
			if(data && data.hasOwnProperty(value)) {
				return data[value];
			}
			return "";
		}
		
		public function get large():String {
			return getProperty("large");
		}
		
		public function get medium():String {
			return getProperty("medium");
		}
		
		public function get small():String {
			return getProperty("small");
		}
		
		/**
		 * A url with <code>{width}</code> and <code>{height}</code> tokens. 
		 * By replacing the tokens you can retrieve a preview image at any given size.
		 */ 
		public function get template():String {
			return getProperty("template");
		}
		
	}
	
}

/**
 * preview = (Object)#7
 *   large = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-640x400.jpg"
 *   medium = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-320x200.jpg"
 *   small = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-80x50.jpg"
 *   template = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-{width}x{height}.jpg"
 */