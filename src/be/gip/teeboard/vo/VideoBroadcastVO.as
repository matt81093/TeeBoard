package be.gip.teeboard.vo {
	
	import be.gip.teeboard.utils.TimeFormatter;
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class VideoBroadcastVO extends BaseVO implements IVideoVO {
		
		//private var data:Object;
		private var _channel:ChannelVO;
		
		public function VideoBroadcastVO(value:Object)	{
			super(value);
			if(value && value.channel) _channel = new ChannelVO(value.channel);
		}
		
		public function get videoId():String {
				return getProperty("_id");
		}
		
		public function get channel():ChannelVO {
			return _channel;
		}
		
		/**
		 * Formatted version of <code>length</code> property.
		 */ 
		public function get duration():String {
			var len:String = TimeFormatter.formatSeconds(length);
			return len;
		}
		
		public function get description():String {
			return getProperty("description");
		}
		
		/**
		public function get embed():String {
			return getProperty("embed");
		}
		*/
		
		public function get game():String {
			return getProperty("game");
		}
		
		public function get length():uint {
			if(data && data.hasOwnProperty("length")) {
				return data["length"]
			}
			return 0;
		}
		
		public function get previewUrl():String {
			return getProperty("preview");
		}
		
		public function get recordedAt():Date {
			var d:Date = DateUtil.parseW3CDTF(data.recorded_at);
			return d;
		}
		
		public function get title():String {
			return getProperty("title");
		}
		
		public function get url():String {
			return getProperty("url");
		}
		
		public function get popoutUrl():String {
			var id:String = videoId.substring(1);
			var str:String = "http://www.twitch.tv/archive/archive_popout?id=" + id;
			return str;
		}
		
		public function get numViews():uint {
			if(data && data.hasOwnProperty("views")) {
				return data["views"]
			}
			return 0;
		}
		
	}
	
}

/**
 * [2] (Object)#9
 *   _id = "a422091246"
 *   _links = (Object)#10
 *     channel = "https://api.twitch.tv/kraken/channels/smitegame"
 *     self = "https://api.twitch.tv/kraken/videos/a422091246"
 *   channel = (Object)#11
 *     display_name = "Smitegame"
 *     name = "smitegame"
 *   description = (null)
 *   embed = "<object bgcolor='#000000' data='http://www.twitch.tv/widgets/archive_embed_player.swf' height='300' id='clip_embed_player_flash' type='application/x-shockwave-flash' width='400'>
 *         <param name='movie' value='http://www.twitch.tv/widgets/archive_embed_player.swf' />
 *         <param name='allowScriptAccess' value='always' />
 *         <param name='allowNetworking' value='all' />
 *         <param name='allowFullScreen' value='true' />
 *         <param name='flashvars' value='title=SMITE%2Bwith%2BTrendKiLL%2B-%2BCovering%2BPatch%2BNotes%2Band%2Bother%2Bthings%2Bduring%2BSmite%2Bmaintenance%2521&amp;channel=smitegame&amp;auto_play=false&amp;archive_id=422091246&amp;start_volume=25' />
 *     </object>
 *     "
 *   game = "Smite"
 *   length = 7063
 *   preview = "http://static-cdn.jtvnw.net/jtv.thumbs/archive-422091246-320x240.jpg"
 *   recorded_at = "2013-06-26T14:03:59Z"
 *   title = "SMITE with TrendKiLL - Covering Patch Notes and other things during Smite maintenance!"
 *   url = "http://www.twitch.tv/smitegame/b/422091246"
 *   views = 67
 **/ 
