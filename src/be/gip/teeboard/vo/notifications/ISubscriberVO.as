package be.gip.teeboard.vo.notifications {
	
	import be.gip.teeboard.vo.UserVO;
	
	public interface ISubscriberVO {
		
		function get createdAt():Date;
		function get user():UserVO;
		
	}
	
}