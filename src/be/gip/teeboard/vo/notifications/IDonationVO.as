package be.gip.teeboard.vo.notifications {
	
	
	public interface IDonationVO {
		
		function setData(value:Object):void;
		
		function get name():String;
		function get amount():String;
		function get currencySymbol():String;
		function get currencyCode():String;
		function get date():Date;
		function get message():String;
		
	}
	
}
