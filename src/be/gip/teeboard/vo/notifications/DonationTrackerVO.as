package be.gip.teeboard.vo.notifications {
	
	[Bindable]
	/**
	 * ValueObject for DonationTracker donation.
	 * 
	 * @see http://www.donation-tracker.com
	 */ 
	public class DonationTrackerVO extends AbstractNotification implements IDonationVO {
		
		private var data:Object;
		
		private var _amount:String;
		private var _currencySymbol:String;
		private var _currencyCode:String;
		private var _date:Date;
		private var _message:String;
		private var _name:String;
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		/**
		 * 
		 */ 
		public function setData(value:Object):void {
			data = value;
			
			_amount = data.amount;
			_currencyCode = data.currency;
			_currencySymbol = data.currency_symbol;
			_date = new Date(Number(data.timestamp)*1000);
			_message = data.note;
			_name = data.username;
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		public function get name():String {
			return _name
		}
		
		public function get amount():String {
			return _amount;
		}
		
		/**
		 * EUR, USD, etc..
		 */ 
		public function get currencyCode():String {
			return _currencyCode;
		}
		
		public function get currencySymbol():String {
			return _currencySymbol;
		}
		
		public function get date():Date {
			return _date
		}
		
		public function get message():String {
			return _message
		}
		
		override public function get notificationType():String {
			return "donation";
		}
		
	}
	
}

/**
 *  (Object)#0
 *   api_check = "1"
 *   donations = (Array)#1
 *     [0] (Object)#2
 *       amount = "10.00"
 *       currency = "EUR"
 *       currency_symbol = "€"
 *       note = "Some text for testing purposes."
 *       paypal_email = "dashinsert@donation-tracker.com"
 *       timestamp = "1396062350"
 *       transaction_id = "DT-4SHLOOFVK"
 *       username = "Anonymous"
 */ 
 