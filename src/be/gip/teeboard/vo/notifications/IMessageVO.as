package be.gip.teeboard.vo.notifications {
	
	
	public interface IMessageVO {
		
		function get message():String;
		function set message(value:String):void;
		function get tag():String;
		function set tag(value:String):void;
		
	}
	
}