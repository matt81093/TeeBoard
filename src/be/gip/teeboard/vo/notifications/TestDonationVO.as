package be.gip.teeboard.vo.notifications {
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class TestDonationVO extends AbstractNotification implements IDonationVO {
		
		private var data:Object;
		
		private var _amount:String = (Math.random() * 100).toFixed(2);
		private var _currencySymbol:String = "$";
		private var _currencyCode:String = "USD";
		private var _date:Date = new Date();
		private var _message:String = "This is not a test.";
		private var _name:String = "TeeBoard";
		
		//==============================================================
		//
		// CONSTRUCTOR
		//
		//==============================================================
		
		public function TestDonationVO():void {
			super.isTest = true;
		}
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		/**
		 * 
		 */ 
		public function setData(value:Object):void {
			//
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		public function get amount():String {
			return _amount;
		}
		
		public function get currencySymbol():String {
			return _currencySymbol;
		}
		
		public function get currencyCode():String {
			return _currencyCode;
		}
		
		public function get date():Date {
			return _date;
		}
		
		public function get message():String {
			return _message;
		}
		
		public function get name():String {
			return _name;
		}
		
		override public function get notificationType():String {
			return "donation"
		}
		
	}
	
}