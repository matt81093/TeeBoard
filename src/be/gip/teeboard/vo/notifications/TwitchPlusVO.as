package be.gip.teeboard.vo.notifications {
	
	import be.gip.teeboard.utils.CurrencyUtil;
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class TwitchPlusVO extends AbstractNotification implements IDonationVO {
		
		private var data:Object;
		
		private var _amount:String;
		private var _currencySymbol:String;
		private var _currencyCode:String;
		private var _date:Date;
		private var _message:String;
		private var _name:String;
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		public function setData(value:Object):void {
			data = value;
			
			_amount = (Number(data.amount)/100).toFixed(2);
			_currencySymbol = CurrencyUtil.getCurrencySymbol(data.currency_code);
			_currencyCode = data.currency_code;
			//2014-10-17T01:08:21Z
			_date = DateUtil.parseW3CDTF(data.created_at);
			_message = data.message;
			_name = data.donator_name;
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		public function get amount():String {
			return _amount;
		}
		
		public function get currencySymbol():String {
			return _currencySymbol;
		}
		
		public function get currencyCode():String {
			return _currencyCode;
		}
		
		public function get date():Date {
			return _date;
		}
		
		public function get message():String {
			return _message;
		}
		
		public function get name():String {
			return _name;
		}
		
		override public function get notificationType():String {
			return "donation"
		}
		
	}
	
}

/**
 *    donations = (Array)#1
 *     [0] (Object)#2
 *       _id = "130"
 *       amount = "1000"
 *       created_at = "2014-10-17T01:45:22Z"
 *       currency_code = "USD"
 *       donator_email = "gip.deezjavu@gmail.com"
 *       donator_name = "TeeBoard"
 *       message = "Some text for testing purposes."
 *       payment_type = "test"
 *     [1] (Object)#3
 *       _id = "129"
 *       amount = "250"
 *       created_at = "2014-10-17T01:08:21Z"
 *       currency_code = "EUR"
 *       donator_email = "gip.deezjavu@gmail.com"
 *       donator_name = "TeeBoard"
 *       message = "Some text for testing purposes. This text is not supposed to be read."
 *       payment_type = "test"
 * 
 */ 
 
