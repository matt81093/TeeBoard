package be.gip.teeboard.vo.notifications {
	
	import be.gip.teeboard.vo.UserVO;
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	/**
	 * Used by <code>TwitchEvent.GET_CHANNEL_SUBSCRIBERS</code>.
	 * 
	 */ 
	public class TestSubscriberVO extends AbstractNotification implements ISubscriberVO {
		
		private var _user:UserVO;
		
		private var _date:Date;
		
		public function TestSubscriberVO() {
			_user = new UserVO({display_name:"TeeBoard", name:"teeboard"});
			_date = new Date();
			super.isTest = true;
		}
		
		/**
		 * 
		 */ 
		public function get createdAt():Date {
			return _date;
		}
		
		/**
		 * 
		 */ 
		public function get user():UserVO {
			return _user;
		}
		
		override public function get notificationType():String {
			return "subscriber"
		}
		
	}
	
}

/**
 *     [9] (Object)#39
 *       _id = "88d4621871b7274c34d5c3eb5dad6780c8533318"
 *       _links = (Object)#40
 *         self = "https://api.twitch.tv/kraken/users/summonmebro/follows/channels/deezjavu"
 *       created_at = "2014-01-02T13:17:22Z"
 *       user = (Object)#41
 *         _id = 53518327
 *         _links = (Object)#42
 *           self = "https://api.twitch.tv/kraken/users/summonmebro"
 *         bio = (null)
 *         created_at = "2013-12-19T23:32:49Z"
 *         display_name = "SummonMeBro"
 *         logo = (null)
 *         name = "summonmebro"
 *         type = "user"
 *         updated_at = "2013-12-19T23:39:22Z"
 * 
 */ 
