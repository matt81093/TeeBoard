package be.gip.teeboard.vo.notifications {
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class StreamTipVO extends AbstractNotification implements IDonationVO {
		
		private var data:Object;
		
		private var _amount:String;
		private var _currencySymbol:String;
		private var _currencyCode:String;
		private var _date:Date;
		private var _message:String;
		private var _name:String;
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		public function setData(value:Object):void {
			data = value;
			
			_amount = data.amount;
			_currencySymbol = data.currencySymbol;
			_currencyCode = "";
			_date = DateUtil.parseW3CDTF(data.date);
			_message = data.note;
			_name = data.username;
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		public function get amount():String {
			return _amount;
		}
		
		public function get currencyCode():String {
			return _currencyCode;
		}
		
		public function get currencySymbol():String {
			return _currencySymbol;
		}
		
		public function get date():Date {
			return _date;
		}
		
		public function get message():String {
			return _message;
		}
		
		public function get name():String {
			return _name;
		}
		
		override public function get notificationType():String {
			return "donation"
		}
		
	}
	
}