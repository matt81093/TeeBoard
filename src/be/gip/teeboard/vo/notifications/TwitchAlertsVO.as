package be.gip.teeboard.vo.notifications {
	
	import be.gip.teeboard.utils.CurrencyUtil;
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class TwitchAlertsVO extends AbstractNotification implements IDonationVO {
		
		private var data:Object;
		
		private var _amount:String;
		private var _currencySymbol:String;
		private var _currencyCode:String;
		private var _date:Date;
		private var _message:String;
		private var _name:String;
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		/**
		 * 
		 */ 
		public function setData(value:Object):void {
			data = value;
			
			_amount = Number(data.amount).toFixed(2);
			_currencySymbol = CurrencyUtil.getCurrencySymbol(data.currency);
			_currencyCode = data.currency;
			_date = DateUtil.parseW3CDTF(data.created_at);
			_message = data.message;
			_name = data.donator.name;
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		public function get amount():String {
			return _amount
		}
		
		public function get currencySymbol():String {
			return _currencySymbol
		}
		
		public function get currencyCode():String {
			return _currencyCode
		}
		
		public function get date():Date {
			return _date;
		}
		
		public function get message():String {
			return _message;
		}
		
		public function get name():String {
			return _name;
		}
		
		override public function get notificationType():String {
			return "donation"
		}
		
	}
	
}

/**
 * (Object)#0
 *   donations = (Array)#1
 *     [0] (Object)#2
 *       amount = 12.81
 *       amount_label = "$12.81"
 *       created_at = "2014-10-17T05:27:00Z"
 *       currency = "USD"
 *       donator = (Object)#3
 *         id = "da39a3ee5e6b4b0d3255bfef95601890afd80709"
 *         name = "FishStickLoL"
 *       id = 77823690
 *       message = "Take my money, pweez"
 *     [1] (Object)#4
 *       amount = 64.71
 *       amount_label = "$64.71"
 *       created_at = "2014-09-09T18:30:00Z"
 *       currency = "USD"
 *       donator = (Object)#5
 *         id = "da39a3ee5e6b4b0d3255bfef95601890afd80709"
 *         name = "TeeBoard"
 *       id = 77786682
 *       message = "" 
 */
