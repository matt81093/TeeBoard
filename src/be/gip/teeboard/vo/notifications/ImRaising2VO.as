package be.gip.teeboard.vo.notifications {
	import be.gip.teeboard.utils.CurrencyUtil;
	
	import com.adobe.utils.DateUtil;
	
	import flash.globalization.CurrencyFormatter;
	
	import mx.formatters.CurrencyFormatter;
	
	
	[Bindable]
	public class ImRaising2VO extends AbstractNotification implements IDonationVO {
		
		private var data:Object;
		
		private var _amount:String;
		private var _currencySymbol:String;
		private var _currencyCode:String;
		private var _date:Date;
		private var _message:String;
		private var _name:String;
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		public function setData(value:Object):void {
			data = value;
			
			_amount = data.amount.display.total;
			_name = data.nickname;
			_currencyCode = data.amount.display.currency;
			_currencySymbol = CurrencyUtil.getCurrencySymbol(_currencyCode);
			_date = DateUtil.parseW3CDTF(data.time);
			_message = data.message;
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		public function get name():String {
			return _name;
		}
		
		public function get amount():String	{
			return _amount;
		}
		
		public function get currencySymbol():String	{
			return _currencySymbol;
		}
		
		public function get currencyCode():String {
			return _currencyCode;
		}
		
		public function get date():Date	{
			return _date;
		}
		
		public function get message():String {
			return _message
		}
		
		override public function get notificationType():String {
			return "donation";
		}
		
	}
	
}

/**
 * Array)#0
 *   [0] (Object)#1
 *     _id = "53f0aa55fb11480e1234dd97"
 *     amount = (Object)#2
 *       display = (Object)#3
 *         currency = "EUR"
 *         total = 15
 *     message = "test"
 *     nickname = "TeeBoard"
 *     time = "2014-08-17T13:12:53.870Z"
 * 
 */ 