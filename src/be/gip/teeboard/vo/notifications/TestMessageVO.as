package be.gip.teeboard.vo.notifications {
	
	/**
	 * 
	 */ 
	public class TestMessageVO extends AbstractNotification implements IMessageVO {
		
		private var _message:String = "Some text for testing purposes";
		private var _tag:String = "txt";
		
		
		/**
		 * 
		 */ 
		public function get message():String {
			return _message;
		}
		public function set message(value:String):void {
			if(value == _message) return;
			_message = value;
		}
		
		/**
		 * 
		 */ 
		public function get tag():String {
			return _tag;
		}
		public function set tag(value:String):void {
			if(value == _tag) return;
			_tag = value;
		}
		
		/**
		 * 
		 **/
		override public function get notificationType():String {
			return "message";
		}
		
	}
	
}

