package be.gip.teeboard.vo.notifications {
	
	import be.gip.teeboard.vo.UserVO;
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class TestFollowerVO extends AbstractNotification implements IFollowerVO {
		
		public var data:Object;
		
		private var _date:Date;
		private var _user:UserVO;
		
		public function TestFollowerVO():void {
			_date = new Date();
			_user = new UserVO({name:"teeboard", display_name:"TeeBoard"});
			super.isTest = true;
		}
		
		/**
		 * 
		 */ 
		public function get createdAt():Date {
			return _date;
		}
		
		/**
		 * 
		 */ 
		public function get user():UserVO {
			return _user;
		}
		
		/**
		 * 
		 **/
		override public function get notificationType():String {
			return "follower";
		}
		
	}
	
}