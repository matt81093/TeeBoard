package be.gip.teeboard.vo.notifications {
	
	
	/**
	 * Abstract class used as the base for notifications. 
	 * 
	 * <p>Don't create instances from this class, but extend the class to gain its functionality.</p>
	 */ 
	public class AbstractNotification {
		
		/**
		 * Indicates if this is a test or a real notification.
		 * 
		 * <p>
		 * Test notifications are displayed on screen, 
		 * but removed again when the Notification Window is hidden.
		 * </p>
		 * 
		 * @default false
		 */ 
		public var isTest:Boolean = false;
		
		/**
		 * One of the available <code>NoticationsType</code> constants.
		 * 
		 * @see be.gip.teeboard.utils.NotificationsType NotificationsType
		 */ 
		public function get notificationType():String {
			return "";
		}
		
	}
	
}
