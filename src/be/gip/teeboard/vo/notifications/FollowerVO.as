package be.gip.teeboard.vo.notifications {
	
	import be.gip.teeboard.vo.UserVO;
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	/**
	 * Used by <code>TwitchEvent.GET_CHANNEL_FOLLOWERS</code>.
	 * 
	 */ 
	public class FollowerVO extends AbstractNotification implements IFollowerVO {
		
		private var _user:UserVO;
		
		private var _data:Object;
		
		public function FollowerVO(value:Object) {
			_data = value;
			if(value && value.hasOwnProperty("user")) {
				_user = new UserVO(value.user);
			}
		}
		
		private function getProperty(value:String):String {
			if(_data && _data.hasOwnProperty(value)) {
				return _data[value];
			}
			return "";
		}
		
		/**
		 * 
		 */ 
		public function get createdAt():Date {
			var d:Date;
			if(_data && _data.hasOwnProperty("created_at")) {
				d = DateUtil.parseW3CDTF(_data.created_at);
			}
			return d;
		}
		
		/**
		 * 
		 */ 
		public function get user():UserVO {
			return _user;
		}
		
		/**
		 * 
		 **/
		override public function get notificationType():String {
			return "follower";
		}
		
	}
	
}

/**
 *     [9] (Object)#39
 *       _links = (Object)#40
 *         self = "https://api.twitch.tv/kraken/users/summonmebro/follows/channels/deezjavu"
 *       created_at = "2014-01-02T13:17:22Z"
 *       user = (Object)#41
 *         _id = 53518327
 *         _links = (Object)#42
 *           self = "https://api.twitch.tv/kraken/users/summonmebro"
 *         bio = (null)
 *         created_at = "2013-12-19T23:32:49Z"
 *         display_name = "SummonMeBro"
 *         logo = (null)
 *         name = "summonmebro"
 *         type = "user"
 *         updated_at = "2013-12-19T23:39:22Z"
 * 
 */ 
