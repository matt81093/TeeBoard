package be.gip.teeboard.vo {
	
	/**
	 * Twitch Game object.
	 * 
	 */ 
	[Bindable]
	public class GameVO {
		
		private var _data:Object;
		private var _box:ImagesVO;
		private var _logo:ImagesVO;
		
		public function GameVO(value:Object) {
			_data = value;
			_box = new ImagesVO(value.game.box);
			_logo = new ImagesVO(value.game.logo);
		}
		
		/**
		 * Number of live channels for this game.
		 * 
		 * @return 
		 * 
		 */
		public function get numChannels():int {
			return _data.channels;
		}
		
		/**
		 * The game id (no idea what it's used for).
		 * 
		 * @return 
		 * 
		 */
		public function get gameId():int {
			return _data.game._id;
		}
		
		/**
		 * Links (seems to be empty).
		 * 
		 * @return 
		 * 
		 */
		public function get links():Object {
			return _data.game._links;
		}
		
		/**
		 * Box images in several sizes (small/medium/large/custom).
		 * 
		 * @return 
		 * 
		 */
		public function get box():ImagesVO {
			return _box;
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get giantbombId():int {
			return _data.game.giantbomb_id;
		}
		
		/**
		 * Logo images in several sizes (small/medium/large/custom).
		 * 
 		 * @return 
		 * 
		 */
		public function get logo():ImagesVO {
			return _logo;
		}
		
		/**
		 * The name of the game (pun intended).
		 * 
		 * @return 
		 * 
		 */
		public function get name():String {
			return _data.game.name;
		}
		
		/**
		 * Total number of viewers currently watching this game on Twitch.
		 * IOW, this is the combined viewer count for all live streams broadcasting this game.
		 * 
		 * @return 
		 * 
		 */
		public function get numViewers():int {
			return _data.viewers;
		}
		
	}
	
}

/**
 *      [0] (Object)#3
 *       channels = 111
 *       game = (Object)#4
 *         _id = 21779
 *         _links = (Object)#5
 *         box = (Object)#6
 *           large = "http://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg"
 *           medium = "http://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg"
 *           small = "http://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg"
 *           template = "http://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-{width}x{height}.jpg"
 *         giantbomb_id = 24024
 *         logo = (Object)#7
 *           large = "http://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg"
 *           medium = "http://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg"
 *           small = "http://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg"
 *           template = "http://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-{width}x{height}.jpg"
 *         name = "League of Legends"
 *       viewers = 6356
 *  
 */