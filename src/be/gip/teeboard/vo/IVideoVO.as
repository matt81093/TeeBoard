package be.gip.teeboard.vo {
	
	public interface IVideoVO  {
		
		function get videoId():String;
		function get channel():ChannelVO;
		function get duration():String;
		function get description():String;
		function get game():String;
		function get length():uint;
		function get previewUrl():String;
		function get recordedAt():Date;
		function get title():String;
		function get url():String;
		function get popoutUrl():String;
		function get numViews():uint;
	}
	
}
