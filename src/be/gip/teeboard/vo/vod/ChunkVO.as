package be.gip.teeboard.vo.vod {
	
	import be.gip.teeboard.utils.TimeFormatter;
	
	import mx.messaging.channels.StreamingAMFChannel;
	
	[Bindable]
	/**
	 * A piece of the VOD puzzle (chunk). VOD's (highlights & broadcasts) are stored in 30 min chunks on Twitch.
	 */ 
	public class ChunkVO {
		
		private var data:Object;
		
		public function ChunkVO(value:Object) {
			data = value;
		}
		
		public function get url():String {
			return data.url;
		}
		
		public function get length():Number {
			return data.length;
		}
		
		public function get vodCountUrl():String {
			return data.vod_count_url;
		}
		
		public function get duration():String {
			return TimeFormatter.formatSeconds(data.length);
		}
		
	}
	
}
