package be.gip.teeboard.vo {
	
	import com.adobe.utils.DateUtil;
	
	public class ChannelVO extends BaseVO {
		
		
		public function ChannelVO(value:Object)	{
			super(value);
		}
		
		public function get channelId():int {
			if(data && data.hasOwnProperty("_id")) {
				return data._id;
			}
			return -1;
		}
		
		public function get links():Object {
			if(data && data.hasOwnProperty("_links")) {
				return data._links;
			}
			return null;
		}
		
		public function get createdAt():Date {
			var d:Date = DateUtil.parseW3CDTF(data.created_at);
			return d;
		}
		
		public function get updatedAt():Date {
			var d:Date = DateUtil.parseW3CDTF(data.updated_at);
			return d;
		}
		
		public function get displayName():String {
			return getProperty("display_name");
		}
		
		public function get email():String {
			return getProperty("email");
		}
		
		public function get game():String {
			return getProperty("game");
		}
		
		public function get logo():String {
			return getProperty("logo");
		}
		
		public function get name():String {
			return getProperty("name");
		}
		
		public function get status():String {
			return getProperty("status");
		}
		
		public function get streamKey():String {
			return getProperty("stream_key");
		}
		
		public function get url():String {
			return getProperty("url");
		}
		
		public function get popoutUrl():String {
			var value:String = "";
			if(data.hasOwnProperty("url")) value = data.url + "/popout";
			return value;
		}
		
	}
	
}

/**
 * (Object)#0
 *   _id = 23773615
 *   _links = (Object)#1
 *     chat = "https://api.twitch.tv/kraken/chat/deezjavu"
 *     commercial = "https://api.twitch.tv/kraken/channels/deezjavu/commercial"
 *     editors = "https://api.twitch.tv/kraken/channels/deezjavu/editors"
 *     features = "https://api.twitch.tv/kraken/channels/deezjavu/features"
 *     follows = "https://api.twitch.tv/kraken/channels/deezjavu/follows"
 *     self = "https://api.twitch.tv/kraken/channels/deezjavu"
 *     stream_key = "https://api.twitch.tv/kraken/channels/deezjavu/stream_key"
 *     subscriptions = "https://api.twitch.tv/kraken/channels/deezjavu/subscriptions"
 *     teams = "https://api.twitch.tv/kraken/channels/deezjavu/teams"
 *     videos = "https://api.twitch.tv/kraken/channels/deezjavu/videos"
 *   background = "http://static-cdn.jtvnw.net/jtv_user_pictures/deezjavu-channel_background_image-ee574395b05bc867.jpeg"
 *   banner = (null)
 *   created_at = "2011-07-31T10:48:20Z"
 *   delay = 0
 *   display_name = "DeezjaVu"
 *   email = "gip.deezjavu@gmail.com"
 *   game = "Smite"
 *   logo = "http://static-cdn.jtvnw.net/jtv_user_pictures/deezjavu-profile_image-1460b1a19850ba30-300x300.jpeg"
 *   mature = (null)
 *   name = "deezjavu"
 *   status = "SmiteFire Lobby overlay"
 *   stream_key = "live_23773615_iONvFdDcEleNm1rUtZTcX3jvOBFJYj"
 *   updated_at = "2013-06-19T01:45:35Z"
 *   url = "http://www.twitch.tv/deezjavu"
 *   video_banner = "http://static-cdn.jtvnw.net/jtv_user_pictures/deezjavu-channel_offline_image-d21468e915e6ef9a-640x360.png"
 * 
 */ 

