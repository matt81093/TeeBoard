package be.gip.teeboard.vo {
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	public class RootVO {
		
		private var data:Object;
		
		public function RootVO(value:Object) {
			data = value;
		}
		
		public function get links():Object {
			return data._links;
		}
		
		public function get valid():Boolean {
			return data.token.valid;
		}
		
		public function get username():String {
			return data.token.user_name;
		}
		
		public function get scopes():Array {
			var s:Array;
			if(data.token.authorization != null) {
				s = data.token.authorization.scopes;
			}
			return s;	
		}
		
		public function get createdAt():Date {
			var d:Date;
			if(data.token.authorization != null) {
				d = DateUtil.parseW3CDTF(data.token.authorization.created_at);
			}
			return d;
		}
		
		public function get updatedAt():Date {
			var d:Date;
			if(data.token.authorization != null) {
				d = DateUtil.parseW3CDTF(data.token.authorization.updated_at);
			}
			return d;
		}
	}
	
	/**
	 * <listing>
	 * (Object)#0
	 *   _links = (Object)#1
	 *     channel = "https://api.twitch.tv/kraken/channel"
	 *     channels = "https://api.twitch.tv/kraken/channels/deezjavu"
	 *     chat = "https://api.twitch.tv/kraken/chat/deezjavu"
	 *     ingests = "https://api.twitch.tv/kraken/ingests"
	 *     search = "https://api.twitch.tv/kraken/search"
	 *     streams = "https://api.twitch.tv/kraken/streams/deezjavu"
	 *     teams = "https://api.twitch.tv/kraken/teams"
	 *     user = "https://api.twitch.tv/kraken/user"
	 *     users = "https://api.twitch.tv/kraken/users/deezjavu"
	 *   token = (Object)#2
	 *     authorization = (Object)#3
	 *       created_at = "2013-06-16T19:12:44Z"
	 *       scopes = (Array)#4
	 *         [0] "user_read"
	 *         [1] "channel_read"
	 *         [2] "user_follows_edit"
	 *         [3] "channel_editor"
	 *       updated_at = "2013-06-17T16:13:50Z"
	 *     user_name = "deezjavu"
	 *     valid = true
	 * </listing>
	 */ 
	
}
