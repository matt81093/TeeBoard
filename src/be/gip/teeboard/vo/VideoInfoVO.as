package be.gip.teeboard.vo {
	
	import be.gip.teeboard.vo.vod.ChunkVO;
	
	[RemoteClass (alias="be.gip.teeboard.vo.VideoInfoVO")]
	
	[Bindable]
	/**
	 * Contains info about a twitch VOD for download. 
	 * Twitch stores VOD's in 30 min chunks.
	 */ 
	public class VideoInfoVO {
		
		private var data:Object;
		private var _chunks:Array;
		private var _length:uint = 0;
		
		public function VideoInfoVO(value:Object) {
			data = value;
			setChunks();
		}
		
		private function setChunks():void {
			_chunks = new Array();
			var len:uint = data.chunks.live.length;
			var vo:ChunkVO;
			_length = len;
			for(var i:uint=0; i<len; i++) {
				vo = new ChunkVO(data.chunks.live[i]);
				_chunks.push(vo);
			}
		}
		
		//=====================================
		//
		// IMPLICIT GETTER/SETTERS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function get channelName():String {
			return data.channel;
		}
		
		/**
		 * A list of ChunkVO's
		 */ 
		public function get chunks():Array {
			return _chunks;
		}
		
		/**
		 * The number of <code>ChunkVO</code>'s for this vod.
		 */ 
		public function get length():uint {
			return _length;
		}
		
		/**
		 * 
		 */ 
		public function get endOffset():Number {
			return data.end_offset;
		}
		
		/**
		 * 
		 */ 
		public function get playOffset():Number {
			return data.play_offset;
		}
		
		/**
		 * 
		 */ 
		public function get startOffset():Number {
			return data.start_offset;
		}
		
	}
	
}

/**
 * (Object)#0
 *   channel = "denialtv"
 *   chunks = (Object)#1
 *     live = (Array)#2
 *       [0] (Object)#3
 *         length = 1811
 *         url = "http://store29.media29.justin.tv/archives/2013-9-12/live_user_denialtv_1378955880.flv"
 *         vod_count_url = "http://countess.twitch.tv/ping.gif?u=%7B%22type%22:%22vod%22,%22id%22:4601192414%7D"
 *       [1] (Object)#4
 *         length = 1795
 *         url = "http://store61.media7.justin.tv/archives/2013-9-12/live_user_denialtv_1378957693.flv"
 *         vod_count_url = "http://countess.twitch.tv/ping.gif?u=%7B%22type%22:%22vod%22,%22id%22:4601296764%7D"
 *       [2] (Object)#5
 *         length = 1797
 *         url = "http://store43.media43.justin.tv/archives/2013-9-12/live_user_denialtv_1378959489.flv"
 *         vod_count_url = "http://countess.twitch.tv/ping.gif?u=%7B%22type%22:%22vod%22,%22id%22:4601390444%7D"
 *       [3] (Object)#6
 *         length = 234
 *         url = "http://store21.media21.justin.tv/archives/2013-9-12/live_user_denialtv_1378961286.flv"
 *         vod_count_url = "http://countess.twitch.tv/ping.gif?u=%7B%22type%22:%22vod%22,%22id%22:4601398104%7D"
 *   end_offset = 5637
 *   increment_view_count_url = "http://countess.twitch.tv/ping.gif?u=%7B%22type%22:%22archive%22,%22id%22:460119241%7D"
 *   play_offset = 0
 *   restrictions = (Object)#7
 *   start_offset = 0
 * 
 */ 
