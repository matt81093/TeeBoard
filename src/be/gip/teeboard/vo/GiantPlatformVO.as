package be.gip.teeboard.vo {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	import mx.collections.XMLListCollection;
	
	[Bindable]
	public class GiantPlatformVO extends AbstractDTO {
		
		//-----------------------------------------------
		//
		// PRIVATE PROPERTIES
		//
		//-----------------------------------------------
		
		private var _data:Object;
		private var _games:XMLListCollection;
		
		//-----------------------------------------------
		//
		// PUBLIC PROPERTIES
		//
		//-----------------------------------------------
		
		public var url:String;
		
		//-----------------------------------------------
		//
		// PRIVATE METHODS
		//
		//-----------------------------------------------
		
		private function setData():void {
			_games = new XMLListCollection(_data.game);
		}
		
		//-----------------------------------------------
		//
		// PUBLIC METHODS
		//
		//-----------------------------------------------
		
		public function updateGames(value:XMLList):void {
			(_data as XML).setChildren(value);
			setData();
		}
		
		//-----------------------------------------------
		//
		// IMPLICIT GETTER / SETTERS
		//
		//-----------------------------------------------
		
		/**
		 * 
		 */ 
		public function get data():Object {
			return _data;
		}
		public function set data(value:Object):void {
			_data = value;
			setData();
		}
		
		/**
		 * 
		 */ 
		public function get games():XMLListCollection {
			return _games;
		}
		
		/**
		 * 
		 */ 
		public function get label():String {
			return _data.@platform;
		}
		
		/**
		 * 
		 */ 
		public function get platform():String {
			return _data.@platform;
		}
		
		/**
		 * 
		 */ 
		public function get updatedAt():Date {
			var d:Date;
			if(_data.attribute("updatedAt")) {
				d = new Date(_data.@updatedAt);
			}
			return d;
		}
		
		/**
		 * 
		 */ 
		public function get id():String {
			return _data.@id;
		}
		
	}
	
}
