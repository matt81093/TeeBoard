package be.gip.teeboard.vo {
	
	[Bindable]
	public class IntervalVO {
		
		public var label:String;
		public var time:uint;
		
		public function IntervalVO(label:String, time:uint) {
			this.label = label;
			this.time = time;
		}
		
		public function get milliseconds():uint {
			return time*1000;
		}
	}
	
}
