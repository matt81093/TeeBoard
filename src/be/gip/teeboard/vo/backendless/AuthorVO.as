package be.gip.teeboard.vo.backendless {
	
	[Bindable]
	[RemoteClass(alias="be.gip.teeboard.vo.backendless.Author")]
	public class AuthorVO {
		
		public var objectId:String;
		public var ownerId:String;
		public var created:Date;
		public var updated:Date;
		
		public var name:String;
		public var email:String;
		public var info:String;
		
	}
	
}

/**
 * (Object)#0
 *   created = "04/25/2014 04:26:55 GMT+0000"
 *   email = "gip.deezjavu@gmail.com"
 *   name = "deezjavu"
 *   objectId = "C1158500-88CB-56A6-FFA1-7347437EF900"
 *   ownerId = (null)
 *   updated = (null)
 */