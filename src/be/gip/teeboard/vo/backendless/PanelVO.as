package be.gip.teeboard.vo.backendless {
	
	[Bindable]
	[RemoteClass(alias="be.gip.teeboard.vo.backendless.Panel")]
	public class PanelVO {
		
		public var objectId:String;
		public var ownerId:String;
		public var authorId:String;
		public var created:Date;
		public var updated:Date;
		public var name:String;
		public var url:String;
		public var editable:Boolean;
		
	}
	
}

/**
 *     [0] (Object)#2
 *       authorId = "C1158500-88CB-56A6-FFA1-7347437EF900"
 *       created = "04/23/2014 01:00:15 GMT+0000"
 *       editable = true
 *       name = "resist-panels-01.png"
 *       objectId = "0C9D5106-626F-4CC9-FF89-8EF8E359B900"
 *       ownerId = "C2FFDD7D-153D-67C0-FFDE-5CCE1D6E5800"
 *       updated = "04/25/2014 04:42:06 GMT+0000"
 *       url = "https://api.backendless.com/5dd52e1c-05f1-2f31-ff85-a91ee7375400/v1/files/teeboard/deezjavu/resist-panels-01.png"
 */