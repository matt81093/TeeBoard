package be.gip.teeboard.vo {
	
	import be.gip.teeboard.events.DownloadEvent;
	import be.gip.teeboard.vo.vod.ChunkVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import be.gip.teeboard.utils.VideoDownloadStatus;
	
	/**
	 * Dispatched when vod info (list of flv's) is available.
	 */ 
	[Event(name="infoComplete", type="flash.events.Event")]
	
	/**
	 * Dispatched when a load operation has begun or has received data.
	 */ 
	[Event(name="chunkProgress", type="be.gip.teeboard.events.DownloadEvent")]
	
	/**
	 * Dispatched when a vod chunk has finished downloading.
	 */ 
	[Event(name="chunkComplete", type="be.gip.teeboard.events.DownloadEvent")]
	
	/**
	 * Dispatched when downloading begins.
	 */ 
	[Event(name="downloadStart", type="be.gip.teeboard.events.DownloadEvent")]
	
	/**
	 * Dispatched when all chunks have finished downloading.
	 */ 
	[Event(name="downloadEnd", type="be.gip.teeboard.events.DownloadEvent")]
	
	[RemoteClass (alias="be.gip.twitch.teeboard.vo.VideoDownloadVO")]
	
	[Bindable]
	
	public class VideoDownloadVO extends EventDispatcher {
		
		public var video:IVideoVO;
		public var info:VideoInfoVO;
		
		private var loader:URLLoader;
		private var _stream:URLStream;
		private var _isDownloading:Boolean = false;
		private var _numChunks:uint = 0;
		private var _currentChunkIndex:int = -1;
		private var _status:String = VideoDownloadStatus.QUEUED;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function VideoDownloadVO(vod:IVideoVO) {
			trace("VideoDownloadVO ::: CONSTRUCTOR");
			video = vod;
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, loader_completeHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loader_ioErrorHandler);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, loader_securityErrorHandler);
			
			_stream = new URLStream();
			_stream.addEventListener(ProgressEvent.PROGRESS, stream_progressHandler);
			_stream.addEventListener(Event.COMPLETE, stream_completeHandler);
			_stream.addEventListener(IOErrorEvent.IO_ERROR, stream_ioErrorHandler);
			_stream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, stream_securityErrorHandler);
		}
		
		//------------------------------------
		// loader_completeHandler()
		//------------------------------------
		private function loader_completeHandler(event:Event):void {
			trace("VideoDownloadVO ::: loader_completeHandler");
			var loader:URLLoader = event.currentTarget as URLLoader;
			trace("    - loader: ", loader);
			trace("    - data: ", loader.data);
			var json:JSONDecoder = new JSONDecoder(loader.data, true);
			var value:Object = json.getValue();
			//trace(ObjectUtil.toString(value));
			info = new VideoInfoVO(value);
			trace(ObjectUtil.toString(info));
			_numChunks = info.length;
			dispatchEvent(new DownloadEvent(DownloadEvent.INFO_COMPLETE));
		}
		
		//------------------------------------
		// loader_ioErrorHandler()
		//------------------------------------
		private function loader_ioErrorHandler(event:IOErrorEvent):void {
			trace("VideoDownloadVO ::: loader_ioErrorHandler");
			Alert.show(event.text, "IO Error:");
		}
		
		//------------------------------------
		// loader_securityErrorHandler()
		//------------------------------------
		private function loader_securityErrorHandler(event:SecurityErrorEvent):void {
			trace("VideoDownloadVO ::: loader_securityErrorHandler");
			Alert.show(event.text, "Security Error:");
		}
		
		//------------------------------------
		// stream_progressHandler()
		//------------------------------------
		private function stream_progressHandler(event:ProgressEvent):void {
			//trace("VideoDownloadVO ::: stream_progressHandler");
			//var p:Number = (event.bytesLoaded / event.bytesTotal) * 100;
			//trace("    - progress: ", p);
			dispatchEvent(new DownloadEvent(DownloadEvent.CHUNK_PROGRESS));
		}
		
		//------------------------------------
		// stream_securityErrorHandler()
		//------------------------------------
		private function stream_securityErrorHandler(event:SecurityErrorEvent):void {
			trace("VideoDownloadVO ::: stream_securityErrorHandler");
			Alert.show(event.text, "Security Error:");
		}
		
		//------------------------------------
		// stream_ioErrorHandler()
		//------------------------------------
		private function stream_ioErrorHandler(event:IOErrorEvent):void {
			trace("VideoDownloadVO ::: stream_ioErrorHandler");
			Alert.show(event.text, "IO Error:");
		}
		
		//------------------------------------
		// stream_completeHandler()
		//------------------------------------
		private function stream_completeHandler(event:Event):void {
			trace("VideoDownloadVO ::: stream_completeHandler");
			// dispatch event before moving to the next chunk
			// so that manager has time to save the file to disk
			dispatchEvent(new DownloadEvent(DownloadEvent.CHUNK_COMPLETE));
			// load next chunk or finish if all chunks are downloaded
			if(_currentChunkIndex < numChunks-1) {
				_currentChunkIndex += 1;
				loadNextChunk();
			}else {
				// all chunks downloaded
				_isDownloading = false;
				_status = VideoDownloadStatus.COMPLETED;
				_stream.close();
				trace("    - connected: ", _stream.connected);
				dispatchEvent(new DownloadEvent(DownloadEvent.DOWNLOAD_END));
			}
		}
		
		//------------------------------------
		// loadNextChunk()
		//------------------------------------
		private function loadNextChunk():void {
			trace("VideoDownloadVO ::: loadNextChunk");
			var idx:int = _currentChunkIndex;
			var chunk:ChunkVO = info.chunks[idx] as ChunkVO;
			var url:String = chunk.url;
			_stream.load(new URLRequest(url));
			_isDownloading = true;
			_status = VideoDownloadStatus.DOWNLOADING;
			dispatchEvent(new DownloadEvent(DownloadEvent.CHUNK_START));
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Loads the vod's download info (list of flv's).
		 * When info is loaded a <code>Event.COMPLETE</code> event is dispatched.
		 * 
		 * @param value the url from which to retrieve the vod's info.
		 * 
		 */ 
		public function loadInfo(value:String):void { 
			trace("VideoDownloadVO ::: loadInfo");
			if(loader.data) return;
			trace("    - apiUrl: ", value);
			loader.load(new URLRequest(value));
		}
		
		/**
		 * Starts downloading chunks 1 by 1 untill all chunks have been downloaded. 
		 * <p>
		 * When a chunk has finished downloading, a <code>DownloadEvent.CHUNK_COMPLETE</code> event is dispatched.
		 * <br />
		 * When all chunks have finished downloading, a <code>DownloadEvent.DOWNLOAD_COMPLETE</code> is dispatched.
		 * </p>
		 * @throws Error Throws an error when chunk info is not available (yet).
		 */ 
		public function loadChunks():void {
			trace("VideoDownloadVO ::: loadChunks");
			if(_isDownloading) return;
			if(!info) throw new Error("Cannot start downloading as info is not available.");
			_currentChunkIndex = 0;
			dispatchEvent(new DownloadEvent(DownloadEvent.DOWNLOAD_START));
			loadNextChunk();
		}
		
		/**
		 * 
		 */ 
		public function stopChunks():void {
			trace("VideoDownloadVO ::: stopChunks");
			trace("    - connected: ", _stream.connected);
			trace("    - isDownloading: ", _isDownloading);
			// if the stream has already completed (all chunks are downloaded)
			// don't do anything - especially leave the status as completed
			if(_isDownloading) {
				_stream.close();
				_isDownloading = false;
				_currentChunkIndex = -1;
				_status = VideoDownloadStatus.QUEUED;
				dispatchEvent(new DownloadEvent(DownloadEvent.DOWNLOAD_CANCELED));
			}
		}
		
		//=====================================
		//
		// IMPLICIT GETTER/SETTERS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function get isDownloading():Boolean {
			trace("    - stream connected: ", _stream.connected);
			return _isDownloading;
		}
		
		/**
		 * The current vod download status.
		 * <p>
		 * The value is one the <code>VideoDownloadStatus</code> static properties.
		 * <li><code>VideoDownloadStatus.QUEUED</code>: VOD is in queue. No chunks have been downloaded yet.</li>
		 * <li><code>VideoDownloadStatus.DOWNLOADING</code>: VOD is currently downloading a chunk.</li>
		 * <li><code>VideoDownloadStatus.COMPLETED</code>: VOD has finished downloading all chunks.</li>
		 * </p>
		 * The initial value is <code>VideoDownloadStatus.QUEUED</code>.
		 * 
		 * @see be.gip.twitch.multiviewer.vo.VideoDownloadStatus
		 */ 
		public function get status():String {
			return _status;
		}
		
		/**
		 * The (zero-based) index of the current chunk being downloaded.
		 * The value is <code>-1</code> if downloading hasn't started yet.
		 */ 
		public function get currentChunkIndex():int {
			return _currentChunkIndex;
		}
		
		/**
		 * The number of <code>ChunkVO</code>'s in the <code>info</code> property for this vod.
		 */ 
		public function get numChunks():uint {
			return _numChunks;
		}
		
		/**
		 * 
		 */ 
		public function get stream():URLStream {
			return _stream;
		}
		
		/**
		 * The relative path for this vod, consisting of the vod channel name and vod ID.
		 * <p>
		 * When the vod info is not loaded yet, the value is an empty string.
		 * </p>
		 */
		public function get relativePath():String {
			var str:String = "";
			if(info) str = info.channelName + "/" + video.videoId;
			return str;
		}
		
	}
	
}
