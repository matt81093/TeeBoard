package be.gip.teeboard.vo {
	
	[RemoteClass(alias="be.gip.teeboard.vo.SettingsVO")]
	[Bindable]
	/**
	 * App settings stored in SharedObject
	 */ 
	public class SettingsVO {
		
		/**
		 * The selected platform containing a list of games for auto completion.
		 * <p>
		 * Used for the <code>AutoComplete TextInput</code> control.
		 * </p>
		 */ 
		public var selectedPlatform:String = "";
		
		/**
		 * Indicates if viewer stats are enabled.
		 */ 
		public var viewerStatsEnabled:Boolean = false;
		
	}
	
}
