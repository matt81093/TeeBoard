package be.gip.teeboard.vo {
	
	[Bindable]
	public class StreamVO {
		
		private var data:Object;
		private var _channel:ChannelVO;
		
		public function StreamVO(value:Object){
			data = value;
			// when a stream is offline, value will be null
			if(value != null) {
				if(value.channel) _channel = new ChannelVO(value.channel);
			}
		}
		
		private function getProperty(value:String):String {
			if(data && data.hasOwnProperty(value)) {
				return data[value];
			}
			return "";
		}
		
		public function get channel():ChannelVO {
			return _channel;
		}
		
		public function get streamId():int {
			if(data && data.hasOwnProperty("_id")) {
				return data._id;
			}
			return -1;
		}
		
		public function get game():String {
			return getProperty("game");
		}
		
		public function get links():Object {
			if(data && data.hasOwnProperty("_links")) {
				return data._links;
			}
			return null;
		}
		
		public function get preview():Object {
			if(data && data.hasOwnProperty("preview")) {
				return data.preview;
			}
			return null;
		}
		
		public function get numViewers():uint {
			if(data && data.hasOwnProperty("viewers")) {
				return data.viewers;
			}
			return 0;
		}
		
	}
	
}
/**
 *     [0] (Object)#3
 *       _id = 5952323088
 *       _links = (Object)#4
 *         self = "https://api.twitch.tv/kraken/streams/smitegame"
 *       channel = (Object)#5
 *         _id = 31500812
 *         _links = (Object)#6
 *           chat = "https://api.twitch.tv/kraken/chat/smitegame"
 *           commercial = "https://api.twitch.tv/kraken/channels/smitegame/commercial"
 *           editors = "https://api.twitch.tv/kraken/channels/smitegame/editors"
 *           features = "https://api.twitch.tv/kraken/channels/smitegame/features"
 *           follows = "https://api.twitch.tv/kraken/channels/smitegame/follows"
 *           self = "https://api.twitch.tv/kraken/channels/smitegame"
 *           stream_key = "https://api.twitch.tv/kraken/channels/smitegame/stream_key"
 *           subscriptions = "https://api.twitch.tv/kraken/channels/smitegame/subscriptions"
 *           teams = "https://api.twitch.tv/kraken/channels/smitegame/teams"
 *           videos = "https://api.twitch.tv/kraken/channels/smitegame/videos"
 *         background = "http://static-cdn.jtvnw.net/jtv_user_pictures/smitegame-channel_background_image-fbef770926d43493.png"
 *         banner = "http://static-cdn.jtvnw.net/jtv_user_pictures/smitegame-channel_header_image-0bd76e5eb1ad0ca9-640x125.png"
 *         created_at = "2012-06-20T21:05:55Z"
 *         delay = 0
 *         display_name = "Smitegame"
 *         game = "Smite"
 *         logo = "http://static-cdn.jtvnw.net/jtv_user_pictures/smitegame-profile_image-e01dc00990c02fbe-300x300.png"
 *         mature = false
 *         name = "smitegame"
 *         status = "SMITE w/ Drydork - 6 hour flower power shower with flour"
 *         updated_at = "2013-06-21T02:35:36Z"
 *         url = "http://www.twitch.tv/smitegame"
 *         video_banner = "http://static-cdn.jtvnw.net/jtv_user_pictures/smitegame-channel_offline_image-41d2d00e18165223-640x360.png"
 *       game = "Smite"
 *       preview = (Object)#7
 *         large = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-640x400.jpg"
 *         medium = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-320x200.jpg"
 *         small = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-80x50.jpg"
 *         template = "http://static-cdn.jtvnw.net/previews-ttv/live_user_smitegame-{width}x{height}.jpg"
 *       viewers = 1389
 */ 
