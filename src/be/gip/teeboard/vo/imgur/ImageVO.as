package be.gip.teeboard.vo.imgur {
	
	[Bindable]
	public class ImageVO {
		
		public var animated:Boolean = false;
		public var bandwidth:Number;
		public var datetime:Number;
		public var description:String;
		public var width:Number;
		public var height:Number;
		public var id:String;
		public var link:String;
		public var nsfw:Object;
		public var section:Object
		public var size:Number;
		public var title:String;
		public var type:String;
		public var views:Number;
		
	}
	
}
/**
 *      images = (Array)#2
 *       [0] (Object)#3
 *         animated = false
 *         bandwidth = 5015
 *         datetime = 1398515436
 *         description = (null)
 *         favorite = false
 *         height = 60
 *         id = "0BRPpl9"
 *         link = "http://i.imgur.com/0BRPpl9.png"
 *         nsfw = (null)
 *         section = (null)
 *         size = 5015
 *         title = (null)
 *         type = "image/png"
 *         views = 1
 *         width = 320
 */ 