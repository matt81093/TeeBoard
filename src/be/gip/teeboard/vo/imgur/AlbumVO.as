package be.gip.teeboard.vo.imgur {
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class AlbumVO {
		
		
		public var accountUrl:String;
		public var cover:String;
		public var coverHeight:Number;
		public var coverWidht:Number;
		public var datetime:Number;
		public var description:String;
		public var favorite:Boolean;
		public var id:String;
		public var link:String;
		public var title:String;
		public var views:Number;
		public var privacy:String = "public";
		
		/**
		 * Collection of images (ImageVO).
		 */ 
		public var images:ArrayCollection;
		
	}
	
}

/**
 * (Object)#0
 *   data = (Object)#1
 *     account_url = "deezjavu"
 *     cover = "1iIxdcU"
 *     cover_height = 50
 *     cover_width = 320
 *     datetime = 1398512672
 *     description = "A few Twitch channel panel examples for use in TeeBoard.
 * Images should be 320x50 up to 320x100, png format."
 *     favorite = false
 *     id = "2mQPO"
 *     images = (Array)#2
 *       [0] (Object)#3
 *         animated = false
 *         bandwidth = 5015
 *         datetime = 1398515436
 *         description = (null)
 *         favorite = false
 *         height = 60
 *         id = "0BRPpl9"
 *         link = "http://i.imgur.com/0BRPpl9.png"
 *         nsfw = (null)
 *         section = (null)
 *         size = 5015
 *         title = (null)
 *         type = "image/png"
 *         views = 1
 *         width = 320
 *     images_count = 16
 *     layout = "blog"
 *     link = "http://imgur.com/a/2mQPO"
 *     nsfw = (null)
 *     privacy = "public"
 *     section = (null)
 *     title = "Channel Panels"
 *     views = 2
 *   status = 200
 *   success = true
 */ 