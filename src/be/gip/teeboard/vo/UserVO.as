package be.gip.teeboard.vo {
	
	import com.adobe.utils.DateUtil;
	
	[Bindable]
	/**
	 * Used by <code>TwitchEvent.GET_USER</code> and <code>TwitchEvent.GET_CHANNEL_EDITORS</code>.
	 * 
	 */ 
	public class UserVO {
		
		private var data:Object;
		
		public function UserVO(value:Object) {
			data = value;
		}
		
		private function getProperty(value:String):String {
			if(data && data.hasOwnProperty(value)) {
				return data[value];
			}
			return "";
		}
		
		/**
		 * 
		 */ 
		public function get userId():uint {
			var uid:uint;
			if(data.hasOwnProperty("_id")) {
				uid = data._id;
			}
			return uid;
		}
		
		/**
		 * 
		 */ 
		public function get links():Object {
			if(data && data.hasOwnProperty("_links")) {
				return data._links;
			}
			return null;
		}
		
		/**
		 * 
		 */ 
		public function get bio():String {
			return getProperty("bio");
		}
		
		/**
		 * 
		 */ 
		public function get createdAt():Date {
			if(data && data.hasOwnProperty("created_at")) {
				var d:Date = DateUtil.parseW3CDTF(data.created_at);
				return d;
			}
			return null;
		}
		
		/**
		 * 
		 */ 
		public function get displayName():String {
			return getProperty("display_name");
		}
		
		/**
		 * 
		 */ 
		public function get email():String {
			return getProperty("email");
		}
		
		/**
		 * 
		 */ 
		public function get logo():String {
			return getProperty("logo");
		}
		
		/**
		 * 
		 */ 
		public function get name():String {
			return getProperty("name");
		}
		
		/**
		 * 
		 */ 
		public function get updatedAt():Date {
			if(data && data.hasOwnProperty("updated_at")) {
				var d:Date = DateUtil.parseW3CDTF(data.updated_at);
				return d;
			}
			return null;
		}
		
		/**
		 * 
		 */ 
		public function get isPartnered():Boolean {
			if(data && data.hasOwnProperty("partnered")) {
				return data.partnered;
			}
			return false;
		}
		
		/**
		 * 
		 */ 
		public function get isStaff():Boolean {
			if(data && data.hasOwnProperty("staff")) {
				return data.staff;
			}
			return false;
		}
		
	}
	
}

/**
 * (Object)#0
 *   _id = 23773615
 *   _links = (Object)#1
 *     self = "https://api.twitch.tv/kraken/users/deezjavu"
 *   bio = (null)
 *   created_at = "2011-07-31T10:48:20Z"
 *   display_name = "DeezjaVu"
 *   email = "gip.deezjavu@gmail.com"
 *   logo = "http://static-cdn.jtvnw.net/jtv_user_pictures/deezjavu-profile_image-1460b1a19850ba30-300x300.jpeg"
 *   name = "deezjavu"
 *   partnered = false
 *   staff = false
 *   updated_at = "2013-06-27T19:00:24Z"
 * 
 */

