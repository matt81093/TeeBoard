package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.notifications.DonationServiceDTO;
	import be.gip.teeboard.dto.widgets.DonationTrackerDTO;
	import be.gip.teeboard.net.api.Donations;
	import be.gip.teeboard.vo.notifications.DonationTrackerVO;
	import be.gip.teeboard.vo.notifications.IDonationVO;
	import be.gip.teeboard.vo.notifications.ImRaising2VO;
	import be.gip.teeboard.vo.notifications.StreamTipVO;
	import be.gip.teeboard.vo.notifications.TwitchAlertsVO;
	import be.gip.teeboard.vo.notifications.TwitchPlusVO;
	
	import flash.events.EventDispatcher;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.utils.Base64Encoder;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class DonationsModel extends EventDispatcher {
		
		private static var INSTANCE:DonationsModel;
		
		/**
		 * This property is set when something goes wrong loading donation tracker data
		 * or when a donation tracker is invalid.
		 */ 
		public var validationError:Object;
		
		
		private var _accounts:ArrayCollection;
		private var _selectedAccount:DonationServiceDTO;
		
		private var _latestDonations:ArrayCollection;
		private var _topDonations:ArrayCollection;
		
		private var _newDonations:ArrayCollection;
		private var lastDonateDate:Date;
		
		
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/donations/settings", "/");
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		public function DonationsModel() {
			trace("DonationsModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one DonationsModel instance allowed. " +
					"To access the Singleton instance, DonationsModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():DonationsModel {
			if(!INSTANCE) INSTANCE = new DonationsModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("DonationsModel ::: initModel");
			
			_latestDonations = new ArrayCollection();
			_topDonations = new ArrayCollection();
			_newDonations = new ArrayCollection();
			
			_accounts = new ArrayCollection();
			
			if(so.data.hasOwnProperty("accounts") && so.data.accounts != null) {
				//_selectedAccount = so.data.selectedAccount;
				_accounts.source = so.data.accounts;
			}else {
				so.data.accounts = new Array();
				so.flush();
			}
			
			var oldSelected:DonationTrackerDTO = so.data.selectedAccount as DonationTrackerDTO;
			var dto:DonationTrackerDTO;
			var serviceDTO:DonationServiceDTO;
			var len:int = _accounts.length;
			
			// if selected account is old DonationTrackerDTO, 
			// migrate to new DonationServiceDTO
			if(oldSelected != null) {
				trace("    - migrating DonationTrackerDTO to DonationServiceDTO");
				_selectedAccount = new DonationServiceDTO();
				_selectedAccount.channel = oldSelected.channel;
				_selectedAccount.clientID = oldSelected.channel;
				_selectedAccount.key = oldSelected.key;
				if(oldSelected.tracker == "streamtip") {
					_selectedAccount.service = Donations.STREAM_TIP;
				}else if(oldSelected.tracker == "imraising2") {
					_selectedAccount.service = Donations.IM_RAISING2;
				}else {
					_selectedAccount = null;
				}
				so.data.selectedAccount = _selectedAccount;
				so.flush();
			}else if(so.data.hasOwnProperty("selectedAccount")) {
				_selectedAccount = so.data.selectedAccount;
			}
			
			trace("SELECTED DONATION ACCOUNT");
			trace(ObjectUtil.toString(_selectedAccount));
			
			// remove invalid donation services
			// and migrate old DonationTrackerDTO to new DonationServiceDTO
			for(var i:int=len; i!=0; i--) {
				dto = _accounts.getItemAt(i-1) as DonationTrackerDTO;
				
				trace(ObjectUtil.toString(dto));
				
				if(dto && (dto.tracker == Donations.IM_RAISING || dto.tracker == Donations.STREAM_DONATIONS)) {
					trace("    - found outdated donation service: ", dto.tracker);
					_accounts.removeItemAt(i-1);
				}else if(dto && dto is DonationTrackerDTO) {
					trace("    - found old DonationTrackerDTO");
					serviceDTO = new DonationServiceDTO();
					serviceDTO.channel = dto.channel;
					serviceDTO.clientID = dto.channel;
					serviceDTO.key = dto.key;
					if(dto.tracker == "imraising2") {
						serviceDTO.service = Donations.IM_RAISING2;
					}else if(dto.tracker == "streamtip") {
						serviceDTO.service = Donations.STREAM_TIP;
					}
					_accounts.setItemAt(serviceDTO, i-1);
				}
				
			}
			
			if(_accounts.length != len) {
				trace("    - saving modified donation accounts");
				so.data.accounts = _accounts.source;
				so.flush();
			}
			
			var nd:Date = new Date();
			nd.date = 17;
			_selectedAccount.expires = nd;
			
			
			lastDonateDate = new Date();
			
			trace("DONATION ACCOUNTS");
			trace(ObjectUtil.toString(_accounts));
			trace("==========================================");
			
		}
		
		//------------------------------------
		// donationIsNew()
		//------------------------------------
		private function donationIsNew(vo:IDonationVO):Boolean {
			//trace("DonationsModel ::: donationIsNew");
			var isNew:Boolean = false;
			//trace("    - donator name: ", vo.name);
			//trace("    - last date: ", lastDonateDate);
			//trace("    - donation created: ", vo.date);
			//trace("    - is new: ", (vo.date.time > lastDonateDate.time));
			//trace("----------------------------------");
			if(vo.date.time > lastDonateDate.time) {
				isNew = true;
			}
			return isNew;
		}
		
		//------------------------------------
		// getDonationVO()
		//------------------------------------
		private function getDonationVO():IDonationVO {
			//trace("DonationsModel ::: getDonationVO");
			var vo:IDonationVO;
			var tracker:String = selectedAccount.service;
			if(tracker == Donations.DONATION_TRACKER) {
				vo = new DonationTrackerVO();
			}else if(tracker == Donations.IM_RAISING2) {
				vo = new ImRaising2VO();
			}else if(tracker == Donations.STREAM_TIP) {
				vo = new StreamTipVO();
			}else if(tracker == Donations.TWITCH_ALERTS) {
				vo = new TwitchAlertsVO();
			}else if(tracker == Donations.TWITCH_PLUS) {
				vo = new TwitchPlusVO();
			}
			return vo;
		}
		
		//------------------------------------
		// dispatchChangeEvent()
		//------------------------------------
		private function dispatchChangeEvent(value:String):void {
			trace("DonationsModel ::: dispatchChangeEvent");
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = value;
			dispatchEvent(propEvent);
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Adds a verified donation tracker account to the list of donation trackers.
		 * 
		 */ 
		public function addServiceAccount(dto:DonationServiceDTO):void {
			trace("DonationsModel ::: addServiceAccount");
			
			_accounts.addItem(dto);
			
			so.data.accounts = _accounts.source;
			
			// set new service account as the active one
			// this will load the data for the service
			selectedAccount = dto;
			so.data.selectedAccount = dto;
			so.flush();
			
			dispatchChangeEvent("accounts");
		}
		
		/**
		 * 
		 */ 
		public function updateServiceAccount(dto:DonationServiceDTO):void {
			trace("DonationsModel ::: updateDonationServiceAccount");
			var idx:int = getDonationServiceIndex(dto);
			trace("    - index: ", idx);
			if(idx != -1) {
				_accounts.setItemAt(dto, idx);
				so.data.accounts = _accounts.toArray();
				so.flush();
				if(_selectedAccount) {
					if(dto.channel == _selectedAccount.channel && dto.service == _selectedAccount.service) {
						_selectedAccount = dto;
					}
				}
			}else {
				trace("    - unable to update donation service", dto.channel, dto.service);
			}
		}
		
		/**
		 * 
		 */ 
		public function removeServiceAccount(dto:DonationServiceDTO):void {
			trace("DonationsModel ::: removeDonationServiceAccount");
			var idx:int = getDonationServiceIndex(dto);
			var item:DonationServiceDTO
			if(idx != -1) {
				item = _accounts.removeItemAt(idx) as DonationServiceDTO;
				if(item.channel == _selectedAccount.channel && item.service == _selectedAccount.service) {
					_latestDonations = new ArrayCollection();
					if(_accounts.length != 0) {
						trace("    - setting first service as selected account");
						selectedAccount = _accounts.getItemAt(0) as DonationServiceDTO;
					}else {
						trace("    - clearing selected service account");
						selectedAccount = null;
					}
				}
				dispatchChangeEvent("accounts");
			}
		}
		
		/**
		 * Processes and stores the <code>value</code> in model.
		 * 
		 * @param value the data (donations) to store in model
		 * 
		 * @param type the donation account type: 
		 * <li>streamdonations: <code>DonationsModel.TRACKER_STREAMDONATIONS</code></li>
		 * <li>imraising: <code>DonationsModel.TRACKER_IMRAISING</code></li>
		 * <li>donationtracker: <code>DonationsModel.TRACKER_DONATIONTRACKER</code></li>
		 */
		public function setDonations(result:Object):void {
			trace("DonationsModel ::: setDonations");
			
			//trace(ObjectUtil.toString(result));
			
			var recent:Array;
			var top:Array;
			
			var tracker:String = selectedAccount.service;
			
			// check the selected account tracker and parse data accordingly
			if(tracker == Donations.DONATION_TRACKER) {
				recent = result.donations;
			}else if(tracker == Donations.IM_RAISING2) {
				recent = result as Array;
			}else if(tracker == Donations.STREAM_TIP) {
				recent = result.tips;
			}else if(tracker == Donations.TWITCH_ALERTS) {
				recent = result.donations;
			}else if(tracker == Donations.TWITCH_PLUS) {
				recent = result.donations;
			}else {
				return;
			}
			
			var recent_ac:ArrayCollection = new ArrayCollection();
			var top_ac:ArrayCollection = new ArrayCollection();
			var new_ac:ArrayCollection = new ArrayCollection();
			
			var len:uint;
			var i:uint;
			var vo:IDonationVO;
			
			// may not have recent donations, so make sure to check
			if(recent != null) {
				len = recent.length;
				for(i=0; i<len; i++) {
					vo = getDonationVO();
					vo.setData(recent[i]);
					recent_ac.addItem(vo);
					if(donationIsNew(vo)) {
						new_ac.addItem(vo);
					}
				}
			}
			
			trace("RECENT DONATIONS");
			trace("    - length: ", recent_ac.length);
			trace(ObjectUtil.toString(recent_ac));
			_latestDonations = recent_ac;
			
			dispatchChangeEvent("latestDonations");
			
			//trace("TOP DONATIONS");
			//trace("    - length: ", top_ac.length);
			//trace(ObjectUtil.toString(top_ac));
			_topDonations = top_ac;
			
			dispatchChangeEvent("topDonations");
			
			//trace("NEW DONATIONS");
			//trace("    - length: ", new_ac.length);
			//trace(ObjectUtil.toString(top_ac));
			_newDonations = new_ac;
			
			// Set the last checked date only if we have new donations.
			// This is to prevent new donations from not being noticed as they may not be added immediately
			// but the actual date of the donation is always correct
			
			if(_newDonations.length != 0) {
				lastDonateDate = new Date();
				// Dispatch new donations event, so that the widget can do it's thing. 
				// This event is (and should be) handled by the main application 
				// as that is the only view that is guaranteed to be listening
				dispatchChangeEvent("newDonations");
			}
			
		}
		
		/**
		 * Returns the index of the donation tracker object (<code>DonationTrackerDTO</code>).
		 * <p>Returns -1 if no entry for the donation tracker channel exists</p>
		 */ 
		public function getDonationServiceIndex(value:DonationServiceDTO):int {
			trace("DonationsModel ::: getDonationServiceIndex");
			var len:int = _accounts.length;
			var dto:DonationServiceDTO;
			var idx:int = -1;
			
			if(value == null) {
				return idx;
			}
			
			trace("    - channel: ", value.channel);
			trace("    - tracker: ", value.service);
			
			//trace(ObjectUtil.toString(_accounts));
			
			for(var i:int=0; i<len; i++) {
				dto = _accounts.getItemAt(i) as DonationServiceDTO;
				//trace(ObjectUtil.toString(dto));
				//trace("--------------------");
				if(dto.channel == value.channel && dto.service == value.service) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
		/**
		 * 
		 */ 
		public function getImRaisinHeaders(key:String):Object {
			trace("DonationsModel ::: getImRaisinHeaders");
			trace("    - argument key: ", key);
			var head:Object = new Object();
			head["Content-Type"] = "application/json";
			//head["Accept"] = "application/json";
			//head["Authorization"] = "APIKey apikey=qRKR0r35wZzUuBBLZQNuig";
			head["Authorization"] = "APIKey apikey=" + key;
			return head;
		}
		
		/**
		 * 
		 */ 
		public function getTwitchPlusHeaders(dto:DonationServiceDTO):Object {
			trace("DonationsModel ::: getTwitchPlusHeaders");
			trace("    - client id: ", dto.clientID);
			trace("    - key: ", dto.key);
			var bEnc:Base64Encoder = new Base64Encoder();
			bEnc.insertNewLines = false;
			bEnc.encodeUTFBytes(dto.clientID + ":" + dto.key);
			var auth:String = bEnc.toString();
			trace("    - authorization: ", auth);
			var head:Object = new Object();
			//head["Content-Type"] = "application/json";
			head["Authorization"] = "Basic " + auth;
			return head;
		}
		
		/**
		 * 
		 */ 
		public function hasTwitchPlusExpired(value:Date):Boolean {
			trace("DonationsModel ::: hasTwitchPlusExpired");
			var expired:Boolean = true;
			var ms:Number = new Date().time;
			var exp:Number = value.time;
			trace("    - now: ", new Date());
			trace("    - now ms: ", ms);
			trace("    - expired:", value);
			trace("    - expires ms: ", exp);
			if(ms < exp) {
				expired = false;
			}
			return expired;
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		public function get accounts():ArrayCollection {
			return _accounts;
		}
		
		/**
		 * 
		 */ 
		public function get latestDonations():ArrayCollection {
			return _latestDonations;
		}
		
		/**
		 * 
		 */ 
		public function get topDonations():ArrayCollection {
			return _topDonations;
		}

		/**
		 * 
		 */ 
		public function get newDonations():ArrayCollection {
			return _newDonations;
		}
		
		/**
		 * The currently active (selected) donation tracker account.
		 */
		public function get selectedAccount():DonationServiceDTO {
			return _selectedAccount;
		}
		public function set selectedAccount(value:DonationServiceDTO):void {
			_selectedAccount = value;
			so.data.selectedAccount = _selectedAccount;
			so.flush();
		}
		
	}
	
}
