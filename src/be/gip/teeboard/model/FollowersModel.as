package be.gip.teeboard.model {
	
	import be.gip.teeboard.vo.notifications.FollowerVO;
	
	import flash.events.EventDispatcher;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	/**
	 * Model for Twitch data related to a channel's followers.
	 * <p>
	 * The <code>FollowersModel</code> only handles data from Twitch (received via the Twitch API).
	 * It does not handle alerts / notifications. That is handled by the <code>WidgetsModel</code>
	 * </p>
	 */ 
	public class FollowersModel extends EventDispatcher {
		
		private static var INSTANCE:FollowersModel;
		
		private var _numFollowers:Number;
		private var _followers:ArrayCollection;
		private var _newFollowers:ArrayCollection;
		
		/**
		 * List of names of banned followers.
		 * This list is stored in SharedObject as an Array of Strings
		 */ 
		private var _bannedFollowers:ArrayCollection;
		
		/**
		 * New followers are added to this list over time.
		 * 
		 */ 
		private var _followersBlacklist:ArrayCollection;
		
		// shared object used by FollowersModel, NotificationsModel and WidgetsModel
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/widgets/settings", "/");
		
		private var lastFollowDate:Date;
		
		private var testFollowers:Boolean = false;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		public function FollowersModel() {
			trace("FollowersModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one FollowersModel instance allowed. " +
					"To access the Singleton instance, FollowersModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():FollowersModel {
			if(!INSTANCE) INSTANCE = new FollowersModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("FollowersModel ::: initModel");
			_followers = new ArrayCollection();
			_newFollowers = new ArrayCollection();
			_followersBlacklist = new ArrayCollection();
			
			_bannedFollowers = new ArrayCollection();
			
			if(so.data.hasOwnProperty("bannedFollowers") && so.data.bannedFollowers != null) {
				_bannedFollowers.source = so.data.bannedFollowers;
			}
			
			trace("BANNED FOLLOWERS");
			trace("===============================================");
			trace(ObjectUtil.toString(_bannedFollowers));
			trace("===============================================");
			
			lastFollowDate = new Date();
			
		}
		
		//------------------------------------
		// followerIsNew()
		//------------------------------------
		private function followerIsNew(vo:FollowerVO):Boolean {
			//trace("FollowersModel ::: followerIsNew");
			var isNew:Boolean = false;
			//trace("    - follower name: ", vo.user.displayName);
			//trace("    - last date: ", lastFollowDate);
			//trace("    - follower created: ", vo.createdAt);
			//trace("    - is new: ", (vo.createdAt.time > lastFollowDate.time));
			//trace("----------------------------------");
			if(vo.createdAt.time > lastFollowDate.time) {
				isNew = true;
			}
			return isNew;
		}
		
		//------------------------------------
		// followerIsNew()
		//------------------------------------
		private function followerIsBanned(vo:FollowerVO):Boolean {
			//trace("FollowersModel ::: followerIsNew");
			var isBanned:Boolean = false;
			var len:int = _bannedFollowers.length;
			var n:String;
			trace("    - follower name: ", vo.user.displayName);
			for(var i:int=0; i<len; i++) {
				n = _bannedFollowers.getItemAt(i) as String;
				if(vo.user.name.toLowerCase() == n.toLowerCase()) {
					trace("    - banned user found: ", n);
					isBanned = true;
					break;
				}
			}
			//trace("----------------------------------");
			return isBanned;
		}
		
		//------------------------------------
		// followerIsBlacklisted()
		//------------------------------------
		private function followerIsBlacklisted(vo:FollowerVO):Boolean {
			var exists:Boolean = false;
			var len:int = _followersBlacklist.length;
			var n:String;
			for(var i:int=0; i<len; i++) {
				n = _followersBlacklist.getItemAt(i) as String;
				if(n.toLowerCase() == vo.user.name.toLowerCase()) {
					exists = true;
					break;
				}
			}
			return exists;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Processes and stores the <code>value</code> in model.
		 * 
		 * @param value the data (channel followers) to store in model
		 */
		public function setFollowers(value:Object):void {
			trace("FollowersModel ::: setFollowers");
			var f:Array = value.follows;
			var len:uint = f.length;
			var ac:ArrayCollection = new ArrayCollection();
			var new_ac:ArrayCollection = new ArrayCollection();
			var vo:FollowerVO;
			
			var isBanned:Boolean = false;;
			var isNew:Boolean = false;
			var isListed:Boolean = false;
			
			for(var i:uint=0; i<len; i++) {
				vo = new FollowerVO(f[i]);
				
				isBanned = followerIsBanned(vo);
				if(!isBanned) {
					ac.addItem(vo);
				}
				
				isNew = followerIsNew(vo);
				isListed = followerIsBlacklisted(vo);
				if(isNew && !isListed && !isBanned) {
					new_ac.addItem(vo);
					_followersBlacklist.addItem(vo.user.name);
				}
				
				trace("    - name: ", vo.user.name);
				trace("    - is new: ", isNew);
				trace("    - is listed: ", isListed);
				trace("    - is banned: ", isBanned);
				trace("----------------------------------");
				
			}
			
			//testFollowers = true;
			if(testFollowers) {
				testFollowers = false;
				new_ac.addItem(ac.getItemAt(0));
				new_ac.addItem(ac.getItemAt(1));
				new_ac.addItem(ac.getItemAt(2));
			}
			
			trace("    - new followers:");
			trace(ObjectUtil.toString(new_ac));
			trace("----------------------------------");
			
			trace("    - blacklisted: ");
			trace(ObjectUtil.toString(_followersBlacklist));
			trace("----------------------------------");
			
			_followers = ac;
			//
			_newFollowers = new_ac;
			//trace(ObjectUtil.toString(new_ac));
			_numFollowers = value._total;
			
			// Set the last checked date only if we have new followers.
			// This is to prevent new followers from not being noticed as the twitch server doesn't always add new followers immediately
			// but it does register the actual date they followed correctly
			
			var nEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			nEvent.property = "newFollowers";
			
			if(_newFollowers.length != 0) {
				lastFollowDate = new Date();
				// dispatch new followers event, so that the widget can do it's thing
				dispatchEvent(nEvent);
			}
			
			// dispatch event to listeners
			var fEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			fEvent.property = "followers";
			dispatchEvent(fEvent);
		}
		
		/**
		 * 
		 */ 
		public function banFollower(name:String):void {
			trace("FollowersModel ::: banFollower");
			_bannedFollowers.addItem(name);
			so.data.bannedFollowers = _bannedFollowers.source;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function banFollowers(value:Array):void {
			trace("FollowersModel ::: banFollowers");
			trace(value.toString());
			var len:uint = value.length;
			var n:String;
			var idx:int;
			for(var i:int=0; i<len; i++) {
				n = value[i];
				if(n != "") {
					idx = _bannedFollowers.getItemIndex(n);
					if(idx == -1) _bannedFollowers.addItem(n);
				}
			}
			so.data.bannedFollowers = _bannedFollowers.source;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function removeBannedFollowerAt(idx:int):void {
			trace("FollowersModel ::: removeBannedFollowerAt");
			_bannedFollowers.removeItemAt(idx);
			so.data.bannedFollowers = _bannedFollowers.source;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function removeAllBannedFollowers():void {
			trace("FollowersModel ::: removeAllBannedFollowers");
			_bannedFollowers.removeAll();
			so.data.bannedFollowers = null;
			so.flush();
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		public function get numFollowers():Object {
			return _numFollowers;
		}
		
		public function get followers():ArrayCollection {
			return _followers;
		}
		
		public function get newFollowers():ArrayCollection {
			return _newFollowers;
		}
		
		/**
		 * A list of names of the last 5 followers.
		 */ 
		public function get latestFollowerNames():Array {
			var len:int = Math.min(5, _followers.length);
			var vo:FollowerVO;
			var n:String;
			var names:Array = new Array();
			for(var i:int=0; i<len; i++) {
				vo = _followers.getItemAt(i) as FollowerVO;
				n = vo.user.displayName;
				names.push(n);
			}
			return names;
		}
		
		/**
		 * A list of names of the banned followers.
		 * <p>Names in the list are ignored and not displayed in TeeBoard.</p>
		 */ 
		public function get bannedFollowers():ArrayCollection {
			return _bannedFollowers;
		}
		
	}
	
}
