package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.datastore.ChannelDTO;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class DataStoreModel extends EventDispatcher {
		
		private static var INSTANCE:DataStoreModel;
		
		
		
		//-----------------------------------------------
		//
		// PRIVATE PROPERTIES
		//
		//-----------------------------------------------
		
		private var _channels:ArrayCollection;
		
		//-----------------------------------------------
		//
		// PUBLIC PROPERTIES
		//
		//-----------------------------------------------
		
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		public function DataStoreModel() {
			trace("DataStoreModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one DataStoreModel instance allowed. " +
					"To access the Singleton instance, DataStoreModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():DataStoreModel {
			if(!INSTANCE) INSTANCE = new DataStoreModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("DataStoreModel ::: initModel");
			//_channels = new ArrayCollection();
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function setChannels(data:Array):void {
			trace("DataStoreModel ::: setChannels");
			_channels = new ArrayCollection(data);
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "channels";
			dispatchEvent(propEvent);
		}
		
		/**
		 * 
		 */ 
		public function getChannelByName(value:String):ChannelDTO {
			trace("DataStoreModel ::: getChannelByName");
			if(_channels == null) return null;
			var dto:ChannelDTO;
			var len:int = _channels.length;
			var ch:ChannelDTO;
			for(var i:int=0; i<len; i++) {
				ch = _channels.getItemAt(i) as ChannelDTO;
				if(value == ch.name) {
					dto = ch;
					break;
				}
			}
			return dto;
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		
		/**
		 * List of channels stored in database.
		 */ 
		public function get channels():ArrayCollection {
			return _channels;
		}
		
	}
	
}
