package be.gip.teeboard.model {
	
	import be.gip.teeboard.vo.IVideoVO;
	import be.gip.teeboard.vo.VideoBroadcastVO;
	import be.gip.teeboard.vo.VideoHighlightVO;
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class VideosModel extends EventDispatcher {
		
		private static var INSTANCE:VideosModel;
		
		public static var VOD_BROADCASTS:String = "vodBroadcasts";
		public static var VOD_HIGHLIGHTS:String = "vodHighlights";
		
		public var broadcasts:ArrayCollection;
		public var highlights:ArrayCollection;
		public var broadcastsTotal:uint = 0;
		public var broadcastsLinks:Object;
		public var highlightsTotal:uint = 0;
		public var highlightsLinks:Object;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function VideosModel() {
			trace("VideosModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one VideosModel instance allowed. " +
					"To access the Singleton instance, VideosModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():VideosModel {
			if(!INSTANCE) INSTANCE = new VideosModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("VideosModel ::: initModel");
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 * 
		 */ 
		public function setBroadcasts(value:Object):void {
			trace("VideosModel ::: setBroadcasts");
			var vids:Array = value.videos;
			var len:uint = vids.length;
			var vo:IVideoVO;
			// create a temp collection as otherwise
			// the videos collection triggers an event every time
			// an item is added to it
			var ac:ArrayCollection = new ArrayCollection();
			broadcastsTotal = value._total;
			broadcastsLinks = value._links;
			for(var i:uint=0; i<len; i++) {
				vo = new VideoBroadcastVO(vids[i]);
				ac.addItem(vo);
			}
			// assign temp collection to instance property
			broadcasts = ac;
			//trace(ObjectUtil.toString(broadcasts));
		}
		
		/**
		 * 
		 * 
		 */ 
		public function setHighlights(value:Object):void {
			trace("VideosModel ::: setHighlights");
			var vids:Array = value.videos;
			var len:uint = vids.length;
			var vo:IVideoVO;
			// create a temp collection as otherwise
			// the videos collection triggers an event every time
			// an item is added to it
			var ac:ArrayCollection = new ArrayCollection();
			highlightsTotal = value._total;
			highlightsLinks = value._links;
			for(var i:uint=0; i<len; i++) {
				vo = new VideoHighlightVO(vids[i]);
				ac.addItem(vo);
			}
			// assign temp collection to instance property
			highlights = ac;
			//trace(ObjectUtil.toString(highlights));
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		
	}
	
}
