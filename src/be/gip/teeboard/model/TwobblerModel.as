package be.gip.teeboard.model {
	
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.dto.TwobblerFaultDTO;
	import be.gip.teeboard.events.TwobblerEvent;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.log.TeeLogger;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import mx.events.PropertyChangeEvent;
	import mx.graphics.ImageSnapshot;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	[Event(name="serviceFault", type="be.gip.teeboard.events.TwobblerEvent")]
	
	[Bindable]
	public class TwobblerModel extends EventDispatcher {
		
		private static var INSTANCE:TwobblerModel;
		
		public static const DEFAULT_SPACES:Number = 8;
		public static const DEFAULT_DIV:String = "\u2014";
		public static const DEFAULT_PREFIX:String = "\u005B";
		public static const DEFAULT_POSTFIX:String = "\u005D";
		public static const DEFAULT_UPPERCASE:Boolean = true;
		public static const DEFAULT_INCLUDE_ALBUM:Boolean = true;
		public static const DEFAULT_ORDER_INDEX:int = 0;
		public static const DEFAULT_LOAD_COVER:Boolean = false;
		
		/**
		 * Default delay for last.fm timer in milliseconds.
		 * 
		 * @default 15&#x2a;1000 (50 seconds)
		 */ 
		public static const DEFAULT_DELAY:Number = 15*1000;
		
		/**
		 * Alternate timer delay in milliseconds used when an error occurs.
		 * 
		 * @default 30&#x2a;1000 (30 seconds)
		 */ 
		public static const LONG_DELAY:Number = 30*1000;
		
		/**
		 * URL to the Last.fm API to retrieve a user's recent played tracks.
		 * <p>
		 * 	<li>url: 		<code>http://ws.audioscrobbler.com/</code></li>
		 * 	<li>method: 	<code>user.getrecenttracks</code></li>
		 * 	<li>limit: 		<code>10</code></li>
		 * 	<li>api key: 	<code>99562e51dc22b5e68dbfab8ad7c34980</code></li>
		 * </p>
		 * 
		 * @see <a href="http://www.last.fm/api/show/user.getRecentTracks">last.fm API - user.getRecentTracks</a>
		 *  
		 */
		public static const LASTFM_XML_URL:String = "http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=[USER]&limit=10&api_key=99562e51dc22b5e68dbfab8ad7c34980";
		
		/**
		 * Returns the currently (or last played) track in JSON format.
		 * http://ajax.last.fm/user/DeezjaVu/now
		 **/
		public static const LASTFM_AJAX_URL:String = "http://ajax.last.fm/user/[USER]/now";
		
		// USER DOCUMENTS FILE PATHS
		public static const TEEBOARD_TWOBBLER_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/twobbler");
		public static const COVER_FILE:File = File.documentsDirectory.resolvePath("TeeBoard/twobbler/cover.png");
		public static const DESTINATION_FILE:File = File.documentsDirectory.resolvePath("TeeBoard/twobbler/nowplaying.txt");
		
		// APPLICATION FILE PATHS
		public static const NOWPLAYING_FILE:File = File.applicationDirectory.resolvePath("twobbler/data/nowplaying.txt");
		public static const DEFAULT_COVER_FILE:File = File.applicationDirectory.resolvePath("twobbler/data/default_album.png");
		
		/**
		 * The number of spaces to pad the text info with.
		 * <p>This is the spacing inbetween repeating info</p>
		 * 
		 * @default 8
		 */ 
		public var numSpaces:Number = DEFAULT_SPACES;
		
		/**
		 * The divider inbetween track title, artist and album
		 * 
		 * @default \u2014 (em dash)
		 */ 
		public var div:String = DEFAULT_DIV;
		
		/**
		 * The wrapper character at the beginning of the track info.
		 * 
		 * @default [
		 */ 
		public var preChar:String = DEFAULT_PREFIX;
		
		/**
		 * The wrapper character at the end of the track info.
		 * 
		 * @default ]
		 */ 
		public var postChar:String = DEFAULT_POSTFIX;
		
		/**
		 * Indicates if the info written to the text file should be in all caps.
		 * 
		 * @default true
		 */ 
		public var upperCase:Boolean = DEFAULT_UPPERCASE;
		
		/**
		 * Indicates if the album title should be included in the text file.
		 * 
		 * @default true
		 */ 
		public var includeAlbum:Boolean = DEFAULT_INCLUDE_ALBUM;
		
		/**
		 * The order in which to display song info.
		 * <li>0 = Track Title - Artist Name</li>
		 * <li>1 = Artist Name - Track title</li>
		 * 
		 * @default 0 (Track Title - Artist Name)
		 */ 
		public var orderIndex:int = DEFAULT_ORDER_INDEX;
		
		/**
		 * Whether the album cover needs to be downloaded.
		 * 
		 * @default false
		 */ 
		public var loadCover:Boolean = DEFAULT_LOAD_COVER;
		
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/twobbler/settings", "/");
		
		private var logger:TeeLogger = TeeLogger.getInstance();
		
		private var _twobblerEnabled:Boolean = false;
		private var _fmName:String = "";
		
		private var _recentTracks:XMLList;
		private var _track:XML;
		
		private var numLastTracks:int = 0;
		private var fmTimer:Timer;
		private var coverLoader:Loader;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function TwobblerModel() {
			if (INSTANCE != null ) {
				throw new Error("Only one TwobblerModel instance allowed. " +
					"To access the Singleton instance, TwobblerModel.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 */ 
		public static function getInstance():TwobblerModel {
			trace("TwobblerModel ::: getInstance");
			if(INSTANCE == null) INSTANCE = new TwobblerModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("TwobblerModel ::: init");
			if(!TwobblerModel.DESTINATION_FILE.exists) {
				TwobblerModel.NOWPLAYING_FILE.copyTo(TwobblerModel.DESTINATION_FILE);
			}
			
			if(so.size != 0 && so.data.hasOwnProperty("fmName")) {
				_fmName = so.data.fmName;
				//_fmName = "";
			}
			
			if(so.data.hasOwnProperty("twobblerEnabled")) {
				_twobblerEnabled = so.data.twobblerEnabled;
			}
			
			if(so.data.hasOwnProperty("numSpaces")) {
				trace("    - setting values from SharedObject");
				numSpaces = so.data.numSpaces;
				div = so.data.div;
				preChar = so.data.preChar;
				postChar = so.data.postChar;
				loadCover = so.data.loadCover;
				upperCase = so.data.upperCase;
			}else {
				trace("    - setting default values");
				setDefaults();
			}
			
			if(so.data.hasOwnProperty("includeAlbum")) {
				includeAlbum = so.data.includeAlbum;
			}
			
			if(so.data.hasOwnProperty("orderIndex")) {
				orderIndex = so.data.orderIndex;
			}
			
			trace("    - numSpaces: ", numSpaces);
			trace("    - div: ", div);
			trace("    - preChar: ", preChar);
			trace("    - postChar: ", postChar);
			trace("    - orderIndex: ", orderIndex);
			trace("    - includeAlbum: ", includeAlbum);
			trace("    - upperCase: ", upperCase);
			trace("    - loadCover: ", loadCover);
			
			coverLoader = new Loader();
			coverLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, coverLoader_completeHandler);
			coverLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, coverLoader_ioErrorHandler);
			
			fmTimer = new Timer(DEFAULT_DELAY);
			fmTimer.addEventListener(TimerEvent.TIMER, fmTimer_timerHandler);
		}
		
		//------------------------------------
		// fmTimer_timerHandler()
		//------------------------------------
		private function fmTimer_timerHandler(event:TimerEvent):void {
			trace("TwobblerModel ::: fmTimer_timerHandler");
			MVCEventDispatcher.getInstance().dispatchEvent(new TwobblerEvent(TwobblerEvent.GET_RECENT_TRACKS));
			getRecentTracks();
		}
		
		//------------------------------------
		// loadAlbumCover()
		//------------------------------------
		private function loadAlbumCover():void {
			trace("TwobblerView ::: loadAlbumCover");
			var imgUrl:String = "";
			trace("    - track has image: ", _track.hasOwnProperty("image"));
			if(_track.hasOwnProperty("image")) {
				trace(_track.image.toXMLString());
				imgUrl = _track.image.(@size == "large");
				trace("    - track img: ", imgUrl);
			}
			if(imgUrl == "") imgUrl = TwobblerModel.DEFAULT_COVER_FILE.url;
			//trace("    - large image url: ", imgUrl);
			coverLoader.unload();
			coverLoader.load(new URLRequest(imgUrl));
			trace("    - imgUrl: ", imgUrl);
		}
		
		//------------------------------------
		// loader_completeHandler()
		//------------------------------------
		private function coverLoader_completeHandler(event:Event):void {
			trace("TwobblerView ::: loader_completeHandler");
			//trace("    - target: ", event.currentTarget);
			//trace("    - coverLoader url: ", coverLoader.contentLoaderInfo.url);
			var fs:FileStream = new FileStream();
			var snap:ImageSnapshot;
			var ba:ByteArray;
			snap = ImageSnapshot.captureImage(coverLoader);
			ba = snap.data;
			trace("    - ba length: ", ba.length);
			try {
				fs.open(TwobblerModel.COVER_FILE, FileMode.WRITE);
				fs.writeBytes(ba, 0, ba.length);
				fs.close();
			}catch(e:Error) {
				trace("    - ERROR: ", e.message);
				var msg:String = "Failed to write album cover to disk. \r";
				msg += "Error: " + e.message;
				//Alert.show(msg, "Error:");
				logError(msg);
				fs.close();
			}
		}
		
		//------------------------------------
		// loader_ioErrorHandler()
		//------------------------------------
		private function coverLoader_ioErrorHandler(event:IOErrorEvent):void {
			trace("TwobblerView ::: loader_ioErrorHandler");
			trace("    - text: ", event.text);
			var loaderUrl:String = coverLoader.contentLoaderInfo.url;
			var imgUrl:String = DEFAULT_COVER_FILE.url;
			//trace("    - loader url: ", loaderUrl);
			//trace("    - no cover url: ", imgUrl);
			// if something went wrong loading the album cover
			// load the default album cover - this in turn will trigger
			// the complete event and write the default album cover to disk
			if(imgUrl != loaderUrl) {
				coverLoader.unload();
				coverLoader.load(new URLRequest(imgUrl));
			}
			logError(event.text);
		}
		
		
		//==============================================================
		//
		// PUBLIC METHODS
		//
		//==============================================================
		
		/**
		 * Logs messages to disk.
		 */ 
		public function logError(value:String, append:Boolean=true):void {
			trace("TwobblerModel ::: logError");
			var msg:LogMessage = new LogMessage();
			msg.message = "Twobbler :: " + value;
			logger.log(msg);
		}
		
		/**
		 * Sets the default text formatting options.
		 */ 
		public function setDefaults():void {
			trace("TwobblerModel ::: setDefaults");
			numSpaces = DEFAULT_SPACES;
			div = DEFAULT_DIV;
			preChar = DEFAULT_PREFIX;
			postChar = DEFAULT_POSTFIX;
			upperCase = DEFAULT_UPPERCASE;
			orderIndex = DEFAULT_ORDER_INDEX;
			includeAlbum = DEFAULT_INCLUDE_ALBUM;
			loadCover = DEFAULT_LOAD_COVER;
			save();
		}
		
		/**
		 * Saves the current text formatting to the twobbler <code>SharedObject</code>.
		 */ 
		public function save():void {
			trace("TwobblerModel ::: save");
			so.data.numSpaces = numSpaces;
			so.data.div = div;
			so.data.preChar = preChar;
			so.data.postChar = postChar;
			so.data.orderIndex = orderIndex;
			so.data.includeAlbum = includeAlbum;
			so.data.loadCover = loadCover;
			so.data.upperCase = upperCase;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function getRecentTracks(fetchNow:Boolean=false):void {
			trace("TwobblerModel ::: getRecentTracks");
			// only fetch data if twobbler is enabled
			// and a username is present
			if(_twobblerEnabled && fmName != null && fmName != "") {
				if(fetchNow) {
					fmTimer_timerHandler(null);
				}else{
					fmTimer.delay = DEFAULT_DELAY;
					fmTimer.start();
				}
			}else {
				fmTimer.reset();
			}
		}
		
		/**
		 * 
		 */ 
		public function setRecentTracks(value:XML):void {
			trace("TwobblerModel ::: setRecentTracks");
			trace("    - twobbler enabled: ", _twobblerEnabled);
			var status:String = value.@status.toString();
			var msg:String = "";
			trace("    - status: ", status);
			fmTimer.reset();
			
			// if status is not "ok" write error to log
			// and delay the next call using the LONG_DELAY
			if(status.toLowerCase() != "ok") {
				// display info - something went wrong
				if(value.child("error")) {
					msg = value.error.toString();
					logError(msg);
				}
				if(_twobblerEnabled) {
					fmTimer.delay = TwobblerModel.LONG_DELAY;
					fmTimer.start();
				}
				return;
			}
			
			_recentTracks = value.recenttracks.track;
			var len:int = _recentTracks.length();
			trace("    - tracks: ", len);
			//trace(recenttracks.toXMLString());
			
			if(len == 0) {
				//var win:Alert = Alert.show("No recent tracks found for user: " + fmName, "Info:");
				//showAlert("No recent tracks found for user: " + fmName, "Info:");
				logError("No recent tracks found for user: " + fmName);
				if(_twobblerEnabled) {
					fmTimer.delay = TwobblerModel.LONG_DELAY;
					fmTimer.start();
				}
				return;
			}
			
			// start the timer with default delay (10sec)
			fmTimer.delay = DEFAULT_DELAY;
			if(_twobblerEnabled) fmTimer.start();
			
			if(_track != null) {
				//trace("COMPARING NEW WITH OLD TRACK");
				var nTrack:XML = _recentTracks[0];
				var np:String = nTrack.@nowplaying;
				
				trace("    - tracks: ", len);
				trace("    - last tracks: ", numLastTracks);
				trace("    - is now playing: ", np);
				
				// increase delay if current track is playing
				// this is to prevent a bug with Grooveshark
				if(np && (len > 10 && numLastTracks != 11)) {
					trace("    - INCREASING TWOBBLER DELAY");
					fmTimer.delay = TwobblerModel.LONG_DELAY;
				}
				numLastTracks = len;
				// if nothing has changed, don't update
				if(nTrack.artist.toString() == _track.artist.toString() && nTrack.name.toString() == _track.name.toString()) {
					trace("BOTH ARTIST AND SONG TITLE ARE EQUAL");
					return;
				}
			}
			
			_track = _recentTracks[0];
			
			//trace(ObjectUtil.toString(track));
			
			writeText();
			
			// dispatch property change event
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "track";
			dispatchEvent(propEvent);
		}
		
		//------------------------------------
		// writeText()
		//------------------------------------
		public function writeText():void {
			trace("TwobblerModel ::: writeText");
			if(_track == null) return;
			
			// track2String doesn't include padding, so make sure to add it
			var str:String = track2String() + spacer;
			trace("    - nowplaying: ", str);
			
			var fs:FileStream = new FileStream();
			try {
				fs.open(DESTINATION_FILE, FileMode.WRITE);
				fs.writeUTFBytes("");
				fs.writeUTFBytes(str);
				fs.close();
			}catch(e:Error) {
				trace("    - ERROR: ", e.message);
				var msg:String = "Failed to write track info to disk. \r";
				msg += "Error: " + e.message;
				//Alert.show(msg, "Error:");
				logError(msg);
				fs.close();
			}
			if(loadCover) loadAlbumCover();
		}
		
		//------------------------------------
		// track2String()
		//------------------------------------
		public function track2String():String {
			trace("TwobblerView ::: track2String");
			if(_track == null) return "";
			//trace(_track);
			var div:String = " " + div + " ";
			var preChar:String = preChar;
			var postChar:String = postChar;
			var order:int = orderIndex;
			var inc:Boolean = includeAlbum;
			var upperCase:Boolean = upperCase;
			
			var str:String = "";
			
			if(preChar != "") {
				preChar = preChar + " ";
			}
			if(postChar != "") {
				postChar = " " + postChar;
			}
			// 0 = track title - artist name
			// 1 = artist name - track title
			if(order == 0) {
				str = preChar + _track.name.toString() + div + _track.artist.toString();
			}else {
				str = preChar + _track.artist.toString() + div + _track.name.toString();
			}
			// album is not always available
			if(_track.hasOwnProperty("album") && _track.album.toString() != "" && inc) {
				str += div + _track.album.toString();
			}
			str += postChar;
			if(upperCase) str = str.toUpperCase();
			return str;
		}
		
		//------------------------------------
		// setServiceFault()
		//------------------------------------
		public function setServiceFault(value:String, title:String="Info"):void {
			trace("TwobblerModel ::: setServiceFault");
			var dto:TwobblerFaultDTO = new TwobblerFaultDTO();
			dto.message = value;
			dto.title = title;
			dispatchEvent(new TwobblerEvent(TwobblerEvent.SERVICE_FAULT, dto));
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		/**
		 * Returns a string containing the number of spaces defined by <code>numSpaces</code>.
		 * If numSpaces is 0, an empty string is returned.
		 */ 
		public function get spacer():String {
			var len:Number = numSpaces;
			var sp:String = "";
			for(var i:Number=0; i<len; i++) {
				sp += " ";
			}
			return sp;
		}

		/**
		 * Last.fm username.
		 */
		public function get fmName():String {
			return _fmName;
		}
		public function set fmName(value:String):void {
			_fmName = value;
			so.data.fmName = _fmName;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function get twobblerEnabled():Boolean {
			return _twobblerEnabled;
		}
		public function set twobblerEnabled(value:Boolean):void {
			if(value == _twobblerEnabled) return;
			_twobblerEnabled = value;
			so.data.twobblerEnabled = value;
			so.flush();
			getRecentTracks(true);
		}
		
		/**
		 * List of latest tracks retrieved from last.fm
		 * <p>Listen to the <code>PopertyChangeEvent.PROPERTYCHANGE</code> event to be notified when this property changes.</p>
		 */ 
		public function get recentTracks():XMLList {
			return _recentTracks;
		}
		
		public function get lastTrack():XML {
			return _track;
		}
		
	}
	
}
