package be.gip.teeboard.model {
	
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.events.DonationsEvent;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.vo.notifications.IMessageVO;
	
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	/**
	 * NotificationsModel Singleton.
	 * <p>
	 * Handles widget notifications.
	 * </p>
	 */ 
	public class NotificationsModel extends EventDispatcher {
		
		public static const DOCUMENTS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard");
		
		public static const WIDGET_NOTIFICATIONS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/notifications/");
		
		/**
		 * List of mp3 files.
		 */ 
		public var sounds:ArrayCollection;
		
		/**
		 * Directional animation options for the notification widget (followers, subscribers, donations).
		 */ 
		public var animDirections:ArrayCollection;
		
		private static const APP_WIDGETS_DIR:File = File.applicationDirectory.resolvePath("widgets");
		private static const APP_SOUND_DIR:File = File.applicationDirectory.resolvePath("sounds");
		
		private static var INSTANCE:NotificationsModel;
		
		// shared object used by FollowersModel, NotificationsModel and WidgetsModel
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/widgets/settings", "/");
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		private var sound:Sound;
		private var sChannel:SoundChannel;
		
		private var followTimer:Timer;
		private var subTimer:Timer;
		private var donateTimer:Timer;
		private var messageTimer:Timer;
		private var currentMessageIndex:int = -1;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function NotificationsModel() {
			trace("NotificationsModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one NotificationsModel instance allowed. " +
					"To access the Singleton instance, NotificationsModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():NotificationsModel {
			if(!INSTANCE) INSTANCE = new NotificationsModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("NotificationsModel ::: initModel");
			
			trace("    - shared object size: ", so.size, "bytes");
			
			//so.clear();
			
			if(so.data.hasOwnProperty("selectedAnimDirection")) {
				_selectedAnimDirection = so.data.selectedAnimDirection;
			}else {
				so.data.selectedAnimDirecation = _selectedAnimDirection;
			}
			
			trace("    - so has followWidgetEnabled: ", so.data.hasOwnProperty("followWidgetEnabled"));
			trace("    - so has subWidgetEnabled: ", so.data.hasOwnProperty("subWidgetEnabled"));
			trace("    - so has donateWidgetEnabled: ", so.data.hasOwnProperty("donateWidgetEnabled"));
			trace("    - so has messageWidgetEnabled: ", so.data.hasOwnProperty("messageWidgetEnabled"));
			trace("    - so has messages: ", so.data.hasOwnProperty("messages"));
			
			trace("===============================================");
			trace("SharedObject: be/gip/teeboard/widgets/settings");
			trace(ObjectUtil.toString(so.data));
			trace("===============================================");
			
			//delete so.data._followSound;
			
			// FOLLOW WIDGET PROPERTIES
			if(so.data.hasOwnProperty("followWidgetEnabled")) {
				_followWidgetEnabled = so.data.followWidgetEnabled;
				_followSoundEnabled = so.data.followSoundEnabled;
				_followStreamEnabled = so.data.followStreamEnabled;
				_followTextEnabled = so.data.followTextEnabled;
				_followNotifyEnabled = so.data.followNotifyEnabled;
				_followVolume = so.data.followVolume;
				_followSound = so.data.followSound;
				_followMessage = so.data.followMessage;
				_followColor = so.data.followColor;
				_followBackgroundColor = so.data.followBackgroundColor;
			}else {
				so.data.followWidgetEnabled = _followWidgetEnabled;
				so.data.followSoundEnabled = _followSoundEnabled;
				so.data.followStreamEnabled = _followStreamEnabled;
				so.data.followTextEnabled = _followTextEnabled;
				so.data.followNotifyEnabled = _followNotifyEnabled;
				so.data.followVolume = _followVolume;
				so.data.followSound = _followSound;
				so.data.followMessage= _followMessage;
				so.data.followColor = _followColor;
				so.data.followBackgroundColor = _followBackgroundColor;
			}
			
			// SUBCRIBERS WIDGET PROPERTIES
			if(so.data.hasOwnProperty("subWidgetEnabled")) {
				_subWidgetEnabled = so.data.subWidgetEnabled;
				_subSoundEnabled = so.data.subSoundEnabled;
				_subStreamEnabled = so.data.subStreamEnabled;
				_subTextEnabled = so.data.subTextEnabled;
				_subNotifyEnabled = so.data.subNotifyEnabled;
				_subVolume = so.data.subVolume;
				_subSound = so.data.subSound;
				_subMessage = so.data.subMessage;
				_subColor = so.data.subColor;
				_subBackgroundColor = so.data.subBackgroundColor;
			}else {
				so.data.subWidgetEnabled = _subWidgetEnabled;
				so.data.subSoundEnabled = _subSoundEnabled;
				so.data.subStreamEnabled = _subStreamEnabled;
				so.data.subTextEnabled = _subTextEnabled;
				so.data.subNotifyEnabled = _subNotifyEnabled;
				so.data.subVolume = _subVolume;
				so.data.subSound = _subSound;
				so.data.subMessage = _subMessage;
				so.data.subColor = _subColor;
				so.data.subBackgroundColor = _subBackgroundColor;
			}
			
			// DONATION WIDGET PROPERTIES
			if(so.data.hasOwnProperty("donateWidgetEnabled")) {
				_donateWidgetEnabled = so.data.donateWidgetEnabled;
				_donateSoundEnabled = so.data.donateSoundEnabled;
				_donateStreamEnabled = so.data.donateStreamEnabled;
				_donateTextEnabled = so.data.donateTextEnabled;
				_donateNotifyEnabled = so.data.donateNotifyEnabled;
				_donateVolume = so.data.donateVolume;
				_donateSound = so.data.donateSound;
				_donateMessage = so.data.donateMessage;
				_donateColor = so.data.donateColor;
				_donateBackgroundColor = so.data.donateBackgroundColor;
			}else {
				so.data.donateWidgetEnabled = _donateWidgetEnabled;
				so.data.donateSoundEnabled = _donateSoundEnabled;
				so.data.donateStreamEnabled = _donateStreamEnabled;
				so.data.donateTextEnabled = _donateTextEnabled;
				so.data.donateNotifyEnabled = _donateNotifyEnabled;
				so.data.donateVolume = _donateVolume;
				so.data.donateSound = _donateSound;
				so.data.donateMessage = _donateMessage;
				so.data.donateColor = _donateColor;
				so.data.donateBackgroundColor = _donateBackgroundColor;
			}
			
			// MESSAGES WIDGET PROPERTIES
			if(so.data.hasOwnProperty("messageWidgetEnabled")) {
				_messages = new ArrayCollection(so.data.messages);
				
				_messageWidgetEnabled = so.data.messageWidgetEnabled;
				_messageSoundEnabled = so.data.messageSoundEnabled;
				_messageUseDefaultWidget = so.data.messageUseDefaultWidget;
				
				_messageBackgroundColor = so.data.messageBackgroundColor;
				_messageColor = so.data.messageColor;
				_messageInterval = so.data.messageInterval;
				_messageSound = so.data.messageSound;
				_messageVolume = so.data.messageVolume;
			}else {
				_messages = new ArrayCollection();
				so.data.messages = new Array();
				so.data.messageWidgetEnabled = _messageWidgetEnabled;
				so.data.messageSoundEnabled = _messageSoundEnabled;
				so.data.messageUseDefaultWidget = _messageUseDefaultWidget;
				
				so.data.messageBackgroundColor = _messageBackgroundColor;
				so.data.messageColor = _messageColor;
				so.data.messageInterval = _messageInterval;
				so.data.messageSound = _messageSound;
				so.data.messageVolume = _messageVolume;
			}
			
			trace(ObjectUtil.toString(_messages));
			
			// NEW PROPERTIES
			if(so.data.hasOwnProperty("bgAlpha")) {
				_bgAlpha = so.data.bgAlpha;
			}
			
			// copy sound directory from app to user documents
			var soundsDest:File = DOCUMENTS_DIR.resolvePath("sounds");
			if(!soundsDest.exists) {
				APP_SOUND_DIR.copyTo(soundsDest, false);
			}
			
			// read all mp3 files in sound dir
			readSoundsDir();
			
			// set the default follower sound (mp3) if none was in SharedObject
			if(_followSound == "") {
				_followSound = sounds.getItemAt(0).name;
			}
			
			// set the default subscription sound (mp3) if none was in SharedObject
			if(_subSound == "") {
				_subSound = sounds.getItemAt(0).name;
			}
			
			// set the default donation sound (mp3) if none was in SharedObject
			if(_donateSound == "") {
				_donateSound = sounds.getItemAt(0).name;
			}
			
			// set the default message sound (mp3) if none was in SharedObject
			if(_messageSound == "") {
				_messageSound = sounds.getItemAt(0).name;
			}
			
			// copy widgets folder from app to user documents
			var widgetDest:File = DOCUMENTS_DIR.resolvePath("widgets");
			if(!widgetDest.exists) {
				APP_WIDGETS_DIR.copyTo(widgetDest, false);
			}
			
			animDirections = new ArrayCollection();
			animDirections.addItem({label:"top to bottom", direction:"top"});
			animDirections.addItem({label:"bottom to top", direction:"bottom"});
			animDirections.addItem({label:"left to right", direction:"left"});
			animDirections.addItem({label:"right to left", direction:"right"});
			
			followTimer = new Timer(1000 * 25);
			followTimer.addEventListener(TimerEvent.TIMER, followerTimer_timerHandler);
			if(_followWidgetEnabled) followTimer.start();
			
			donateTimer = new Timer(1000 * 30);
			donateTimer.addEventListener(TimerEvent.TIMER, donateTimer_timerHandler);
			if(_donateWidgetEnabled) donateTimer.start();
			
			subTimer = new Timer(1000 * 35);
			subTimer.addEventListener(TimerEvent.TIMER, subTimer_timerHandler);
			if(_subWidgetEnabled) subTimer.start();
			
			messageTimer = new Timer(1000 * 60 * _messageInterval);
			//messageTimer = new Timer(1000 * 20);
			messageTimer.addEventListener(TimerEvent.TIMER, messageTimer_timerHandler);
			if(_messageWidgetEnabled) messageTimer.start();
		}
		
		//------------------------------------
		// readSoundsDir()
		//------------------------------------
		private function readSoundsDir():void {
			trace("NotificationsModel ::: readSoundsDir");
			var soundsDir:File = DOCUMENTS_DIR.resolvePath("sounds");
			// read all mp3 files in sound dir
			var files:Array = soundsDir.getDirectoryListing();
			var len:uint = files.length;
			var f:File;
			var arr:Array = new Array();
			for(var i:uint=0; i<len; i++) {
				f = files[i];
				if(!f.isDirectory && f.extension == "mp3") {
					//trace("    - mp3 file: ", f.nativePath);
					arr.push(f);
				}
			}
			// this should trigger property change event
			sounds = new ArrayCollection(arr);
		}
		
		//------------------------------------
		// followerTimer_timerHandler()
		//------------------------------------
		private function followerTimer_timerHandler(event:TimerEvent):void {
			trace("NotificationsModel ::: followerTimer_timerHandler");
			dispatcher.dispatchEvent(new TwitchEvent(TwitchEvent.GET_CHANNEL_FOLLOWERS));
			if(!_followWidgetEnabled) {
				followTimer.reset();
			}
		}
		
		//------------------------------------
		// subTimer_timerHandler()
		//------------------------------------
		private function subTimer_timerHandler(event:TimerEvent):void {
			trace("NotificationsModel ::: subTimer_timerHandler");
			dispatcher.dispatchEvent(new TwitchEvent(TwitchEvent.GET_CHANNEL_SUBSCRIBERS));
			if(!_subWidgetEnabled) {
				subTimer.reset();
			}
		}
		
		//------------------------------------
		// donateTimer_timerHandler()
		//------------------------------------
		private function donateTimer_timerHandler(event:TimerEvent):void {
			trace("NotificationsModel ::: donateTimer_timerHandler");
			dispatcher.dispatchEvent(new DonationsEvent(DonationsEvent.GET_DONATIONS));
			if(!_donateWidgetEnabled) {
				donateTimer.reset();
			}
		}
		
		//------------------------------------
		// messageTimer_timerHandler()
		//------------------------------------
		private function messageTimer_timerHandler(event:TimerEvent):void {
			trace("NotificationsModel ::: messageTimer_timerHandler");
			if(_messages.length == 0) {
				// disable message widget - this will also stop the timer
				messageWidgetEnabled = false;
				messageTimer.reset();
				currentMessageIndex = -1;
				return;
			}
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "showMessage";
			
			trace("    - message index before: ", currentMessageIndex);
			
			// increase the index and dispatch event
			if(currentMessageIndex >= _messages.length-1) {
				trace("    - resetting message index to 0");
				currentMessageIndex = 0;
			}else {
				currentMessageIndex += 1;
			}
			
			trace("    - message index after: ", currentMessageIndex);
			
			if(!_messageWidgetEnabled) {
				messageTimer.reset();
			}
			
			dispatchEvent(propEvent);
			
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function playFollowAlert():void {
			trace("NotificationsModel ::: playFollowAlert");
			var idx:int = Math.max(getSoundIndex(_followSound), 0);
			var f:File = sounds.getItemAt(idx) as File;
			var uri:URLRequest = new URLRequest(f.url);
			var trans:SoundTransform = new SoundTransform(_followVolume/10);
			trace("    - volume: ", _followVolume/10);
			if(sChannel) {
				sChannel.stop();
			}
			if(f.exists) {
				sound = new Sound(uri);
				sChannel = sound.play(0, 0, trans);
			}else{
				// selected file is missing, re-read sounds dir
				readSoundsDir();
			}
		}
		
		/**
		 * 
		 */ 
		public function playSubscriberAlert():void {
			trace("NotificationsModel ::: playSubscriberAlert");
			var idx:int = Math.max(getSoundIndex(_subSound), 0);
			var f:File = sounds.getItemAt(idx) as File;
			var uri:URLRequest = new URLRequest(f.url);
			var trans:SoundTransform = new SoundTransform(_subVolume/10);
			trace("    - volume: ", _subVolume/10);
			if(sChannel) {
				sChannel.stop();
			}
			sound = new Sound(uri);
			sChannel = sound.play(0, 0, trans);
		}
		
		/**
		 * 
		 */ 
		public function playDonationAlert():void {
			trace("NotificationsModel ::: playDonationAlert");
			var idx:int = Math.max(getSoundIndex(_donateSound), 0);
			var f:File = sounds.getItemAt(idx) as File;
			var uri:URLRequest = new URLRequest(f.url);
			var trans:SoundTransform = new SoundTransform(_donateVolume/10);
			trace("    - volume: ", _donateVolume/10);
			if(sChannel) {
				sChannel.stop();
			}
			sound = new Sound(uri);
			sChannel = sound.play(0, 0, trans);
		}
		
		//------------------------------------
		// playMessageAlert()
		//------------------------------------
		public function playMessageAlert():void {
			trace("NotificationsModel ::: playMessageAlert");
			var idx:int = Math.max(getSoundIndex(_messageSound), 0);
			var f:File = sounds.getItemAt(idx) as File;
			var uri:URLRequest = new URLRequest(f.url);
			var trans:SoundTransform = new SoundTransform(_messageVolume/10);
			trace("    - volume: ", _messageVolume/10);
			if(sChannel) {
				sChannel.stop();
			}
			sound = new Sound(uri);
			sChannel = sound.play(0, 0, trans);
		}
		
		/**
		 * 
		 * @param mp3 the file to copy to the TeeBoard sounds folder.
		 */ 
		public function addSound(mp3:File):void {
			trace("NotificationsModel ::: addSound");
			var n:String = mp3.name.toLowerCase().replace(" ", "-");
			var e:String = mp3.extension;
			var mp3loc:File = DOCUMENTS_DIR.resolvePath("sounds/" + n);
			mp3.copyTo(mp3loc, true);
			// rather than adding the file to the existing sounds list, 
			// reread the sounds folder, as this will automatically
			// sort them by name
			readSoundsDir();
		}
		
		/**
		 * 
		 */ 
		public function getSoundIndex(value:String):int {
			var idx:int = -1;
			var len:int = sounds.length;
			var f:File;
			for(var i:int=0; i<len; i++) {
				f = sounds.getItemAt(i) as File;
				if(f.name == value) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
		/**
		 * This overrides a partnered channel that has no subscription program.
		 * 
		 * Dispatches a property change event with the property: noSubProgram
		 */ 
		public function setNoSubProgram():void {
			trace("NotificationsModel ::: setNoSubProgram");
			_subWidgetEnabled = false;
			subTimer.reset();
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "noSubProgram";
			dispatchEvent(propEvent);
		}
		
		/**
		 * Returns the currently selected message for the Message Widget.
		 * Returns an empty string if there are no messages.
		 */ 
		public function getCurrentMessage():IMessageVO {
			trace("NotificationsModel ::: getCurrentMessage");
			var msg:IMessageVO;
			var idx:int = currentMessageIndex;
			if(idx != -1) {
				msg = _messages.getItemAt(idx) as IMessageVO;
			}
			return msg;
		}
		
		/**
		 * Adds a new message (<code>MessageVO</code>) to the list of messages and saves it to the SharedObject.
		 */ 
		public function addMessage(value:IMessageVO):void {
			trace("NotificationsModel ::: addMessage");
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "messages";
			
			_messages.addItem(value);
			
			so.data.messages = _messages.toArray();
			so.flush();
			
			// dispatch event to listeners to indicate we have messages
			// important to enable/disable toggle switches
			dispatchEvent(propEvent);
		}
		
		/**
		 * Updates the list of messages in the SharedObject.
		 * 
		 * <p>Useful for when the order of the messages has changed</p>
		 */ 
		public function updateMessages():void {
			trace("NotificationsModel ::: updateMessages");
			so.data.messages = _messages.toArray();
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function updateMessageAt(msg:IMessageVO, idx:int):void {
			trace("NotificationsModel ::: updateMessageAt");
			_messages.setItemAt(msg, idx);
			
			so.data.messages = _messages.toArray();
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function removeMessageAt(idx:int):void {
			trace("NotificationsModel ::: removeMessageAt");
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "messages";
			
			_messages.removeItemAt(idx);
			
			var len:int = _messages.length;
			if(len == 0) {
				currentMessageIndex = -1;
				// disable widget and stop timer
				messageWidgetEnabled = false;
				messageTimer.reset();
			}
			
			so.data.messages = _messages.toArray();
			so.flush();
			
			dispatchEvent(propEvent);
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		private var _selectedAnimDirection:String = "top";
		/**
		 * Animation direction for all notification widgets (followers, subscribers, donations)
		 */ 
		public function get selectedAnimDirection():String {
			return _selectedAnimDirection;
		}
		public function set selectedAnimDirection(value:String):void {
			if(value == _selectedAnimDirection) return;
			_selectedAnimDirection = value;
			so.data.selectedAnimDirection = value;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function get selectedAnimIndex():int {
			var len:int = animDirections.length;
			var idx:int = -1;
			for(var i:int=0; i<len; i++) {
				if(animDirections.getItemAt(i).direction == _selectedAnimDirection) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
		private var _bgAlpha:Number = 0.65;
		/**
		 * The alpha of the widget background.
		 * <p>This is a shared property and applies to all notification types.</p>
		 */ 
		public function get bgAlpha():Number {
			return _bgAlpha;
		}
		public function set bgAlpha(value:Number):void {
			if(value == _bgAlpha) return;
			_bgAlpha = value;
			so.data.bgAlpha = value;
			so.flush();
		}
		
		//------------------------------------
		// FOLLOWERS WIDGET PROPERTIES
		//------------------------------------
		
		private var _followWidgetEnabled:Boolean = false;
		/**
		 * Indicates if the follower widget is enabled.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 * 
		 * @default false
		 */
		public function get followWidgetEnabled():Boolean {
			return _followWidgetEnabled;
		}
		public function set followWidgetEnabled(value:Boolean):void {
			if(value == _followWidgetEnabled) return;
			_followWidgetEnabled = value;
			if(value) {
				followTimer.start();
			}else {
				followTimer.reset();
			}
			so.data.followWidgetEnabled = value;
			so.flush();
		}
		
		private var _followSound:String = "";
		/**
		 * The selected alert sound (mp3) for new followers.
		 * <p>This is the name + extension of the audio, not the path to the audio file.</p>
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get followSound():String {
			return _followSound;
		}
		public function set followSound(value:String):void {
			if(value == _followSound) return;
			_followSound = value;
			so.data.followSound = value;
			so.flush();
		}
		
		private var _followVolume:Number = 5;
		/**
		 * Volume at which to play alerts for new followers.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get followVolume():Number {
			return _followVolume;
		}
		public function set followVolume(value:Number):void {
			if(value == _followVolume) return;
			_followVolume = value;
			so.data.followVolume = value;
			so.flush();
		}
		
		private var _followColor:uint = 0xFFFFFF;
		/**
		 * The text color of the widget on stream.
		 * @default 0xFFFFFF;
		 */ 
		public function get followColor():uint {
			return _followColor;
		}
		public function set followColor(value:uint):void {
			if(value == _followColor) return;
			_followColor = value;
			so.data.followColor = value;
			so.flush();
		}
		
		private var _followBackgroundColor:uint = 0x00FF00;
		/**
		 * The background color of the widget on stream.
		 * @default 0x00FF00;
		 */ 
		public function get followBackgroundColor():uint {
			return _followBackgroundColor;
		}
		public function set followBackgroundColor(value:uint):void {
			if(value == _followBackgroundColor) return;
			_followBackgroundColor = value;
			so.data.followBackgroundColor = value;
			so.flush();
		}
		
		private var _followSoundEnabled:Boolean = false;
		/**
		 * 
		 */
		public function get followSoundEnabled():Boolean {
			return _followSoundEnabled;
		}
		public function set followSoundEnabled(value:Boolean):void {
			if(value == _followSoundEnabled) return;
			_followSoundEnabled = value;
			so.data.followSoundEnabled = value;
			so.flush();
		}
		
		private var _followStreamEnabled:Boolean = false;
		/**
		 * 
		 */ 
		public function get followStreamEnabled():Boolean {
			return _followStreamEnabled;
		}
		public function set followStreamEnabled(value:Boolean):void {
			if(value == _followStreamEnabled) return;
			_followStreamEnabled = value;
			so.data.followStreamEnabled = value;
			so.flush();
		}
		
		private var _followTextEnabled:Boolean = false;
		/**
		 * Write 5 latest followers to a text file.
		 */ 
		public function get followTextEnabled():Boolean {
			return _followTextEnabled;
		}
		public function set followTextEnabled(value:Boolean):void {
			if(value == _followTextEnabled) return;
			_followTextEnabled = value;
			so.data.followTextEnabled = value;
			so.flush();
		}
		
		private var _followNotifyEnabled:Boolean = false;
		/**
		 * On screen notification.
		 */ 
		public function get followNotifyEnabled():Boolean {
			return _followNotifyEnabled;
		}
		public function set followNotifyEnabled(value:Boolean):void {
			if(value == _followNotifyEnabled) return;
			_followNotifyEnabled = value;
			so.data.followNotifyEnabled = value;
			so.flush();
		}
		
		private var _followMessage:String = "%name% is now following the channel.";
		/**
		 * The message to display on stream.
		 */ 
		public function get followMessage():String {
			return _followMessage;
		}
		public function set followMessage(value:String):void {
			if(value == _followMessage) return;
			_followMessage = value;
			so.data.followMessage = value;
			so.flush();
		}
		
		//------------------------------------
		// SUBSCRIBERS WIDGET PROPERTIES
		//------------------------------------
		
		private var _subWidgetEnabled:Boolean = false;
		/**
		 * Indicates if the subscriber widget is enabled.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get subWidgetEnabled():Boolean {
			return _subWidgetEnabled;
		}
		public function set subWidgetEnabled(value:Boolean):void {
			if(value == _subWidgetEnabled) return;
			_subWidgetEnabled = value;
			if(value) {
				subTimer.start();
			}else {
				subTimer.reset();
			}
			so.data.subWidgetEnabled = value;
			so.flush();
		}
		
		private var _subSound:String = "";
		/**
		 * The selected alert sound for new subscriptions (partners only).
		 * <p>This is the name + extension of, not the path to, the audio file.</p>
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get subSound():String {
			return _subSound;
		}
		public function set subSound(value:String):void {
			if(value == _subSound) return;
			_subSound = value;
			so.data.subSound = value;
			so.flush();
		}
		
		private var _subVolume:Number = 5;
		/**
		 * The volume at which to play alerts for new subs.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get subVolume():Number {
			return _subVolume;
		}
		public function set subVolume(value:Number):void {
			if(value == _subVolume) return;
			_subVolume = value;
			so.data.subVolume = value;
			so.flush();
		}
		
		private var _subColor:uint = 0xFFFFFF;
		/**
		 * The text color of the widget on stream.
		 * @default 0xFFFFFF;
		 */ 
		public function get subColor():uint {
			return _subColor;
		}
		public function set subColor(value:uint):void {
			if(value == _subColor) return;
			_subColor = value;
			so.data.subColor = value;
			so.flush();
		}
		
		private var _subBackgroundColor:uint = 0xFF0000;
		/**
		 * The background color of the widget on stream.
		 * @default 0xFF0000;
		 */ 
		public function get subBackgroundColor():uint {
			return _subBackgroundColor;
		}
		public function set subBackgroundColor(value:uint):void {
			if(value == _subBackgroundColor) return;
			_subBackgroundColor = value;
			so.data.subBackgroundColor = value;
			so.flush();
		}
		
		private var _subSoundEnabled:Boolean = false;
		/**
		 * 
		 */ 
		public function get subSoundEnabled():Boolean {
			return _subSoundEnabled;
		}
		public function set subSoundEnabled(value:Boolean):void {
			if(value == _subSoundEnabled) return;
			_subSoundEnabled = value;
			so.data.subSoundEnabled = value;
			so.flush();
		}
		
		private var _subStreamEnabled:Boolean = false;
		/**
		 * 
		 */ 
		public function get subStreamEnabled():Boolean {
			return _subStreamEnabled;
		}
		public function set subStreamEnabled(value:Boolean):void {
			if(value == _subStreamEnabled) return;
			_subStreamEnabled = value;
			so.data.subStreamEnabled = value;
			so.flush();
		}
		
		private var _subTextEnabled:Boolean = false;
		/**
		 * Write 5 latest subscribers to a text file.
		 */ 
		public function get subTextEnabled():Boolean {
			return _subTextEnabled;
		}
		public function set subTextEnabled(value:Boolean):void {
			if(value == _subTextEnabled) return;
			_subTextEnabled = value;
			so.data.subTextEnabled = value;
			so.flush();
		}
		
		private var _subNotifyEnabled:Boolean = false;
		/**
		 * On screen notification.
		 */ 
		public function get subNotifyEnabled():Boolean {
			return _subNotifyEnabled;
		}
		public function set subNotifyEnabled(value:Boolean):void {
			if(value == _subNotifyEnabled) return;
			_subNotifyEnabled = value;
			so.data.subNotifyEnabled = value;
			so.flush();
		}
		
		private var _subMessage:String = "%name% has subscribed to the channel.";
		/**
		 * The message to display on stream.
		 */ 
		public function get subMessage():String {
			return _subMessage;
		}
		public function set subMessage(value:String):void {
			if(value == _subMessage) return;
			_subMessage = value;
			so.data.subMessage = value;
			so.flush();
		}
		
		//------------------------------------
		// DONATIONS WIDGET PROPERTIES
		//------------------------------------
		
		private var _donateWidgetEnabled:Boolean = false;
		/**
		 * Indicates if the donation widget is enabled.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get donateWidgetEnabled():Boolean {
			return _donateWidgetEnabled;
		}
		public function set donateWidgetEnabled(value:Boolean):void {
			if(value == _donateWidgetEnabled) return;
			_donateWidgetEnabled = value;
			if(value) {
				donateTimer.start();
			}else {
				donateTimer.reset();
			}
			so.data.donateWidgetEnabled = value;
			so.flush();
		}
		
		private var _donateSound:String = "";
		/**
		 * The selected alert sound for new donations.
		 * <p>This is the name + extension of, not the path to, the audio file.</p>
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get donateSound():String {
			return _donateSound;
		}
		public function set donateSound(value:String):void {
			if(value == _donateSound) return;
			_donateSound = value;
			so.data.donateSound = value;
			so.flush();
		}
		
		private var _donateVolume:Number = 5;
		/**
		 * The volume at which to play alerts for new donations.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get donateVolume():Number {
			return _donateVolume;
		}
		public function set donateVolume(value:Number):void {
			if(value == _donateVolume) return;
			_donateVolume = value;
			so.data.donateVolume = value;
			so.flush();
		}
		
		private var _donateColor:uint = 0xFFFFFF;
		/**
		 * The text color of the widget on stream for new donations.
		 * @default 0xFFFFFF;
		 */ 
		public function get donateColor():uint {
			return _donateColor;
		}
		public function set donateColor(value:uint):void {
			if(value == _donateColor) return;
			_donateColor = value;
			so.data.donateColor = value;
			so.flush();
		}
		
		private var _donateBackgroundColor:uint = 0x00FFFF;
		/**
		 * The background color of the widget on stream for new donations.
		 * @default 0x00FFFF;
		 */ 
		public function get donateBackgroundColor():uint {
			return _donateBackgroundColor;
		}
		public function set donateBackgroundColor(value:uint):void {
			if(value == _donateBackgroundColor) return;
			_donateBackgroundColor = value;
			so.data.donateBackgroundColor = value;
			so.flush();
		}
		
		private var _donateSoundEnabled:Boolean = false;
		/**
		 * Indicates if a sound should be played when there's a new donation.
		 */ 
		public function get donateSoundEnabled():Boolean {
			return _donateSoundEnabled;
		}
		public function set donateSoundEnabled(value:Boolean):void {
			if(value == _donateSoundEnabled) return;
			_donateSoundEnabled = value;
			so.data.donateSoundEnabled = value;
			so.flush();
		}
		
		private var _donateStreamEnabled:Boolean = false;
		/**
		 * Indicates if a message is displayed on stream when there's a new donation.
		 */ 
		public function get donateStreamEnabled():Boolean {
			return _donateStreamEnabled;
		}
		public function set donateStreamEnabled(value:Boolean):void {
			if(value == _donateStreamEnabled) return;
			_donateStreamEnabled = value;
			so.data.donateStreamEnabled = value;
			so.flush();
		}
		
		private var _donateTextEnabled:Boolean = false;
		/**
		 * Indicates if the 5 latest donators + top donator should be written to a text file.
		 */ 
		public function get donateTextEnabled():Boolean {
			return _donateTextEnabled;
		}
		public function set donateTextEnabled(value:Boolean):void {
			if(value == _donateTextEnabled) return;
			_donateTextEnabled = value;
			so.data.donateTextEnabled = value;
			so.flush();
		}
		
		private var _donateNotifyEnabled:Boolean = false;
		/**
		 * Indicates if a message should be displayed on screen 
		 * when there's a new donation.
		 */ 
		public function get donateNotifyEnabled():Boolean {
			return _donateNotifyEnabled;
		}
		public function set donateNotifyEnabled(value:Boolean):void {
			if(value == _donateNotifyEnabled) return;
			_donateNotifyEnabled = value;
			so.data.donateNotifyEnabled = value;
			so.flush();
		}
		
		private var _donateMessage:String = "%name% has donated %amount% to the channel.";
		/**
		 * The message to display on stream.
		 */ 
		public function get donateMessage():String {
			return _donateMessage;
		}
		public function set donateMessage(value:String):void {
			if(value == _donateMessage) return;
			_donateMessage = value;
			so.data.donateMessage = value;
			so.flush();
		}

		//------------------------------------
		// MESSAGES WIDGET PROPERTIES
		//------------------------------------
		
		private var _messages:ArrayCollection;
		/**
		 * List of text messages to display on stream.
		 */ 
		public function get messages():ArrayCollection {
			return _messages;
		}
		
		private var _messageWidgetEnabled:Boolean = false;
		/**
		 * Indicates if the messages widget is enabled.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get messageWidgetEnabled():Boolean {
			return _messageWidgetEnabled;
		}
		public function set messageWidgetEnabled(value:Boolean):void {
			if(value == _messageWidgetEnabled) return;
			_messageWidgetEnabled = value;
			if(value == true) {
				// interval from minutes to milliseconds
				//messageTimer.delay = _messageInterval*60*1000;
				messageTimer.start();
			}else {
				messageTimer.reset();
			}
			so.data.messageWidgetEnabled = value;
			so.flush();
		}
		
		private var _messageUseDefaultWidget:Boolean = true;
		/**
		 * 
		 */ 
		public function get messageUseDefaultWidget():Boolean {
			return _messageUseDefaultWidget;
		}
		public function set messageUseDefaultWidget(value:Boolean):void {
			if(value == _messageUseDefaultWidget) return;
			_messageUseDefaultWidget = value;
			so.data.messageUseDefaultWidget = value;
			so.flush();
		}
		
		private var _messageInterval:Number = 10;
		/**
		 * Interval between each message in minutes.
		 * The default value is 10 minutes.
		 * 
		 * @default 10 minutes
		 */ 
		public function get messageInterval():Number {
			return _messageInterval;
		}
		public function set messageInterval(value:Number):void {
			if(value == _messageInterval) return;
			_messageInterval = value;
			// update timer delay with new value
			messageTimer.delay = value * 60 * 1000;
			so.data.messageInterval = value;
			so.flush();
		}
		
		private var _messageSoundEnabled:Boolean = false;
		/**
		 * 
		 */ 
		public function get messageSoundEnabled():Boolean {
			return _messageSoundEnabled;
		}
		public function set messageSoundEnabled(value:Boolean):void {
			if(value == _messageSoundEnabled) return;
			_messageSoundEnabled = value;
			so.data.messageSoundEnabled = value;
			so.flush();
		}

		private var _messageSound:String = "";
		/**
		 * 
		 */ 
		public function get messageSound():String {
			return _messageSound;
		}
		public function set messageSound(value:String):void {
			if(value == _messageSound) return;
			_messageSound = value;
			so.data.messageSound = value;
			so.flush();
		}
		
		private var _messageVolume:Number = 5;
		/**
		 * The volume at which to play alerts for new messages.
		 * <p>This property is stored in a <code>SharedObject</code>.
		 */
		public function get messageVolume():Number {
			return _messageVolume;
		}
		public function set messageVolume(value:Number):void {
			if(value == _messageVolume) return;
			_messageVolume = value;
			so.data.messageVolume = value;
			so.flush();
		}
		
		private var _messageColor:uint = 0xFFFFFF;
		/**
		 * The text color of the widget on stream for new donations.
		 * @default 0xFFFFFF;
		 */ 
		public function get messageColor():uint {
			return _messageColor;
		}
		public function set messageColor(value:uint):void {
			if(value == _messageColor) return;
			_messageColor = value;
			so.data.messageColor = value;
			so.flush();
		}
		
		private var _messageBackgroundColor:uint = 0xFF7F00;
		/**
		 * The background color of the widget on stream for messages.
		 * @default 0x00FFFF;
		 */ 
		public function get messageBackgroundColor():uint {
			return _messageBackgroundColor;
		}
		public function set messageBackgroundColor(value:uint):void {
			if(value == _messageBackgroundColor) return;
			_messageBackgroundColor = value;
			so.data.messageBackgroundColor = value;
			so.flush();
		}
		
	}
	
}
