package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.imgur.AlbumDTO;
	import be.gip.teeboard.vo.imgur.AlbumVO;
	import be.gip.teeboard.vo.imgur.ImageVO;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.filesystem.File;
	import flash.system.Security;
	import flash.system.System;
	
	import mx.charts.CategoryAxis;
	import mx.collections.ArrayCollection;
	
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class ImgurModel extends EventDispatcher {
		
		public static const APP_PROMO_DIR:File = File.applicationDirectory.resolvePath("panels/promo");
		
		public static const PANELS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/panels");
		public static const PANELS_PROMO_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/panels/promo");
		
		public static const IMGUR_CLIENT_ID:String = "dba7e6769a2a12f";
		
		//-----------------------------------------------
		//
		// PRIVATE PROPERTIES
		//
		//-----------------------------------------------
		
		private static var INSTANCE:ImgurModel;
		
		
		//-----------------------------------------------
		//
		// PUBLIC PROPERTIES
		//
		//-----------------------------------------------
		
		public var albums:ArrayCollection;
		
		public var selectedAlbum:AlbumVO;
		public var selectedImage:ImageVO;
		
		public function ImgurModel() {
			trace("ImgurModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one ImgurModel instance allowed. " +
					"To access the Singleton instance, ImgurModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():ImgurModel {
			if(!INSTANCE) INSTANCE = new ImgurModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("ImgurModel ::: initModel");
			
			//Security.loadPolicyFile("http://imgur.com/crossdomain.xml");
			//Security.loadPolicyFile("http://i.imgur.com/crossdomain.xml");
			
			albums = new ArrayCollection();
			
			var dto:AlbumDTO;
			
			// deezjavu
			dto = new AlbumDTO();
			dto.id = "2mQPO";
			dto.author = "TeeBoard";
			dto.url = "http://imgur.com/a/2mQPO";
			albums.addItem(dto);
			
			// alexis
			dto = new AlbumDTO;
			dto.id = "SKeQg";
			dto.author = "Alexis Gepty";
			dto.url = "http://imgur.com/a/SKeQg";
			albums.addItem(dto);
			
			// DoomkittenPlays: Simple-Color
			dto = new AlbumDTO;
			dto.id = "hUbrr";
			dto.author = "DoomkittenPlays: Simple-Color";
			dto.url = "http://imgur.com/a/hUbrr";
			albums.addItem(dto);
			
			// DoomkittenPlays: Classic-Social-Media
			dto = new AlbumDTO;
			dto.id = "dhWFi";
			dto.author = "DoomkittenPlays: Classic-Social-Media";
			dto.url = "http://imgur.com/a/dhWFi";
			albums.addItem(dto);
			
			// DoomkittenPlays: Futuristic-Tech
			dto = new AlbumDTO;
			dto.id = "21FVc";
			dto.author = "DoomkittenPlays: Futuristic-Tech";
			dto.url = "http://imgur.com/a/21FVc";
			albums.addItem(dto);
			
			// DoomkittenPlays: Tech-Metal
			dto = new AlbumDTO;
			dto.id = "RNksu";
			dto.author = "DoomkittenPlays: Tech-Metal";
			dto.url = "http://imgur.com/a/RNksu";
			albums.addItem(dto);
			
			// WormyFlipper
			dto = new AlbumDTO;
			dto.id = "X7VFJ";
			dto.author = "WormyFlipper";
			dto.url = "http://imgur.com/a/X7VFJ";
			albums.addItem(dto);
			
			// Lord VandeKieft
			dto = new AlbumDTO;
			dto.id = "Ey8NX";
			dto.author = "Lord VandeKieft";
			dto.url = "http://imgur.com/a/Ey8NX";
			albums.addItem(dto);
			
			// Rav3nzZ
			//http://imgur.com/a/RgavP
			dto = new AlbumDTO;
			dto.id = "RgavP";
			dto.author = "Rav3nzZ";
			dto.url = "http://imgur.com/a/RgavP";
			albums.addItem(dto);
			
			
			copyPromoAssets();
			
		}
		
		//------------------------------------
		// copyPromoAssets()
		//------------------------------------
		private function copyPromoAssets():void {
			trace("ImgurModel ::: copyPromoAssets");
			if(!PANELS_PROMO_DIR.exists) {
				try {
					APP_PROMO_DIR.copyTo(PANELS_PROMO_DIR);
				}catch(e:Error) {
					// do nothing
				}
			}
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		//------------------------------------
		// setAlbum()
		//------------------------------------
		public function setAlbum(value:Object):void {
			trace("ImgurModel ::: setAlbum");
			var album:AlbumVO = new AlbumVO();
			album.accountUrl = value.account_url;
			album.cover = value.cover;
			album.coverHeight = value.cover_height;
			album.coverWidht = value.cover_width;
			album.datetime = value.datetime;
			album.description = value.description;
			album.favorite = value.favorite;
			album.id = value.id;
			album.link = value.link;
			album.privacy = value.privacy;
			album.title = value.title;
			album.views = value.views;
			var img:ImageVO;
			var aimg:Object;
			var images:Array = value.images;
			var len:uint = images.length;
			var ac:ArrayCollection = new ArrayCollection();
			for(var i:int=0; i<len; i++) {
				aimg = images[i];
				img = new ImageVO();
				img.id = aimg.id;
				img.animated = aimg.animated;
				img.bandwidth = aimg.bandwidth;
				img.datetime = aimg.datetime;
				img.description = aimg.description;
				img.height = aimg.height;
				img.width = aimg.width;
				img.link = aimg.link;
				img.nsfw = aimg.nsfw;
				img.section = aimg.section;
				img.size = aimg.size;
				img.title = aimg.title;
				img.type = aimg.type;
				img.views = aimg.views;
				ac.addItem(img);
			}
			album.images = ac;
			selectedAlbum = album;
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * Request headers
		 * 
		 * Authorization: Client-ID IMGUR_CLIENT_ID
		 */ 
		public function get httpHeaders():Object {
			trace("ImgurModel ::: get httpHeaders");
			var headers:Object = new Object();
			headers["Authorization"] = "Client-ID " + IMGUR_CLIENT_ID;
			return headers;
		}
		
	}
	
}