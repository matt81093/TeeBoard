package be.gip.teeboard.model {
	
	import be.gip.teeboard.vo.StreamVO;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class FollowedModel extends EventDispatcher {
		
		private static var INSTANCE:FollowedModel;
		
		/**
		 * Streams the currently authenticated user is following.
		 * The list only contains streams, meaning channels that are currenlty online, 
		 * offline channels are not included.
		 */ 
		public var streams:ArrayCollection;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function FollowedModel() {
			trace("FollowedModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one FollowedModel instance allowed. " +
					"To access the Singleton instance, FollowedModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():FollowedModel {
			if(!INSTANCE) INSTANCE = new FollowedModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("FollowedModel ::: initModel");
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		//------------------------------------
		// setData()
		//------------------------------------
		public function setData(value:Object):void {
			trace("FollowedModel ::: setData");
			// create a temp collection as otherwise
			// the videos collection triggers an event every time
			// an item is added to it
			var ac:ArrayCollection = new ArrayCollection();
			var arr:Array = value.streams;
			var len:uint = arr.length;
			var vo:StreamVO;
			for(var i:uint=0; i<len; i++) {
				vo = new StreamVO(arr[i]);
				ac.addItem(vo);
			}
			//trace(ObjectUtil.toString(ac));
			// assign temp collection to instance property
			streams = ac;
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		
	}
	
}
