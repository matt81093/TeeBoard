package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.ChannelDTO;
	import be.gip.teeboard.dto.UserDTO;
	import be.gip.teeboard.vo.ChannelVO;
	import be.gip.teeboard.vo.RootVO;
	import be.gip.teeboard.vo.SettingsVO;
	import be.gip.teeboard.vo.UserVO;
	import be.gip.teeboard.vo.notifications.FollowerVO;
	
	import flash.display.Screen;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.managers.ICursorManager;
	import mx.utils.ObjectUtil;
	import mx.utils.URLUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class ApplicationModel extends EventDispatcher {
		
		private static var INSTANCE:ApplicationModel;
		
		/**
		 * Twitch authorization URL.
		 */ 
		private static const AUTHORIZE_URL:String = "https://api.twitch.tv/kraken/oauth2/authorize?response_type=token";
		
		/**
		 * Twitch ID for this application.
		 */ 
		private static const CLIENT_ID:String = "pvrwmjzjzsvsimy8he9jy3w9lekkwp7";
		
		/**
		 * May never need this, so might be wise to remove it.
		 */ 
		private static const CLIENT_SECRET:String = "eytuuxaboxnikrdv3szyqeongpuebyg";
		
		/**
		 * String containing the scopes for this application.
		 * 
		 * <p>Available scopes:</p>
		 * <ul>
		 * 	<li>user_read: Read access to non-public user information, such as email address.</li>
		 * 	<li>user_blocks_edit: Ability to ignore or unignore on behalf of a user.</li>
		 * 	<li>user_blocks_read: Read access to a user's list of ignored users.</li>
		 * 	<li>user_follows_edit: Access to manage a user's followed channels.</li>
		 * 	<li>channel_read: Read access to non-public channel information, including email address and stream key.</li>
		 * 	<li>channel_editor: Write access to channel metadata (game, status, etc).</li>
		 * 	<li>channel_commercial: Access to trigger commercials on channel.</li>
		 * 	<li>channel_stream: Ability to reset a channel's stream key.</li>
		 * 	<li>channel_subscriptions: Read access to all subscribers to your channel.</li>
		 * 	<li>channel_check_subscription: Read access to check if a user is subscribed to your channel.</li>
		 * 	<li>chat_login: Ability to log into chat and send messages.</li>
		 * </ul>
		 * 
		 * <p>The requested scopes are:</p> 
		 * <ul>
		 * 	<li>user_read</li>
		 * 	<li>user_follows_edit</li>
		 * 	<li>channel_read</li>
		 * 	<li>channel_editor</li>
		 * 	<li>channel_commercial</li>
		 *  <li>channel_subscriptions</li>
		 * </ul>
		 * 
		 */
		public static const SCOPES:String = "user_read+user_follows_edit+channel_read+channel_editor+channel_commercial+channel_subscriptions";
		
		/**
		 * Redirect URL. User is sent to this url after authorization.
		 */ 
		public static const REDIRECT_URL:String = "https://sites.google.com/site/deezja/tools/twitch-oauth";
		
		/**
		 * Twitch logout URL - allows for user to switch accounts
		 */ 
		public static const LOGOUT_URL:String = "http://www.twitch.tv/user/logout";
		
		/**
		 * Paypal donation url.
		 * 
		 * @default https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SVAYVXT2VVHQ2
		 */ 
		public static const DONATE_URL:String = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SVAYVXT2VVHQ2";
		
		/**
		 * TeeBoard website url.
		 */ 
		public static const APP_SITE_URL:String = "https://sites.google.com/site/deezja/tools/teeboard";
		
		//-----------------------------------------------
		//
		// PRIVATE PROPERTIES
		//
		//-----------------------------------------------
		
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/settings", "/");
		
		/**
		 * The stored window bounds (size / position)
		 */ 
		private var _windowBounds:Rectangle;
		
		/**
		 * The stored user preferences.
		 */ 
		private var _settings:SettingsVO;
		
		/**
		 * The doashboards for each user.
		 */ 
		private var _dashboards:Object;
		
		/**
		 * 
		 */ 
		private var _user:UserVO;
		
		/**
		 * 
		 */ 
		private var _users:ArrayCollection;
		
		/**
		 * 
		 */ 
		private var _currentVersion:String;
		
		//-----------------------------------------------
		//
		// PUBLIC PROPERTIES
		//
		//-----------------------------------------------
		
		/**
		 * Indicates if the main application window is visible.
		 * When the application is minimized, it becomes invisible. 
		 * Views can respond to changes in app visibility by watching this property, 
		 * by listening for a propertyChange event.
		 * 
		 */ 
		public var windowVisible:Boolean = false;
		
		/**
		 * Twitch access token for this application.
		 */ 
		public var token:String = "";
		
		/**
		 * The scopes this application has been authorized for.
		 */ 
		public var scopes:Array;
		
		/**
		 * Twitch root object.
		 */ 
		public var root:RootVO;
		
		/**
		 * Twitch channel representing the current logged in user.
		 */ 
		public var channel:ChannelVO;
		
		/**
		 * 
		 */ 
		public var cursorManager:ICursorManager;
		
		/**
		 * Twitch channel representing the selected editing channel.
		 * 
		 * <p>
		 * This is not necessarily the logged in user's channel, 
		 * but rather the channel of which the user is an editor and 
		 * has selected (avitived) to edit.
		 * </p>
		 * 
		 */ 
		public var selectedChannel:ChannelVO;
		
		/**
		 * The total number of followers for the current user.
		 */ 
		public var totalFollowers:Number;
		
		/**
		 * List of followers
		 */ 
		public var followers:ArrayCollection;
		
		/**
		 * 
		 */ 
		public var editors:ArrayCollection;
		
		/**
		 * Streams the currently authenticated user is following.
		 */ 
		public var followedStreams:ArrayCollection;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		public function ApplicationModel() {
			trace("ApplicationModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one ApplicationModel instance allowed. " +
					"To access the Singleton instance, ApplicationModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():ApplicationModel {
			if(!INSTANCE) INSTANCE = new ApplicationModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		//------------------------------------
		// initModel()
		//------------------------------------
		private function initModel():void {
			trace("ApplicationModel ::: initModel");
			var soBounds:Object;
			var winBounds:Rectangle;
			var screens:Array;
			var i:int;
			
			if(so.size == 0) {
				// FIRST TIME USER
				so.data.isFirstRun = false;
			}else {
				_isFirstRun = false;
			}
			
			if(so.data.hasOwnProperty("token") && so.data.token != null) {
				token = so.data.token;
				scopes = so.data.scopes;
			}
			
			if(so.data.hasOwnProperty("bounds") && so.data.bounds != null) {
				soBounds = so.data.bounds;
				trace("    - stored bounds: ", soBounds);
				trace(ObjectUtil.toString(soBounds));
				trace("    - bounds x: ", soBounds.x);
				trace("    - bounds y: ", soBounds.y);
				trace("    - bounds width: ", soBounds.width);
				trace("    - bounds height: ", soBounds.height);
				trace("=============================================");
				//winBounds = new Rectangle(soBounds.x, soBounds.y, soBounds.width, soBounds.height);
				// force 840x600 size
				//winBounds = new Rectangle(soBounds.x, soBounds.y, 840, 600);
				winBounds = new Rectangle(soBounds.x, soBounds.y, 870, 630);
				// check if bounds are valid
				screens = Screen.getScreensForRectangle(winBounds);
				//trace(ObjectUtil.toString(screens));
				if(screens.length != 0) {
					//bounds are valid, use them
					_windowBounds = winBounds;
				}
			}
			
			if(so.data.hasOwnProperty("chatBounds") && so.data.chatBounds != null) {
				soBounds = so.data.chatBounds;
				trace("    - stored chat bounds: ", soBounds);
				trace("    - bounds chat x: ", soBounds.x);
				trace("    - bounds chat y: ", soBounds.y);
				trace("    - bounds chat width: ", soBounds.width);
				trace("    - bounds chat height: ", soBounds.height);
				trace("=============================================");
				winBounds = new Rectangle(soBounds.x, soBounds.y, soBounds.width, soBounds.height);
				// check if bounds are valid
				screens = Screen.getScreensForRectangle(winBounds);
				//trace(ObjectUtil.toString(screens));
				if(screens.length != 0) {
					//bounds are valid, use them
					_chatBounds = winBounds;
				}
			}
			
			if(so.data.hasOwnProperty("settings") && so.data.settings != null) {
				_settings = so.data.settings;
			}else {
				_settings = new SettingsVO();
			}
			
			trace("STORED SETTINGS");
			trace(ObjectUtil.toString(settings));
			trace("=============================================");
			
			if(so.data.hasOwnProperty("minimizeToTray")) {
				_minimizeToTray = so.data.minimizeToTray;
			}
			
			//so.data.dashboards = new Object();
			
			// check for dashboards
			if(so.data.hasOwnProperty("dashboards") && so.data.dashboards != null) {
				_dashboards = so.data.dashboards;
			}else {
				_dashboards = new Object();
			}
			
			trace("STORED DASHBOARDS");
			trace(ObjectUtil.toString(_dashboards));
			trace("=============================================");
			
			_users = new ArrayCollection();
			if(so.data.hasOwnProperty("users") && so.data.users != null) {
				if(so.data.users is ArrayCollection) {
					trace("    - copying users from ArrayCollection");
					_users.addAll((so.data.users as ArrayCollection).list);
					so.data.users = _users.source;
					so.flush();
				}else {
					_users.source = so.data.users;
				}
			}
			
			//_users.removeItemAt(_users.length-1);
			
			trace("STORED USERS:");
			trace(ObjectUtil.toString(_users));
			trace("=============================================");
			
			if(so.data.hasOwnProperty("chatUsersOn")) {
				_chatUsersOn = so.data.chatUsersOn;
			}
			
			//invalidate();
		}
		
		//------------------------------------
		// dispatchPropertyEvent()
		//------------------------------------
		private function dispatchPropertyEvent(prop:String):void {
			trace("ApplicationModel ::: dispatchPropertyEvent");
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = prop;
			dispatchEvent(propEvent);
		}
		
		//------------------------------------
		// flush()
		//------------------------------------
		private function flush():void {
			trace("ApplicationModel ::: flush");
			so.data.token = token;
			so.data.scopes = scopes;
			so.flush();
		}
		
		//------------------------------------
		// setCurrentVersion()
		//------------------------------------
		private function setCurrentVersion():void {
			trace("ApplicationModel ::: setCurrentVersion");
			trace("----------------------------------");
			trace("    - currentVersion: ", _currentVersion);
			trace("----------------------------------");
		}
		
		//------------------------------------
		// setUserDashboards()
		//------------------------------------
		private function setUserDashboards():void {
			trace("ApplicationModel ::: setUserDashboards");
			// when a user is set, get the dashboards for that user
			// if none exists, create one and store it.
			var vo:UserVO = _user;
			trace("DASHBOARD ==================================");
			trace("    - user: ", _user.name);
			trace("    - user dashboards: ", _dashboards[vo.name]);
			var dashVO:ChannelDTO;
			if(_dashboards[vo.name] == undefined) {
				trace("    - CREATING NEW DASHBOARD FOR USER: ", vo.name);
				dashVO = new ChannelDTO();
				dashVO.displayName = vo.displayName;
				dashVO.name = vo.name;
				_dashboards[vo.name] = new ArrayCollection();
				_dashboards[vo.name].addItem(dashVO);
				so.data.dashboards = _dashboards;
				so.flush();
			}else if(_dashboards[vo.name] != undefined && _dashboards[vo.name].length == 0) {
				trace("    - CREATING NEW DASHBOARD FOR USER: ", vo.name);
				dashVO = new ChannelDTO();
				dashVO.displayName = vo.displayName;
				dashVO.name = vo.name;
				_dashboards[vo.name] = new ArrayCollection();
				_dashboards[vo.name].addItem(dashVO);
				so.data.dashboards = _dashboards;
				so.flush();
			}
			var evt:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			evt.property = "userDashboards";
			dispatchEvent(evt);
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Clears all user related data from the model and the SharedObject.
		 * 
		 */
		public function invalidate():void {
			trace("ApplicationModel ::: invalidate");
			token = "";
			scopes = null;
			root = null;
			channel = null;
			_user = null;
			so.data.token = "";
			so.data.scopes = null;
			so.flush();
		}
		
		/**
		 * Removes a user with the given token from the stored users list.
		 * If the specified token is also the currenlty active one, 
		 * the model is invalidated.
		 * 
		 * @see #invalidate() invalidate()
		 * 
		 */ 
		public function removeToken(value:String):void {
			trace("ApplicationModel ::: removeToken");
			var len:int = _users.length;
			var dto:UserDTO;
			for(var i:int=0; i<len; i++) {
				dto = _users.getItemAt(i) as UserDTO;
				if(dto.token == value) {
					_users.removeItemAt(i);
					break;
				}
			}
			so.data.users = _users.source;
			so.flush();
			// if the removed user is also the current user
			// invalidate all user properties
			if(value == token) invalidate();
		}
		
		/**
		 * Parses the <code>token</code> and <code>scopes</code> from the redirect url and stores them in the model.
		 * <p>
		 * The <code>value</code> argument should be the redirect url the user is sent to after authorizing the app.
		 * This url contains a hashtag followed by the access token and authorized scopes: <br/>
		 * <code>http://[redurect_url]#access_token=[an access token]&scope=[authorized scopes]</code>.
		 * </p>
		 * 
		 * @param value The redirect url containing the Twitch token and scope. 
		 */ 
		public function parseTokenFromHash(value:String):void {
			trace("ApplicationModel ::: parseTokenFromHash");
			var hash:Array = value.split("#");
			var params:Object;
			if(hash.length != 0) {
				//access_token=[an access token]&scope=[authorized scopes]
				params = mx.utils.URLUtil.stringToObject(hash[1], "&");
				trace(params);
				trace(ObjectUtil.toString(params));
				if(params) {
					token = params.access_token;
					scopes = params.scope.split("+");
					flush();
				}
			}
		}
		
		/**
		 * 
		 */ 
		public function setToken(value:String):void {
			trace("ApplicationModel ::: setToken");
			invalidate();
			token = value;
			scopes = null;
			flush();
		}
		
		/**
		 * 
		 */ 
		public function setUser(vo:UserVO):void {
			trace("ApplicationModel ::: setUser");
			
			_user = vo;
			
			var len:int = _users.length;
			var dto:UserDTO;
			var exists:Boolean = false;
			// check if user exists in stored users list
			for(var i:int=0; i<len; i++) {
				dto = _users.getItemAt(i) as UserDTO;
				if(dto.name == _user.name) {
					trace("    - updating user with current token");
					dto.token = token;
					exists = true;
					break;
				}
			}
			
			trace("    - user exists in stored users list: ", exists);
			// is user doesn't exist yet, create one and store it
			if(!exists) {
				trace("    - adding user to users list");
				dto = new UserDTO();
				dto.displayName = vo.displayName;
				dto.name = vo.name;
				dto.token = token;
				_users.addItem(dto);
				so.data.users = _users.source;
				so.flush();
			}
			
			setUserDashboards();
			
			var evt:PropertyChangeEvent = new PropertyChangeEvent("propertyChange");
			evt.property = "user";
			dispatchEvent(evt);
		}
		
		/**
		 * Removes the specified user from the stored users list.
		 * If the specified user is also the currenlty active one, 
		 * the model is invalidated.
		 * 
		 * @see #invalidate() invalidate()
		 * 
		 */ 
		public function removeUser(value:UserDTO):void {
			// remove the user from list of users
			var len:int = users.length;
			var dto:UserDTO;
			for(var i:int=0; i<len; i++) {
				dto = users.getItemAt(i) as UserDTO;
				if(dto.token == value.token) {
					trace("    - removing user: ", dto.displayName);
					users.removeItemAt(i);
					break;
				}
			}
			so.data.users = _users.source;
			so.flush();
			// if the removed user is also the current user
			// invalidate all user properties
			if(token == value.token) {
				invalidate();
			}
		}
		
		/**
		 * Switches to the provided user.
		 */ 
		public function switchToUser(dto:UserDTO):void {
			trace("ApplicationModel ::: switchToUser");
			
		}
		
		/**
		 * 
		 */ 
		public function setEditors(value:Array):void {
			trace("ApplicationModel ::: setEditors");
			var ac:ArrayCollection = new ArrayCollection();
			var len:uint = value.length;
			var vo:UserVO;
			for(var i:uint=0; i<len; i++) {
				vo = new UserVO(value[i]);
				ac.addItem(vo);
			}
			editors = ac;
		}
		
		/**
		 * 
		 */
		public function setFollowers(value:Object):void {
			trace("ApplicationModel ::: setEditors");
			var f:Array = value.follows;
			var len:uint = f.length;
			var ac:ArrayCollection = new ArrayCollection();
			var vo:FollowerVO;
			for(var i:uint=0; i<len; i++) {
				vo = new FollowerVO(f[i]);
				ac.addItem(vo);
			}
			followers = ac;
			totalFollowers = value._total;
		}
		
		/**
		 * 
		 */
		public function saveSelectedPlatform(value:String):void {
			trace("ApplicationModel ::: saveSelectedPlatform");
			_settings.selectedPlatform = value;
			so.data.settings = _settings;
			so.flush();
		}
		
		/**
		 * Stores the app bounds (size + location) in a SharedObject.
		 * 
		 */
		public function saveWindowBounds(bounds:Rectangle):void {
			trace("ApplicationModel ::: saveWindowBounds");
			trace("    - bounds: ", bounds);
			bounds.y = Math.max(0, bounds.y);
			so.data.bounds = bounds;
			so.flush();
		}
		
		/**
		 * Adds a dashboard (<code>ChannelDTO</code>) to the user's list of dashboards.
		 * 
		 */
		public function addDashboard(dto:ChannelDTO):void {
			trace("Application ::: addDashboard");
			var dash:ArrayCollection = _dashboards[user.name];
			// first check if the dashboard is not already present
			var len:int = dash.length;
			var vo:ChannelDTO;
			var exists:Boolean = false;
			for(var i:int=0; i<len; i++) {
				vo = dash.getItemAt(i) as ChannelDTO;
				if(vo.name == dto.name) {
					trace("    - WARNING: dashboard already exists: ");
					exists = true;
					break;
				}
			}
			if(!exists)	dash.addItem(dto);
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * The authorization url, including query parameters to authorize the application for a Twitch user.
		 * 
		 */ 
		public function get authorizeUrl():String {
			trace("ApplicationModel ::: get authorizeUrl");
			// response type=token doesn't require client_secret
			var authURL:String = AUTHORIZE_URL + "&client_id=" + CLIENT_ID;
			authURL += "&redirect_uri=" + REDIRECT_URL;
			authURL += "&scope=" + SCOPES;
			return authURL;
		}
		
		/**
		 * 
		 */ 
		public function get logoutUrl():String {
			trace("ApplicationModel ::: get logoutUrl");
			return LOGOUT_URL;
		}
		
		/**
		 * The http headers required to communicate with the Twich API.
		 * The returned Object contains the <code>Accept</code>, <code>Authorization</code> and <code>Client-ID</code> headers.
		 * 
		 * @return The Object containing required header properties.
		 */ 
		public function get httpHeaders():Object {
			trace("ApplicationModel ::: get httpHeaders");
			// curl -H 'Accept: application/vnd.twitchtv.v3+json' -H 'Authorization: OAuth <access_token>' \
			// -X GET https://api.twitch.tv/kraken
			var headers:Object = new Object();
			headers["Accept"] = "application/vnd.twitchtv.v3+json";
			headers["Authorization"] = "OAuth " + token;
			headers["Client-ID"] = CLIENT_ID;
			// prevent caching of API calls
			headers["Pragma"] = "no-cache";
			headers["Cache-Control"] = "no-cache";
			return headers;
		}
		
		/**
		 * 
		 */
		public function get windowBounds():Rectangle {
			return _windowBounds;
		}
		
		/**
		 * 
		 */
		public function get settings():SettingsVO {
			return _settings;
		}
		
		/**
		 * 
		 */ 
		public function get user():UserVO {
			return _user;
		}
		
		/**
		 * 
		 */ 
		public function get userDashboards():ArrayCollection {
			return _dashboards[user.name];
		}
		
		/**
		 * 
		 */
		public function get users():ArrayCollection {
			return _users;
		}

		/**
		 * 
		 */
		public function get currentVersion():String {
			return _currentVersion;
		}
		public function set currentVersion(value:String):void {
			if(value == _currentVersion) return;
			_currentVersion = value;
			setCurrentVersion();
		}
		
		private var _minimizeToTray:Boolean = true;
		/**
		 * 
		 */ 
		public function get minimizeToTray():Boolean {
			return _minimizeToTray;
		}
		public function set minimizeToTray(value:Boolean):void {
			if(_minimizeToTray == value) return;
			_minimizeToTray = value;
			so.data.minimizeToTray = value;
			so.flush();
		}
		
		private var _isFirstRun:Boolean = true;
		/**
		 * 
		 */ 
		public function get isFirstRun():Boolean {
			return _isFirstRun;
		}
		
		private var _chatBounds:Rectangle;
		/**
		 * 
		 */ 
		public function get chatBounds():Rectangle {
			return _chatBounds;
		}
		public function set chatBounds(value:Rectangle):void {
			if(_chatBounds == value) return;
			_chatBounds = value;
			so.data.chatBounds = value;
			so.flush();
		}
		
		private var _chatUsersOn:Boolean;
		/**
		 * 
		 */ 
		public function get chatUsersOn():Boolean {
			return _chatUsersOn;
		}
		public function set chatUsersOn(value:Boolean):void {
			if(_chatUsersOn == value) return;
			_chatUsersOn = value;
			so.data.chatUsersOn = value;
			so.flush();
		}
		
		private var _currentView:String;
		/**
		 * 
		 */ 
		public function get currentView():String {
			return _currentView;
		}
		public function set currentView(value:String):void {
			if(value == _currentView) return;
			_currentView = value;
			//dispatchPropertyEvent("currentView");
		}
		
	}
	
}
