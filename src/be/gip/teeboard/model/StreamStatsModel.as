package be.gip.teeboard.model {
	
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.dto.FollowersStatDTO;
	import be.gip.teeboard.dto.StreamStatDTO;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.vo.notifications.FollowerVO;
	import be.gip.teeboard.vo.StreamVO;
	
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.net.SharedObject;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.events.PropertyChangeEventKind;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class StreamStatsModel extends EventDispatcher {
		
		private static var INSTANCE:StreamStatsModel;
		private static var DEFAULT_TIMER_DELAY:Number = 60*5;
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		private var statsTimer:Timer;
		private var streamStats_ac:ArrayCollection;
		private var followerStats_ac:ArrayCollection;
		
		private var _stream:StreamVO;
		private var _delay:Number = DEFAULT_TIMER_DELAY;
		private var _streamStartedAt:Date;
		
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/stats/settings", "/");
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function StreamStatsModel() {
			trace("StreamStatsModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one StreamStatsModel instance allowed. " +
					"To access the Singleton instance, StreamStatsModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():StreamStatsModel {
			if(!INSTANCE) INSTANCE = new StreamStatsModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("StreamStatsModel ::: initModel");
			
			if(so.data.hasOwnProperty("delay")) {
				_delay = so.data.delay;
			}
			trace("    - timer delay: ", _delay);
			
			streamStats_ac = new ArrayCollection();
			followerStats_ac = new ArrayCollection();
			
			statsTimer = new Timer(_delay * 1000);
			//statsTimer = new Timer(30 * 1000);
			statsTimer.addEventListener(TimerEvent.TIMER, clock_timerHandler);
			
			_streamStartedAt = new Date();
			
		}
		
		//------------------------------------
		// clock_timerHandler()
		//------------------------------------
		private function clock_timerHandler(event:TimerEvent):void {
			trace("StreamStatsModel ::: clock_timerHandler");
			dispatcher.dispatchEvent(new TwitchEvent(TwitchEvent.GET_STREAM));
			var now:Date = new Date();
			var diff:Number = now.time - _streamStartedAt.time;
			var elapsed:Date = new Date(diff);
			//trace("    - hours: ", elapsed.hoursUTC);
			//trace("    - duration: ", elapsed);
		}
		
		//------------------------------------
		// setDelay()
		//------------------------------------
		private function setDelay():void {
			trace("StreamStatsModel ::: setDelay");
			trace("    - delay: ", _delay);
			so.data.delay = _delay;
			so.flush();
			statsTimer.delay = _delay * 1000;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Starts the timer for retrieving stream info.
		 * 
		 */
		public function start():void {
			trace("StreamStatsModel ::: start");
			clock_timerHandler(null);
			statsTimer.start();
		}
		
		/**
		 * Stops the timer.
		 */
		public function stop():void {
			trace("StreamStatsModel ::: stop");
			statsTimer.stop();
		}
		
		/**
		 * Adds the stream object to the model 
		 * and adds stream stats (viewcount + local time) to the stats list.
		 * 
		 * <p>
		 * Calling this method triggers a <code>PropertyChangeEvent.PROPERTY_CHANGE</code> event,  
		 * with the <code>event.property</code> set to "stream".
		 * </p>
		 * <p>
		 * View controls (<code>List</code>, <code>ComboBox</code>, <code>DataGrid</code>, etc..) 
		 * using the stream list as dataProvider are notified of updates 
		 * through the <code>IViewCollection</code> event system.
		 * </p>
		 */ 
		public function addStreamItem(value:StreamVO):void {
			trace("StreamStatsModel ::: addStreamItem");
			var vo:StreamStatDTO = new StreamStatDTO();
			// keep the old stream object to send along with event
			var oldStream:StreamVO = _stream;
			// store the stream item
			_stream = value;
			// when a stream is offline, item.channel = null.
			if(value != null && value.channel) {
				// set number of viewers, 
				// date is set automatically to the current local date
				vo.numViewers = value.numViewers;
				streamStats_ac.addItem(vo);
				// the date + time of when the stream was last updated
				_streamStartedAt = value.channel.updatedAt;
				//trace(ObjectUtil.toString(stats));
				trace("    - stream start date: ", value.channel.updatedAt);
			}
			// dispatch "stream" change event to listeners.
			var evt:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			evt.kind = PropertyChangeEventKind.UPDATE;
			evt.property = "stream";
			evt.oldValue = oldStream;
			evt.newValue = _stream;
			dispatchEvent(evt);
		}
		
		/**
		 * 
		 */ 
		public function addFollowerItem(value:Object):void {
			trace("StreamStatsModel ::: addFollowersItem");
			var dto:FollowersStatDTO = new FollowersStatDTO();
			if(value.hasOwnProperty("_total")) {
				dto.numFollowers = value._total;
				// add item to collection
				followerStats_ac.addItem(dto);
			}
			trace(ObjectUtil.toString(dto));
			var evt:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			evt.kind = PropertyChangeEventKind.UPDATE;
			evt.property = "follower";
			//evt.oldValue = oldStream;
			//evt.newValue = _stream;
			dispatchEvent(evt);
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * Stream object for the current channel.
		 */ 
		public function get stream():StreamVO {
			return _stream;
		}
		
		/**
		 * List of streams stats containing the stream's viewer count 
		 * at a given time, which can be used in a chart.
		 */ 
		public function get streamStats():ArrayCollection {
			return streamStats_ac;
		}
		
		/**
		 * List of follower stats containing the channel's follower count 
		 * at a given time, which can be used in a chart.
		 */ 
		public function get followerStats():ArrayCollection {
			return followerStats_ac;
		}
		
		/**
		 * 
		 */ 
		public function get medianNumViewers():uint {
			var len:int = streamStats_ac.length;
			var t:uint = 0;
			var avg:uint = 0;
			if(len != 0) {
				for(var i:uint=0; i<len; i++) {
					t += streamStats_ac.getItemAt(i).numViewers;
				}
				avg = t/len;
			}
			return avg;
		}
		
		/**
		 * Determines the interval (in seconds) at which to retrieve stats.
		 * 
		 * @default 5 minutes
		 * @return The time (in seconds) between each stat request.
		 */
		public function get delay():Number {
			return _delay;
		}
		public function set delay(value:Number):void {
			if(value == _delay) return;
			_delay = value;
			setDelay();
		}
		
		/**
		 * The date and time the stream started.
		 * <p>
		 * The value also changes when the channel title or game title are updated, 
		 * so it is not accurate, meaning it is not necessarily the date and time the stream started.
		 * </p>
		 */ 
		public function get streamStartedAt():Date {
			return _streamStartedAt;
		}
		
	}
	
}
