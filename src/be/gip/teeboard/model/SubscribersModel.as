package be.gip.teeboard.model {
	
	import be.gip.teeboard.vo.notifications.SubscriberVO;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	/**
	 * Model for Twitch data related to a channel's subscribers.
	 * <p>
	 * The <code>SubscribersModel</code> only handles data from Twitch (received via the Twitch API).
	 * It does not handle alerts / notifications, which is done by the <code>WidgetsModel</code>
	 * </p>
	 */ 
	public class SubscribersModel extends EventDispatcher {
		
		private static var INSTANCE:SubscribersModel;
		
		private var _numSubscribers:Number = 0;
		private var _subscribers:ArrayCollection;
		private var _newSubscribers:ArrayCollection;
		
		private var lastSubscriberDate:Date;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function SubscribersModel() {
			trace("SubscribersModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one SubscribersModel instance allowed. " +
					"To access the Singleton instance, SubscribersModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():SubscribersModel {
			if(!INSTANCE) INSTANCE = new SubscribersModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("SubscribersModel ::: initModel");
			_subscribers = new ArrayCollection();
			_newSubscribers = new ArrayCollection();
			
			lastSubscriberDate = new Date();
			
		}
		
		//------------------------------------
		// findFollower()
		//------------------------------------
		private function subscriberIsNew(vo:SubscriberVO):Boolean {
			trace("SubscribersModel ::: subscriberIsNew");
			var isNew:Boolean = false;
			trace("    - subscriber name: ", vo.user.displayName);
			trace("    - last date: ", lastSubscriberDate);
			trace("    - follower created: ", vo.createdAt);
			trace("    - is new: ", (vo.createdAt.time > lastSubscriberDate.time));
			trace("----------------------------------");
			if(vo.createdAt.time > lastSubscriberDate.time) {
				isNew = true;
			}
			return isNew;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Processes and stores the <code>value</code> in model.
		 * 
		 * @param value the data (channel subscribers) to store in model
		 */
		public function setSubscribers(value:Object):void {
			trace("SubscribersModel ::: setSubscribers");
			var subs:Array = value.subscriptions;
			var len:uint = subs.length;
			var ac:ArrayCollection = new ArrayCollection();
			var new_ac:ArrayCollection = new ArrayCollection();
			var vo:SubscriberVO;
			for(var i:uint=0; i<len; i++) {
				vo = new SubscriberVO(subs[i]);
				ac.addItem(vo);
				var isNew:Boolean = subscriberIsNew(vo);
				if(isNew) {
					new_ac.addItem(vo);
				}
			}
			
			//new_ac.addItem(ac.getItemAt(0));
			
			_subscribers = ac;
			//
			_newSubscribers = new_ac;
			//trace(ObjectUtil.toString(new_ac));
			_numSubscribers = value._total;
			
			// Set the last checked date only if we have new followers.
			// This is to prevent new subscribers from not being noticed as the twitch server doesn't always adds new subscribers immediately
			// but it does register the actual date they subscribed correctly
			
			var nEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			nEvent.property = "newSubscribers";
			
			if(_newSubscribers.length != 0) {
				lastSubscriberDate = new Date();
				// dispatch new subscribers event, so that the widget can do it's thing
				dispatchEvent(nEvent);
			}
			
			// dispatch event to listeners
			var subEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			subEvent.property = "subscribers";
			dispatchEvent(subEvent);
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function get numSubscribers():Object {
			return _numSubscribers;
		}
		
		/**
		 * 
		 */ 
		public function get subscribers():ArrayCollection {
			return _subscribers;
		}
		
		/**
		 * 
		 */ 
		public function get newSubscribers():ArrayCollection {
			return _newSubscribers;
		}
		
		/**
		 * A list of names of the last 5 subscribers.
		 */ 
		public function get latestSubscriberNames():Array {
			var len:int = Math.min(5, _subscribers.length);
			var vo:SubscriberVO;
			var n:String;
			var names:Array = new Array();
			for(var i:int=0; i<len; i++) {
				vo = _subscribers.getItemAt(i) as SubscriberVO;
				n = vo.user.displayName;
				names.push(n);
			}
			return names;
		}
		
	}
	
}
