package be.gip.teeboard.model {
	
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.events.WidgetEvent;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.log.TeeLogger;
	import be.gip.teeboard.utils.TimeFormatter;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SecurityErrorEvent;
	import flash.events.StatusEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.LocalConnection;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.effects.SoundEffect;
	import mx.events.PropertyChangeEvent;
	import mx.events.PropertyChangeEventKind;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	/**
	 * WidgetsModel Singleton.
	 * <p>
	 * Handles widget notifications.
	 * </p>
	 */ 
	public class WidgetsModel extends EventDispatcher {
		
		public static const DOCUMENTS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard");
		
		public static const WIDGET_CHAT_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/chat/");
		public static const WIDGET_CHAT_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/chat/config.xml");
		public static const WIDGET_CHAT_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/chat/teeboard-chat.xml");
		
		public static const WIDGET_CLOCK_DIR:File  = File.documentsDirectory.resolvePath("TeeBoard/widgets/clock/");
		public static const WIDGET_CLOCK_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/clock/config.xml");
		public static const WIDGET_CLOCK_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/clock/teeboard-clock.xml");
		public static const WIDGET_CLOCK_DATE:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/clock/date.txt");
		
		public static const WIDGET_COUNTDOWN_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/countdown/");
		public static const WIDGET_COUNTDOWN_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/countdown/config.xml");
		public static const WIDGET_COUNTDOWN_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/countdown/teeboard-countdown.xml");
		
		public static const WIDGET_GIVEAWAY_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/giveaways/");
		public static const WIDGET_GIVEAWAY_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/giveaways/config.xml");
		//public static const WIDGET_GIVEAWAY_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/giveaways/teeboard-giveaway.xml");
		
		public static const WIDGET_MEDIA_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/media/");
		public static const WIDGET_MEDIA_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/media/config.xml");
		public static const WIDGET_MEDIA_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/media/teeboard-media.xml");
		
		public static const WIDGET_POLL_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/poll/");
		public static const WIDGET_POLL_OBS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/poll/obs/");
		public static const WIDGET_POLL_OBS_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/poll/obs/config.xml");
		//public static const WIDGET_POLL_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/poll/obs/teeboard-poll.xml");
		// javascript poll widget
		public static const WIDGET_POLL_JS_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/poll/obs-js/config.xml");
		
		public static const WIDGET_SPECTRUM_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/spectrum/");
		public static const WIDGET_SPECTRUM_CONFIG:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/spectrum/config.xml");
		public static const WIDGET_SPECTRUM_XSPLIT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/spectrum/teeboard-spectrum.xml");
		
		public static const WIDGET_TICKERTAPE_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/tickertape/");
		public static const WIDGET_TICKERTAPE_TEXT:File = File.documentsDirectory.resolvePath("TeeBoard/widgets/tickertape/tickertape.txt");
		
		/**
		 * The chat widget configuration settings.
		 * <p>Settings are loaded from and saved to an xml file in the widget directory.</p>
		 */ 
		public var chatConfig:XML;
		
		/**
		 * The clock widget configuration settings.
		 * <p>Settings are loaded from and saved to an xml file in the widget directory.</p>
		 */ 
		public var clockConfig:XML;
		
		/**
		 * The countdown widget configuration settings.
		 * <p>Settings are loaded from and saved to an xml file in the widget directory.</p>
		 */ 
		public var countdownConfig:XML;
		
		/**
		 * The media widget configuration settings. 
		 * <p>Settings are loaded from and saved to an xml file in the widget directory.</p>
		 */ 
		public var mediaConfig:XML;
		
		/**
		 * The poll widget configuration settings. 
		 * <p>Settings are loaded from and saved to an xml file in the widget directory.</p>
		 */ 
		public var pollConfig:XML;
		
		/**
		 * The spectrum widget configuration settings.
		 * <p>Settings are loaded from and saved to an xml file in the widget directory.</p>
		 */ 
		public var spectrumConfig:XML;
		
		/**
		 * A list of user names (chat bots) to exclude from showing in Chat Widget
		 */
		public var chatBlacklist:ArrayCollection
		
		
		private static const APP_WIDGETS_DIR:File = File.applicationDirectory.resolvePath("widgets");
		private static const APP_SOUND_DIR:File = File.applicationDirectory.resolvePath("sound");
		
		private static var INSTANCE:WidgetsModel;
		
		// shared object used by FollowersModel, NotificationsModel and WidgetsModel
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/widgets/settings", "/");
		
		private var clockDateTimer:Timer;
		
		private var tickerTimer:Timer;
		private var currentTickerIndex:int = -1;
		
		private var logger:TeeLogger = TeeLogger.getInstance();
		private var log:LogMessage = new LogMessage();
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function WidgetsModel() {
			trace("WidgetsModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one WidgetsModel instance allowed. " +
					"To access the Singleton instance, WidgetsModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():WidgetsModel {
			if(!INSTANCE) INSTANCE = new WidgetsModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("WidgetsModel ::: initModel");
			
			if(so.data.hasOwnProperty("chatWidgetEnabled")) {
				_chatWidgetEnabled = so.data.chatWidgetEnabled;
			}else {
				so.data.chatWidgetEnabled = _chatWidgetEnabled;
				so.flush();
			}
			
			if(so.data.hasOwnProperty("clockDateEnabled")) {
				_clockDateEnabled = so.data.clockDateEnabled;
			}
			
			if(so.data.hasOwnProperty("clockDateFormat")) {
				_clockDateFormat = so.data.clockDateFormat;
			}
			
			chatBlacklist = new ArrayCollection();
			if(so.data.hasOwnProperty("chatBlacklist") && so.data.chatBlacklist != null) { 
				chatBlacklist.source = so.data.chatBlacklist;
			}
			
			_tickertape = new ArrayCollection();
			if(so.data.hasOwnProperty("tickerWidgetEnabled")) {
				_tickerWidgetEnabled = so.data.tickerWidgetEnabled;
				_tickerUpperCase = so.data.tickerUpperCase;
				_tickertape.source = so.data.tickertape;
			}else {
				so.data.tickerWidgetEnabled = _tickerWidgetEnabled;
				so.data.tickerUpperCase = _tickerUpperCase;
			}
			
			/**
			 * clear chat properties as they are no longer needed.
			 * 	chatAutoHide = false
			 * 	chatAutoHideDelay = 20
			 * 	chatBackgroundAlpha = 0.1
			 * 	chatBackgroundColor = 0
			 * 	chatFontFamily = "Lucida Sans Unicode"
			 * 	chatFontSize = 20
			 * 	chatGlowEnabled = true
			 * 	chatShowTime = true
			 * 	chatTextColor = 13421772
			 * 	chatUserCaps = false
			 * 	chatUserColor = 16764006
			 */
			if(so.data.hasOwnProperty("chatAutoHide")) {
				delete so.data.chatAutoHide;
				delete so.data.chatAutoHideDelay;
				delete so.data.chatBackgroundAlpha;
				delete so.data.chatBackgroundColor;
				delete so.data.chatFontFamily;
				delete so.data.chatFontSize;
				delete so.data.chatGlowEnabled;
				delete so.data.chatShowTime;
				delete so.data.chatTextColor;
				delete so.data.chatUserCaps;
				delete so.data.chatUserColor;
				so.flush();
			}
			
			clockDateTimer = new Timer(24*60*60*1000, 1);
			clockDateTimer.addEventListener(TimerEvent.TIMER_COMPLETE, dateTimer_completeHandler);
			setClockDateTimer();
			
			tickerTimer = new Timer(_tickerInterval*1000);
			tickerTimer.addEventListener(TimerEvent.TIMER, tickerTape_timerHandler);
			if(_tickerWidgetEnabled) {
				tickerTimer.start();
			}
			
			copyWidgetsAssets();
			
		}
		
		/**
		 * Copies the widgets folder from the application install directory to the user's documents directory.
		 * <p>If the folder already exists, nothing happens.</p>
		 */ 
		private function copyWidgetsAssets():void {
			trace("WidgetsModel ::: copyWidgetsAssets");
			// copy widgets folder from app directory to user documents directory
			var widgetDest:File = DOCUMENTS_DIR.resolvePath("widgets");
			if(widgetDest.exists == false) {
				trace("    - copying widget assets folder");
				APP_WIDGETS_DIR.copyTo(widgetDest, true);
				log.message = "WidgetsModel :: copying widgets folder to user documents directory.";
				logger.log(log);
			}
		}
		
		//------------------------------------
		// dateTimer_completeHandler()
		//------------------------------------
		private function dateTimer_completeHandler(event:TimerEvent):void {
			trace("WidgetsModel ::: dateTimer_completeHandler");
			// reset the date timer
			setClockDateTimer();
		}
		
		//------------------------------------
		// setClockDateTimer()
		//------------------------------------
		private function setClockDateTimer():void {
			trace("WidgetsModel ::: setClockDateTimer");
			clockDateTimer.reset();
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "updateClockDate";
			
			var ms:Number;
			var fmt:String;
			
			var now:Date = new Date();
			var midnight:Date = new Date();
			midnight.date += 1;
			midnight.hours = 0;
			midnight.minutes = 0;
			midnight.seconds = 0;
			midnight.milliseconds = 100;
			trace("    - now: ", now);
			trace("    - midnight: ", midnight);
			ms = midnight.time - now.time;
			trace("    - remaining ms: ", ms);
			fmt = TimeFormatter.formatMilliseconds(ms);
			trace("    - remaining time: ", fmt);
			clockDateTimer.delay = ms;
			
			if(_clockDateEnabled) {
				trace("    - dispatching clock date event");
				dispatchEvent(propEvent);
				clockDateTimer.start();
			}
		}
		
		//------------------------------------
		// tickerTape_timerHandler()
		//------------------------------------
		private function tickerTape_timerHandler(event:TimerEvent):void {
			trace("WidgetsModel ::: tickerTape_timerHandler");
			if(!tickerWidgetEnabled) {
				tickerTimer.reset();
				return;
			}
			
			var len:int = _tickertape.length;
			
			if(currentTickerIndex >= len-1) {
				currentTickerIndex = 0;
			}else {
				currentTickerIndex += 1;
			}
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "tickerTime";
			dispatchEvent(propEvent);
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Copies the chat widgets folder from the application install directory to the user's documents directory.
		 * <p>Calling this method checks if the folder exists, if so the existing folder is renamed.</p>
		 */ 
		public function updateWidgetsAssets():void {
			trace("WidgetsModel ::: updateWidgetsAssets");
			// copy widgets folder from app directory to user documents directory
			var widgetDest:File = DOCUMENTS_DIR.resolvePath("widgets");
			var widgetCopy:File = DOCUMENTS_DIR.resolvePath("widgets-old");
			if(widgetDest.exists) {
				trace("    - renaming existing widgets folder");
				widgetDest.moveTo(widgetCopy, true);
				_widgetsBackupCreated = true;
				log.message = "WidgetsModel :: renaming existing widgets folder to widgets-old.";
				logger.log(log);
			}
			trace("    - copying widget assets folder");
			APP_WIDGETS_DIR.copyTo(widgetDest, true);
			log.message = "WidgetsModel :: copying widgets folder from install directory to " + widgetDest;
			logger.log(log);
		}
		
		/**
		 * 
		 */ 
		public function getBlacklistIndex(value:String):int {
			trace("WidgetsModel ::: getBlacklistIndex");
			var from:String = (value.indexOf("* ") == 0) ? value.split(" ")[1] : value;
			var idx:int = chatBlacklist.getItemIndex(from);
			return idx;
		}
		
		/**
		 * 
		 */ 
		public function addBlacklistName(value:String):void {
			trace("WidgetsModel ::: addBlacklistName");
			// first see if name is already present
			var n:String = value.toLowerCase();
			var idx:int = chatBlacklist.getItemIndex(n);
			trace("    - idx: ", idx);
			if(idx == -1) {
				chatBlacklist.addItem(n);
				so.data.chatBlacklist = chatBlacklist.source;
				so.flush();
			}
		}
		
		/**
		 * 
		 */ 
		public function removeBlacklistAt(idx:int):void {
			trace("WidgetsModel ::: removeBlacklistAt");
			chatBlacklist.removeItemAt(idx);
			so.data.chatBlacklist = chatBlacklist.source;
			so.flush();
		}
		
		/**
		 * 
		 */ 
		public function addTickerText(value:String):void {
			trace("WidgetsModel ::: addTickerText");
			_tickertape.addItem(value);
			
			updateTickertape();
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "tickertape";
			dispatchEvent(propEvent);
		}
		
		/**
		 * 
		 */ 
		public function removeTickerTextAt(idx:int):void {
			trace("WidgetsModel ::: removeTickerTextAt");
			_tickertape.removeItemAt(idx);
			if(_tickertape.length == 0) {
				tickerWidgetEnabled = false;
				tickerTimer.reset();
			}
			
			updateTickertape();
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "tickertape";
			dispatchEvent(propEvent);
		}
		
		/**
		 * 
		 */ 
		public function updateTickerTextAt(value:String, idx:int):void {
			trace("WidgetsModel ::: updateTickerTextAt");
			_tickertape.setItemAt(value, idx);
			
			updateTickertape();
		}
		
		/**
		 * 
		 */ 
		public function getCurrentTickerText():String {
			trace("WidgetsModel ::: getCurrentTickerText");
			return _tickertape.getItemAt(currentTickerIndex).toString();
		}
		
		/**
		 * Updates the list of tickertape texts in the <code>SharedObject</code>.
		 * 
		 * <p>Call this method for instance when the order of the text items changes.</p>
		 */ 
		public function updateTickertape():void {
			trace("WidgetsModel ::: updateTickertape");
			so.data.tickertape = _tickertape.toArray();
			so.flush();
		}
		
		//==============================================================
		//
		// IMPLICIT GETTER / SETTERS
		//
		//==============================================================
		
		private  var _widgetsBackupCreated:Boolean = false;
		/**
		 * Indicates if a backup of the widgets folder was created after an application update.
		 */ 
		public function get widgetsBackupCreated():Boolean {
			return _widgetsBackupCreated;
		}
		
		//------------------------------------
		// IRC WIDGETS PROPERTIES
		//------------------------------------
		
		private var _chatWidgetEnabled:Boolean = false;
		/**
		 * 
		 */
		public function get chatWidgetEnabled():Boolean {
			return _chatWidgetEnabled;
		}
		public function set chatWidgetEnabled(value:Boolean):void {
			if(value == _chatWidgetEnabled) return;
			_chatWidgetEnabled = value;
			so.data.chatWidgetEnabled = value;
			so.flush();
		}
		
		//------------------------------------
		// CLOCK WIDGET PROPERTIES
		//------------------------------------
		
		private var _clockDateFormat:String = "MMM DD YYYY";
		/**
		 * 
		 */ 
		public function get clockDateFormat():String {
			return _clockDateFormat;
		}
		public function set clockDateFormat(value:String):void {
			if(value == _clockDateFormat) return;
			_clockDateFormat = value;
			so.data.clockDateFormat = value
			so.flush()
		}
		
		private var _clockDateEnabled:Boolean = false;
		/**
		 * 
		 */ 
		public function get clockDateEnabled():Boolean {
			return _clockDateEnabled;
		}
		public function set clockDateEnabled(value:Boolean):void {
			if(_clockDateEnabled == value) return;
			_clockDateEnabled = value;
			so.data.clockDateEnabled = value;
			so.flush();
			setClockDateTimer();
		}
		
		//------------------------------------
		// TICKERTAPE WIDGET PROPERTIES
		//------------------------------------
		
		private var _tickerWidgetEnabled:Boolean = false;
		/**
		 * 
		 */ 
		public function get tickerWidgetEnabled():Boolean {
			return _tickerWidgetEnabled;
		}
		public function set tickerWidgetEnabled(value:Boolean):void {
			if(value == _tickerWidgetEnabled) return;
			_tickerWidgetEnabled = value;
			
			if(value == false) {
				tickerTimer.reset();
			}else {
				tickerTimer.delay = _tickerInterval * 1000;
				tickerTimer.start();
			}
			
			so.data.tickerWidgetEnabled = value;
			so.flush();
		}
		
		private var _tickertape:ArrayCollection;
		/**
		 * 
		 */ 
		public function get tickertape():ArrayCollection {
			return _tickertape;
		}
		
		private var _tickerUpperCase:Boolean = true;
		/**
		 * 
		 */ 
		public function get tickerUpperCase():Boolean {
			return _tickerUpperCase;
		}
		public function set tickerUpperCase(value:Boolean):void {
			if(value == _tickerUpperCase) return;
			_tickerUpperCase = value;
			so.data.tickerUpperCase = value;
			so.flush();
		}
		
		private var _tickerInterval:Number = 30;
		/**
		 * Amount of time to dislay each ticker tape text in seconds.
		 */ 
		public function get tickerInterval():Number {
			return _tickerInterval;
		}
		public function set tickerInterval(value:Number):void {
			if(value == _tickerInterval) return;
			_tickerInterval = value;
			
			tickerTimer.delay = value * 1000;
			
			so.data.tickerInterval = value;
			so.flush();
		}
		
	}
	
}
