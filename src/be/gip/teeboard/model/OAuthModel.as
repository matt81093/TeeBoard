package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.ScopeDTO;
	import be.gip.teeboard.dto.TokenDTO;
	import be.gip.teeboard.vo.RootVO;
	
	import flash.events.EventDispatcher;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	/**
	 * 
	 * 
	 **/
	public class OAuthModel extends EventDispatcher {
		
		public static const CLIENT_ID:String = "jdscx2olcredq285aoadrwiksyg4ixx";
		public static const LOGOUT_URL:String = "http://www.twitch.tv/logout";
		public static const REDIRECT_URL:String = "https://sites.google.com/site/deezja/tools/twitch-oauth";
		
		private static const API_URL:String = "https://api.twitch.tv/kraken/oauth2/authorize?response_type=token";
		private static const SCOPE:String = "chat_login";
		
		private static var INSTANCE:OAuthModel;
		
		public var tokens:ArrayCollection;
		public var scopes:ArrayCollection;
		public var validatedRoot:RootVO;
		
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/oauth/settings", "/");
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function OAuthModel():void {
			if (INSTANCE != null ) {
				throw new Error("Only one OAuthModel instance allowed. " +
					"To access the Singleton instance, use OAuthModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point. 
		 * If no instance exists, it is created.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():OAuthModel {
			if(!INSTANCE) INSTANCE = new OAuthModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("OAuthModel ::: initModel");
			
			var t:Array;
			var i:uint;
			var len:uint;
			var vo:TokenDTO;
			var o:Object;
			
			tokens = new ArrayCollection();
			
			if(so.data.hasOwnProperty("tokens") && so.data.tokens != null) {
				tokens.source = so.data.tokens;
			}
			
			//trace(ObjectUtil.toString(tokens));
			
			// clean up tokens that have no name
			
			len = tokens.length;
			for(i=0; i<len; i++) {
				vo = tokens.getItemAt(i) as TokenDTO;
				if(vo.name == null || vo.name == "") {
					tokens.removeItemAt(i);
				}
			}
			
			
			// create all available scopes
			//createScopes();
			
		}
		
		/**
		 * Stores data into SharedObject
		 * 
		 */
		private function flush():void {
			trace("OAuthModel ::: flush");
			so.data.tokens = tokens.source;
			so.flush();
		}
		
		//------------------------------------
		// createScopes()
		//------------------------------------
		private function createScopes():void {
			trace("OAuthModel ::: createScopes");
			scopes = new ArrayCollection();
			var scope:ScopeDTO;
			scope = new ScopeDTO();
			scope.name = "user_read";
			scope.description = "Read access to non-public user information, such as email address.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "user_blocks_edit";
			scope.description = "Ability to ignore or unignore on behalf of a user.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "user_blocks_read";
			scope.description = "ead access to a user's list of ignored users.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "user_follows_edit";
			scope.description = "Access to manage a user's followed channels.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "channel_read";
			scope.description = "Read access to non-public channel information, including email address and stream key.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "channel_editor";
			scope.description = "Write access to channel metadata (game, status, etc).";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "channel_commercial";
			scope.description = "Access to trigger commercials on channel.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "channel_stream";
			scope.description = "Ability to reset a channel's stream key.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "channel_subscriptions";
			scope.description = "Read access to all subscribers to your channel.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "user_subscriptions";
			scope.description = "Read access to subscriptions of a user.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "channel_check_subscription";
			scope.description = "Read access to check if a user is subscribed to your channel.";
			scopes.addItem(scope);
			
			scope = new ScopeDTO();
			scope.name = "chat_login";
			scope.description = "Ability to log into chat and send messages.";
			scopes.addItem(scope);
			//trace(ObjectUtil.toString(scopes));
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Adds a <code>token</code> to the list of tokens and stores it in the <code>SharedObject</code>.
		 * 
		 */ 
		public function addToken(value:TokenDTO):void {
			trace("OAuthModel ::: setToken");
			// check if token name already exists 
			// as we can not have duplicates
			var len:int = tokens.length;
			var vo:TokenDTO;
			var exists:Boolean = false;
			if(value.name == null || value.token == null) return;
			for(var i:int=0; i<len; i++) {
				vo = tokens.getItemAt(i) as TokenDTO;
				if(vo.name == null) continue;
				if(vo.name.toLowerCase() == value.name.toLowerCase()) {
					exists = true;
					// replace existing token with new token
					vo.token = value.token;
					vo.updatedAt = value.updatedAt;
					vo.createdAt = value.createdAt;
					vo.scopes = value.scopes;
					break;
				}
			}
			if(!exists) {
				tokens.addItem(value);
			}
			flush();
		}
		
		/**
		 * 
		 * 
		 */ 
		public function removeToken(value:TokenDTO):void {
			trace("OAuthModel ::: removeToken");
			var len:int = tokens.length;
			var vo:TokenDTO;
			var idx:int = -1
			for(var i:int=0; i<len; i++) {
				vo = tokens.getItemAt(i) as TokenDTO;
				if(vo.token == value.token) {
					idx = i;
					break;
				}
			}
			if(idx != -1) {
				tokens.removeItemAt(idx);
			}
			flush();
		}
		
		/**
		 * Parses the <code>token</code> from a redirect url and returns it.
		 * <p>
		 * The <code>value</code> argument should be the redirect url the user is sent to after authorizing the app.
		 * This url contains a hashtag followed by the access token and authorized scopes:
		 * <pre>http://app.redirect.url#access_token=[an access token]&scope=[authorized scopes]</pre>
		 * </p>
		 * 
		 * @param value The redirect url containing the Twitch token and scope. 
		 * 
		 * @return String an access token.
		 * 
		 */ 
		public function parseTokenFromHash(value:String):String {
			trace("OAuthModel ::: parseTokenFromHash");
			var hash:Array = value.split("#");
			var params:Object;
			var token:String = "";
			if(hash.length != 0) {
				//access_token=[an access token]&scope=[authorized scopes]
				params = mx.utils.URLUtil.stringToObject(hash[1], "&");
				trace(ObjectUtil.toString(params));
				if(params) {
					token = params.access_token;
				}
			}
			return token;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function get oauthUrl():String {
			var str:String = API_URL + "&amp;client_id=" + CLIENT_ID + "&amp;redirect_uri=" + REDIRECT_URL + "&amp;scope=" + SCOPE;
			return str;
		}
		
		/**
		 * 
		 */ 
		public function getHttpHeadersForToken(vo:TokenDTO):Object {
			trace("OAuthModel ::: getHttpHeadersForToken");
			// curl -H 'Accept: application/vnd.twitchtv.v3+json' -H 'Authorization: OAuth <access_token>' \
			// -X GET https://api.twitch.tv/kraken
			var headers:Object = new Object();
			headers["Accept"] = "application/vnd.twitchtv.v2+json";
			headers["Authorization"] = "OAuth " + vo.token;
			headers["Client-ID"] = OAuthModel.CLIENT_ID;
			return headers;
		}
	}
	
}
