package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.ResolutionDTO;
	
	import flash.events.EventDispatcher;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.collections.IList;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.core.INavigatorContent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.ListEvent;
	import mx.utils.ObjectUtil;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	
	public class ResolutionsModel extends EventDispatcher {
		
		public var resolutions:ArrayCollection;
		
		private static var INSTANCE:ResolutionsModel;
		
		private var so:SharedObject = SharedObject.getLocal("be/gip/teeboard/calculator/settings", "/");
		private var defaultResolution:ResolutionDTO = new ResolutionDTO(1280, 720, "16:9", false);
		private var resolutionSort:Sort;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		public function ResolutionsModel() {
			trace("ResolutionsModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one instance allowed. " +
					"To access the Singleton instance, use ResolutionsModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():ResolutionsModel {
			if(!INSTANCE) INSTANCE = new ResolutionsModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("ResolutionsModel ::: initModel");
			
			if(so.data.hasOwnProperty("resolutions") && so.data.resolutions != null) {
				resolutions = new ArrayCollection(so.data.resolutions);
			}else {
				createDefaultResolutions();
			}
			// TODO: remove new resolutions after 0.1.6
			addResolution(new ResolutionDTO(1536, 864, "16:9", false));
			addResolution(new ResolutionDTO(1096, 616, "16:9", false));
			addResolution(new ResolutionDTO(960, 540, "16:9", false));
			
			//createDefaultResolutions();
			
			resolutionSort = new Sort();
			resolutionSort.fields = [new SortField("height", false, true, true), new SortField("width", false, true, true)]
			resolutions.sort = resolutionSort;
			resolutions.refresh();
			
			//trace(ObjectUtil.toString(resolutions));
			
		}
		
		//------------------------------------
		// createDefaultResolutions()
		//------------------------------------
		private function createDefaultResolutions():void {
			trace("ResolutionsModel ::: createDefaultResolutions");
			resolutions = new ArrayCollection();
			resolutions.addItem(new ResolutionDTO(1920, 1200, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1920, 1080, "16:9", false));
			resolutions.addItem(new ResolutionDTO(1728, 1080, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1680, 1050, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1600, 1000, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1440, 900, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1536, 864, "16:9", false));
			resolutions.addItem(new ResolutionDTO(1280, 800, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1280, 720, "16:9", false));
			resolutions.addItem(new ResolutionDTO(1152, 720, "16:10", false));
			resolutions.addItem(new ResolutionDTO(1096, 616, "16:9", false));
			resolutions.addItem(new ResolutionDTO(1024, 576, "16:9", false));
			resolutions.addItem(new ResolutionDTO(922, 576, "16:10", false));
			resolutions.addItem(new ResolutionDTO(960, 540, "16:9", false));
			resolutions.addItem(new ResolutionDTO(854, 480, "16:9", false));
			resolutions.addItem(new ResolutionDTO(768, 480, "16:10", false));
			resolutions.addItem(new ResolutionDTO(640, 360, "16:9", false));
			resolutions.addItem(new ResolutionDTO(576, 360, "16:10", false));
			flush();
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Stores data in SharedObject.
		 */ 
		public function flush():void {
			trace("ResolutionsModel ::: flush");
			so.data.resolutions = resolutions.source;
			so.flush();
		}
		
		/**
		 * Returns the index of a given item (resolution) in the unfiltered but sorted collection.
		 * 
		 */ 
		public function getResolutionIndex(item:ResolutionDTO):int {
			trace("ResolutionsModel ::: getResolutionIndex");
			//trace(ObjectUtil.toString(resolutions.source));
			// Have to search in the unfiltered, but sorted, collection
			// therefor, create a temp collection with alll existing items
			// and sort them using the same Sort as the original collection
			var list:ArrayCollection = new ArrayCollection(resolutions.source);
			list.sort = resolutionSort;
			list.refresh();
			var len:int = list.length;
			var vo:ResolutionDTO;
			var idx:int = -1;
			//trace("    - length: ", len);
			for(var i:int=0; i<len; i++) {
				vo = list.getItemAt(i) as ResolutionDTO;
				if(vo.equals(item)) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
		/**
		 * 
		 */ 
		public function addResolution(item:ResolutionDTO):void {
			trace("ResolutionsModel ::: addResolution");
			var idx:int = getResolutionIndex(item);
			trace("    - item index: ", idx);
			if(idx == -1) resolutions.addItem(item);
		}
		
		/**
		 * Removes an item (resolution) from the collection and returns it.
		 * Only custom items can be removed. When trying to remove a non custom item, 
		 * <code>null</code> is returned.
		 */ 
		public function removeResolution(item:ResolutionDTO):ResolutionDTO {
			trace("ResolutionsModel ::: removeResolution");
			// only custom resolutions can be removed
			if(!item.isCustom) return null;
			var idx:int = resolutions.getItemIndex(item);
			trace("    - index: ", idx);
			var vo:ResolutionDTO = resolutions.removeItemAt(idx) as ResolutionDTO;
			resolutions.refresh();
			return vo;
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * Returns the index of the default resolution to display.
		 * 
		 */ 
		public function get defaultSelectedIndex():int {
			return getResolutionIndex(defaultResolution);
		}
		
	}
	
}
