package be.gip.teeboard.model {
	
	import be.gip.teeboard.vo.backendless.AuthorVO;
	import be.gip.teeboard.vo.backendless.PanelVO;
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.net.URLRequestHeader;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.formatters.DateFormatter;
	import mx.utils.ObjectUtil;
	import mx.utils.object_proxy;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class BackendlessModel extends EventDispatcher {
		
		public static const APP_ID:String = "5DD52E1C-05F1-2F31-FF85-A91EE7375400";
		public static const APP_VERSRION:String  = "v1";
		public static const REST_KEY:String = "78DA46E4-F1F2-F2B2-FFC0-4A0ECCE39A00";
		
		public static const PANELS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/panels");
		
		//-----------------------------------------------
		//
		// PRIVATE PROPERTIES
		//
		//-----------------------------------------------
		
		private static var INSTANCE:BackendlessModel;
		
		
		//-----------------------------------------------
		//
		// PUBLIC PROPERTIES
		//
		//-----------------------------------------------
		
		public var panels:ArrayCollection;
		public var author:AuthorVO;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function BackendlessModel() {
			trace("BackendlessModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one BackendlessModel instance allowed. " +
					"To access the Singleton instance, BackendlessModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():BackendlessModel {
			if(!INSTANCE) INSTANCE = new BackendlessModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("BackendlessModel ::: initModel");
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * 
		 */ 
		public function setPanels(value:Object):void {
			trace("BackendlessModel ::: setPanels");
			var data:Array = value.data;
			var len:uint = data.length;
			var vo:PanelVO;
			var p:Object;
			var ac:ArrayCollection = new ArrayCollection();
			for(var i:uint=0; i<len; i++) {
				p = data[i];
				vo = new PanelVO();
				vo.created = DateFormatter.parseDateString(p.created);
				vo.name = p.name;
				vo.objectId = p.objectId;
				vo.ownerId = p.ownerId;
				vo.updated = DateFormatter.parseDateString(p.updated);
				vo.url = p.url;
				vo.editable = p.editable;
				vo.authorId = p.authorId;
				trace("    - created: ", p.created);
				trace("    - formatted created: ", DateFormatter.parseDateString(p.created));
				ac.addItem(vo);
			}
			//panels_rep.dataProvider = result.data;
			trace(ObjectUtil.toString(ac));
			panels = ac;
		}
		
		/**
		 * 
		 * 
		 */ 
		public function setPanelAuthor(value:Object):void {
			trace("BackendlessModel ::: setPanelAuthor");
			var vo:AuthorVO = new AuthorVO;
			vo.objectId = value.objectId;
			vo.ownerId = value.ownerId;
			vo.created = DateFormatter.parseDateString(value.created);
			vo.updated = DateFormatter.parseDateString(value.updated);
			
			vo.name = value.name;
			vo.email = value.email;
			vo.info = value.info;
			
			author = vo;
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * HTTP Request headers
		 * 
		 * application-id: app-id-value
		 * secret-key: secret-key-value
		 * application-type: REST
		 */ 
		public function get httpHeaders():Object {
			trace("BackendlessModel ::: get httpHeaders");
			var headers:Object = new Object();
			headers["application-id"] = APP_ID;
			headers["secret-key"] = REST_KEY;
			headers["application-type"] = "REST";
			return headers;
		}
		
		/**
		 * File/Form URLRequest headers
		 * 
		 * application-id: app-id-value
		 * secret-key: secret-key-value
		 * application-type: REST
		 * Content-Type": multipart/form-data
		 */ 
		public function get requestHeaders():Array {
			var headers:Array = new Array();
			headers.push(new URLRequestHeader("application-id", APP_ID));
			headers.push(new URLRequestHeader("secret-key", REST_KEY));
			headers.push(new URLRequestHeader("application-type", "REST"));
			headers.push(new URLRequestHeader("Content-Type", "multipart/form-data"));
			return headers;
		}
		
	}
	
}
