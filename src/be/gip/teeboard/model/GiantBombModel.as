package be.gip.teeboard.model {
	
	import be.gip.teeboard.vo.GiantPlatformVO;
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	
	public class GiantBombModel extends EventDispatcher {
		
		public static const DOCUMENTS_DIR:File = File.documentsDirectory.resolvePath("TeeBoard");
		
		// platforms:
		// http://www.giantbomb.com/api/platforms/?api_key=586f325201d9030e95370084b16ebd07550a156f&field_list=id,name&sort=name:asc
		
		// platform:
		// http://www.giantbomb.com/api/platform/?api_key=586f325201d9030e95370084b16ebd07550a156f&field_list=id,name&sort=name:asc
		
		// games:
		// http://www.giantbomb.com/api/games/?api_key=586f325201d9030e95370084b16ebd07550a156f&field_list=id,name&sort=name:asc
		
		public static const API_GAMES_URL:String = "http://www.giantbomb.com/api/games/";
		public static const API_PLATFORMS_URL:String = "http://www.giantbomb.com/api/platforms";
		
		//public static const API_PLATFORM_URL:String = "http://www.giantbomb.com/api/platform";
		
		public static const API_KEY:String = "586f325201d9030e95370084b16ebd07550a156f";
		
		private static const APP_DATA_DIR:File = File.applicationDirectory.resolvePath("data");
		private static const DOCUMENTS_DATA_DIR:File = File.documentsDirectory.resolvePath("TeeBoard/data");
		
		private static var INSTANCE:GiantBombModel;
		
		/**
		 * List of all available platforms retrieved from giantbomb.com
		 * <p>
		 * This list is not automatically retrieved 
		 * as it's only needed when a user wants to add a new platform to the dashboard.
		 * </p>
		 */ 
		public var allPlatforms:XMLListCollection;
		
		/**
		 * List of game titles.
		 * Used for selecting the channel's currently playing game.
		 */ 
		public var platforms:ArrayCollection;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function GiantBombModel() {
			trace("GiantBombModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one GiantBombModel instance allowed. " +
					"To access the Singleton instance, GiantBombModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():GiantBombModel {
			if(!INSTANCE) INSTANCE = new GiantBombModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("GiantBombModel ::: initModel");
			// read all xml files in data dir
			var files:Array;
			var len:uint;
			var f:File;
			var vo:GiantPlatformVO;
			
			// check if data folder exists in user documents folder
			// if no, copy it over from app dir
			if(!DOCUMENTS_DATA_DIR.exists) {
				APP_DATA_DIR.copyTo(DOCUMENTS_DATA_DIR, false);
			}
			
			files = DOCUMENTS_DATA_DIR.getDirectoryListing();
			len = files.length;
			
			platforms = new ArrayCollection();
			
			for(var i:uint=0; i<len; i++) {
				f = files[i];
				if(!f.isDirectory && f.extension == "xml") {
					//trace("    - xml file: ", f.nativePath);
					// read the xml file
					vo = readPlatformFile(f);
					if(vo.platform != null && vo.platform != "") {
						//trace("    - id: ", vo.id);
						//trace("    - platform: ", vo.platform);
						//trace("    - name length: ", vo.platform.length);
						platforms.addItem(vo);
					}
					//trace("----------------------------");
				}
			}
		}
		
		//------------------------------------
		// readPlatformFile()
		//------------------------------------
		private function readPlatformFile(f:File):GiantPlatformVO {
			//trace("GiantBombModel ::: readPlatformFile");
			var str:String;
			var xml:XML;
			var fs:FileStream = new FileStream();
			var vo:GiantPlatformVO = new GiantPlatformVO();
			try {
				fs.open(f, FileMode.READ);
				str = fs.readUTFBytes(fs.bytesAvailable);
				xml = new XML(str);
				//trace("    - platform: ", vo.platform);
				vo.data = xml;
				vo.url = f.url;
				fs.close();
			}catch(e:Error) {
				trace(e.toString());
			}
			return vo;
		}
		
		//==============================================
		//
		// PUBLIC METHODS
		//
		//==============================================
		
		/**
		 * Returns the index of the platform (<code>GiantPlatformVO</code>) in the <code>platforms</code> collection.
		 * 
		 * @param value The name of the platform for which to return the index.
		 * @return The index of the item in the games collection. -1 if no item exists for the given platform name.
		 * 
		 */ 
		public function getPlatformIndexByName(value:String):int {
			//trace("GiantBombModel ::: getPlatformIndexByName");
			if(platforms == null || value == null || value == "") return -1;
			
			var idx:int = -1;
			var len:int = platforms.length;
			var vo:GiantPlatformVO;
			
			for(var i:int=0; i<len; i++) {
				vo = platforms.getItemAt(i) as GiantPlatformVO;
				if(vo.platform == value) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
		/**
		 * Returns the index of the platform (<code>GiantPlatformVO</code>) in the <code>platforms</code> collection.
		 * 
		 * @param value The GiantBomb id of the platform for which to return the index.
		 * @return The index of the item in the <code>platforms</code> collection. -1 if no item exists for the given platform id.
		 * 
		 */ 
		public function getPlatformIndexById(value:String):int {
			//trace("GiantBombModel ::: getPlatformIndexByName");
			if(platforms == null || value == null || value == "") return -1;
			
			var idx:int = -1;
			var len:int = platforms.length;
			var vo:GiantPlatformVO;
			
			for(var i:int=0; i<len; i++) {
				vo = platforms.getItemAt(i) as GiantPlatformVO;
				if(vo.id === value) {
					idx = i;
					break;
				}
			}
			return idx;
		}
		
	}
	
}
