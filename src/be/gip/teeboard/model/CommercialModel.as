package be.gip.teeboard.model {
	
	import be.gip.teeboard.dto.CommercialDTO;
	import be.gip.teeboard.utils.TimeFormatter;
	import be.gip.teeboard.vo.IntervalVO;
	
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	
	[Event(name="propertyChange", type="mx.events.PropertyChangeEvent")]
	
	[Bindable]
	public class CommercialModel extends EventDispatcher {
		
		private static var INSTANCE:CommercialModel;
		
		/**
		 * The timeout between 2 commercials in milliseconds.
		 * @default 8 minutes (8 x 60 x 1000 milliseconds).
		 */ 
		private static const TIMEOUT_MILLISECONDS:Number = 8*60*1000;
		
		/**
		 * 
		 */ 
		private static const TIMEOUT_MINUTES:int = 8;
		
		private var intervalClock:Timer;
		private var timeoutClock:Timer;
		
		private var _commercials_ac:ArrayCollection;
		private var _intervals_ac:ArrayCollection;
		private var _autorun:Boolean = false;
		private var _runEvery:uint = 60;
		
		private var _lastCommercial:Date;
		private var _nextCommercial:Date;
		
		private var _timeoutMinutes:int = 0;
		
		//=====================================
		//
		// CONSTRUCTOR
		//
		//=====================================
		
		public function CommercialModel() {
			trace("CommercialModel ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one CommercialModel instance allowed. " +
					"To access the Singleton instance, CommercialModel.getInstance().");	
			}
			initModel();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():CommercialModel {
			if(!INSTANCE) INSTANCE = new CommercialModel();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		private function initModel():void {
			trace("CommercialModel ::: initModel");
			_commercials_ac = new ArrayCollection();
			_commercials_ac.addItem(new CommercialDTO("30 sec", 30));
			_commercials_ac.addItem(new CommercialDTO("60 sec", 60));
			_commercials_ac.addItem(new CommercialDTO("90 sec", 90));
			_commercials_ac.addItem(new CommercialDTO("120 sec", 120));
			_commercials_ac.addItem(new CommercialDTO("180 sec", 180));
			//
			_intervals_ac = new ArrayCollection();
			_intervals_ac.addItem(new IntervalVO("15 min", 15*60));
			_intervals_ac.addItem(new IntervalVO("30 min", 30*60));
			_intervals_ac.addItem(new IntervalVO("45 min", 45*60));
			_intervals_ac.addItem(new IntervalVO("60 min", 60*60));
			
			intervalClock = new Timer(60*60*1000);
			intervalClock.addEventListener(TimerEvent.TIMER, intervalClock_timerHandler);
			
			//timeoutClock = new Timer(DEFAULT_TIMEOUT, 1);
			// update every minute - for 8 minutes
			timeoutClock = new Timer(60*1000, TIMEOUT_MINUTES);
			timeoutClock.addEventListener(TimerEvent.TIMER, timeoutClock_timerHandler);
			timeoutClock.addEventListener(TimerEvent.TIMER_COMPLETE, timeoutClock_completeHandler);
			
			// test commercial timeout
			//startTimeout();
		}
		
		private function setAutorun():void {
			trace("CommercialModel ::: setAutorun");
			
		}
		
		private function setRunEvery():void {
			trace("CommercialModel ::: setRunEvery");
			
		}
		
		//------------------------------------
		// intervalClock_timerHandler()
		//------------------------------------
		private function intervalClock_timerHandler(event:TimerEvent):void {
			trace("CommercialModel ::: intervalClock_timerHandler");
		}
		
		//------------------------------------
		// intervalClock_timerHandler()
		//------------------------------------
		private function timeoutClock_timerHandler(event:TimerEvent):void {
			trace("CommercialModel ::: timeoutClock_timerHandler");
			trace("    - clock currentCount: ", timeoutClock.currentCount);
			trace("    - timeoutMinutes: ", _timeoutMinutes);
			_timeoutMinutes = TIMEOUT_MINUTES - timeoutClock.currentCount;
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "timeout";
			dispatchEvent(propEvent);
		}
		
		//------------------------------------
		// intervalClock_timerHandler()
		//------------------------------------
		private function timeoutClock_completeHandler(event:TimerEvent):void {
			trace("CommercialModel ::: timeoutClock_completeHandler");
			_timeoutMinutes = 0;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		/**
		 * Starts an 8 minute timer, which is the time required between 2 commercials.
		 * <p>From the Twitch API:
		 * <li><code>You may only trigger a commercial longer than 30 seconds once every 8 minutes.</code></li>
		 * </p>
		 */ 
		public function startTimeout():void {
			trace("CommercialModel ::: startTimeout");
			// start timeout clock
			if(timeoutClock.running) timeoutClock.reset();
			
			_lastCommercial = new Date();
			
			_timeoutMinutes = TIMEOUT_MINUTES;
			
			timeoutClock.start();
			
			var propEvent:PropertyChangeEvent = new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE);
			propEvent.property = "timeout";
			dispatchEvent(propEvent);
		}
		
		//=====================================
		//
		// GETTER / SETTER METHODS
		//
		//=====================================
		
		/**
		 * Determines whether to automatically run commercials at a certain interval.
		 * The interval at which to run commercials is determined by the <code>runEvery</code> property.
		 * 
		 * @default false
		 * @return true if autorun is enabled, false otherwise.
		 * @see #runEvery CommercialModel.getInstance().runEvery
		 */ 
		public function get autorun():Boolean {
			return _autorun;
		}
		public function set autorun(value:Boolean):void {
			_autorun = value;
			setAutorun();
		}
		
		/**
		 * Determines the interval (in minutes) at which to run commercials.
		 * Requires <code>autorun</code> to be true.
		 * 
		 * @default 60
		 * @return The time (in minutes) to auto run commericals at.
		 * @see #autorun CommercialModel.getInstance().autorun
		 */
		public function get runEvery():uint {
			return _runEvery;
		}
		public function set runEvery(value:uint):void {
			_runEvery = value;
			setRunEvery();
		}
		
		/**
		 * A list of commercial lengths (30/60/90 seconds).
		 * 
		 */ 
		public function get commercialTimes():ArrayCollection {
			return _commercials_ac;
		}
		
		/**
		 * A list of intervalVO's used when automatically running commercials.
		 * 
		 */ 
		public function get intervals():ArrayCollection {
			return _intervals_ac;
		}
		
		/**
		 * 
		 */ 
		public function get lastCommercial():Date {
			return _lastCommercial;
		}
		
		/**
		 * 
		 */
		public function get nextCommercial():Date {
			var d:Date = new Date(_lastCommercial.time);
			d.minutes += _timeoutMinutes;
			return d;
		}
		
		/**
		 * The number of minutes before the next commercial can be run.
		 */
		public function get timeoutMinutes():int {
			return _timeoutMinutes;
		}
		
	}
	
}
