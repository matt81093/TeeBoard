package be.gip.teeboard.events {
	
	import flash.events.Event;
	
	public class RepeaterItemEvent extends Event {
		
		/**
		 * 
		 */ 
		public static const WATCH:String = "watch";
		
		/**
		 * 
		 */ 
		public static const WEB:String = "web";
		
		/**
		 * 
		 */ 
		public static const DOWNLOAD:String = "download";
		
		/**
		 * 
		 */ 
		public static const COPY:String = "copy";
		
		/**
		 * 
		 */ 
		public static const HIGHLIGHT:String = "highlight";
		
		
		public function RepeaterItemEvent(type:String) {
			super(type);
		}
		
		override public function clone():Event {
			return new RepeaterItemEvent(type);
		}
		
	}
	
}
