package be.gip.teeboard.events {
	
	import flash.events.Event;
	
	public class TeeBoardEvent extends Event {
		
		public static const SUBMIT:String = "submit";
		public static const CANCEL:String = "cancel";
		
		public function TeeBoardEvent(type:String) {
			super(type);
		}
		
		override public function clone():Event {
			return new TeeBoardEvent(type);
		}
		
	}
	
}
