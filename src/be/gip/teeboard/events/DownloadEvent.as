package be.gip.teeboard.events {
	
	import flash.events.Event;
	
	public class DownloadEvent extends Event {
		
		public static const INFO_COMPLETE:String = "infoComplete";
		public static const CHUNK_START:String = "chunkStart";
		public static const CHUNK_PROGRESS:String = "chunkProgress";
		public static const CHUNK_COMPLETE:String = "chunkComplete";
		public static const DOWNLOAD_END:String = "downloadEnd";
		public static const DOWNLOAD_START:String = "downloadStart";
		public static const DOWNLOAD_CANCELED:String = "downloadCanceled";
		
		public function DownloadEvent(type:String) {
			super(type);
		}
		
		override public function clone():Event {
			return new DownloadEvent(type);
		}
		
	}
	
}
