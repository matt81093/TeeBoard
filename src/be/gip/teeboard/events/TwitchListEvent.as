package be.gip.teeboard.events {
	
	import flash.events.Event;
	
	import mx.controls.listClasses.IListItemRenderer;
	import mx.events.ListEvent;
	
	public class TwitchListEvent extends ListEvent {
		
		/*public static const IMAGE_CLICK:String = "imageClick";*/
		public static const WATCH:String = "watch";
		public static const VISIT:String = "visit";
		public static const FOLLOW:String = "follow";
		public static const UNFOLLOW:String = "unfollow";
		public static const DOWNLOAD:String = "download";
		public static const VODS:String = "vods";
		
		/**
		 * The index in the data provider for the selected item renderer.
		 */ 
		public var index:int = -1;
		
		public function TwitchListEvent(type:String, itemRenderer:IListItemRenderer=null, index:int=-1) {
			super(type);
			super.itemRenderer = itemRenderer;
			this.index = index;
		}
		
		override public function clone():Event {
			return new TwitchListEvent(type, itemRenderer, index);
		}
		
	}
	
}
