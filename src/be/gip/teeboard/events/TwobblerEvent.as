package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class TwobblerEvent extends MVCEvent {
		
		public static const GET_RECENT_TRACKS:String = "getRecentTracks";
		public static const OPTIONS_CHANGE:String = "optionsChange";
		public static const SERVICE_FAULT:String = "serviceFault";
		
		public function TwobblerEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new TwobblerEvent(type, data);
		}
		
	}
	
}
