package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class DonationsEvent extends MVCEvent {
		
		public static const GET_DONATIONS:String = "getDonations";
		
		public static const VERIFY_SERVICE:String = "verifyTracker";
		
		public static const GET_TWITCHPLUS_TOKEN:String = "getTwitchPlusToken";
		
		public function DonationsEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new DonationsEvent(type, data);
		}
		
	}
	
}
