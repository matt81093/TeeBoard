package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class GiantBombEvent extends MVCEvent {
		
		public static const UPDATE_PLATFORM:String = "updatePlatform";
		public static const UPDATE_PLATFORM_SUCCESS:String = "updateSuccess";
		public static const UPDATE_PLATFORM_ERROR:String = "updateError";
		
		public function GiantBombEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new GiantBombEvent(type, data);
		}
		
	}
	
}
