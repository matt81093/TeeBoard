package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class BackendlessEvent extends MVCEvent {
		
		public static const GET_PANELS:String = "getPanels";
		
		public static const GET_AUTHOR:String = "getAuthor";
		
		public function BackendlessEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new BackendlessEvent(type, data);
		}
		
	}
	
}
