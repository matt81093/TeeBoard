package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.vo.StreamVO;
	
	import flash.events.Event;
	
	public class NotificationEvent extends MVCEvent {
		
		// FOLLOWERS WIDGET EVENT TYPES
		public static const WRITE_FOLLOWERS:String = "writeFollowers";
		
		// SUBSCRIBER WIDGET EVENT TYPES
		public static const WRITE_SUBSCRIBERS:String = "writeSubscribers";
		
		// DONATIONS WIDGET EVENT TYPES
		public static const WRITE_DONATIONS:String = "writeDonations";
		
		/**
		 * New notifications event
		 */ 
		public static const NEW_NOTIFICATIONS:String = "newNotifications";
		
		public function NotificationEvent(type:String, dto:AbstractDTO=null)	{
			super(type, dto);
		}
		
		override public function clone():Event {
			return new NotificationEvent(type, data);
		}
		
	}
	
}
