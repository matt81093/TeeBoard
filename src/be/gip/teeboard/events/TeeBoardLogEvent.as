package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class TeeBoardLogEvent extends MVCEvent {
		
		public static const WRITE_APP_LOG:String = "writeAppLog";
		
		public function TeeBoardLogEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new TeeBoardLogEvent(type, data);
		}
		
	}
	
}
