package be.gip.teeboard.events {
	
	import flash.events.Event;
	
	public class ApplicationMenuEvent extends Event {
		
		public static const ABOUT:String = "about";
		
		public static const DONATE:String = "donate";
		
		public static const DOWNLOAD_DIRECTORY:String = "downloadDirectory";
		
		public static const DOWNLOAD_MANAGER:String = "downloadManager";
		
		public static const EXIT:String = "exit";
		
		public static const HELP:String = "help";
		
		public static const MINIMIZE_TO_TRAY:String = "minimizeToTray";
		
		public static const SHOW:String = "show";
		
		public static const PATCH_NOTES:String = "patchNotes";
		
		public static const WEBSITE:String = "website";
		
		public static const YOUTUBE:String = "youtube";
		
		public static const NOTIFICATION_CHANGE:String = "notificationChange";
		
		public static const NOTIFICATION_ON_TOP:String = "notificationOnTop";
		
		public static const VIEW_LOG:String = "viewLog";
		
		public function ApplicationMenuEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new ApplicationMenuEvent(type);
		}
		
	}
	
}
