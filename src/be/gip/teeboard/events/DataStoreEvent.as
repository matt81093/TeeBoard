package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class DataStoreEvent extends MVCEvent {
		
		/**
		 * 
		 */
		public static const GET_CHANNEL:String = "getDataStoreChannel";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNELS:String = "getDataStoreChannels";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_TITLES:String = "getDataStoreChannelTitles";
		
		/**
		 * 
		 */ 
		public static const ADD_CHANNEL:String = "addDataStoreChannel";
		
		public function DataStoreEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new DataStoreEvent(type, data);
		}
		
	}
	
}
