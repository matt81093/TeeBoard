package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	
	public class IRCChatEvent extends MVCEvent {
		
		public static const CHAT_MESSAGE:String = "chatMessage";
		
		public static const POLL_MESSAGE:String = "pollMessage";
		
		public function IRCChatEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new IRCChatEvent(type, data);
		}
		
	}
	
}