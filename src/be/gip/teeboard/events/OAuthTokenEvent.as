package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class OAuthTokenEvent extends MVCEvent {
		
		public static const VALIDATE_TOKEN:String = "validateToken";
		
		public function OAuthTokenEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new OAuthTokenEvent(type, data);
		}
		
	}
	
}
