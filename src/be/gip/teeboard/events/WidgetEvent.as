package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	/**
	 * Event class associated with widgets.
	 */ 
	public class WidgetEvent extends MVCEvent {
		
		public static const WRITE_FP_CONFIG:String = "writeFlashPlayerConfig";
		
		// WIDGETS EVENT TYPES
		public static const FIX:String = "fixWidgets";
		
		// CHAT WIDGET EVENT TYPES
		public static const GET_CHAT_CONFIG:String = "getChatConfig";
		public static const UPDATE_CHAT_CONFIG:String = "updateChatConfig";
		
		// CLOCK WIDGET EVENT TYPES
		public static const GET_CLOCK_CONFIG:String = "getClockConfig";
		public static const UPDATE_CLOCK_CONFIG:String = "updateClockConfig";
		public static const WRITE_CLOCK_DATE:String = "writeClockDate";
		
		// COUNTDOWN WIDGET EVENT TYPES
		public static const GET_COUNTDOWN_CONFIG:String = "getCountdownConfig";
		public static const UPDATE_COUNTDOWN_CONFIG:String = "updateCountdownConfig";
		
		// SPECTRUM WIDGET EVENT TYPES
		public static const GET_SPECTRUM_CONFIG:String = "getSpectrumConfig";
		public static const UPDATE_SPECTRUM_CONFIG:String = "updateSpectrumConfig";
		
		// MEDIA WIDGET EVENT TYPES
		public static const GET_MEDIA_CONFIG:String = "getMediaConfig";
		public static const UPDATE_MEDIA_CONFIG:String = "updateMediaConfig";
		public static const GET_MEDIA_LISTING:String = "getMediaListing";
		public static const CREATE_MEDIA_WIDGET:String = "createMediaWidget";
		public static const REMOVE_MEDIA_WIDGET:String = "removeMediaWidget";
		
		// POLL WIDGET EVENT TYPES
		public static const GET_POLL_CONFIG:String = "getPollConfig";
		public static const UPDATE_POLL_CONFIG:String = "updatePollConfig";
		public static const WRITE_POLL:String = "writePoll";
		
		// TICKERTAPE WIDGET EVENT TYPES
		public static var WRITE_TICKERTAPE_TEXT:String = "writeTickertapeText";
		
		public function WidgetEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new WidgetEvent(type, data);
		}
		
	}
	
}
