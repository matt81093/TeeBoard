package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class ImgurEvent extends MVCEvent {
		
		public static const GET_IMGUR_ALBUM:String = "getImgurAlbum";
		
		public function ImgurEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new ImgurEvent(type, data);
		}
		
	}
	
}
