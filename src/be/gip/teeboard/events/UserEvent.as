package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class UserEvent extends MVCEvent {
		
		public static const REMOVE:String = "removeUser";
		public static const SWITCH:String = "switchUser";
		
		public function UserEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new UserEvent(type, data);
		}
		
	}
	
}
