package be.gip.teeboard.events {
	
	import be.gip.mvc.dto.AbstractDTO;
	import be.gip.mvc.events.MVCEvent;
	
	import flash.events.Event;
	
	public class TwitchEvent extends MVCEvent {
		
		/**
		 * 
		 */ 
		public static const GET_ROOT:String = "getRoot";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL:String = "getChannel";
		
		/**
		 * 
		 */ 
		public static const GET_EDITOR_CHANNEL:String = "getEditorChannel";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_BROADCASTS:String = "getChannelBroadcasts";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_DASHBOARD:String = "getChannelDashboard";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_EDITORS:String = "getChannelEditors";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_FOLLOWERS:String = "getChannelFollowers";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_HIGHLIGHTS:String = "getChannelHighlights";
		
		/**
		 * 
		 */ 
		public static const GET_CHANNEL_SUBSCRIBERS:String = "getChannelSubscribers";
		
		/**
		 * 
		 */ 
		public static const GET_FOLLOWED_STREAMS:String = "getFollowStreams";
		
		/**
		 * 
		 */ 
		public static const GET_STREAM:String = "getStream";
		
		/**
		 * 
		 */ 
		public static const GET_TOP_GAMES:String = "getTopGames";
		
		/**
		 * 
		 */ 
		public static const GET_USER:String = "getUser";
		
		/**
		 * 
		 */ 
		public static const RUN_COMMERCIAL:String = "runCommercial";
		
		/**
		 * 
		 */ 
		public static const UPDATE_CHANNEL_STATUS:String = "updateChannelStatus";
		
		public function TwitchEvent(type:String, dto:AbstractDTO=null) {
			super(type, dto);
		}
		
		override public function clone():Event {
			return new TwitchEvent(type, data);
		}
		
	}
	
}
