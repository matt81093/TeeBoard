package be.gip.teeboard.controls {
	
	import flash.display.Sprite;
	
	import mx.collections.ArrayCollection;
	import mx.controls.TileList;
	import mx.controls.listClasses.IListItemRenderer;
	
	/*[Event(name="imageClick", type="be.gip.twitch.teeboard.events.TwitchListEvent")]*/
	[Event(name="watch", type="be.gip.teeboard.events.TwitchListEvent")]
	[Event(name="visit", type="be.gip.teeboard.events.TwitchListEvent")]
	[Event(name="follow", type="be.gip.teeboard.events.TwitchListEvent")]
	[Event(name="unfollow", type="be.gip.teeboard.events.TwitchListEvent")]
	[Event(name="download", type="be.gip.teeboard.events.TwitchListEvent")]
	[Event(name="vods", type="be.gip.teeboard.events.TwitchListEvent")]
	
	/**
	 * Extends <code>TileList</code> and removes the visual selection of items.
	 */ 
	public class TwitchTileList extends TileList {
		
		private var _selectedItem:Object;
		private var _selectedIndex:int = -1;
		
		public function TwitchTileList() {
			super();
		}
		
		//------------------------------------
		// setSelectedIndex()
		//------------------------------------
		private function setSelectedIndex():void {
			//trace("TwitchTileList ::: setSelectedIndex");
			if(!collection) { 
				_selectedIndex = -1; 
				return;
			}
			var idx:int = -1;
			var len:int = collection.length;
			var o:Object
			for(var i:int=0; i<len; i++) {
				o = (collection as ArrayCollection).getItemAt(i);
				if(o == _selectedItem) {
					idx = i;
					break;
				}
			}
			_selectedIndex = idx;
		}
		
		//------------------------------------
		// setSelectedItem()
		//------------------------------------
		private function setSelectedItem():void {
			//trace("TwitchTileList ::: setSelectedItem");
			if(!collection || _selectedIndex == -1) { 
				_selectedItem = null; 
				return;
			}
			var idx:int = _selectedIndex;
			_selectedItem = (collection as ArrayCollection).getItemAt(idx);
		}
		
		override protected function drawHighlightIndicator(indicator:Sprite, x:Number, y:Number, width:Number, height:Number, color:uint, itemRenderer:IListItemRenderer):void {
			// super.drawHighlightIndicator(indicator, 0, y, unscaledWidth - viewMetrics.left - viewMetrics.right, height, color, itemRenderer);
			return;
		}
		
		override protected function drawSelectionIndicator(indicator:Sprite, x:Number, y:Number, width:Number, height:Number, color:uint, itemRenderer:IListItemRenderer):void {
			return;
		}
		
		// Keep an eye on TwitchTileList selectedItem behavior	
		
		/**
		 * Overriden to avoid the data on item renderers being set unnecessary.
		 * @inheritDoc
		 */ 
		override public function get selectedItem():Object {
			return _selectedItem;
		}
		override public function set selectedItem(value:Object):void {
			_selectedItem = value;
			setSelectedIndex();
		}
		
		/**
		 * Overriden to avoid the data on item renderers being set unnecessary.
		 * @inheritDoc
		 */ 
		override public function get selectedIndex():int {
			return _selectedIndex;
		}
		override public function set selectedIndex(value:int):void {
			_selectedIndex = value;
			setSelectedItem();
		}
		
		/**
		 * @inheritDoc
		 */ 
		override public function get dataProvider():Object {
			return super.dataProvider;
		}
		override public function set dataProvider(value:Object):void {
			super.dataProvider = value;
			_selectedIndex = -1;
			_selectedItem = null;
		}
		
	}
	
}
