package be.gip.teeboard.update {
	
	import air.update.ApplicationUpdaterUI;
	import air.update.events.StatusFileUpdateErrorEvent;
	import air.update.events.StatusUpdateErrorEvent;
	import air.update.events.UpdateEvent;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import mx.core.FlexTextField;
	import mx.events.FlexEvent;
	
	[Event(name="error", type="flash.events.ErrorEvent")]
	[Event(name="initialized", type="air.update.events.UpdateEvent")]
	[Event(name="updateComplete", type="flash.events.Event")]
	
	public class Updater extends EventDispatcher {
		
		private static const UPDATE_URL:String = "https://sites.google.com/site/deezja/teeboard-update.xml?attredirects=0&d=1";
		private static const UPDATE_TEST_URL:String = "https://sites.google.com/site/deezja/teeboard-update-test.xml?attredirects=0&d=1";
		
		private var updaterUI:ApplicationUpdaterUI;
		
		private var updateTimer:Timer;
		
		//------------------------------------
		// CONSTRUCTOR
		//------------------------------------
		public function Updater() {
			trace("Updater ::: CONSTRUCTOR");
			checkForUpdate();
		}
		
		//------------------------------------
		// checkForUpdate()
		//------------------------------------
		public function checkForUpdate():void {
			trace("Updater ::: checkForUpdate");
			updaterUI =  new ApplicationUpdaterUI();
			
			//updaterUI.updateURL = UPDATE_TEST_URL;
			updaterUI.updateURL = UPDATE_URL;
			
			trace("    - update URL: ", updaterUI.updateURL);
			trace("    - current version: ", updaterUI.currentVersion);
			trace("    - is first run: ", updaterUI.isFirstRun);
			updaterUI.addEventListener(UpdateEvent.INITIALIZED, update_initializedHandler);
			updaterUI.addEventListener(ErrorEvent.ERROR, update_errorHandler);
			updaterUI.addEventListener(StatusUpdateErrorEvent.UPDATE_ERROR, update_statusUpdateErrorHandler);
			
			// Enables the visibility of the Unexpected Error dialog box. 
			// When set to true, displays this dialog box as part of the update process. 
			updaterUI.isUnexpectedErrorVisible = true;
			
			// Hide the dialog asking for permission for checking for a new update;
			// if you want to see it just leave the default value (or set true).
			updaterUI.isCheckForUpdateVisible = false;
			
			// if isFileUpdateVisible is set to true, File Update, File No Update, 
			// and File Error dialog boxes will be displayed
			updaterUI.isFileUpdateVisible = true;
			
			// if isInstallUpdateVisible is set to true, the dialog box for installing the update is visible
			//updaterUI.isInstallUpdateVisible = false;
			updaterUI.isInstallUpdateVisible = true;
			
			// initialize the updater
			updaterUI.initialize();
		}
		
		//------------------------------------
		// update_initializedHandler()
		//------------------------------------
		private function update_initializedHandler(event:Event):void {
			trace("Updater ::: update_initializedHandler");
			trace("    - current version:", updaterUI.currentVersion);
			updaterUI.checkNow();
			dispatchEvent(event.clone());
			
			updateTimer = new Timer(1000);
			updateTimer.addEventListener(TimerEvent.TIMER, update_timerHandler);
			updateTimer.start();
			
		}
		
		//------------------------------------
		// update_timerHandler()
		//------------------------------------
		private function update_timerHandler(event:TimerEvent):void {
			trace("Updater ::: update_timerHandler");
			trace("    - in progress: ", updaterUI.isUpdateInProgress);
			if(!updaterUI.isUpdateInProgress) {
				updateTimer.reset();
				dispatchEvent(new Event("updateComplete"));
			}
		}
		
		//------------------------------------
		// update_errorHandler()
		//------------------------------------
		private function update_errorHandler(event:ErrorEvent):void {
			trace("Updater ::: update_errorHandler");
			//Alert.show(event.toString());
			dispatchEvent(event.clone());
		}
		
		//------------------------------------
		// update_statusUpdateErrorHandler()
		//------------------------------------
		private function update_statusUpdateErrorHandler(event:StatusUpdateErrorEvent):void {
			trace("Updater ::: update_statusUpdateErrorHandler");
			trace(event.toString());
		}
		
		/**
		 * 
		 */ 
		public function get currentVersion():String {
			return updaterUI.currentVersion;
		}
		
		/**
		 * 
		 */ 
		public function get isFirstRun():Boolean {
			return updaterUI.isFirstRun;
		}
		
		/**
		 * 
		 */ 
		public function get isUpdateInProgress():Boolean {
			return updaterUI.isUpdateInProgress;
		}
		
	}
	
}
