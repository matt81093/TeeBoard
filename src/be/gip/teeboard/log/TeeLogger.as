package be.gip.teeboard.log {
	
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;
	
	import mx.utils.ObjectUtil;
	
	/**
	 * TeeBoard application logger. This is a <code>Singleton</code> class.
	 * 
	 * <p>Log messages are written to application storage directory.</p>
	 */ 
	public class TeeLogger {
		
		private static var INSTANCE:TeeLogger;
		
		/**
		 * Path to application log file (log.txt) in the application storage directory.
		 */ 
		public static const APP_LOG_FILE:File = File.applicationStorageDirectory.resolvePath("log/log.txt");
		
		public function TeeLogger()	{
			trace("TeeLogger ::: CONSTRUCTOR");
			if (INSTANCE != null ) {
				throw new Error("Only one TeeLogger instance allowed. " +
					"To access the Singleton instance, TeeLogger.getInstance().");	
			}
			init();
		}
		
		/**
		 * Singleton access point.
		 * 
		 * @return The Singleton instance.
		 */ 
		public static function getInstance():TeeLogger {
			if(!INSTANCE) INSTANCE = new TeeLogger();
			return INSTANCE;
		}
		
		//=====================================
		//
		// PRIVATE METHODS
		//
		//=====================================
		
		
		//------------------------------------
		// init()
		//------------------------------------
		private function init():void {
			trace("TeeLogger ::: init");
			var dto:LogMessage = new LogMessage();
			dto.mode = FileMode.WRITE;
			dto.message = "LOGGING STARTED";
			log(dto);
			
			dto.mode = FileMode.APPEND;
			dto.message = "Platform :: " + Capabilities.os;
			log(dto);
		}
		
		//------------------------------------
		// getTimeStamp()
		//------------------------------------
		private function getTimeStamp():String {
			//trace("TeeLogger ::: getTimeStamp");
			var d:Date = new Date();
			var hh:String = d.getHours() < 10 ? "0" + d.getHours() : "" + d.getHours();
			var mm:String  = d.getMinutes() < 10 ? "0" + d.getMinutes() :  "" + d.getMinutes();
			var ss:String  = d.getSeconds() < 10 ? "0" + d.getSeconds() :  "" + d.getSeconds();
			var tt:String = "[" + hh + ":" + mm + ":" + ss + "] - ";
			return tt;
		}
		
		//=====================================
		//
		// PUBLIC METHODS
		//
		//=====================================
		
		//------------------------------------
		// log()
		//------------------------------------
		public function log(value:LogMessage):void {
			//trace("TeeLogger ::: log");
			//trace(ObjectUtil.toString(value));
			var fs:FileStream = new FileStream();
			var f:File = TeeLogger.APP_LOG_FILE; 
			var ts:String = getTimeStamp();
			
			var msg:String = ts + value.message + File.lineEnding;
			
			try {
				fs.open(f, value.mode);
				fs.writeUTFBytes(msg);
				fs.close();
			}catch(e:Error) {
				// do nothing
			}
			
		}
		
	}
	
}