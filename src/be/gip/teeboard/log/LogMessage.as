package be.gip.teeboard.log {
	
	import flash.filesystem.FileMode;
	
	/**
	 * Message to be written to the log by the <code>TeeLogger</code>.
	 */ 
	public class LogMessage {
		
		public var message:String;
		public var mode:String = FileMode.APPEND;
		
	}
	
}
