package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.dto.StatusDTO;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.vo.RootVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class UpdateChannelStatusCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("UpdateChannelStatusCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			var dto:StatusDTO = evt.data as StatusDTO;
			trace(ObjectUtil.toString(dto));
			service = ServiceLocator.getInstance().getHTTPService("updateChannelService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access-token and client-id header  
			var headers:Object = model.httpHeaders;
			//headers["X-HTTP-Method-Override"] = "PUT";
			service.headers = headers;
			// service requires PUT method, which Flex doesn't support directly
			// instead use POST and add a _method=PUT query argument
			service.method = URLRequestMethod.POST;
			//var url:String = Kraken.CHANNEL_UPDATE_URL.replace(":channel", "odieman1231");
			//var url:String = Kraken.CHANNEL_UPDATE_URL.replace(":channel", "twitchboard");
			var url:String = Kraken.CHANNEL_UPDATE_URL.replace(":channel", model.channel.name);
			//url += "?channel[status]=Status update from TwitchBoard!&channel[game]=Smite";
			service.url = url;
			//"channel[status]=Playing+cool+new+game!&channel[game]=Diablo"
			var args:Object = {_method:"PUT", "channel[status]":dto.status, "channel[game]":dto.game};
			service.request = args;
			trace(ObjectUtil.toString(args));
			var atoken:AsyncToken = service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("UpdateChannelStatusCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
			// when update was successful, 
			// update model (or dispatch event to get channel data again from server)
			MVCEventDispatcher.getInstance().dispatchEvent(new TwitchEvent(TwitchEvent.GET_CHANNEL));
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("UpdateChannelStatusCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
