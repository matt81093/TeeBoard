package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.CommercialModel;
	import be.gip.teeboard.model.ApplicationModel;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetChannelEditorsCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetChannelEditorsCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			service = ServiceLocator.getInstance().getHTTPService("editorsService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access token  
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			var url:String = Kraken.CHANNEL_EDITORS_URL.replace(":channel", model.channel.name);
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetChannelEditorsCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			model.setEditors(result.users);
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetChannelEditorsCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			var content:Object = event.fault.content;
			//  content = "{"status":404,"message":"Channel ':channel' does not exist","error":"Not Found"}"
			try {
				var json:JSONDecoder = new JSONDecoder(content.toString(), true);
				var value:Object = json.getValue();
				Alert.show("status: " + value.status + "\n" + "message: " + value.message, "Error: " + value.error);
			}catch(e:Error) {
				Alert.show(event.fault.faultString, "Error:");
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
