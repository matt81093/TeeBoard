package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.VideosModel;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetChannelHighlightsCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetChannelHighlightsCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			// replace :channel with the proper channel name
			var url:String = Kraken.CHANNEL_HIGHLIGHTS_URL.replace(":channel", model.channel.name);
			url += "&r=" + new Date().time;
			//var url:String = Kraken.CHANNEL_VIDEOS_URL.replace(":channel", "smitegame");
			service = ServiceLocator.getInstance().getHTTPService("highlightsService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access-token headers
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetChannelHighlightsCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			//trace(ObjectUtil.toString(result));
			VideosModel.getInstance().setHighlights(result);
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetChannelHighlightsCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
