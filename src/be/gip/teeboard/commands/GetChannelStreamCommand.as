package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.StreamStatsModel;
	import be.gip.teeboard.vo.ChannelVO;
	import be.gip.teeboard.vo.RootVO;
	import be.gip.teeboard.vo.StreamVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetChannelStreamCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var statsModel:StreamStatsModel = StreamStatsModel.getInstance();
		
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetChannelStreamCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			var url:String = Kraken.STREAMS_CHANNEL_URL.replace(":channel", model.channel.name);
			//var url:String = Kraken.STREAMS_CHANNEL_URL.replace(":channel", "ferretbomb");
			
			service = ServiceLocator.getInstance().getHTTPService("streamService");
			service.showBusyCursor = false;
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access-token
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetChannelStreamCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			var vo:StreamVO = new StreamVO(result.stream);
			//trace(ObjectUtil.toString(vo));
			// addStatItem adds data to stats list (viewcount + time)  
			// and additionally stores stream object in model 
			//StreamStatsModel.getInstance().stream = vo;
			statsModel.addStreamItem(vo);
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetChannelStreamCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
