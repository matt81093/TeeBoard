package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.FollowedModel;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.vo.StreamVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetFollowedStreamsCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var followedModel:FollowedModel = FollowedModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetFollowedStreamsCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			service = ServiceLocator.getInstance().getHTTPService("followedStreamsService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access token  
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = Kraken.STREAMS_FOLLOWED_URL;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetFollowedStreamsCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			followedModel.setData(result);
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetFollowedStreamsCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
