package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.log.TeeLogger;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.vo.RootVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetRootCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var logger:TeeLogger = TeeLogger.getInstance();
		
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetRootCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			service = ServiceLocator.getInstance().getHTTPService("rootService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			
			model.cursorManager.setBusyCursor();
			
			// call service with access token  
			// this will return basic user info and authorization status
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = Kraken.ROOT_URL;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetRootCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			//trace(ObjectUtil.toString(result));
			var vo:RootVO = new RootVO(result);
			trace(ObjectUtil.toString(vo));
			model.root = vo;
			if(vo.valid) {
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
			
			model.cursorManager.removeBusyCursor();
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetRootCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			model.cursorManager.removeBusyCursor();
			var content:Object = event.fault.content;
			var json:JSONDecoder;
			var body:Object;
			// try converting to JSON
			try {
				json = new JSONDecoder(content.toString(), true);
				body = json.getValue();
			}catch(e:Error) {
				trace("    - json error: ", e.message);
				Alert.show(content.toString(), event.fault.name, Alert.OK);
			}
			var lm:LogMessage = new LogMessage();
			lm.message = event.fault.toString();
			logger.log(lm);
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
