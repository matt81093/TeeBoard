package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.dto.notifications.DonationServiceDTO;
	import be.gip.teeboard.events.DonationsEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.DonationsModel;
	import be.gip.teeboard.net.api.Donations;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	import flash.utils.setTimeout;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	public class VerifyServiceCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var donationsModel:DonationsModel = DonationsModel.getInstance();
		private var service:HTTPService;
		
		private var dto:DonationServiceDTO;
		
		public function execute(event:MVCEvent):void {
			trace("VerifyTrackerCommand ::: execute");
			var evt:DonationsEvent = event as DonationsEvent;
			dto = evt.data as DonationServiceDTO;
			
			var url:String = "";
			var tracker:String = dto.service;
			var headers:Object = new Object();
			var method:String = URLRequestMethod.GET;
			
			service = ServiceLocator.getInstance().getHTTPService(Services.VERIFY_DONATION_SERVICE);
			service.cancel();
			
			if(tracker == Donations.DONATION_TRACKER) {
				url = Donations.DONATION_TRACKER_URL;
				// uses channel name and API key
				url = url.replace(":channel", dto.channel.toLowerCase());
				url = url.replace(":key", dto.key); 
			}else if(tracker == Donations.IM_RAISING2) {
				url = Donations.IM_RAISING_URL2;
				// uses HTTP headers
				headers = donationsModel.getImRaisinHeaders(dto.key);
			}else if(tracker == Donations.STREAM_TIP) {
				url = Donations.STREAM_TIP_URL;
				url = url.replace(":clientID", dto.clientID);
				url = url.replace(":key", dto.key);
			}else if(tracker == Donations.TWITCH_ALERTS) {
				url = Donations.TWITCH_ALERTS_URL;
				url = url.replace(":key", dto.key);
			}else if(tracker == Donations.TWITCH_PLUS) {
				url = Donations.TWITCH_PLUS_OAUTH_URL;
				// requires authentication via HTTP headers
				headers = donationsModel.getTwitchPlusHeaders(dto);
				// requires POST method
				method = URLRequestMethod.POST;
			}
			
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			
			service.method = method;
			service.headers = headers;
			service.url = url;
			trace("    - donation tracker url: ", url);
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("VerifyTrackerCommand ::: resultHandler");
			// result format is json - convert to AS
			trace(ObjectUtil.toString(event.result));
			var r:String = event.result.toString();
			var json:JSONDecoder = new JSONDecoder(r, true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			
			if(dto.service == Donations.DONATION_TRACKER) {
				if(result.hasOwnProperty("api_check") && result.api_check == "1") {
					setTimeout(donationsModel.addServiceAccount, 5000, dto);
				}else if(result.hasOwnProperty("api_check_message")) {
					donationsModel.validationError = {error:result.api_check_message, status:"Error:"};
				}else {
					donationsModel.validationError = {error:"Unknown error. Verification failed", status:"Error:"};
				}
			}
			
			if(dto.service == Donations.IM_RAISING2) {
				trace("    - TRACKER IS IM_RAISING v2");
				donationsModel.addServiceAccount(dto);
			}
			
			if(dto.service == Donations.STREAM_TIP) {
				trace("    - TRACKER IS STREAM_TIP");
				if(result.status == 200) {
					trace("    - donation track is valid");
					donationsModel.addServiceAccount(dto);
				}else{
					trace("    - something went wrong loading donations");
					donationsModel.validationError = result;
				}
			}
			
			if(dto.service == Donations.TWITCH_ALERTS) {
				donationsModel.addServiceAccount(dto);
			}
			
			if(dto.service == Donations.TWITCH_PLUS) {
				// store token and expiration in dto
				dto.token = result.access_token;
				dto.expires = new Date(result.expires_in * 1000);
				
				donationsModel.addServiceAccount(dto);
			}
			
			trace(ObjectUtil.toString(dto));
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("VerifyTrackerCommand ::: faultHandler");
			
			var content:Object = event.fault.content;
			trace(ObjectUtil.toString(event.fault));
			
			var validationError:Object = {error:"An unknown error occured. Failed to verify donation service.", status:"Service Error:"};
			
			if(content && content is String) {
				var json:JSONDecoder = new JSONDecoder(content.toString(), true);
				var result:Object = json.getValue();
				if(result && dto.service == Donations.TWITCH_ALERTS) {
					validationError = {error:result.message, status:"Service Error:"};
				}else {
					validationError = result;
				}
			}else {
				validationError = {error:event.fault.faultString, status:event.fault.name};
			}
			
			donationsModel.validationError = validationError;
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}

