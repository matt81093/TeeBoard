package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.dto.notifications.DonationServiceDTO;
	import be.gip.teeboard.dto.widgets.DonationTrackerDTO;
	import be.gip.teeboard.events.DonationsEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.DonationsModel;
	import be.gip.teeboard.net.api.Donations;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	import flash.utils.setTimeout;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	public class GetTwitchPlusTokenCommand implements ICommand {
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var donationsModel:DonationsModel = DonationsModel.getInstance();
		private var service:HTTPService;
		
		private var dto:DonationServiceDTO;
		
		public function execute(event:MVCEvent):void {
			trace("GetTwitchPlusTokenCommand ::: execute");
			var evt:DonationsEvent = event as DonationsEvent;
			dto = evt.data as DonationServiceDTO;
			
			var tracker:String = dto.service;
			
			if(tracker != Donations.TWITCH_PLUS) {
				trace("    - This command can only be used for TwitchPlus donation services");
				return;
			}
			
			service = ServiceLocator.getInstance().getHTTPService(Services.TWITCHPLUS_AUTH_SERVICE);
			// requires authentication via HTTP headers
			service.headers = donationsModel.getTwitchPlusHeaders(dto);
			service.method = URLRequestMethod.POST;
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			service.url = Donations.TWITCH_PLUS_OAUTH_URL;
			
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetTwitchPlusTokenCommand ::: resultHandler");
			var account:DonationServiceDTO;
			// result format is json - convert to AS
			trace(ObjectUtil.toString(event.result));
			var r:String = event.result.toString();
			var json:JSONDecoder = new JSONDecoder(r, true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			
			// store token and expiration in dto
			dto.token = result.access_token;
			dto.expires = new Date(result.expires_in * 1000);
			
			trace(ObjectUtil.toString(dto));
			
			// update model
			donationsModel.updateServiceAccount(dto);
			
			account = donationsModel.selectedAccount;
			
			if(account && (account.channel == dto.channel && account.service == dto.service)) {
				trace("    - retrieving donations for TwitchPlus service with new token");
				dispatcher.dispatchEvent(new DonationsEvent(DonationsEvent.GET_DONATIONS));
			}
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetTwitchPlusTokenCommand ::: faultHandler");
			
			trace(ObjectUtil.toString(event.fault));
			
			donationsModel.validationError = {error:event.fault.faultString, status:event.fault.name};
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}

