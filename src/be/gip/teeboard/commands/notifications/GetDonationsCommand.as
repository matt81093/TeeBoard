package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.dto.notifications.DonationServiceDTO;
	import be.gip.teeboard.dto.widgets.DonationTrackerDTO;
	import be.gip.teeboard.events.DonationsEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.DonationsModel;
	import be.gip.teeboard.net.api.Donations;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	public class GetDonationsCommand implements ICommand {
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var donationsModel:DonationsModel = DonationsModel.getInstance();
		private var service:HTTPService;
		
		private var dto:DonationServiceDTO;
		
		public function execute(event:MVCEvent):void {
			trace("GetDonationsCommand ::: execute");
			var evt:DonationsEvent = event as DonationsEvent;
			dto = donationsModel.selectedAccount;
			var tracker:String = dto.service;
			var url:String;
			var method:String = URLRequestMethod.GET;
			var headers:Object = new Object();
			
			service = ServiceLocator.getInstance().getHTTPService(Services.DONATION_TRACKER_SERVICE);
			// cancel any ongoing operation
			service.cancel();
			
			var r:String = new Date().time.toString();
			
			if (tracker == Donations.DONATION_TRACKER) {
				url = Donations.DONATION_TRACKER_URL + "&r=" + r;
				// donation-tracker requires channel name and API key
				url = url.replace(":channel", dto.channel.toLowerCase());
				url = url.replace(":key", dto.key);
			}else if (tracker == Donations.IM_RAISING2) {
				url = Donations.IM_RAISING_URL2 + "&r=" + r;
				// ImRaising uses HTTP headers instead of query parameters
				headers = donationsModel.getImRaisinHeaders(dto.key);
			}else if(tracker == Donations.STREAM_TIP) {
				url = Donations.STREAM_TIP_URL + "&r=" + r;
				// StreamTip uses ClientID and API key
				url = url.replace(":clientID", dto.clientID);
				url = url.replace(":key", dto.key);
			}else if(tracker == Donations.TWITCH_ALERTS) {
				url = Donations.TWITCH_ALERTS_URL + "&r=" + r;
				// TwitchAlerts uses key
				url = url.replace(":key", dto.key);
			}else if(tracker == Donations.TWITCH_PLUS) {
				url = Donations.TWITCH_PLUS_URL + "&r=" + r;
				// TwitchPlus uses generated token
				url = url.replace(":token", dto.token);
			}
			
			trace("    - donation service url: ", url);
			
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// (re)set headers
			service.headers = headers;
			service.method = method;
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetDonationsCommand ::: resultHandler");
			var tracker:String  = dto.service;
			var r:String = event.result.toString();
			
			trace("    - r: ", r);
			
			// result format is json - convert to AS
			
			var json:JSONDecoder = new JSONDecoder(r, true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			
			if(dto.service == Donations.DONATION_TRACKER) {
				trace("    - SERVICE IS DONATION_TRACKER");
				trace("    - status: ", result.status);
				if(result.api_check == "1") {
					trace("    - valid donations");
					donationsModel.setDonations(result);
				}else {
					trace("    - something went wrong loading donations");
					//donationsModel.validationError = result;
					donationsModel.validationError = {error:"Failed to load donations.", status:"Error:"};
				}
			}else if(dto.service == Donations.IM_RAISING2) {
				trace("    - TRACKER IS IM_RAISING v2");
				donationsModel.setDonations(result);
			}else if(dto.service == Donations.STREAM_TIP) {
				trace("    - SERVICE IS STREAM_TIP");
				donationsModel.setDonations(result);
			}else if(dto.service == Donations.TWITCH_ALERTS) {
				donationsModel.setDonations(result);
			}else if(dto.service == Donations.TWITCH_PLUS) {
				trace("    - SERVICE IS TWITCH_PLUS");
				trace("    - url: ", service.url);
				donationsModel.setDonations(result);
			}
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetDonationsCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			var content:Object = event.fault.content;
			
			var validationError:Object = {error:"An unknown error occured. Failed to load donations.", status:"Service Error:"};
			
			if(content && content is String) {
				var json:JSONDecoder = new JSONDecoder(content.toString(), true);
				var result:Object = json.getValue();
				if(result && dto.service == Donations.TWITCH_ALERTS) {
					//content = "{"error":"Not found","message":"User not found or invalid API key"}"
					validationError = {error:result.message, status:"Service Error:"};
				}else {
					validationError = result;
				}
			}else {
				validationError = {error:event.fault.faultString, status:event.fault.name};
			}
			
			donationsModel.validationError = validationError;
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}

/**
 * 
 * (mx.rpc::Fault)#0
 *   content = "{"error":"Internal Server Error","status":500,"message":null}"
 *   errorID = 0
 *   faultCode = "Server.Error.Request"
 *   faultDetail = "Error: [IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2032: Stream Error. URL: https://api.twitch.tv/kraken/user" errorID=2032]. URL: https://api.twitch.tv/kraken/user"
 *   faultString = "HTTP request error"
 *   message = "faultCode:Server.Error.Request faultString:'HTTP request error' faultDetail:'Error: [IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2032: Stream Error. URL: https://api.twitch.tv/kraken/user" errorID=2032]. URL: https://api.twitch.tv/kraken/user'"
 *   name = "Error"
 *   rootCause = (flash.events::IOErrorEvent)#1
 *     bubbles = false
 *     cancelable = false
 *     currentTarget = (flash.net::URLLoader)#2
 *       bytesLoaded = 61
 *       bytesTotal = 61
 *       data = "{"error":"Internal Server Error","status":500,"message":null}"
 *       dataFormat = "text"
 *     errorID = 2032
 *     eventPhase = 2
 *     target = (flash.net::URLLoader)#2
 *     text = "Error #2032: Stream Error. URL: https://api.twitch.tv/kraken/user"
 *     type = "ioError"
 */
