package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.DonationsModel;
	import be.gip.teeboard.model.NotificationsModel;
	import be.gip.teeboard.vo.notifications.IDonationVO;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Updates the donators text file with the latest and top donations.
	 * 
	 */ 
	public class WriteDonationsCommand implements ICommand {
		
		private var donationsModel:DonationsModel = DonationsModel.getInstance();
		
		/**
		 * Executes the command.
		 * 
		 */ 
		public function execute(event:MVCEvent):void {
			trace("WriteDonationsCommand ::: execute");
			
			var f:File;
			
			var fs:FileStream = new FileStream();
			
			var latest:ArrayCollection = donationsModel.latestDonations;
			var top:ArrayCollection = donationsModel.topDonations;
			
			var len:int = Math.min(latest.length, 5);
			
			var vo:IDonationVO;
			var pad:String = "          ";
			
			// LATEST DONATIONS
			
			var bytes:String = "";
			
			var bytes2:String = "";
			
			trace("    - num donations: ", len);
			
			for(var i:int=0; i<len; i++) {
				vo = latest.getItemAt(i) as IDonationVO;
				bytes += vo.name + " (" + vo.currencySymbol + "" + vo.amount + ")";
				bytes += (i < len-1) ? " \u2014 " : pad;
				
				bytes2 += vo.name + " (" + vo.currencySymbol + "" + vo.amount + ")" + " " + vo.message;
				bytes2 += (i < len-1) ? " \u2014 " : pad;
			}
			
			trace("    - LATEST DONATIONS: ", bytes.toUpperCase());
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("donations-latest.txt");
			
			// LAST 5 DONATIONS - NO NOTES
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// LAST 5 DONATIONS - INCLUDING NOTES
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("donations-latest-notes.txt");
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes2.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// LAST DONATION
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("donation-last.txt");
			bytes = "";
			if(len != 0) {
				vo = latest.getItemAt(0) as IDonationVO;
				bytes = vo.name + " (" + vo.currencySymbol + "" + vo.amount + ")" + pad;
			}
			
			trace("    - LAST DONATION: ", bytes.toUpperCase());
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}			
			
			// LAST DONATION - NO SCROLL
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("donation-last-noscroll.txt");
			bytes = "";
			if(len != 0) {
				vo = latest.getItemAt(0) as IDonationVO;
				bytes = vo.name + " (" + vo.currencySymbol + "" + vo.amount + ")";
			}
			
			trace("    - LAST DONATION: ", bytes.toUpperCase());
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// TOP DONATOR
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("donations-top.txt");
			bytes = "";
			
			if(top.length != 0) {
				vo = top.getItemAt(0) as IDonationVO;
				bytes += vo.name + " (" + vo.currencySymbol + "" + vo.amount + ")" + pad;
			}
			
			trace("    - TOP DONATOR: ", bytes.toUpperCase());
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// TOP DONATOR - NO SCROLL
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("donations-top-noscroll.txt");
			bytes = "";
			
			if(top.length != 0) {
				vo = top.getItemAt(0) as IDonationVO;
				bytes += vo.name + " (" + vo.currencySymbol + "" + vo.amount + ")";
			}
			
			trace("    - TOP DONATOR: ", bytes.toUpperCase());
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			fs.close();
		}
		
	}
	
}
