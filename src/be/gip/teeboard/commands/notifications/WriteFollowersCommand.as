package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.FollowersModel;
	import be.gip.teeboard.model.NotificationsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the followers text file with the latest followers.
	 * 
	 */ 
	public class WriteFollowersCommand implements ICommand {
		
		private var followersModel:FollowersModel = FollowersModel.getInstance();
		
		/**
		 * Executes the command.
		 * 
		 */ 
		public function execute(event:MVCEvent):void {
			trace("WriteFollowersCommand ::: execute");
			var f:File = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("followers.txt");
			var f2:File = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("follower-last.txt");
			var f3:File = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("follower-last-noscroll.txt");
			var fs:FileStream = new FileStream();
			var names:Array = followersModel.latestFollowerNames;
			var len:uint = names.length;
			trace("    - names: ", names);
			
			var pad:String = "          ";
			
			// LATEST FOLLOWERS
			
			var bytes:String = (len != 0) ? names.toString().split(",").join(" \u2014 ") + pad : "";
			trace("    - LATEST FOLLOWERS: ", bytes.toUpperCase());
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// LAST FOLLOWER
			
			bytes = (len != 0) ? names[0] + pad : "";
			
			trace("    - LAST FOLLOWER: ", bytes);
			
			try {
				fs.open(f2, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			fs.close();
			
			// LAST FOLLOWER - NO SCROLL
			
			bytes = (len != 0) ? names[0] : "";
			
			trace("    - LAST FOLLOWER: ", bytes);
			
			try {
				fs.open(f3, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			fs.close();
		}
		
	}
	
}
