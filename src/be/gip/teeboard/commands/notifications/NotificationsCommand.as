package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.dto.widgets.NotificationDTO;
	import be.gip.teeboard.managers.NotificationsManager;
	import be.gip.teeboard.managers.WidgetsConnectionManager;
	import be.gip.teeboard.model.FollowersModel;
	import be.gip.teeboard.model.NotificationsModel;
	import be.gip.teeboard.utils.NotificationsType;
	import be.gip.teeboard.vo.notifications.AbstractNotification;
	import be.gip.teeboard.vo.notifications.IDonationVO;
	import be.gip.teeboard.vo.notifications.IFollowerVO;
	import be.gip.teeboard.vo.notifications.IMessageVO;
	import be.gip.teeboard.vo.notifications.ISubscriberVO;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	public class NotificationsCommand implements ICommand {
		
		private var followersModel:FollowersModel = FollowersModel.getInstance();
		private var notificationsModel:NotificationsModel = NotificationsModel.getInstance();
		private var notificationMgr:NotificationsManager = NotificationsManager.getInstance();
		
		private var queueTimer:Timer;
		private var queue:ArrayCollection;
		
		public function execute(event:MVCEvent):void {
			trace("NotficationsCommand ::: execute");
			
			queue = notificationMgr.notificationQueue;
			
			if(queue == null) return;
			
			// only display a maximum of 3 followers
			// as it takes 10 seconds to display each follower
			// and the timer is 30 seconds
			var len:int = queue.length;
			
			// don't do anything if list is empty for some reason
			if(len == 0) return;
			
			// if there's a queue, we're good to go
			// set flag in manager that we're running
			notificationMgr.isQRunning = true;
			
			trace("    - queue lenght: ", len);
			
			trace("    - follower widget enabled: ", notificationsModel.followWidgetEnabled);
			trace("    - follower play sound enabled: ", notificationsModel.followSoundEnabled);
			trace("    - follower notification on stream: ", notificationsModel.followStreamEnabled);
			trace("    - follower notification on screen: ", notificationsModel.followNotifyEnabled);
			
			// delay is shorter than the total duration of the notification animation
			// because the "fade out" isn't needed when there's more than one notification.
			
			//var delay:Number = (len == 1) ? 10 * 1000 : 6 * 1000;
			var delay:Number = 10 * 1000;
			
			queueTimer = new Timer(delay, 1);
			queueTimer.addEventListener(TimerEvent.TIMER, queueTimerHandler);
			
			executeNotification();
			
		}
		
		//------------------------------------
		// executeNotification()
		//------------------------------------
		private function executeNotification():void {
			trace("NotficationsCommand ::: executeNotification");
			
			var q:ArrayCollection = notificationMgr.notificationQueue;
			var vo:AbstractNotification = q.removeItemAt(0) as AbstractNotification;
			
			if(vo == null) {
				notificationMgr.isQRunning = false;
				return;
			}
			
			trace("    - notification type: ", vo.notificationType);
			
			trace(ObjectUtil.toString(vo));
			
			var dto:NotificationDTO;
			
			var dvo:IDonationVO;
			var fvo:IFollowerVO;
			var svo:ISubscriberVO;
			var mvo:IMessageVO;
			
			var fName:String = "";
			var amount:String = "";
			var msg:String = "";
			
			var o:Object;
			
			if(vo.notificationType == NotificationsType.DONATION) {
				dvo = vo as IDonationVO;
				trace("    - donation name: ", dvo.name);
				
				fName = dvo.name;
				amount = dvo.currencySymbol + dvo.amount;
				
				// show on user screen if enabled
				if(notificationsModel.donateNotifyEnabled) {
					dto = new NotificationDTO();
					dto.title = "New Donation";
					dto.message = fName + " has donated " + dvo.currencySymbol + dvo.amount;
					dto.date = dvo.date;
					dto.type = vo.isTest ? NotificationsType.TEST : NotificationsType.DONATION;
					
					notificationMgr.addNotification(dto);
				}
				
				// play sound if enabled
				if(notificationsModel.donateSoundEnabled) {
					notificationsModel.playDonationAlert();
				}
				
				// show notification on stream if enabled
				if(notificationsModel.donateStreamEnabled) {
					msg = notificationsModel.donateMessage.replace("%name%", fName);
					msg = msg.replace("%amount%", amount);
					
					o = new Object();
					o.type = NotificationsType.DONATION;
					o.message = msg;
					o.color = notificationsModel.donateColor;
					o.bgColor = notificationsModel.donateBackgroundColor;
					o.bgAlpha = notificationsModel.bgAlpha;
					o.direction = notificationsModel.selectedAnimDirection;
					
					WidgetsConnectionManager.getInstance().sendNotification(o);
				}
			}
			
			if(vo.notificationType == NotificationsType.FOLLOWER) {
				fvo = vo as IFollowerVO;
				fName = fvo.user.displayName;
				
				// show on user screen if enabled
				if(notificationsModel.followNotifyEnabled) {
					dto = new NotificationDTO();
					dto.title = "New Follower";
					dto.message = fvo.user.displayName + " is following the channel";
					dto.date = fvo.createdAt;
					dto.type = vo.isTest ? NotificationsType.TEST : NotificationsType.FOLLOWER;
					
					notificationMgr.addNotification(dto);
				}
				
				// play sound if enabled 
				if(notificationsModel.followSoundEnabled) {
					notificationsModel.playFollowAlert();
				}
				
				// show notification on stream if enabled
				if(notificationsModel.followStreamEnabled) {
					o = new Object();
					o.message = notificationsModel.followMessage.replace("%name%", fName);
					o.type = NotificationsType.FOLLOWER
					o.color = notificationsModel.followColor;
					o.bgColor = notificationsModel.followBackgroundColor;
					o.bgAlpha = notificationsModel.bgAlpha;
					o.direction = notificationsModel.selectedAnimDirection;
					
					WidgetsConnectionManager.getInstance().sendNotification(o);
				}
			}
			
			if(vo.notificationType == NotificationsType.SUBSCRIBER) {
				svo = vo as ISubscriberVO;
				fName = svo.user.displayName;
				trace("    - subscriber name: ", fName);
				
				// play sound if enabled
				if(notificationsModel.subSoundEnabled) {
					notificationsModel.playSubscriberAlert();
				}
				
				// show notification on screen if enabled
				if(notificationsModel.subNotifyEnabled) {
					dto = new NotificationDTO();
					dto.title = "New Subscriber";
					dto.message = fName + " has subscribed to the channel";
					dto.date = svo.createdAt;
					dto.type = vo.isTest ? NotificationsType.TEST : NotificationsType.SUBSCRIBER;
					
					notificationMgr.addNotification(dto);
				}
				
				// show notification on stream if enabled
				if(notificationsModel.subStreamEnabled) {
					o = new Object();
					o.type = NotificationsType.SUBSCRIBER;
					o.message = notificationsModel.subMessage.replace("%name%", fName);
					o.color = notificationsModel.subColor;
					o.bgColor = notificationsModel.subBackgroundColor;
					o.bgAlpha = notificationsModel.bgAlpha;
					o.direction = notificationsModel.selectedAnimDirection;
					
					WidgetsConnectionManager.getInstance().sendNotification(o);
				}
			}
			
			if(vo.notificationType == NotificationsType.MESSAGE) {
				mvo = vo as IMessageVO;
				// 
				o = new Object();
				o.message = mvo.message;
				o.tag = mvo.tag;
				o.type = NotificationsType.MESSAGE;
				
				if(notificationsModel.messageUseDefaultWidget) {
					o.color = notificationsModel.messageColor;
					o.bgColor = notificationsModel.messageBackgroundColor;
					o.bgAlpha = notificationsModel.bgAlpha;
					o.direction = notificationsModel.selectedAnimDirection;
					
					WidgetsConnectionManager.getInstance().sendNotification(o);
				}else {
					WidgetsConnectionManager.getInstance().sendNotificationMessage(o);
				}
				
				if(notificationsModel.messageSoundEnabled) {
					notificationsModel.playMessageAlert();
				}
			}
			
			// start queue timer, regardless if there are more notifications.
			// this is so that notifications can end fully before a new queue is started
			queueTimer.reset();
			queueTimer.start();
		}
		
		//------------------------------------
		// notifyTimerHandler()
		//------------------------------------
		private function queueTimerHandler(event:TimerEvent):void {
			trace("NotficationsCommand ::: notifyTimerHandler");
			if(notificationMgr.notificationQueue.length != 0) {
				executeNotification();
			}else {
				notificationMgr.isQRunning = false;
			}
		}
		
	}
	
}