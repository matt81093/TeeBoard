package be.gip.teeboard.commands.notifications {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.NotificationsModel;
	import be.gip.teeboard.model.SubscribersModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the subscribers text file with the latest followers.
	 * 
	 */ 
	public class WriteSubscribersCommand implements ICommand {
		
		private var subModel:SubscribersModel = SubscribersModel.getInstance();
		
		/**
		 * Executes the command.
		 * 
		 */ 
		public function execute(event:MVCEvent):void {
			trace("WriteSubscribersCommand ::: execute");
			var f:File = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("subscribers.txt");
			var names:Array = subModel.latestSubscriberNames;
			var len:uint = names.length;
			trace("    - names: ", names);
			
			var pad:String = "          ";
			
			// LATEST SUBSCRIBERS
			
			var bytes:String = (len != 0) ? (names.toString().split(",").join(" \u2014 ") + pad) : "";
			trace("    - bytes: ", bytes.toUpperCase());
			var fs:FileStream = new FileStream();
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// LAST SUBSCRIBER
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("subscriber-last.txt");
			bytes = len != 0 ? names[0] + pad : "";
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			// LAST SUBSCRIBER - NO SCROLL
			
			f = NotificationsModel.WIDGET_NOTIFICATIONS_DIR.resolvePath("subscriber-last-noscroll.txt");
			bytes = len != 0 ? names[0] : "";
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			fs.close();
		}
		
	}
	
}
