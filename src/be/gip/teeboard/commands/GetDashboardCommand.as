package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.dto.ChannelDTO;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.views.DashboardView;
	import be.gip.teeboard.vo.ChannelVO;
	import be.gip.teeboard.vo.RootVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	/**
	 * This basically does the same as the GetChannelCommand, 
	 * but this command serves a different purpose.
	 * 
	 * The end goal of this command is to see if a channel about to be added as dashboard for editing exists.
	 * If the channel exists, another command is executed to check if the current user is an editor of that channel, 
	 * which is required to be able to manage it.
	 */ 
	public class GetDashboardCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var service:HTTPService;
		private var dto:ChannelDTO;
		private var twitchEvent:MVCEvent;
		
		public function execute(event:MVCEvent):void {
			trace("GetDashboardCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			dto = evt.data as ChannelDTO;
			twitchEvent = event.clone() as MVCEvent;
			trace("    - channel name: ", dto.name);
			service = ServiceLocator.getInstance().getHTTPService(Services.DASHBOARD_SERVICE);
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access-token - this will return user channel info
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = Kraken.CHANNELS_CHANNEL_URL.replace(":channel", dto.name) + "&r=" + new Date().time;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetDashboardCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			//trace(ObjectUtil.toString(result));
			var vo:ChannelVO = new ChannelVO(result);
			trace(ObjectUtil.toString(vo));
			// check if the returned channel matches the searched channel
			if(vo.name == dto.name) {
				trace("    - we have a match");
				// add the channel to the user's list of dashboard
				dto.displayName = vo.displayName;
				model.addDashboard(dto);
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetDashboardCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			trace(ObjectUtil.toString(event.fault));
			var content:Object = event.fault.content;
			//  content = "{"status":404,"message":"Channel ':channel' does not exist","error":"Not Found"}"
			try {
				var json:JSONDecoder = new JSONDecoder(content.toString(), true);
				var value:Object = json.getValue();
				Alert.show("status: " + value.status + "\n" + "message: " + value.message, "Error: " + value.error);
			}catch(e:Error) {
				Alert.show(event.fault.faultString, "Error:");
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);

		}
		
	}
	
}
