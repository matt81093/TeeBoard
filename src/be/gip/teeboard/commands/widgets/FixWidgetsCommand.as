package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.log.TeeLogger;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates XSplit configuration files with path to the widget's folder.
	 * <p>
	 * Widgets added to XSplit are wrapped by an XSplit specific swf and 
	 * this changes the base folder path of the loaded widget to the path of the wrapper swf, 
	 * meaning reative paths point to the wrapper swf's directory instead of the widget's directory.
	 * The actual widget's directory is therefor written to the XSplit configuration file and passed on to the widget via flashvars.
	 * </p>
	 */ 
	public class FixWidgetsCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		private var logger:TeeLogger = TeeLogger.getInstance();
		private var logMessage:LogMessage = new LogMessage();
		
		public function execute(event:MVCEvent):void {
			trace("FixWidgetsCommand ::: execute");
			
			logMessage.message = "FixWidgetsCommand :: updating widget config files for XSplit";
			logger.log(logMessage);
			
			// XSplit widgets (swf files) need to know the path to the widget config file
			// which is set in the XSplit configuration xml file inside each widget folder
			
			// Widgets are stored in [USER DOCUMENTS]/TeeBoard/widgets/name-of-widget/
			// Each widget folder contains an xml file with the same name as the widget swf.
			// This file is automatically picked up by XSplit when adding the widget as a media source.
			
			// CHAT WIDGET
			
			var chatXsplit:File = WidgetsModel.WIDGET_CHAT_XSPLIT;
			var chatDir:File = WidgetsModel.WIDGET_CHAT_DIR;
			var chatXML:XML = readXSplitConfig(chatXsplit);
			// update the xsplit xml with path to widget config
			var chatUpdated:Boolean = updateXSplitConfig(chatXML, chatDir);
			trace("    - chat widget config updated: ", chatUpdated);
			logMessage.message = "\t chat widget config updated: " + chatUpdated;
			logger.log(logMessage);
			// only write to disk if actually updated
			if(chatUpdated) {
				writeXSplitConfig(chatXsplit, chatXML);
			}
			
			// CLOCK WIDGET
			
			var clockXsplit:File = WidgetsModel.WIDGET_CLOCK_XSPLIT;
			var clockDir:File = WidgetsModel.WIDGET_CLOCK_DIR;
			var clockXML:XML = readXSplitConfig(clockXsplit);
			// update the xsplit xml with path to widget config
			var clockUpdated:Boolean = updateXSplitConfig(clockXML, clockDir);
			// only write to disk if actually updated
			trace("    - clock widget config updated: ", mediaUpdated);
			logMessage.message = "\t clock widget config updated: " + clockUpdated;
			logger.log(logMessage);
			if(clockUpdated) {
				writeXSplitConfig(clockXsplit, clockXML);
			}
			
			// COUNTDOWN WIDGET
			
			var countXsplit:File = WidgetsModel.WIDGET_COUNTDOWN_XSPLIT;
			var countDir:File = WidgetsModel.WIDGET_COUNTDOWN_DIR;
			var countXML:XML = readXSplitConfig(countXsplit);
			// update the xsplit xml with path to widget config
			var countUpdated:Boolean = updateXSplitConfig(countXML, countDir);
			trace("    - countdown widget config updated: ", mediaUpdated);
			logMessage.message = "\t countdown widget config updated: " + countUpdated;
			logger.log(logMessage);
			// only write to disk if actually updated
			if(countUpdated) {
				writeXSplitConfig(countXsplit, countXML);
			}
			
			// MEDIA WIDGET
			
			var mediaXsplit:File = WidgetsModel.WIDGET_MEDIA_XSPLIT;
			var mediaDir:File = WidgetsModel.WIDGET_MEDIA_DIR;
			var mediaXML:XML = readXSplitConfig(mediaXsplit);
			// update the xsplit xml with path to widget config
			var mediaUpdated:Boolean = updateXSplitConfig(mediaXML, mediaDir);
			trace("    - media widget config updated: ", mediaUpdated);
			logMessage.message = "\t media widget config updated: " + mediaUpdated;
			logger.log(logMessage);
			// only write to disk if actually updated
			if(mediaUpdated) {
				writeXSplitConfig(mediaXsplit, mediaXML);
			}
			
			// SPECTRUM WIDGET
			
			var spectrumXsplit:File = WidgetsModel.WIDGET_SPECTRUM_XSPLIT;
			var spectrumDir:File = WidgetsModel.WIDGET_SPECTRUM_DIR;
			var spectrumXML:XML = readXSplitConfig(spectrumXsplit);
			// update the xsplit xml with path to widget config
			var spectrumUpdated:Boolean = updateXSplitConfig(spectrumXML, spectrumDir);
			trace("    - spectrum widget config updated: ", mediaUpdated);
			logMessage.message = "\t spectrum widget config updated: " + spectrumUpdated;
			logger.log(logMessage);
			// only write to disk if actually updated
			if(spectrumUpdated) {
				writeXSplitConfig(spectrumXsplit, spectrumXML);
			}
			
			// POLL WIDGET
		}
		
		/**
		 * 
		 */ 
		private function readXSplitConfig(f:File):XML {
			//trace("FixWidgetsCommand ::: readXSplitConfig");
			if(!f.exists) return null;
			var xml:XML;
			var fs:FileStream = new FileStream();
			fs.open(f, FileMode.READ);
			var config:String = fs.readUTFBytes(fs.bytesAvailable);
			xml = new XML(config);
			//trace(xml.toXMLString());
			fs.close();
			return xml;
		}
		
		/**
		 * Updates the flashvars value in the xml with the path to the widget folder.
		 * 
		 * <p>If the flashvars attribute is already set and contains the widget folder path, this method will return false.</p>
		 * 
		 * <p><code>&lt;param name="flashvars" value="dir=path-to-widget-folder" /&gt;</code></p>
		 * 
		 */ 
		private function updateXSplitConfig(value:XML, dir:File):Boolean {
			//trace("FixWidgetsCommand ::: updateXSplitConfig");
			var updated:Boolean = false;
			var fvarsValue:String = "dir=" + dir.url;
			var fvars:XML;
			var widgetUrl:String;
			if(value != null && dir.exists) {
				fvars = value.param.(@name == "flashvars")[0];
				//trace("    - param name: ", value.param.@name);
				//trace("    - param value empty: ", (fvars.@value == ""));
				//trace("    - param value contains widget dir: ", (fvars.@value).indexOf(fvarsValue));
				// only update xml if flashvars value does not match (contain) the config url
				if((fvars.@value) != fvarsValue) {
					fvars.@value = fvarsValue;
					trace(value.toXMLString());
					updated = true;
				}
			}
			//trace("    - config updated: ", updated);
			return updated;
		}
		
		/**
		 * 
		 */ 
		private function writeXSplitConfig(f:File, value:XML):void {
			//trace("FixWidgetsCommand ::: writeXSplitConfig");
			//trace("    - value: ", value);
			var str:String = value.toString().split("\n").join(File.lineEnding);
			var fs:FileStream = new FileStream();
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(str);
				fs.close();
			}catch(e:Error) {
				//
			}
			fs.close();
		}
		
	}
	
}
