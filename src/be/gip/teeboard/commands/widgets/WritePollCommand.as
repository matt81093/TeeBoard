package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.FollowersModel;
	import be.gip.teeboard.model.NotificationsModel;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the poll text file with the poll question and options.
	 * 
	 */ 
	public class WritePollCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		
		/**
		 * Executes the command.
		 * 
		 */ 
		public function execute(event:MVCEvent):void {
			trace("WritePollCommand ::: execute");
			var f:File = WidgetsModel.WIDGET_POLL_DIR.resolvePath("poll.txt");
			var fs:FileStream = new FileStream();
			var xml:XML = widgetsModel.pollConfig;
			var config:XMLList = xml.config;
			var options:XMLList = config.option;
			var len:uint = options.length();
			trace(xml.toXMLString());
			
			// LATEST FOLLOWERS
			
			var bytes:String = "Poll: " + config.@question;
			
			for(var i:uint=0; i<len; i++) {
				bytes += " \u2014 ";
				bytes += "option " + (i+1) + ": " + options[i].@label;
				bytes += " \u2014 ";
				bytes += "vote: " + options[i].@keyword;
			}
			bytes += "          "
			
			trace("    - bytes: ", bytes.toUpperCase());
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes.toUpperCase());
				fs.close();
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
			
			fs.close();
		}
		
	}
	
}
