package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.WidgetsModel;
	import be.gip.teeboard.utils.DateFormatterUtil;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the config file (<code>config.xml</code>) for the clock widget.
	 */ 
	public class WriteClockDateCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("WriteClockDateCommand ::: execute");
			var dateFile:File = WidgetsModel.WIDGET_CLOCK_DIR.resolvePath("clock-date.txt");
			var fs:FileStream = new FileStream();
			var bytes:String = DateFormatterUtil.formatDate(new Date(), widgetsModel.clockDateFormat);
			
			trace("    - bytes: ", bytes);
			
			try {
				fs.open(dateFile, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
