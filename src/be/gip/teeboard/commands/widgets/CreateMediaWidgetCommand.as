package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.dto.widgets.MediaDirectoryDTO;
	import be.gip.teeboard.events.WidgetEvent;
	import be.gip.teeboard.managers.FileManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.core.FlexGlobals;
	import mx.core.Window;
	import mx.managers.WindowedSystemManager;
	
	/**
	 * Creates a new copy of the media widget folder and places it in the User Documents directory.
	 * 
	 */ 
	public class CreateMediaWidgetCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		private var fileMgr:FileManager = FileManager.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("CreateMediaWidgetCommand ::: execute");
			var evt:WidgetEvent = event as WidgetEvent;
			var dto:MediaDirectoryDTO = evt.data as MediaDirectoryDTO;
			var orig:File = FileManager.APP_WIDGETS_DIR.resolvePath("media/default");
			var copy:File = FileManager.WIDGET_MEDIA_DIR.resolvePath(dto.name.toLowerCase());
			trace("    - original path: ", orig.nativePath);
			trace("    - new media widget path: ", copy.nativePath);
			try {
				orig.copyTo(copy, true);
				// update media listing
				// widget view checks if currentMediaFolder is listed
				// and if not it reloads the list of media folders
				//fileMgr.mediaFolders.push(copy);
				fileMgr.currentMediaFolder = copy;
			}catch(e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
