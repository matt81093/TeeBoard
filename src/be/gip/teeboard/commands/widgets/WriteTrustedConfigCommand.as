package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.log.TeeLogger;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;
	
	import org.osmf.logging.Logger;
	
	/**
	 * Attempt to write Flash Player trust config file.
	 */ 
	public class WriteTrustedConfigCommand implements ICommand {
		
		private var logger:TeeLogger = TeeLogger.getInstance();
		
		//------------------------------------
		// log()
		//------------------------------------
		private function log(msg:String):void {
			trace("WriteTrustedConfigCommand ::: log");
			var lm:LogMessage = new LogMessage();
			lm.message = "WriteTrustedConfigCommand :: " + msg;
			logger.log(lm);
		}
		
		/**
		 * 
		 */ 
		public function execute(event:MVCEvent):void {
			trace("WriteTrustedConfigCommand ::: execute");
			
			var os:String = Capabilities.os.toLowerCase();
			
			if(os.indexOf("windows") != -1) {
				log("OS is Windows");
			}else{
				log("Non Windows OS. Contact developer.");
				return;
			} 
			
			var userDir:File = File.userDirectory;
			
			var configFile:File = userDir.resolvePath("AppData/Roaming/Macromedia/Flash Player/#Security/FlashPlayerTrust/teeboard-widgets.cfg");
			
			trace("    - user dir: ", userDir.nativePath);
			trace("    - config file: ", configFile.nativePath);
			
			var fs:FileStream = new FileStream();
			var bytes:String = WidgetsModel.DOCUMENTS_DIR.nativePath;
			bytes += File.lineEnding;
			bytes += "http://absolute/";
			
			log("Flash Player Trusted file: " + configFile.nativePath);
			log("File exists: " + configFile.exists);
			
			if(!configFile.exists) {
				try {
					
					log("Creating Flash Player Trusted file.");
					log("Added " + bytes.split(File.lineEnding).join(" + ") + " to the Flash Player trusted locations.");
					
					fs.open(configFile, FileMode.WRITE);
					fs.writeUTFBytes(bytes);
					fs.close();
				}catch (e:Error) {
					trace(e.getStackTrace());
					log(e.message);
				}
			}
		}
		
	}
	
}
