package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.managers.FileManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Reads the config file (<code>config.xml</code>) 
	 * for the media widget and stores it in the <code>WidgetModel</code>.
	 */ 
	public class GetMediaConfigCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("GetMediaConfigCommand ::: execute");
			var f:File = FileManager.getInstance().currentMediaFolder;
			var config:File = f.resolvePath("config.xml");
			var fs:FileStream = new FileStream();
			var bytes:String;
			var xml:XML;
			try {
				fs.open(config, FileMode.READ);
				bytes = fs.readUTFBytes(fs.bytesAvailable);
				xml = new XML(bytes);
				trace(xml.toXMLString());
				fs.close();
				widgetsModel.mediaConfig = xml;
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
