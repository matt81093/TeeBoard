package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.managers.FileManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	
	/**
	 * Retrieves the sub folders of the Media widget folder.
	 */ 
	public class GetMediaListingCommand implements ICommand {
		
		private var fileMgr:FileManager = FileManager.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("GetMediaListingCommand ::: execute");
			var list:Array = FileManager.WIDGET_MEDIA_DIR.getDirectoryListing();
			var dirs:Array = new Array();
			
			var len:uint = list.length;
			var dir:File;
			
			for(var i:uint=0; i<len; i++) {
				dir = list[i] as File;
				trace("    - media dir: ", dir.nativePath);
				trace("    - valid media dir: ", dir.resolvePath("config.xml").exists);
				if(dir.isDirectory && dir.resolvePath("config.xml").exists) {
					dirs.push(list[i]);
				}
			}
			fileMgr.mediaFolders = dirs;
		}
		
	}
	
}
