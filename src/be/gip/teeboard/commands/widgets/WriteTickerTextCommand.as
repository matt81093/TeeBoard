package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Writes the current tickertape text to file.
	 */ 
	public class WriteTickerTextCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("WriteTickerTextCommand ::: execute");
			var fs:FileStream = new FileStream();
			var pad:String = "          ";
			var bytes:String = widgetsModel.getCurrentTickerText() + pad;
			
			if(widgetsModel.tickerUpperCase) {
				bytes = bytes.toUpperCase();
			}
			
			trace("    - current ticker text: ", bytes);
			
			try {
				fs.open(WidgetsModel.WIDGET_TICKERTAPE_TEXT, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
			fs.close();
		}
		
	}
	
}
