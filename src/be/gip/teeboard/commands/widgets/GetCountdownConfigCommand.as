package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Reads the config file (<code>config.xml</code>) 
	 * for the countdown widget and stores it in the <code>WidgetModel</code>.
	 */ 
	public class GetCountdownConfigCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("GetCountdownConfigCommand ::: execute");
			var fs:FileStream = new FileStream();
			var bytes:String;
			var xml:XML;
			try {
				fs.open(WidgetsModel.WIDGET_COUNTDOWN_CONFIG, FileMode.READ);
				bytes = fs.readUTFBytes(fs.bytesAvailable);
				xml = new XML(bytes);
				trace(xml.toXMLString());
				fs.close();
				widgetsModel.countdownConfig = xml;
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
