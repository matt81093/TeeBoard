package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.managers.WidgetsConnectionManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the config file (<code>config.xml</code>) for the poll widget.
	 */ 
	public class UpdatePollConfigCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		private var connManager:WidgetsConnectionManager = WidgetsConnectionManager.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("UpdatePollConfigCommand ::: execute");
			var fs:FileStream = new FileStream();
			var xml:XML = widgetsModel.pollConfig;
			var bytes:String = '<?xml version="1.0" encoding="UTF-8"?>' + File.lineEnding;
			bytes += xml.toString().split("\n").join(File.lineEnding);
			
			trace(xml.toXMLString());
			
			try {
				fs.open(WidgetsModel.WIDGET_POLL_OBS_CONFIG, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
				
				fs.open(WidgetsModel.WIDGET_POLL_JS_CONFIG, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
				
				// send local connection message
				connManager.sendPollUpdate();
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
