package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.dto.widgets.MediaDirectoryDTO;
	import be.gip.teeboard.events.WidgetEvent;
	import be.gip.teeboard.managers.FileManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	
	/**
	 * Creates a new copy of the media widget folder and places it in the User Documents directory.
	 * 
	 */ 
	public class RemoveMediaWidgetCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		private var fileMgr:FileManager = FileManager.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("RemoveMediaWidgetCommand ::: execute");
			var f:File = fileMgr.currentMediaFolder;
			if(f.exists) {
				try {
					f.deleteDirectory(true);
					// set current media folder to null as we just removed it
					fileMgr.currentMediaFolder = null;
				}catch(e:Error) {
					trace(e.getStackTrace());
				}
			}
		}
		
	}
	
}
