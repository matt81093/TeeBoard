package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.dto.widgets.MediaDirectoryDTO;
	import be.gip.teeboard.events.WidgetEvent;
	import be.gip.teeboard.managers.FileManager;
	import be.gip.teeboard.managers.WidgetsConnectionManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the config file (<code>config.xml</code>) for the media widget.
	 */ 
	public class UpdateMediaConfigCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		private var connManager:WidgetsConnectionManager = WidgetsConnectionManager.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("UpdateMediaConfigCommand ::: execute");
			
			var f:File = FileManager.getInstance().currentMediaFolder;
			var config:File = f.resolvePath("config.xml");
			
			var fs:FileStream = new FileStream();
			var xml:XML = widgetsModel.mediaConfig;
			var bytes:String = '<?xml version="1.0" encoding="UTF-8"?>' + File.lineEnding;
			bytes += xml.toString().split("\n").join(File.lineEnding);
			
			trace(xml.toXMLString());
			
			try {
				fs.open(config, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
				// send local connection message
				connManager.sendMediaUpdate(xml.@mediaName);
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
