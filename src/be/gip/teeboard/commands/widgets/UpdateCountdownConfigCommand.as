package be.gip.teeboard.commands.widgets {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.managers.WidgetsConnectionManager;
	import be.gip.teeboard.model.WidgetsModel;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the config file (<code>config.xml</code>) for the countdown widget.
	 */ 
	public class UpdateCountdownConfigCommand implements ICommand {
		
		private var widgetsModel:WidgetsModel = WidgetsModel.getInstance();
		private var connManager:WidgetsConnectionManager = WidgetsConnectionManager.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("UpdateCountdownConfigCommand ::: execute");
			var fs:FileStream = new FileStream();
			var xml:XML = widgetsModel.countdownConfig;
			var bytes:String = '<?xml version="1.0" encoding="UTF-8"?>' + File.lineEnding;
			bytes += "  <!--" + File.lineEnding;
			bytes += "    duration: the time in seconds" + File.lineEnding;
			bytes += "    default text color: 0xFF6600" + File.lineEnding;
			bytes += "  -->" + File.lineEnding;
			
			bytes += xml.toString().split("\n").join(File.lineEnding);
			
			trace(xml.toXMLString());
			
			try {
				fs.open(WidgetsModel.WIDGET_COUNTDOWN_CONFIG, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
				// send local connection message
				connManager.sendCountdownUpdate();
			}catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
	}
	
}
