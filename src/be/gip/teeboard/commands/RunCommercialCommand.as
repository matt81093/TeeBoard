package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.dto.CommercialDTO;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.CommercialModel;
	import be.gip.teeboard.vo.ChannelVO;
	import be.gip.teeboard.vo.RootVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class RunCommercialCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var commercialModel:CommercialModel = CommercialModel.getInstance();
		
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("RunCommercialCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			var dto:CommercialDTO = evt.data as CommercialDTO;
			trace(ObjectUtil.toString(dto));
			service = ServiceLocator.getInstance().getHTTPService("commercialService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with required headers (access-token, client-id)
			service.headers = model.httpHeaders;
			// commercial requires POST method
			service.method = URLRequestMethod.POST;
			service.request = {"length":dto.length};
			// url requires channel name
			//var url:String = Kraken.CHANNEL_COMMERCIAL_URL.replace(":channel", "twitchboard");
			var url:String = Kraken.CHANNEL_COMMERCIAL_URL.replace(":channel", model.channel.name);
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("RunCommercialCommand ::: resultHandler");
			// this service doesn't return data
			// just a statusCode
			trace("    - statusCode: ", event.statusCode);
			var statusCode:int = event.statusCode;
			if(statusCode == 204) {
				// all went well
				commercialModel.startTimeout();
			}
			if(statusCode == 422) {
				// commercial length not allowed
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("RunCommercialCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			var content:Object = event.fault.content;
			var json:JSONDecoder = new JSONDecoder(content.toString(), true);
			var result:Object;
			if(json) {
				result = json.getValue();
				trace(ObjectUtil.toString(result));
				Alert.show("status: " + result.status + "\r" + "message: " + result.message, "Error: " + result.error);
			}
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
