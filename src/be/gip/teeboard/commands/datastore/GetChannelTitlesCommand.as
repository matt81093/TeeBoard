package be.gip.teeboard.commands.datastore {
	
	import be.gip.mvc.business.DataStoreLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.sqlite.Query;
	import be.gip.mvc.sqlite.QueryParams;
	import be.gip.teeboard.dto.datastore.ChannelTitleDTO;
	
	import flash.data.SQLResult;
	import flash.errors.SQLError;
	import flash.net.Responder;
	
	import mx.utils.ObjectUtil;
	
	
	public class GetChannelTitlesCommand extends Responder implements ICommand {
		
		public function GetChannelTitlesCommand() {
			super(onResult, onFault);
		}
		
		public function execute(event:MVCEvent):void {
			trace("GetChannelTitlesCommand ::: execute");
			var q:Query = DataStoreLocator.getInstance().getQuery("getTitlesByChannel");
			var qp:QueryParams = new QueryParams();
			qp.add(":channelName", "teeboard");
			q.parameters = qp;
			q.itemClass = ChannelTitleDTO;
			q.execute(this);
		}
		
		public function onResult(value:SQLResult):void {
			trace("GetChannelTitlesCommand ::: onResult");
			trace(ObjectUtil.toString(value));
		}
		
		public function onFault(info:SQLError):void {
			trace("GetChannelTitlesCommand ::: onFault");
			trace(ObjectUtil.toString(info));
			throw info;
		}
		
	}
	
}