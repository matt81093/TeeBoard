package be.gip.teeboard.commands.datastore {
	
	import be.gip.mvc.business.DataStoreLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.sqlite.Query;
	import be.gip.teeboard.dto.datastore.ChannelDTO;
	import be.gip.teeboard.model.DataStoreModel;
	
	import flash.data.SQLResult;
	import flash.errors.SQLError;
	import flash.net.Responder;
	
	import mx.utils.ObjectUtil;
	
	
	public class GetChannelsCommand extends Responder implements ICommand {
		
		private var dsModel:DataStoreModel = DataStoreModel.getInstance();
		
		public function GetChannelsCommand() {
			super(onResult, onFault);
		}
		
		public function execute(event:MVCEvent):void {
			trace("GetChannelsCommand ::: execute");
			var q:Query = DataStoreLocator.getInstance().getQuery("getChannels");
			q.itemClass = ChannelDTO;
			q.execute(this);
		}
		
		public function onResult(value:SQLResult):void {
			trace("GetChannelsCommand ::: onResult");
			trace(ObjectUtil.toString(value));
			dsModel.setChannels(value.data);
		}
		
		public function onFault(info:SQLError):void {
			trace("GetChannelsCommand ::: onFault");
			trace(ObjectUtil.toString(info));
			throw info;
		}
		
	}
	
}