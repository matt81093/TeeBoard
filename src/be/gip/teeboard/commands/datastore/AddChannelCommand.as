package be.gip.teeboard.commands.datastore {
	
	import be.gip.mvc.business.DataStoreLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.mvc.sqlite.Query;
	import be.gip.mvc.sqlite.QueryParams;
	import be.gip.teeboard.dto.datastore.ChannelDTO;
	import be.gip.teeboard.events.DataStoreEvent;
	import be.gip.teeboard.model.ApplicationModel;
	
	import flash.data.SQLResult;
	import flash.errors.SQLError;
	import flash.net.Responder;
	
	import mx.utils.ObjectUtil;
	
	
	public class AddChannelCommand extends Responder implements ICommand {
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		private var model:ApplicationModel = ApplicationModel.getInstance();
		
		public function AddChannelCommand() {
			super(onResult, onFault);
		}
		
		public function execute(event:MVCEvent):void {
			trace("AddChannelCommand ::: execute");
			var qp:QueryParams = new QueryParams();
			//qp.add(":channelName", model.channel.name);
			qp.add(":channelName", "somerandomname");
			var q:Query = DataStoreLocator.getInstance().getQuery("getChannelByName");
			q.itemClass = ChannelDTO;
			q.parameters = qp;
			q.execute(this);
		}
		
		public function onResult(value:SQLResult):void {
			trace("AddChannelCommand ::: onResult");
			trace(ObjectUtil.toString(value));
			var dto:ChannelDTO;
			trace("    - data: ", value.data);
			if(value.data && value.data.length != 0) {
				trace("    - data length: ", value.data.length);
				// we have a winner
			}else {
				// add new channel to db
				dto = new ChannelDTO();
				dto.name = model.channel.name;
				dispatcher.dispatchEvent(new DataStoreEvent(DataStoreEvent.ADD_CHANNEL, dto));
			}
		}
		
		public function onFault(info:SQLError):void {
			trace("AddChannelCommand ::: onFault");
			trace(ObjectUtil.toString(info));
			throw info;
		}
		
	}
	
}