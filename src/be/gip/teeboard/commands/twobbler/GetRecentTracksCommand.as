package be.gip.teeboard.commands.twobbler {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.events.TwobblerEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.TwobblerModel;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetRecentTracksCommand implements ICommand {
		
		private var twobblerModel:TwobblerModel = TwobblerModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetRecentTracksCommand ::: execute");
			var evt:TwobblerEvent = event as TwobblerEvent;
			var url:String = TwobblerModel.LASTFM_XML_URL.replace("[USER]", twobblerModel.fmName);
			//Droedin
			//alrightm8s
			//var url:String = TwobblerModel.LASTFM_XML_URL.replace("[USER]", "alrightm8s");
			url += "&r=" + new Date().time;
			trace("    - url: ", url);
			
			service = ServiceLocator.getInstance().getHTTPService(Services.LAST_FM_SERVICE);
			service.cancel();
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// 
			service.method = URLRequestMethod.GET;
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetRecentTracksCommand ::: resultHandler");
			// result format is E4X XML
			var result:XML = event.result as XML;
			//trace(ObjectUtil.toString(result));
			
			TwobblerModel.getInstance().setRecentTracks(result);
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetRecentTracksCommand ::: faultHandler");
			trace(event.fault.toString());
			var body:String = event.message.body.toString();
			trace(body);
			var xml:XML = new XML(body);
			var msg:String = body;
			var code:Number = -1;
			if(xml && xml.attribute("status")) {
				//trace("    - status: ", xml.@status.toString());
				//trace("    - error code: ", xml.error.@code);
				//var code:int = int(xml.error.@code);
				msg = xml.error.toString();
				code = xml.error.@code;
			}
			
			twobblerModel.logError(msg);
			
			trace("    - code: ", code);
			if(code == 6) {
				// display Alert if user name doesn't exist.
				twobblerModel.setServiceFault(msg, "Error");
				// stop fetching data since user is not correct
				twobblerModel.twobblerEnabled = false;
			}
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
