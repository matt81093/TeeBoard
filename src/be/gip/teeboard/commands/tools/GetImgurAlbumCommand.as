package be.gip.teeboard.commands.tools {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.dto.imgur.AlbumDTO;
	import be.gip.teeboard.model.ImgurModel;
	
	import be.gip.teeboard.net.api.Imgur;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	/**
	 * Updates the config file (<code>teeboard-clock-config.xml</code>) for the clock widget.
	 * 
	 */ 
	public class GetImgurAlbumCommand implements ICommand {
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		private var imgurModel:ImgurModel= ImgurModel.getInstance();
		
		
		public function execute(event:MVCEvent):void {
			trace("GetImgurAlbumCommand ::: execute");
			var dto:AlbumDTO = event.data as AlbumDTO;
			trace(ObjectUtil.toString(dto));
			
			var headers:Object = imgurModel.httpHeaders;
			
			var url:String = Imgur.IMGUR_ALBUM_URL;
			url = url.replace(":albumId", dto.id);
			
			var service:HTTPService = ServiceLocator.getInstance().getHTTPService(Services.IMGUR_ALBUM_SERVICE);
			service.addEventListener(ResultEvent.RESULT, service_resultHandler);
			service.addEventListener(FaultEvent.FAULT, service_faultHandler);
			service.headers = headers;
			service.url = url;
			service.send();
		}
		
		//------------------------------------
		// service_resultHandler()
		//------------------------------------
		private function service_resultHandler(event:ResultEvent):void {
			trace("GetImgurAlbumCommand ::: service_resultHandler");
			var result:String = event.result.toString();
			trace(ObjectUtil.toString(result));
			var json:Object = JSON.parse(result);
			//trace(ObjectUtil.toString(json));
			imgurModel.setAlbum(json.data);
			
		}
		
		//------------------------------------
		// service_faultHandler()
		//------------------------------------
		private function service_faultHandler(event:FaultEvent):void {
			trace("GetImgurAlbumCommand ::: service_faultHandler");
			
		}
		
	}
	
}
