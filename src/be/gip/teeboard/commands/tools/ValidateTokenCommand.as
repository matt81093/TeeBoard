package be.gip.teeboard.commands.tools {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.events.OAuthTokenEvent;
	import be.gip.teeboard.model.OAuthModel;
	import be.gip.teeboard.vo.RootVO;
	import be.gip.teeboard.dto.TokenDTO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class ValidateTokenCommand implements ICommand {
		
		private var oauthModel:OAuthModel = OAuthModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("ValidateTokenCommand ::: execute");
			var evt:OAuthTokenEvent = event as OAuthTokenEvent;
			var vo:TokenDTO = evt.data as TokenDTO;
			
			service = ServiceLocator.getInstance().getHTTPService(Services.ROOT_SERVICE);
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access token  
			// this will return basic user info and authorization status
			service.headers = oauthModel.getHttpHeadersForToken(vo);
			service.method = URLRequestMethod.GET;
			service.url = Kraken.ROOT_URL;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("ValidateTokenCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			trace(ObjectUtil.toString(result));
			var vo:RootVO = new RootVO(result);
			trace(ObjectUtil.toString(vo));
			oauthModel.validatedRoot = vo;
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("ValidateTokenCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			var content:Object = event.fault.content;
			var json:JSONDecoder;
			var body:Object;
			var msg:String;
			// try converting to JSON
			try {
				json = new JSONDecoder(content.toString(), true);
				body = json.getValue();
				msg = body.message;
			}catch(e:Error) {
				trace("    - JSONDecoder error: ", e.message);
				if(content.toString() != "") {
					msg = content.toString();
				}else {
					msg = event.fault.rootCause.text;
				}
			}
			Alert.show(msg, event.fault.name);
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
