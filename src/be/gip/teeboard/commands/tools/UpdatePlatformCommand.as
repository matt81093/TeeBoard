package be.gip.teeboard.commands.tools {
	
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.events.GiantBombEvent;
	import be.gip.teeboard.managers.WidgetsConnectionManager;
	import be.gip.teeboard.model.GiantBombModel;
	import be.gip.teeboard.model.WidgetsModel;
	import be.gip.teeboard.vo.GiantPlatformVO;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * Updates the config file (<code>teeboard-clock-config.xml</code>) for the clock widget.
	 * 
	 */ 
	public class UpdatePlatformCommand implements ICommand {
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		
		private var giantModel:GiantBombModel = GiantBombModel.getInstance();
		
		public function execute(event:MVCEvent):void {
			trace("UpdatePlatformCommand ::: execute");
			var gEvent:GiantBombEvent = event as GiantBombEvent;
			var vo:GiantPlatformVO = gEvent.data as GiantPlatformVO;
			var data:XML = vo.data as XML;
			
			data.@updatedAt = new Date().time.toString();
			//trace(data.toXMLString());
			
			var fs:FileStream = new FileStream();
			
			var bytes:String = '<?xml version="1.0" encoding="UTF-8"?>' + File.lineEnding;
			
			bytes += "<!-- " + File.lineEnding;
			bytes += "\t Game titles retrieved from http://www.giantbomb.com/ " + File.lineEnding;
			bytes += "\t Platform: " + vo.platform + File.lineEnding;
			bytes += "\t Total: " + vo.games.length.toString() + File.lineEnding;
			bytes += "\t Last updated at: " + new Date().toString() + File.lineEnding;
			bytes += "-->" + File.lineEnding;
			
			bytes += data.toString().split("\n").join(File.lineEnding);
			
			// there's 2 routes to take: 
			// - update existing platform xml --> vo will have a url property defined
			// - create new platform xml -> url property will not be defined
			
			var f:File;
			var fName:String = vo.platform.toLowerCase().split(" ").join("-");
			var path:String = "data/games-" + fName + ".xml";
			if(vo.url && vo.url != "") {
				f = new File(vo.url);
				trace("    - native path: ", f.nativePath);
			}else {
				// create new file path
				f = GiantBombModel.DOCUMENTS_DIR.resolvePath(path);
				trace("    - native path: ", f.nativePath);
			}
			
			try {
				fs.open(f, FileMode.WRITE);
				fs.writeUTFBytes(bytes);
				fs.close();
				dispatcher.dispatchEvent(new GiantBombEvent(GiantBombEvent.UPDATE_PLATFORM_SUCCESS));
			}catch (e:Error) {
				trace(e.getStackTrace());
				dispatcher.dispatchEvent(new GiantBombEvent(GiantBombEvent.UPDATE_PLATFORM_ERROR));
			}
		}
		
	}
	
}
