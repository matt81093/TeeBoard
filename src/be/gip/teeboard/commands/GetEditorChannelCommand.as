package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.vo.ChannelVO;
	import be.gip.teeboard.vo.RootVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetEditorChannelCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetEditorChannelCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			
			var url:String = Kraken.CHANNELS_CHANNEL_URL;
			url = url.replace(":channel", "reasonlive");
			
			service = ServiceLocator.getInstance().getHTTPService("channelService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// set service headers
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetEditorChannelCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			//trace(ObjectUtil.toString(result));
			var vo:ChannelVO = new ChannelVO(result);
			trace(ObjectUtil.toString(vo));
			model.selectedChannel = vo;
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetEditorChannelCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
