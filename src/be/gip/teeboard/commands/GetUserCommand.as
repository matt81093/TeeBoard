package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.vo.RootVO;
	import be.gip.teeboard.vo.UserVO;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetUserCommand implements ICommand {
		
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetUserCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			service = ServiceLocator.getInstance().getHTTPService("userService");
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access-token headers  
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			service.url = Kraken.USER_URL;
			service.send();
			
			model.cursorManager.setBusyCursor();
			
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetUserCommand ::: resultHandler");
			
			model.cursorManager.removeBusyCursor();
			
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			//trace(ObjectUtil.toString(result));
			var vo:UserVO = new UserVO(result);
			//trace(ObjectUtil.toString(vo));
			model.setUser(vo);
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetUserCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			
			model.cursorManager.removeBusyCursor();
			
			var content:Object = event.fault.content;
			var json:JSONDecoder;
			var body:Object;
			// try converting to JSON
			try {
				json = new JSONDecoder(content.toString(), true);
				body = json.getValue();
			}catch(e:Error) {
				trace("    - json error: ", e.message);
				Alert.show(event.fault.faultString, event.fault.name);
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}

/**
 * 
 * (mx.rpc::Fault)#0
 *   content = "{"error":"Internal Server Error","status":500,"message":null}"
 *   errorID = 0
 *   faultCode = "Server.Error.Request"
 *   faultDetail = "Error: [IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2032: Stream Error. URL: https://api.twitch.tv/kraken/user" errorID=2032]. URL: https://api.twitch.tv/kraken/user"
 *   faultString = "HTTP request error"
 *   message = "faultCode:Server.Error.Request faultString:'HTTP request error' faultDetail:'Error: [IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2032: Stream Error. URL: https://api.twitch.tv/kraken/user" errorID=2032]. URL: https://api.twitch.tv/kraken/user'"
 *   name = "Error"
 *   rootCause = (flash.events::IOErrorEvent)#1
 *     bubbles = false
 *     cancelable = false
 *     currentTarget = (flash.net::URLLoader)#2
 *       bytesLoaded = 61
 *       bytesTotal = 61
 *       data = "{"error":"Internal Server Error","status":500,"message":null}"
 *       dataFormat = "text"
 *     errorID = 2032
 *     eventPhase = 2
 *     target = (flash.net::URLLoader)#2
 *     text = "Error #2032: Stream Error. URL: https://api.twitch.tv/kraken/user"
 *     type = "ioError"
 */
