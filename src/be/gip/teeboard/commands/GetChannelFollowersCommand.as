package be.gip.teeboard.commands {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.mvc.events.MVCEventDispatcher;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.events.TwitchEvent;
	import be.gip.teeboard.log.LogMessage;
	import be.gip.teeboard.log.TeeLogger;
	import be.gip.teeboard.model.ApplicationModel;
	import be.gip.teeboard.model.FollowersModel;
	import be.gip.teeboard.model.StreamStatsModel;
	
	import com.adobe.serialization.json.JSONDecoder;
	
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	import tv.twitch.api.Kraken;
	
	public class GetChannelFollowersCommand implements ICommand {
		
		private var dispatcher:MVCEventDispatcher = MVCEventDispatcher.getInstance();
		private var logger:TeeLogger = TeeLogger.getInstance();
		private var model:ApplicationModel = ApplicationModel.getInstance();
		private var followersModel:FollowersModel = FollowersModel.getInstance();
		private var statsModel:StreamStatsModel = StreamStatsModel.getInstance();
		
		private var service:HTTPService;
		
		public function execute(event:MVCEvent):void {
			trace("GetChannelFollowersCommand ::: execute");
			var evt:TwitchEvent = event as TwitchEvent;
			service = ServiceLocator.getInstance().getHTTPService(Services.CHANNEL_FOLLOWERS_SERVICE);
			service.addEventListener(ResultEvent.RESULT, resultHandler);
			service.addEventListener(FaultEvent.FAULT, faultHandler);
			// call service with access token  
			service.headers = model.httpHeaders;
			service.method = URLRequestMethod.GET;
			var url:String = Kraken.CHANNEL_FOLLOWERS_URL.replace(":channel", model.channel.name) + "&r" + new Date().time;
			trace("    - url: ", url);
			//var url:String = Kraken.CHANNEL_FOLLOWERS_URL.replace(":channel", "ferretbomb");
			service.url = url;
			service.send();
		}
		
		private function resultHandler(event:ResultEvent):void {
			trace("GetChannelFollowersCommand ::: resultHandler");
			// result format is json - convert to AS
			//trace(ObjectUtil.toString(event.result));
			var json:JSONDecoder = new JSONDecoder(event.result.toString(), true);
			var result:Object = json.getValue();
			//trace(ObjectUtil.toString(result));
			
			// pass data to model
			followersModel.setFollowers(result);
			
			// add followers to stats model
			//statsModel.addFollowerItem(result);
			
			service.removeEventListener(ResultEvent.RESULT, resultHandler)
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
		private function faultHandler(event:FaultEvent):void {
			trace("GetChannelFollowersCommand ::: faultHandler");
			trace(ObjectUtil.toString(event.fault));
			var content:Object = event.fault.content;
			var dto:LogMessage;
			//  content = "{"status":404,"message":"Channel ':channel' does not exist","error":"Not Found"}"
			try {
				var json:JSONDecoder = new JSONDecoder(content.toString(), true);
				var value:Object = json.getValue();
				Alert.show("status: " + value.status + "\n" + "message: " + value.message, "Error: " + value.error);
			}catch(e:Error) {
				dto = new LogMessage();
				dto.message = "GetChannelFollowersCommand Error - " + event.fault.faultString;
				logger.log(dto);
				//Alert.show(event.fault.faultString, "Error:");
			}
			service.removeEventListener(ResultEvent.RESULT, resultHandler);
			service.removeEventListener(FaultEvent.FAULT, faultHandler);
		}
		
	}
	
}
