package be.gip.teeboard.commands.backendless {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.dto.backendless.AuthorDTO;
	import be.gip.teeboard.model.BackendlessModel;
	
	import be.gip.teeboard.net.api.Backendless;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	
	public class GetAuthorCommand implements ICommand {
		
		private var backendModel:BackendlessModel = BackendlessModel.getInstance();
		
		public function GetAuthorCommand() {
			trace("GetAuthorCommand ::: CONSTRUCTOR");
		}
		
		//------------------------------------
		// execute()
		//------------------------------------
		public function execute(event:MVCEvent):void {
			trace("GetAuthorCommand ::: execute");
			var dto:AuthorDTO = event.data as AuthorDTO;
			var service:HTTPService = ServiceLocator.getInstance().getHTTPService(Services.BACKENDLESS_AUTHOR_SERVICE);
			var url:String = Backendless.DATA_AUTHOR_URL;
			
			url = url.replace(":authorId", dto.authorId);
			trace("    - url: ", url);
			
			service.cancel();
			service.headers = BackendlessModel.getInstance().httpHeaders;
			service.addEventListener(ResultEvent.RESULT, service_resultHandler);
			service.addEventListener(FaultEvent.FAULT, service_faultHandler);
			service.url = url;
			service.send();
		}
		
		//------------------------------------
		// service_resultHandler()
		//------------------------------------
		private function service_resultHandler(event:ResultEvent):void {
			trace("GetAuthorCommand ::: service_resultHandler");
			trace(ObjectUtil.toString(event.result));
			var json:JSON;
			var result:Object = JSON.parse(event.result.toString());
			trace(ObjectUtil.toString(result));
			backendModel.setPanelAuthor(result);
		}
		
		//------------------------------------
		// service_faultHandler()
		//------------------------------------
		private function service_faultHandler(event:FaultEvent):void {
			trace("GetAuthorCommand ::: service_faultHandler");
			trace(ObjectUtil.toString(event.fault));
		}
		
	}
	
}