package be.gip.teeboard.commands.backendless {
	
	import be.gip.mvc.business.ServiceLocator;
	import be.gip.mvc.commands.ICommand;
	import be.gip.mvc.events.MVCEvent;
	import be.gip.teeboard.business.Services;
	import be.gip.teeboard.model.BackendlessModel;
	import be.gip.teeboard.vo.backendless.PanelVO;
	
	import be.gip.teeboard.net.api.Backendless;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	
	
	public class GetPanelsCommand implements ICommand {
		
		private var backendModel:BackendlessModel = BackendlessModel.getInstance();
		
		public function GetPanelsCommand() {
			trace("GetPanelsCommand ::: CONSTRUCTOR");
		}
		
		//------------------------------------
		// execute()
		//------------------------------------
		public function execute(event:MVCEvent):void {
			trace("GetPanelsCommand ::: execute");
			var service:HTTPService = ServiceLocator.getInstance().getHTTPService(Services.BACKENDLESS_PANNELS_SERVICE);
			var url:String = Backendless.DATA_PANELS_URL;
			service.headers = BackendlessModel.getInstance().httpHeaders;
			service.addEventListener(ResultEvent.RESULT, service_resultHandler);
			service.addEventListener(FaultEvent.FAULT, service_faultHandler);
			service.url = url;
			service.send();
		}
		
		//------------------------------------
		// service_resultHandler()
		//------------------------------------
		private function service_resultHandler(event:ResultEvent):void {
			trace("GetPanelsCommand ::: service_resultHandler");
			trace(ObjectUtil.toString(event.result));
			var json:JSON;
			var result:Object = JSON.parse(event.result.toString());
			trace(ObjectUtil.toString(result));
			backendModel.setPanels(result);
		}
		
		//------------------------------------
		// service_faultHandler()
		//------------------------------------
		private function service_faultHandler(event:FaultEvent):void {
			trace("GetPanelsCommand ::: service_faultHandler");
			
		}
		
	}
	
}