package be.gip.teeboard.dto {
	
	[Bindable]
	public class StreamStatDTO {
		
		public var numViewers:uint;
		public var time:Date;
		
		public function StreamStatDTO()	{
			time = new Date();
		}
		
	}
	
}
