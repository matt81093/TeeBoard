package be.gip.teeboard.dto.notifications {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[RemoteClass(alias="be.gip.teeboard.dto.notifications.DonationServiceDTO")]
	[Bindable]
	public class DonationServiceDTO extends AbstractDTO {
		
		/**
		 * Twitch channel name
		 */ 
		public var channel:String = "";
		
		/**
		 * Service API client ID
		 */ 
		public var clientID:String = "";
		
		/**
		 * Service API key.
		 */ 
		public var key:String = "";
		
		/**
		 * Some services generate a token from a <code>ClientID</code>.
		 */ 
		public var token:String;
		
		/**
		 * One of the supported donation services.
		 * 
		 * @see be.gip.teeboard.net.api.Donations
		 */ 
		public var service:String;
		
		/**
		 * If service uses a generated token, this is the time the token expires.
		 */ 
		public var expires:Date;
		
	}
	
}
