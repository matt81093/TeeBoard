package be.gip.teeboard.dto {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindalbe]
	public class StatusDTO extends AbstractDTO {
		
		public var game:String;
		public var status:String;
		
	}
	
}
