package be.gip.teeboard.dto.datastore {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindable]
	[RemoteClass(alias="be.gip.teeboard.dto.datastore.ChannelDTO")]
	public class ChannelDTO extends AbstractDTO {
		
		public var channelID:int;
		public var name:String;
		
	}
	
}
