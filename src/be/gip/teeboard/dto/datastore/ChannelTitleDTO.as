package be.gip.teeboard.dto.datastore {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindable]
	[RemoteClass(alias="be.gip.teeboard.dto.datastore.ChannelTitleDTO")]
	public class ChannelTitleDTO extends AbstractDTO {
		
		public var titleID:int;
		public var channelID:int;
		public var title:String;
		
	}
	
}
