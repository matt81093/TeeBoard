package be.gip.teeboard.dto {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindable]
	[RemoteClass(alias="be.gip.teeboard.dto.UserDTO")]
	public class UserDTO extends AbstractDTO  {
		
		public var name:String;
		public var displayName:String;
		public var token:String;
		
		override public function equals(value:Object):Boolean {
			var eq:Boolean = false;
			if(value && value.hasOwnProperty("token") && value.token == token) {
				eq = true;
			}
			return eq;
		}
		
	}
	
}
