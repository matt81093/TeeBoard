package be.gip.teeboard.dto {
	
	[Bindable]
	public class FollowersStatDTO {
		
		public var numFollowers:uint;
		public var time:Date;
		
		public function FollowersStatDTO()	{
			time = new Date();
		}
		
	}
	
}
