package be.gip.teeboard.dto {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[RemoteClass(alias="be.gip.teeboard.dto.TokenVO")]
	[Bindable]
	/**
	 * 
	 */ 
	public class TokenDTO extends AbstractDTO {
		
		public var name:String;
		public var token:String;
		public var isValid:Boolean = true;
		public var scopes:Array;
		public var createdAt:Date;
		public var updatedAt:Date;
		
	}
	
}
