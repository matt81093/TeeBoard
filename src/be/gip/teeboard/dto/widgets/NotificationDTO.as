package be.gip.teeboard.dto.widgets {
	
	[Bindable]
	public class NotificationDTO {
		
		/**
		 * The notification message.
		 */ 
		public var message:String;
		
		/**
		 * The notification title.
		 */ 
		public var title:String;
		
		/**
		 * The notification type.
		 * <li>donation</li>
		 * <li>follower</li>
		 * <li>subscriber</li>
		 * <li>test</li>
		 */ 
		public var type:String;
		
		/**
		 * The notification date
		 */ 
		public var date:Date;
		
	}
	
}
