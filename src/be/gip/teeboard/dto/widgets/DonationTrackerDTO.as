package be.gip.teeboard.dto.widgets {
	
	// TODO: v0.1.6 remove DonationTrackerDTO class
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[RemoteClass(alias="be.gip.teeboard.dto.widgets.DonationTrackerDTO")]
	[Bindable]
	public class DonationTrackerDTO extends AbstractDTO {
		
		/**
		 * This is either the channel name or the service's <code>ClientID</code>
		 */ 
		public var channel:String = "";
		
		/**
		 * Service API key.
		 */ 
		public var key:String = "";
		
		/**
		 * Some services generate a token from a <code>ClientID</code>.
		 */ 
		public var token:String;
		
		/**
		 * One of the supported donation services.
		 * 
		 * @see be.gip.teeboard.net.api.Donations
		 */ 
		public var tracker:String;
		
	}
	
}
