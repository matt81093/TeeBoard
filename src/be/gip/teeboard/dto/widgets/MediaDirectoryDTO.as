package be.gip.teeboard.dto.widgets {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	
	public class MediaDirectoryDTO extends AbstractDTO {
		
		public var name:String;
		
		public function MediaDirectoryDTO()	{
			super();
		}
	}
	
}