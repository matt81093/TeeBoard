package be.gip.teeboard.dto.widgets {
	
	[Bindable]
	public class CountdownDTO {
		
		private var data:XML;
		
		public function CountdownDTO(value:XML) {
			data = value;
		}
		
		/**
		 * Relative path to the widget directory.
		 * Use this to resolve the path relative to the the user's documents folder.
		 */ 
		public function get directory():String {
			return "TeeBoard/widgets/countdown/";
		}
		
	}
	
}