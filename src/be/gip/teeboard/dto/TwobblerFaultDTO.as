package be.gip.teeboard.dto {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindable]
	public class TwobblerFaultDTO extends AbstractDTO  {
		
		public var message:String = "";
		public var title:String = "Info";
		
	}
	
}
