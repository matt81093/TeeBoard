package be.gip.teeboard.dto {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindable]
	public class CommercialDTO extends AbstractDTO {
		
		/**
		 * The label
		 */ 
		public var label:String;
		
		/**
		 * Length (in seconds) of the commercial.
		 * 
		 * @default 30
		 */ 
		public var length:uint = 30;
		
		public function CommercialDTO(label:String, length:uint):void {
			this.label = label;
			this.length = length;
		}
		
	}
	
}
