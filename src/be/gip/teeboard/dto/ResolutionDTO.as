package be.gip.teeboard.dto {
	
	[RemoteClass(alias="be.gip.teeboard.dto.ResolutionVO")]
	[Bindable]
	
	public class ResolutionDTO {
		
		public var width:Number;
		public var height:Number;
		public var ar:String;
		public var isCustom:Boolean = true;
		
		/**
		 * Note: Default values are set for all parameters 
		 * as otherwise an error is thrown when (re)creating
		 * objects from ShareObject.
		 * 
		 */
		public function ResolutionDTO(w:Number=0, h:Number=0, aratio:String="", custom:Boolean=true) {
			width = w;
			height = h;
			ar = aratio;
			isCustom = custom;
		}
		
		public function get label():String {
			return width + " x " + height + " (" + height  + "p" + " " + ar + ")";
		}
		
		public function equals(vo:ResolutionDTO):Boolean {
			var eq:Boolean = false;
			if(vo.width == width && vo.height == height) eq = true;
			return eq;
		}
		
	}
	
}
