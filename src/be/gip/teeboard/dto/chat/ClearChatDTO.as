package be.gip.teeboard.dto.chat {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	public class ClearChatDTO extends AbstractDTO implements IChatDTO {
		
		private var _type:String = "chat-clear";
		
		public function get type():String {
			return _type;
		}
		public function set type(value:String):void {
			_type = value;
		}
		
	}
	
}
