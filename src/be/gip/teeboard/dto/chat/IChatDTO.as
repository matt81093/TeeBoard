package be.gip.teeboard.dto.chat {
	
	public interface IChatDTO {
		
		function get type():String;
		function set type(value:String):void;
		
	}
	
}
