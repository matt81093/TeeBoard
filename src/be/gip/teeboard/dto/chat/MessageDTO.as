package be.gip.teeboard.dto.chat {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	public class MessageDTO extends AbstractDTO implements IChatDTO {
		
		public var text:String;
		public var from:String;
		public var to:String;
		//public var time:Date = new Date();
		
		private var _type:String = "chat-message";
		
		/**
		 * Valid values are "chat-message and "chat-info"
		 */ 
		public function get type():String {
			return _type;
		}
		
		public function set type(value:String):void {
			_type = value
		}
		
	}
	
}
