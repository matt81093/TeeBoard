package be.gip.teeboard.dto {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	[Bindable]
	[RemoteClass(alias="be.gip.teeboard.dto.ChannelDTO")]
	public class ChannelDTO extends AbstractDTO {
		
		public var name:String = "";
		public var displayName:String = "";
		
		public function ChannelDTO() {
			//
		}
		
		override public function equals(value:Object):Boolean {
			if(value == null ) return false;
			if(!value is ChannelDTO) return false;
			return this.name == value.name;
		}
		
	}
	
}
