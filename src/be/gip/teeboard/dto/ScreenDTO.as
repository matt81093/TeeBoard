package be.gip.teeboard.dto {
	
	import flash.display.Screen;
	
	[Bindable]
	
	public class ScreenDTO {
		
		public var label:String;
		public var screen:Screen;
		
		public function get index():int {
			var idx:int = int(label);
			return idx;
		}
		
	}
	
}
