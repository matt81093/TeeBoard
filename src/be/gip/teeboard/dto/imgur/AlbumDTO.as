package be.gip.teeboard.dto.imgur {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	/**
	 * 
	 */ 
	public class AlbumDTO extends AbstractDTO {
		
		public var id:String;
		public var url:String;
		public var author:String;
		
	}
	
}