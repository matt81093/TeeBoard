package be.gip.teeboard.dto.backendless {
	
	import be.gip.mvc.dto.AbstractDTO;
	
	public class AuthorDTO extends AbstractDTO {
		
		/**
		 * The unique author id.
		 */ 
		public var authorId:String;
		
	}
	
}
