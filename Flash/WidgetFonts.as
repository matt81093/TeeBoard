﻿package {
	import flash.text.Font;
	
	import flash.display.MovieClip;
	
	public class WidgetFonts extends MovieClip {
		
		[Embed(source = "fonts/digitalix.ttf",fontName = "DigitalixFont",mimeType = "application/x-font",unicodeRange = "U+0030-U+003a", embedAsCFF="false")]
		public static var DigitalixFont:Class;
		
		[Embed(source = "fonts/Digital Dismay.otf", fontName="DismayFont", mimeType="application/x-font", unicodeRange = "U+0030-U+003a", embedAsCFF="false")]
		public static var DismayFont:Class;
		
		[Embed(source = "fonts/monofonto.ttf", fontName="MonofontoFont", mimeType="application/x-font", unicodeRange = "U+0030-U+003a", embedAsCFF="false")]
		public static var MonofontoFont:Class;
		
		[Embed(source = "fonts/MonospaceTypewriter.ttf",fontName = "TypewriterFont",mimeType = "application/x-font",unicodeRange = "U+0030-U+003a", embedAsCFF="false")]
		public static var TypewriterFont:Class;
		
		public static var FONT_NAMES:Array = ["DigitalixFont", "DismayFont", "MonofontoFont", "TypewriterFont"];
		
		public function WidgetFonts():void {
			var fs:Array = Font.enumerateFonts();
			trace(fs.toString());
			var len:uint = fs.length;
			for (var i:uint=0; i<len; i++) {
				var f:Font = fs[i];
				trace("- font: ", f.fontName);
				trace("    - has glyphs 0123456789: ", f.hasGlyphs("0123456789:"));
			}
			//Font.registerFont(DismayFont);
			//Font.registerFont(MonofontoFont);
			//Font.registerFont(TypewriterFont);
		}
	
	}

}
